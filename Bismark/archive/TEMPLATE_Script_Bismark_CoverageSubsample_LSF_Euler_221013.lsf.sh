#!/bin/bash

#BSUB -n 4
#BSUB -W 20:00
#BSUB -J "bismark_methylext[1-ZZARRAYLENGTHZZ]%4"
#BSUB -R "rusage[mem=5072,scratch=50000]"


## source /cluster/apps/local/env2lmod.sh  # Switch to the new software stack:: not needed

module load bismark
module load samtools
module load bcftools

## set variables
FILEROOTLIST=ZZFILEROOTLISTZZ

INPUTDIR=ZZINPUTDIRZZ
OUTPUTDIR=ZZOUTPUTDIRZZ


#Internal job index variable
IDX=${LSB_JOBINDEX}

INPUTFILE=$(awk -v IDX="${IDX}" 'NR==IDX{print $0}' ${FILEROOTLIST})
##echo $INPUTFILE

## setting the proportion to subset
PROP="ZZPROPZZ"

# Run commands

# Copy files to local scratch
mkdir -p  ${TMPDIR}/Input_files/
rsync -aq ${INPUTDIR}/${INPUTFILE}*bam ${TMPDIR}/Input_files/

samtools view --threads 4 -o ${TMPDIR}/Input_files/${INPUTFILE}_${PROP}.bam -s ${PROP} ${TMPDIR}/Input_files/${INPUTFILE}*bam

bismark_methylation_extractor --multicore 4 --gzip --bedGraph --buffer_size 15G --comprehensive ${TMPDIR}/Input_files/${INPUTFILE}_${PROP}.bam

## Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.
## make sure none of the temp file accidentally get copied to the results folder
rm *temp*

rm -r  ${TMPDIR}/Input_files/*bam
rsync -auq ${TMPDIR}/* ${OUTPUTDIR}/
