#!/bin/bash

#BSUB -n 2
#BSUB -W 6:00
#BSUB -J bismark_genome_preparation
#BSUB -R "rusage[mem=6000]"
#BSUB -R "rusage[scratch=5000]"

## source /cluster/apps/local/env2lmod.sh  # Switch to the new software stack:: not needed

module load bismark
module load samtools
module load fastqc
module load bcftools
module load bowtie2
module load hisat2

 cd $TMPDIR ## Automatic scratch space

 ## set variables
INPUTDIR=ZZINPUTDIRZZ

REFGENOMEDIR=ZZREFGENOMEDIRZZ
REFGENOME=ZZREFGENOMEZZ

# Run commands


# Copy files to local scratch

rsync -aq ${INPUTDIR}/${REFGENOME} ${TMPDIR}/

gunzip ${TMPDIR}/${REFGENOME}

bismark_genome_preparation --genomic_composition --verbose ${TMPDIR}/
## bismark_genome_preparation --hisat2 --genomic_composition --verbose ${REFGENOME}

# Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.
 rsync -auq ${TMPDIR}/* ${REFGENOMEDIR}/
