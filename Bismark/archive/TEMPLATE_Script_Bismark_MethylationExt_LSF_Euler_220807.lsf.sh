#!/bin/bash

#BSUB -n 6
#BSUB -W  6:00
#BSUB -J "bismark_methylext[1-ZZARRAYLENGTHZZ]%6"
#BSUB -R "rusage[mem=2000,scratch=20000]"

## source /cluster/apps/local/env2lmod.sh  # Switch to the new software stack:: not needed

module load bismark
module load samtools
module load bcftools

## set variables
FILEROOTLIST=ZZFILEROOTLISTZZ

INPUTDIR=ZZINPUTDIRZZ
OUTPUTDIR=ZZOUTPUTDIRZZ


#Internal job index variable
IDX=${LSB_JOBINDEX}

INPUTFILE=$(awk -v IDX="${IDX}" 'NR==IDX{print $0}' ${FILEROOTLIST})
##echo $INPUTFILE

# Run commands

# Copy files to local scratch
UNIQID=$RANDOM

mkdir -p  ${TMPDIR}/TEMP_Input_Files_${UNIQID} ${TMPDIR}/TEMP_Output_${UNIQID}
cd ${TMPDIR}/

rsync -aq ${INPUTDIR}/${INPUTFILE}*bam ${TMPDIR}/TEMP_Input_Files_${UNIQID}/

## --scaffolds  ## can be used for incomplete genomes... but increases runtime
bismark_methylation_extractor --multicore 4  --gzip --bedGraph --buffer_size 8G --comprehensive --output ${TMPDIR}/TEMP_Output_${UNIQID}/  ${TMPDIR}/TEMP_Input_Files_${UNIQID}/${INPUTFILE}*bam

## Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.
## make sure none of the temp file accidentally get copied to the results folder
rm ${TMPDIR}/*temp*
rm -r  ${TMPDIR}/TEMP_Input_Files_${UNIQID}

rsync -auq ${TMPDIR}/TEMP_Output_${UNIQID}/* ${OUTPUTDIR}/
