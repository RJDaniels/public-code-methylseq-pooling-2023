#!/bin/bash

#BSUB -n 1
#BSUB -W 10:00
#BSUB -J "bismark_deduplication[1-ZZARRAYLENGTHZZ]%4"
#BSUB -R "rusage[mem=80000,scratch=150000]"

# With 2 cores, cpu utilization is only 50%
# Wall clock is 5hrs
# 7 tasks on average
# Job fails with less than 70GB of memory requested, but max usage is 104GB so it might also be cutting it close to failing....
# The files do not seem to get bigger than 70GB .... but give 150GB to be safe.


## source /cluster/apps/local/env2lmod.sh  # Switch to the new software stack:: not needed

module load bismark
module load samtools
module load bcftools

## set variables
FILEROOTLIST=ZZFILEROOTLISTZZ

INPUTDIR=ZZINPUTDIRZZ
OUTPUTDIR=ZZOUTPUTDIRZZ


#Internal job index variable
IDX=${LSB_JOBINDEX}
#echo ${IDX}

INPUTFILE=$(awk -v IDX="${IDX}" 'NR==IDX{print $0}' ${FILEROOTLIST})
#echo $INPUTFILE

# Run commands

# Copy files to local scratch
mkdir -p  ${TMPDIR}/Input_files/
rsync -aq ${INPUTDIR}/${INPUTFILE}*bam ${TMPDIR}/Input_files/

deduplicate_bismark --bam  ${TMPDIR}/Input_files/${INPUTFILE}*bam

## Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.
## make sure none of the temp file accidentally get copied to the results folder
rm *temp*

rm -r  ${TMPDIR}/Input_files/*bam
rsync -auq ${TMPDIR}/* ${OUTPUTDIR}/
