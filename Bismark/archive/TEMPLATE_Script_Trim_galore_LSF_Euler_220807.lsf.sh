#!/bin/bash
#BSUB -J "Trim_galore[1-ZZARRAYLENGTHZZ]%10"
#BSUB -n 2
#BSUB -W 04:10
#BSUB -R "rusage[mem=5000,scratch=10000]"

## Nik says only increase mem if the job is killed
## The output files are expected to be slightly smaller, so 4x5GB = 20GB Scratch
## source /cluster/apps/local/env2lmod.sh  # Switch to the new software stack:: not needed

module load trimgalore
module load pigz
module load python
module load fastqc

cd $TMPDIR ## Automatic scratch space

## set variables
FILEROOTLIST=ZZFILEROOTLISTZZ

INPUTDIR=ZZINPUTDIRZZ

OUTPUTDIR=ZZOUTPUTDIRZZ

#Internal job index variable
IDX=${LSB_JOBINDEX}
## echo ${IDX}

## get the sample to work with
INPUTFILE=$(awk -v IDX="${IDX}" 'NR==IDX{print $0}' ${FILEROOTLIST})
## echo $INPUTFILE

# Copy files to local scratch
mkdir ${TMPDIR}/Input_Files/
rsync -aq ${INPUTDIR}/${INPUTFILE}*.gz ${TMPDIR}/Input_Files/

# Run commands
trim_galore --retain_unpaired --paired --fastqc_args "--quiet --threads 4" --clip_R1 10 --clip_R2 10 --three_prime_clip_R1 10 --three_prime_clip_R2 10 -j 2 ${TMPDIR}/Input_Files/${INPUTFILE}*.fastq.gz

## DON'T RENAME THE FILES .. IT CAUSES TROUBLE WITH REPORTS DOWNSTREAM

## Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.

rm -fr ${TMPDIR}/Input_Files
rsync -auq ${TMPDIR}/* ${OUTPUTDIR}/
