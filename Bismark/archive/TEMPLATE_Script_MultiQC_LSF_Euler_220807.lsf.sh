#!/bin/bash

#BSUB -n 1
#BSUB -W 00:30
#BSUB -J "MultiQC"
#BSUB -R "rusage[mem=2048,scratch=5000]"

## source /cluster/apps/local/env2lmod.sh  # Switch to the new software stack:: not needed

module load multiqc

## set variables
INPUTDIR=ZZINPUTDIRZZ

OUTPUTDIR=ZZOUTPUTDIRZZ

## Work directly in the SCRATCH
cd ${TMPDIR}

# Run commands
multiqc \
--outdir ${TMPDIR}/ \
${INPUTDIR}/fastqc/ \
${INPUTDIR}/trim_galore/ \
${INPUTDIR}/alignment/ \
${INPUTDIR}/deduplication/ \
${INPUTDIR}/methylextraction/ \

##${INPUTDIR}/reports/

## ${INPUTDIR}/trimmomatic/ \

## Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.

rsync -auq ${TMPDIR}/ ${OUTPUTDIR}/
