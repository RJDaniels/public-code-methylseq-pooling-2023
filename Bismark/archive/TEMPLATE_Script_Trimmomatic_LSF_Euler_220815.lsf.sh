#!/bin/bash
#BSUB -J "trimmomatic[1-ZZARRAYLENGTHZZ]%10"
#BSUB -n 1
#BSUB -W 03:10
#BSUB -R "rusage[mem=5000,scratch=10000]"

## Nik says only increase mem if the job is killed
## The output files are expected to be slightly smaller, so 4x5GB = 20GB Scratch
## source /cluster/apps/local/env2lmod.sh  # Switch to the new software stack:: not needed

module load gcc/8.2.0
module load trimmomatic/0.38
                                                                         
###Let's hyperthread 
export OMP_NUM_THREADS=4

adapter=/cluster/apps/gdc/trimmomatic/0.35/adapters/TruSeq3-PE.fa
##NexteraPE-PE.fa
##TruSeq3-PE.fa   ## without reverse complements 
##TruSeq3-PE-2.fa ## with reverse compliments
##NexteraPE-PE.fa
##TruSeq3-SE.fa
##TruSeq2-SE.fa
##TruSeq2-PE.fa


cd $TMPDIR ## Automatic scratch space

## set variables
FILEROOTLIST=ZZFILEROOTLISTZZ

INPUTDIR=ZZINPUTDIRZZ

OUTPUTDIR=ZZOUTPUTDIRZZ

#Internal job index variable
IDX=${LSB_JOBINDEX}
## echo ${IDX}

## get the sample to work with
INPUTFILE=$(awk -v IDX="${IDX}" 'NR==IDX{print $0}' ${FILEROOTLIST})
## echo $INPUTFILE

# Copy files to local scratch
mkdir ${TMPDIR}/Input_Files/
rsync -aq ${INPUTDIR}/${INPUTFILE}*.gz ${TMPDIR}/Input_Files/

# Run commands
trimmomatic PE -threads 4 -phred33 \
${TMPDIR}/Input_Files/${INPUTFILE}*.fastq.gz \
${INPUTFILE}R1_trim.fq.gz ${INPUTFILE}R1_un.fq.gz \
${INPUTFILE}R2_trim.fq.gz ${INPUTFILE}R2_un.fq.gz \
ILLUMINACLIP:${adapter}:2:30:10 \
HEADCROP:10 \
LEADING:10 \
TRAILING:10 \
SLIDINGWINDOW:4:20 \
MINLEN:36 \

## Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.

rm -fr ${TMPDIR}/Input_Files
rsync -auq ${TMPDIR}/* ${OUTPUTDIR}/
