#!/bin/bash
#BSUB -J "bismark_alignment[1-ZZARRAYLENGTHZZ]%5"
#BSUB -n 4
#BSUB -W 18:10
#BSUB -R "rusage[mem=2500,scratch=6000]"


## more cores = faster run time
## each core deals with a subset of one samples data; multiple cores per sample.
## additional cores are needed for other parts of the process e.g. gzip
## each 'thread' needs 10-16 GB of RAM; Nik says this refers to each instance of bowtie which will be one multicore
## '--multicore' specifies the number of multicore to use, each consisting of 4 CPUs, 10GB RAM
## e.g. --parallel 4 on the mouse genome uses ~ 20 cores and 48 GB RAM
## You can hyperthread by setting -p 8
## Currently 14 hours per sample


## source /cluster/apps/local/env2lmod.sh  # Switch to the new software stack:: not needed

module load fastqc
module load samtools
module load bcftools
module load bowtie2
module load bismark
# module load hisat2

###Let's hyperthread 
## Bismark recommends only 4 "cores per alignment thread"
export OMP_NUM_THREADS=4

cd $TMPDIR ## Automatic scratch space

## set variables
FILEROOTLIST=ZZFILEROOTLISTZZ

REFGENOMEDIR=ZZREFGENOMEDIRZZ

INPUTDIR=ZZINPUTDIRZZ
OUTPUTDIR=ZZOUTPUTDIRZZ


#Internal job index variable
IDX=${LSB_JOBINDEX}
echo ${IDX}

INPUTFILE=$(awk -v IDX="${IDX}" 'NR==IDX{print $0}' ${FILEROOTLIST})
## echo $INPUTFILE

# Run commands

# Copy files to local scratch
mkdir -p ${TMPDIR}/Input_Files
rsync -aq ${INPUTDIR}/${INPUTFILE}R{1,2}_val* ${TMPDIR}/Input_Files/


bismark --nucleotide_coverage --multicore 1 -p 4 --genome ${REFGENOMEDIR} -1 ${TMPDIR}/Input_Files/${INPUTFILE}R1_val_1.fq.gz -2 ${TMPDIR}/Input_Files/${INPUTFILE}R2_val_2.fq.gz

## Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.
## make sure none of the temp file accidentally get copied to the results folder
rm *temp*

rm -rf ${TMPDIR}/Input_Files
rsync -auq ${TMPDIR}/* ${OUTPUTDIR}/
