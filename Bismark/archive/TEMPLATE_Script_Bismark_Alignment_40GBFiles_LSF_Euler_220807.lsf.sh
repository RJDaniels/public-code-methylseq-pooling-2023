#!/bin/bash
#BSUB -J "bismark_alignment[1-ZZARRAYLENGTHZZ]%1"
#BSUB -n 11
#BSUB -W 100:00
#BSUB -R "rusage[mem=2000,scratch=10000]"

## data inflation is huge. I am hitting 800GB , jobs fail due to insufficient storage on the nodes: with a 40GB input file aim for a TB of space to be safe.
## with the bigger jobs it is better to run one alignment at a time per array to  prevent conflict on storage space between jobs
## more cores = faster run time, maybe
## each part of the multicore deals with a subset of one samples data; multiple cores per sample.
## additional cores are needed for other parts of the process e.g. gzip
## each 'thread' needs 10-16 GB of RAM; Nik says this refers to each instance of bowtie which will be one multicore
## '--multicore' specifies the number of multicore to use, each consisting of 4 CPUs, 10GB RAM
## e.g. --parallel 4 on the mouse genome uses ~ 20 cores and 48 GB RAM
## You can hyperthread by setting -p 8
## Currently 14 hours per sample of 5GB each
## for multicore on Euler, use only 2 cores per "multicore" with 2GB mem per
## takes 100 hours for a pooled dataset with 11 cores

 ## source /cluster/apps/local/env2lmod.sh  # Switch to the new software stack:: not needed

module load fastqc
module load samtools
module load bcftools
module load bowtie2
module load bismark
# module load hisat2

###Let's hyperthread
## Bismark recommends only 4 "cores per alignment thread"
export OMP_NUM_THREADS=8

cd $TMPDIR ## Automatic scratch space

## set variables
FILEROOTLIST=ZZFILEROOTLISTZZ

REFGENOMEDIR=ZZREFGENOMEDIRZZ

INPUTDIR=ZZINPUTDIRZZ
OUTPUTDIR=ZZOUTPUTDIRZZ

#Internal job index variable
IDX=${LSB_JOBINDEX}
echo ${IDX}

INPUTFILE=$(awk -v IDX="${IDX}" 'NR==IDX{print $0}' ${FILEROOTLIST})
## echo $INPUTFILE

# Run commands

# Copy files to local scratch

UNIQID=$RANDOM

mkdir -p ${INPUTDIR}/TEMP_Input_Files_${UNIQID} ${TMPDIR}/ref ${TMPDIR}/Output
rsync -a ${REFGENOMEDIR}/ ${TMPDIR}/ref/
#rsync -a ${INPUTDIR}/${INPUTFILE}R{1,2}_val* ${INPUTDIR}/TEMP_Input_Files_${UNIQID}/

bismark --nucleotide_coverage --multicore 5 -p 8 --genome ${TMPDIR}/ref/ -1 ${INPUTDIR}/${INPUTFILE}R1_val_1.fq.gz -2 ${INPUTDIR}/${INPUTFILE}R2_val_2.fq.gz --output_dir ${TMPDIR}/Output/ --temp_dir ${INPUTDIR}/TEMP_Input_Files_${UNIQID}/

## Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.

## make sure none of the temp file accidentally get copied to the results folder
rm ${TMPDIR}/Output/*temp*
rm -rf ${INPUTDIR}/TEMP_*

rsync -auq ${TMPDIR}/Output ${OUTPUTDIR}/
