#!/bin/bash
#BSUB -J "RScript[1-ZZARRAYLENGTHZZ:1]%10"
#BSUB -n ZZCORESZZ	## Cores, not threads
#BSUB -W ZZTIMEZZ:00
#BSUB -R "rusage[mem=ZZMEMZZ,scratch=ZZSCRATCHZZ]"

 # Move to the old software stack
# source /cluster/apps/local/lmod2env.sh
# module load gcc/4.8.2 bioconductor/3.6  # R is loaded automatically with the bioconductor module but is only available on the old software stack

# Move to the new software stack
source /cluster/apps/local/env2lmod.sh
# env2lmod

 module load  gcc/8.2.0 curl/7.73.0 r/4.2.2

#module load  gcc/4.8.5 r/4.1.3 # load the gcc before loading R
# Do not load extra modules unless you are sure they are needed
#	module load gdal/3.1.2 udunits2/2.2.24 proj/4.9.2 geos/3.6.2 curl/7.73.0 fontconfig/2.13.1 freetype/2.11.1	 glib/2.68.3 cairo/1.17.4 font-util/1.3.1 pkgconf/1.7.3 libfontenc/1.1.3 harfbuzz/2.9.1 fribidi/1.0.5

###Let's hyperthread
## Bismark recommends only 4 "cores per alignment thread"
export OMP_NUM_THREADS=ZZTHREADSZZ

cd $TMPDIR ## Automatic scratch space
# Create the dir for tmp files
mkdir -p ${TMPDIR}/TMP_OUTPUTDIR

## set variables
# INPUTDIR=ZZINPUTDIRZZ # not needed here
OUTPUTDIR=ZZOUTPUTDIRZZ # The final dest. for all outputs to be copied to

JOBSCHED_BOOL=ZZJOBSCHED_BOOLZZ
FILEROOTLIST=ZZFILEROOTLISTZZ
#Internal job index variable
IDX=${LSB_JOBINDEX}
echo ${IDX}

## How index affects job submission
#### Update the input files
#INPUTFILE=$(awk -v IDX="${IDX}" 'NR==IDX{print $0}' ${FILEROOTLIST})
## echo $INPUTFILE
#### Send the index to R
##### Create a job scheduler array input for R
printf "\nJOBSCHED_BOOL=\"${JOBSCHED_BOOL}\"\nFILEROOTLIST=\"${FILEROOTLIST}\"\nARRAYIDX=\"${IDX}\"\nARRAYLENGTH=\"ZZARRAYLENGTHZZ\"\n"	>  ${TMPDIR}/JOBSCHEDARRAY.R

# Copy files to local scratch
#rsync -aq ${INPUTDIR}/ ${TMPDIR}/

# Merge the Startup script and the main script

cat ZZRSTARTUPSCRIPTZZ ${TMPDIR}/JOBSCHEDARRAY.R ZZEXTRAVARIABLESZZ ZZRSCRIPTZZ > ${TMPDIR}/RScript.R

# Run commands
# Don't use the --vanilla option. It prevents setting the custom library folder.
Rscript ${TMPDIR}/RScript.R ZZRGUMENTSZZ

# Remove any files saved to the TMP_OUTPUTDIR 
rm -r ${TMPDIR}/TMP_OUTPUTDIR* ${TMPDIR}/*R ${TMPDIR}/*sh

# Save the output to the Scratch space
# I have removed this from the RScript as it copies a bunch of temporary files. The R code should explicitly save files to the correct directory.
# Brought it back since methylkit seems to not write to the desired outputdir
rsync -auq ${TMPDIR}/* ${OUTPUTDIR}/
