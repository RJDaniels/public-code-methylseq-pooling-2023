#!/bin/bash
#BSUB -J "FastQC[1-ZZARRAYLENGTHZZ:1]%10"
#BSUB -n 2	## Cores, not threads
#BSUB -W 01:10
#BSUB -R "rusage[mem=6000,scratch=6000]"

## with 2 core and 6GB p/core, usage is ~ 100% for both...
## -n = cpu with maybe 9 threads
## You need more threads than you request in the software... the additional threads are for the aux tools of the software to run even if all the memory is used up by the requested threads

## source /cluster/apps/local/env2lmod.sh  # Switch to the new software stack:: not needed

module load fastqc

cd $TMPDIR ## Automatic scratch space

## set variables
FILEROOTLIST=ZZFILEROOTLISTZZ

INPUTDIR=ZZINPUTDIRZZ

OUTPUTDIR=ZZOUTPUTDIRZZ


#Internal job index variable
IDX=${LSB_JOBINDEX}
## echo ${IDX} 

## get the sample to work with
INPUTFILE=$(awk -v IDX="${IDX}" 'NR==IDX{print $0}' ${FILEROOTLIST})
## echo $INPUTFILE

# Copy files to local scratch
rsync -aq ${INPUTDIR}/${INPUTFILE}*.gz ${TMPDIR}/

# Run commands
fastqc --noextract --quiet --threads 6 ${TMPDIR}/${INPUTFILE}*.fastq.gz


## Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.

rm ${TMPDIR}/${INPUTFILE}*.fastq.gz
rsync -auq ${TMPDIR}/* ${OUTPUTDIR}/
