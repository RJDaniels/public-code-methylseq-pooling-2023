#!/bin/bash

#BSUB -n 1
#BSUB -W 00:30
#BSUB -J "bismark_reports"
#BSUB -R "rusage[mem=2048,scratch=5000]"

module load bismark
module load bowtie2
module load bcftools
module load samtools
module load fastqc

## set variables
INPUTDIR=ZZINPUTDIRZZ

OUTPUTDIR=ZZOUTPUTDIRZZ

## Work directly in the SCRATCH
mkdir -p ${TMPDIR}/Reports
cd ${TMPDIR}

# Run commands

rsync -aq ${INPUTDIR}/alignment/*_report.txt ${TMPDIR}/
rsync -aq ${INPUTDIR}/alignment/*bam ${TMPDIR}/
rsync -aq ${INPUTDIR}/deduplication/*deduplication_report.txt ${TMPDIR}/
rsync -aq ${INPUTDIR}/methylextraction/*_splitting_report.txt ${TMPDIR}/
rsync -aq ${INPUTDIR}/methylextraction/*.M-bias.txt ${TMPDIR}/

bismark2report --dir ${TMPDIR}/Reports
bismark2summary 

## Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.

rsync -auq ${TMPDIR}/Reports/* ${OUTPUTDIR}/
rsync -auq ${TMPDIR}/*bismark_summary_report* ${OUTPUTDIR}/
