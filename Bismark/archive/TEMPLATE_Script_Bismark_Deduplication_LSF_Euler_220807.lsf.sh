#!/bin/bash

#BSUB -n 1
#BSUB -W 6:00
#BSUB -J "bismark_deduplication[1-ZZARRAYLENGTHZZ]%4"
#BSUB -R "rusage[mem=6072,scratch=20000]"


## source /cluster/apps/local/env2lmod.sh  # Switch to the new software stack:: not needed

module load bismark
module load samtools
module load bcftools

## set variables
FILEROOTLIST=ZZFILEROOTLISTZZ

INPUTDIR=ZZINPUTDIRZZ
OUTPUTDIR=ZZOUTPUTDIRZZ


#Internal job index variable
IDX=${LSB_JOBINDEX}
#echo ${IDX}

INPUTFILE=$(awk -v IDX="${IDX}" 'NR==IDX{print $0}' ${FILEROOTLIST})
#echo $INPUTFILE

# Run commands

# Copy files to local scratch
mkdir -p  ${TMPDIR}/Input_files/
rsync -aq ${INPUTDIR}/${INPUTFILE}*bam ${TMPDIR}/Input_files/

deduplicate_bismark --bam  ${TMPDIR}/Input_files/${INPUTFILE}*bam

## Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.
## make sure none of the temp file accidentally get copied to the results folder
rm *temp*

rm -r  ${TMPDIR}/Input_files/
rsync -auq ${TMPDIR}/* ${OUTPUTDIR}/
