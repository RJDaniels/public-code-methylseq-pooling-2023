# This script should be appended to a slurm header for submission

#=============
#Job preprep
#=============

source ~/CodeRepos/hpc-workflows/HPC/Script_jobsubmission_preamble_240130.sh && sleep 1s

#Create directory for analysis
if [[ ! -d ${RJDSCRATCH} ]]
then
echo 'RJDSCRATCH not found!'
exit 1
else
echo 'RJDSCRATCH found! at ' ${RJDSCRATCH}
cd ${RJDSCRATCH}
fi

#=============
#Load required modules
#=============

if [[  ${RJDPLATFRM} == 'RACKHAM'  ]];
then
module load bioinfo-tools
module load FastQC
module load MultiQC
fi

if [[  ${RJDPLATFRM} == 'EULER'  ]];
then
# Euler
module load python
module load fastqc
module load multiqc
fi

if [[  ${RJDPLATFRM} == 'ILIFU'  ]];
then
# on ilifu
module load anaconda3/login

# Change env path AFTER loading the module
export CONDA_ENVS_PATH=${PRJDIR}
#source conda_init.sh # DOES NOTHING ON ILIFU
conda activate python39-env

fi

#=============
# Read in vars
#=============

## set variables

export FASTQCDIR=${1}
export TRIMDIR=${2}
export ALIGNDIR=${3}
export DEDUPDIR=${4}
export METHDIR=${5}
export OUTPUTDIR=${6}

#=============

#Create directory for analysis
if [[ ! -d ${OUTPUTDIR} ]]
then
mkdir -p ${OUTPUTDIR}
fi
#===============================

# Do Not Copy files to local scratch!!
#rsync -aq ${INPUTDIR}/${INPUTFILE}*.gz ${RJDSCRATCH}/
#===============================

# ===================
source comstp 'Analyses ...'
# ===================
cd ${RJDSCRATCH}/

multiqc \
--outdir ${RJDSCRATCH}/ \
${FASTQCDIR} \
${TRIMDIR} \
${ALIGNDIR} \
${DEDUPDIR} \
${METHDIR} && touch MultiQC_complete_emptyfile

# =============
source comstp 'Close off & rsync back to outputdir ...'
# =============
rsync -avu ${RJDSCRATCH}/ ${OUTPUTDIR}/ &&  rm -r ${RJDSCRATCH}
#===============================

