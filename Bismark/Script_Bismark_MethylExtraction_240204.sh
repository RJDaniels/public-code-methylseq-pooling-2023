# This script should be appended to a slurm header for submission

#=============
#Job preprep
#=============

source ~/CodeRepos/hpc-workflows/HPC/Script_jobsubmission_preamble_240130.sh && sleep 1s

#Create directory for analysis
if [[ ! -d ${RJDSCRATCH} ]]
then
echo 'RJDSCRATCH not found!'
exit 1
else
echo 'RJDSCRATCH found! at ' ${RJDSCRATCH}
cd ${RJDSCRATCH}
fi

#=============
#Load required modules
#=============

if [[  ${RJDPLATFRM} == 'RACKHAM'  ]];
then
# Rackham
module load bioinfo-tools
module load FastQC
# common
module load samtools
module load bcftools
module load bowtie2
module load bismark
fi

if [[  ${RJDPLATFRM} == 'EULER'  ]];
then
# Euler
module load fastqc
module load samtools
module load bcftools
module load bowtie2
module load bismark
fi

if [[  ${RJDPLATFRM} == 'ILIFU'  ]];
then
echo 'Use the apptainer image!!'
fi

###Let's hyperthread
## Bismark recommends only 4 "cores per alignment thread"
#export OMP_NUM_THREADS=4

#=============
# Read in vars
#=============

## set variables
export FILEROOTLIST=${1}
# backup incase I forgot the variable name
export ROOTLISTFILE=${1}

export INPUTDIR=${2}
export OUTPUTDIR=${3}

#=============

#Create directory for analysis
if [[ ! -d ${OUTPUTDIR} ]]
then
    mkdir -p ${OUTPUTDIR}
fi

if [[ ! -d ${INPUTDIR} ]]
then
echo 'ERR: INPUTDIR does not exist!'
exit
fi

if [[ ! -f ${FILEROOTLIST} ]]
then
echo 'ERR: FILEROOTLIST does not exist!'
exit
fi

# ===================
source comstp 'Internal job index variable ...'
# ===================

#Internal job index variable
echo 'Job IDX running ...' ${RJD_SLURM_ARRAY_TASK_ID}

export INPUTFILE=$(sed -n "${RJD_SLURM_ARRAY_TASK_ID}p" ${FILEROOTLIST})

source comstp 'This is the file name to run ...'
# this needs to be keep out of comstp, for some reason it doesn't get echo'd otherwise
echo ${INPUTFILE}

# ===================
source comstp 'Rsync data to scratch ...'
# ===================

# Copy files to local scratch
mkdir -p ${RJDSCRATCH}/Input_Files/

echo 'Files to upload...'
ls ${INPUTDIR}/split*${INPUTFILE}*bam

rsync -rLptgoD -q ${INPUTDIR}/split*${INPUTFILE}*namesorted.bam ${RJDSCRATCH}/Input_Files/

echo 'Files uploaded...'
ls ${RJDSCRATCH}/Input_Files/

#===============================

# Run commands

## --scaffolds  ## can be used for incomplete genomes... but increases runtime

bismark_methylation_extractor \
--no_overlap \
--paired-end \
--multicore ${OMP_NUM_THREADS}  \
--gzip \
--bedGraph \
--buffer_size 16G \
--comprehensive \
--output ${RJDSCRATCH}/ \
  ${RJDSCRATCH}/Input_Files/split*${INPUTFILE}*_val_*bam

  # NOT PAIREND
bismark_methylation_extractor \
--no_overlap \
--single-end \
--multicore ${OMP_NUM_THREADS}  \
--gzip \
--bedGraph \
--buffer_size 16G \
--comprehensive \
--output ${RJDSCRATCH}/ \
  ${RJDSCRATCH}/Input_Files/split*${INPUTFILE}*_unpaired_*bam

 # =============
source comstp 'Close off & rsync back to outputdir ...'
# =============
## DON'T RENAME THE FILES .. IT CAUSES TROUBLE WITH REPORTS DOWNSTREAM
#===============================
# Remove the input data from the scratch space
rm *temp*
rm -r ${RJDSCRATCH}/Input_Files
rsync -aqu ${RJDSCRATCH}/ ${OUTPUTDIR}/ &&  rm -r ${RJDSCRATCH}/
#===============================
