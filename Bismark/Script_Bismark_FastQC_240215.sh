# This script should be appended to a slurm header for submission

#=============
#Job preprep
#=============

source ~/CodeRepos/hpc-workflows/HPC/Script_jobsubmission_preamble_240130.sh && sleep 1s

#Create directory for analysis
if [[  ! -d "${RJDSCRATCH}" ]]
then
source comstp 'RJDSCRATCH not found!'
exit 1
else
source comstp 'RJDSCRATCH found! at ' ${RJDSCRATCH}
cd ${RJDSCRATCH}
fi

#=============
#Load required modules
#=============

if [[  ${RJDPLATFRM} == 'RACKHAM'  ]];
then
module load bioinfo-tools
module load FastQC
fi

if [[  ${RJDPLATFRM} == 'EULER'  ]];
then
# Euler
module load fastqc
fi

if [[ ${RJDPLATFRM} == 'ILIFU'  ]];
then
# on ilifu
module load anaconda3/login
# Change env path AFTER loading the module
export CONDA_ENVS_PATH=${PRJDIR}
#source conda_init.sh # DOES NOTHING ON ILIFU
conda activate python39-env
fi

#=============
# Read in vars
#=============

## set variables
export FILEROOTLIST=${1}
export INPUTDIR=${2}
export OUTPUTDIR=${3}

# ===================
source comstp 'Safety checks etc.. ...'
# ===================
#Create directory for analysis
if [[ ! -d ${OUTPUTDIR} ]]
then
    mkdir -p ${OUTPUTDIR}
fi

if [[ ! -f ${FILEROOTLIST} ]]
then
 echo 'ERR: FILEROOTLIST not found' 
fi

#===============================

#Internal job index variable
echo 'Job IDX running ...' ${RJD_SLURM_ARRAY_TASK_ID}

export INPUTFILE=$(sed -n "${RJD_SLURM_ARRAY_TASK_ID}p" ${FILEROOTLIST})

source comstp 'This is the file name to run ...'
echo ${INPUTFILE}

# ===================
source comstp 'Rsync data to scratch ...'
# ===================
# You can't use the -a flag if you want rsync to follow links
# -a = -rlptgoD

ls ${INPUTDIR}/${INPUTFILE}*.gz

rsync -rLptgoD -q ${INPUTDIR}/${INPUTFILE}*.gz ${RJDSCRATCH}/
#===============================

# ===================
source comstp 'Run analyses ...'
# ===================
fastqc --noextract --quiet --threads 6 ${RJDSCRATCH}/${INPUTFILE}*.fastq.gz

# =============
source comstp 'Close off & rsync back to outputdir ...'
# =============
rm ${RJDSCRATCH}/${INPUTFILE}*.fastq.gz
rsync -aqu ${RJDSCRATCH}/ ${OUTPUTDIR}/ &&  rm -r ${RJDSCRATCH}
# =========
source comstp 'End of script ...' $0 '...'
