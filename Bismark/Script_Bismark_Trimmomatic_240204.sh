# This script should be appended to a slurm header for submission

#=============
#Job preprep
#=============

source ~/CodeRepos/hpc-workflows/HPC/Script_jobsubmission_preamble_240130.sh && sleep 1s

#Create directory for analysis
if [[ ! -d ${SCRATCH} ]]
then
echo 'SCRATCH not found!'
exit 1
else
echo 'SCRATCH found! at ' ${SCRATCH}
cd ${SCRATCH}
fi

#=============
#Load required modules
#=============

# Rackham
# Euler
module load gcc/8.2.0
module load trimmomatic/0.38

#=============
# Read in vars
#=============

## set variables
export FILEROOTLIST=${1}
export INPUTDIR=${2}
export OUTPUTDIR=${3}

#=============

#Create directory for analysis
if [[ ! -d ${OUTPUTDIR} ]]
then
    mkdir -p ${OUTPUTDIR}
fi
#===============================

#Internal job index variable
IDX=${LSB_JOBINDEX}
## echo ${IDX}

## get the sample to work with
INPUTFILE=$(awk -v IDX="${IDX}" 'NR==IDX{print $0}' ${FILEROOTLIST})
## echo $INPUTFILE

#===============================

# Copy files to local scratch
mkdir ${SCRATCH}/Input_Files/
rsync -aq ${INPUTDIR}/${INPUTFILE}*.gz ${SCRATCH}/Input_Files/
#===============================

# This only works on Euler!!
adapter=/cluster/apps/gdc/trimmomatic/0.35/adapters/TruSeq3-PE.fa
##NexteraPE-PE.fa
##TruSeq3-PE.fa   ## without reverse complements
##TruSeq3-PE-2.fa ## with reverse compliments
##NexteraPE-PE.fa
##TruSeq3-SE.fa
##TruSeq2-SE.fa
##TruSeq2-PE.fa

# Run commands
trimmomatic PE -threads 4 -phred33 \
${SCRATCH}/Input_Files/${INPUTFILE}*.fastq.gz \
${INPUTFILE}R1_trim.fq.gz ${INPUTFILE}R1_un.fq.gz \
${INPUTFILE}R2_trim.fq.gz ${INPUTFILE}R2_un.fq.gz \
ILLUMINACLIP:${adapter}:2:30:10 \
HEADCROP:10 \
LEADING:10 \
TRAILING:10 \
SLIDINGWINDOW:4:20 \
MINLEN:36 \

## DON'T RENAME THE FILES .. IT CAUSES TROUBLE WITH REPORTS DOWNSTREAM
#===============================
# Remove the input data from the scratch space
rm -r ${SCRATCH}/Input_Files
rsync -aqu ${SCRATCH}/ ${OUTPUTDIR}/ &&  rm -r ${SCRATCH}
#===============================
