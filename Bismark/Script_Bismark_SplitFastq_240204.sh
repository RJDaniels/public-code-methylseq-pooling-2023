# This script should be appended to a slurm header for submission

#=============
#Job preprep
#=============

source ~/CodeRepos/hpc-workflows/HPC/Script_jobsubmission_preamble_240130.sh && sleep 1s

#Create directory for analysis
if [[ ! -d ${RJDSCRATCH} ]]
then
echo 'RJDSCRATCH not found!'
exit 1
else
echo 'RJDSCRATCH found! at ' ${RJDSCRATCH}
cd ${RJDSCRATCH}
fi


#=============
# Read in vars
#=============

## set variables
export FILEROOTLIST=${1}
# backup incase I forgot the variable name
export ROOTLISTFILE=${1}

export INPUTDIR=${2}
export OUTPUTDIR=${3}

#=============
#Load required modules
#=============

if [[ ${RJDPLATFRM} == 'RACKHAM' ]];
then
# Rackham
#You may run "source conda_init.sh" to initialise your shell to be able
#to run "conda activate" and "conda deactivate" etc.
ml conda
# Change env path AFTER loading the module
export CONDA_ENVS_PATH=${PRJDIR}

source conda_init.sh
conda activate python39-env
fi


if [[  ${RJDPLATFRM} == 'EULER'  ]];
then
echo 'No modules set up yet! exiting...'
exit
# Euler
fi

if [[  ${RJDPLATFRM} == 'ILIFU'  ]];
then
# on ilifu
module load anaconda3/login
# Change env path AFTER loading the module
export CONDA_ENVS_PATH=${PRJDIR}
#source conda_init.sh # DOES NOTHING ON ILIFU
conda activate python39-env
fi

###Let's hyperthread
## Bismark recommends only 4 "cores per alignment thread"
export OMP_NUM_THREADS=4

# ===================
source comstp 'Safety checks etc.. ...'
# ===================
#Create directory for analysis
if [[ ! -d ${OUTPUTDIR} ]]
then
    mkdir -p ${OUTPUTDIR}
fi

if [[ ! -d ${INPUTDIR} ]]
then
echo 'ERR: INPUTDIR does not exist!'
exit
fi

if [[ ! -f ${FILEROOTLIST} ]]
then
echo 'ERR: FILEROOTLIST does not exist!'
exit
fi

# ===================
source comstp 'Internal job index variable ...'
# ===================

#Internal job index variable
echo 'Job IDX running ...' ${RJD_SLURM_ARRAY_TASK_ID}

export INPUTFILE=$(sed -n "${RJD_SLURM_ARRAY_TASK_ID}p" ${FILEROOTLIST})

source comstp 'This is the file name to run ...'
echo ${INPUTFILE}

# ===================
source comstp 'Rsync data to scratch ...'
# ===================

# Copy files to local scratch
mkdir -p ${RJDSCRATCH}/Input_Files/

echo "Files to download..."
ls ${INPUTDIR}/${INPUTFILE}_R[12]_*.fq.gz

rsync -rLptgoD -q ${INPUTDIR}/${INPUTFILE}_R[12]_*.fq.gz ${RJDSCRATCH}/Input_Files/

echo "Files downloaded..."
ls ${RJDSCRATCH}/Input_Files/

#===============================

# ===================
source comstp 'Run commands...'
# ===================

find ${RJDSCRATCH}/Input_Files/ \
 -name "${INPUTFILE}_R[12]_*.fq.gz" \
 -type f \
 -exec \
 bash -c '
 BASENAME=$(basename -s .fq.gz "{}") ;
fastqsplitter \
--threads-per-file 1 \
-o ${RJDSCRATCH}/split1_${BASENAME}.fq.gz \
-o ${RJDSCRATCH}/split2_${BASENAME}.fq.gz \
-o ${RJDSCRATCH}/split3_${BASENAME}.fq.gz \
-o ${RJDSCRATCH}/split4_${BASENAME}.fq.gz \
-o ${RJDSCRATCH}/split5_${BASENAME}.fq.gz \
-o ${RJDSCRATCH}/split6_${BASENAME}.fq.gz \
-i "{}" 
'  \;

# =============
source comstp 'Close off & rsync back to outputdir ...'
# =============

## DON'T RENAME THE FILES .. IT CAUSES TROUBLE WITH REPORTS DOWNSTREAM
#===============================
# Remove the input data from the scratch space
rm *temp*
rm -r ${RJDSCRATCH}/Input_Files
rsync -aqu ${RJDSCRATCH}/ ${OUTPUTDIR}/ &&  rm -r ${RJDSCRATCH}
#===============================
