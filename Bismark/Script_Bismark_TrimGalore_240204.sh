# This script should be appended to a slurm header for submission

#=============
#Job preprep
#=============

source ~/CodeRepos/hpc-workflows/HPC/Script_jobsubmission_preamble_240130.sh && sleep 1s

#Create directory for analysis
if [[ ! -d ${RJDSCRATCH} ]]
then
echo 'RJDSCRATCH not found!'
exit 1
else
echo 'RJDSCRATCH found! at ' ${RJDSCRATCH}
cd ${RJDSCRATCH}
fi

#=============
#Load required modules
#=============

if [[  ${RJDPLATFRM} == 'RACKHAM'  ]];
then
module load bioinfo-tools
module load FastQC
module load TrimGalore
module load pigz
module load python
fi

if [[  ${RJDPLATFRM} == 'EULER'  ]];
then
# Euler
module load fastqc
module load trimgalore
module load pigz
module load python
fi

if [[  ${RJDPLATFRM} == 'ILIFU'  ]];
then
# on ilifu
module load anaconda3/login

# Change env path AFTER loading the module
export CONDA_ENVS_PATH=${PRJDIR}
#source conda_init.sh # DOES NOTHING ON ILIFU
conda activate python39-env

fi

#=============
# Read in vars
#=============

## set variables
export FILEROOTLIST=${1}
export INPUTDIR=${2}
export OUTPUTDIR=${3}

#=============

#Create directory for analysis
if [[ ! -d ${OUTPUTDIR} ]]
then
    mkdir -p ${OUTPUTDIR}
fi

# ===================
source comstp 'Internal job index variable ...'
# ===================
#Internal job index variable
echo 'Job IDX running ...' ${RJD_SLURM_ARRAY_TASK_ID}

export INPUTFILE=$(sed -n "${RJD_SLURM_ARRAY_TASK_ID}p" ${FILEROOTLIST})

source comstp 'This is the file name to run ...'
echo ${INPUTFILE}

# ===================
source comstp 'Rsync data to scratch ...'
# ===================
mkdir -p ${RJDSCRATCH}/Input_Files/

ls ${INPUTDIR}/${INPUTFILE}*.gz

rsync -rLptgoD -q ${INPUTDIR}/${INPUTFILE}*.gz ${RJDSCRATCH}/Input_Files/
#===============================

# Run commands
trim_galore \
--retain_unpaired \
--paired \
--fastqc_args "--quiet --threads 4" \
--clip_R1 10 \
--clip_R2 10 \
--three_prime_clip_R1 10 \
--three_prime_clip_R2 10 \
-j 2 \
${RJDSCRATCH}/Input_Files/${INPUTFILE}*.fastq.gz

# =============
source comstp 'Close off & rsync back to outputdir ...'
# =============

## DON'T RENAME THE FILES .. IT CAUSES TROUBLE WITH REPORTS DOWNSTREAM
#===============================
# Remove the input data from the scratch space
rm -r ${RJDSCRATCH}/Input_Files
rsync -aqu ${RJDSCRATCH}/ ${OUTPUTDIR}/ &&  rm -r ${RJDSCRATCH}
#===============================
