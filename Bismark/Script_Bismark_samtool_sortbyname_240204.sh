# This script should be appended to a slurm header for submission

#=============
#Job preprep
#=============

source ~/CodeRepos/hpc-workflows/HPC/Script_jobsubmission_preamble_240130.sh && sleep 1s

#Create directory for analysis
if [[ ! -d ${RJDSCRATCH} ]]
then
echo 'RJDSCRATCH not found!'
exit 1
else
echo 'RJDSCRATCH found! at ' ${RJDSCRATCH}
cd ${RJDSCRATCH}
fi

#=============
#Load required modules
#=============

if [[  ${RJDPLATFRM} == 'RACKHAM'  ]];
then
# Rackham
module load bioinfo-tools
module load samtools
fi

if [[  ${RJDPLATFRM} == 'EULER'  ]];
then
# Euler
module load samtools
fi

if [[  ${RJDPLATFRM} == 'ILIFU'  ]];
then
echo 'Use the apptainer image!!'
fi

###Let's hyperthread
## Bismark recommends only 4 "cores per alignment thread"
#export OMP_NUM_THREADS=4

#=============
# Read in vars
#=============

## set variables
export FILEROOTLIST=${1}
# backup incase I forgot the variable name
export ROOTLISTFILE=${1}

export INPUTDIR=${2}
export OUTPUTDIR=${3}

#=============

#Create directory for analysis
if [[ ! -d ${OUTPUTDIR} ]]
then
    mkdir -p ${OUTPUTDIR}
fi

if [[ ! -d ${INPUTDIR} ]]
then
echo 'ERR: INPUTDIR does not exist!'
exit
fi

if [[ ! -f ${FILEROOTLIST} ]]
then
echo 'ERR: FILEROOTLIST does not exist!'
exit
fi

# ===================
source comstp 'Internal job index variable ...'
# ===================

#Internal job index variable
echo 'Job IDX running ...' ${RJD_SLURM_ARRAY_TASK_ID}

export INPUTFILE=$(sed -n "${RJD_SLURM_ARRAY_TASK_ID}p" ${FILEROOTLIST})

source comstp 'This is the file name to run ...'
# this needs to be keep out of comstp, for some reason it doesn't get echo'd otherwise
echo ${INPUTFILE}


# ===================
source comstp 'Rsync data to scratch ...'
# ===================

# Copy files to local scratch
mkdir -p ${RJDSCRATCH}/Input_Files/

echo 'Files to upload...'
ls ${INPUTDIR}/split[1-9]*${INPUTFILE}*deduplicated.bam

rsync -rLptgoD -q ${INPUTDIR}/split[1-9]*${INPUTFILE}*deduplicated.bam ${RJDSCRATCH}/Input_Files/

echo 'Files uploaded...'
ls ${RJDSCRATCH}/Input_Files/

# ===================
source comstp 'Analyses ...'
# ===================

source comstp 'Paired bam files ...'

# The splitfastq file breaks the fastq file into chunks of 100 and then sends each chunk to a separate file and loops over as needed
# so the split1-x inputs cannot be arranged to get the correct order for the reads
# Bismark deduplicate does not sort the sequence names 
# to prevent possible issues with non-sorted reads, re-order with samtools

samtools \
 sort -n \
 -@ ${OMP_NUM_THREADS} \
 -o  ${RJDSCRATCH}/split1_${INPUTFILE}_R1_val_1_bismark_bt2_pe.multiple.deduplicated.namesorted.bam \
${RJDSCRATCH}/Input_Files/split1_${INPUTFILE}_R1_val_1_bismark_bt2_pe.multiple.deduplicated.bam

 
 source comstp 'Unpaired bam ...'

samtools \
 sort -n \
 -@ ${OMP_NUM_THREADS} \
 -o  ${RJDSCRATCH}/split1_${INPUTFILE}_R1_unpaired_1_bismark_bt2.multiple.deduplicated.namesorted.bam \
 ${RJDSCRATCH}/Input_Files/split1_${INPUTFILE}_R1_unpaired_1_bismark_bt2.multiple.deduplicated.bam

 # =============
source comstp 'Close off & rsync back to outputdir ...'
# =============

## DON'T RENAME THE FILES .. IT CAUSES TROUBLE WITH REPORTS DOWNSTREAM
#===============================
# Remove the input data from the scratch space
rm *temp*
rm -r ${RJDSCRATCH}/Input_Files
rsync -aqu ${RJDSCRATCH}/ ${OUTPUTDIR}/ &&  rm -r ${RJDSCRATCH}
#===============================
