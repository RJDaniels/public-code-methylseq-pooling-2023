# This script should be appended to a slurm header for submission

#=============
#Job preprep
#=============

source ~/CodeRepos/hpc-workflows/HPC/Script_jobsubmission_preamble_240130.sh && sleep 1s

#Create directory for analysis
if [[  ! -d "${RJDSCRATCH}" ]]
then
source comstp 'RJDSCRATCH not found!'
exit 1
else
source comstp 'RJDSCRATCH found! at ' ${RJDSCRATCH}
cd ${RJDSCRATCH}
fi

#=============
#Load required modules
#=============
#=============
#Load required modules
#=============

if [[  ${RJDPLATFRM} == 'RACKHAM'  ]];
then
# Rackham
module load bioinfo-tools
module load FastQC
# common
module load samtools
module load bcftools
module load bowtie2
module load bismark
fi

if [[  ${RJDPLATFRM} == 'EULER'  ]];
then
# Euler
module load fastqc
module load samtools
module load bcftools
module load bowtie2
module load bismark
fi

if [[  ${RJDPLATFRM} == 'ILIFU'  ]];
then
echo 'Use the apptainer image!!'
fi

###Let's hyperthread
## Bismark recommends only 4 "cores per alignment thread"
#export OMP_NUM_THREADS=4


#=============

#=============
# Read in vars
#=============

 ## set variables
export REFGENOMEDIR=${1}
export REFGENOMEPRFX=${2}

#=============
# ===================
source comstp 'Safety checks etc.. ...'
# ===================
#Create directory for analysis

if [[ ! -d ${REFGENOMEDIR} ]]
then
echo 'ERR: REFGENOMEDIR does not exist!'
exit
fi


# Copy files to local scratch
# ===================
source comstp 'Rsync data to scratch ...'
# ===================

# You can't use the -a flag if you want rsync to follow links
# IF THERE ARE MULTIPLE FASTAS IN THIS DIR, YOU WILL END UP WITH MULTIPLE REFERENCES INTERFERING

echo 'Files to upload...'
ls ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa*

# -a = -rlptgoD
rsync -rLptgoD -q ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa* ${RJDSCRATCH}/

echo 'Files uploaded...'
ls ${RJDSCRATCH}

# ===================
source comstp 'Run analyses ...'
# ===================

#export CMD=~/.local/bin/bismark_genome_preparation
export CMD=bismark_genome_preparation

#gunzip ${TMPDIR}/${REFGENOME}

${CMD} --genomic_composition --verbose ${RJDSCRATCH}/
#${CMD} --hisat2 --genomic_composition --verbose ${RJDSCRATCH}/

# =============
source comstp 'Close off & rsync back to outputdir ...'
# =============

# Copy new and changed files back. LSF saves the path of the directory from which the job was submitted in $LS_SUBCWD.

rsync -aqu ${RJDSCRATCH}/ ${REFGENOMEDIR}/ &&  rm -r ${RJDSCRATCH}

# =========
source comstp 'End of script ...' $0 '...'
