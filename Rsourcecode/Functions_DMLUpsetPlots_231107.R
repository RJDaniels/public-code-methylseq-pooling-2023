#! /bin/R


# R functions for creating ggupset plots

# ==============
# pulling sample info from paths
# ==============
# Moved to the Methylkit script
if(FALSE){
fn.Verbose.AnalysisInfo = function(item){

tmp.treatment =  ''
tmp.subtreatment =   ''
tmp.handle =   ''
tmp.Stat =   ''

## Tile
 if(
 grepl('tile_', item)
 ){
tmp.res =   'DMR'
 }else{
tmp.res =   'DMS'
 }


 ## Treatment
if(grepl('_pollution_', item)){

tmp.treatment =   'Pollution'

if(grepl('_pool', item)){
tmp.handle =  'Pool sequenced samples'
}
if(grepl('_nonpool', item)){
tmp.handle =  'Individually sequenced samples'
}
if(grepl('_pseudopool', item)){
tmp.handle =   'Individually sequenced samples, pseudopooled'
}
if(grepl('_nonpseudopool', item)){
tmp.handle =   'Individually sequenced samples'
}

}else{

tmp.treatment =  'Pooling'

if(grepl('_pool_nonpol', item)){
tmp.subtreatment =   'Non-pollution samples'
tmp.handle = 'Individually sequenced samples, pseudopooled'
}
if(grepl('_pool_pol', item)){
tmp.subtreatment = 'Pollution samples'
tmp.handle = 'Individually sequenced samples, pseudopooled'
}}

## Decide on the stat
if(grepl('pool', tmp.handle)){
tmp.Stat = 'Fisher’s Exact Test'
}else{
tmp.Stat = 'Logistic Regression'
}

data.frame(
resolution = tmp.res,
treatment = tmp.treatment,
treatment.info = tmp.subtreatment,
Sample.handling = tmp.handle,
Statistical.test = tmp.Stat
) %>%
return

}
}

# ===============
# pulling sample info from paths ... a different version
# ===============
if(FALSE){
fn.Info4rmCol = function(col2use, spp.no=9, task.no=10){
str_split(col2use,"[/]") %>%
lapply(.,function(item){

spp = item[spp.no] %>%
str_split(.,"[_]") %>%
lapply(.,function(item.sub){
item.sub[[length(item.sub)-1]]})  %>%
unlist

DATASRCIDX1 = item[spp.no] %>%
str_split(.,"[_]") %>%
lapply(.,function(item.sub){
item.sub[[length(item.sub)]]
})  %>%
unlist

task = item[task.no] %>%
str_split(.,"[_]",3) %>%
lapply(.,function(item.sub){
item.sub[[length(item.sub)]]
})  %>%
unlist

cov = task %>%
str_split(.,"[_]",2) %>%
lapply(.,function(item.sub){
item.sub[[length(item.sub)-1]]
})  %>%
unlist

treatment.str = task %>%
str_split(.,"[_]",3) %>%
lapply(.,function(item.sub){
item.sub[[length(item.sub)-1]]
})  %>%
unlist

treatment.subset = task %>%
str_split(.,"[_]",4) %>%
lapply(.,function(item.sub){
item.sub[[length(item.sub)-1]]
})  %>%
unlist


DATASRCIDX.all = item[task.no] %>%
str_split(.,"[_]",3) %>%
lapply(.,function(item.sub){
item.sub[[length(item.sub)-1]]
})  %>%
unlist

File = item[length(item)]

if(
grepl('minpergrp|minovrlap', File )
){

minpergrp = File %>%
str_split(.,"[_]") %>%
lapply(.,function(item.sub){
item.sub[[length(item.sub)-1]]
})  %>%
unlist %>% 
gsub('minpergrp','',.) %>%
gsub('minovrlap','',.)

}else{
minpergrp=NA 
}



data.frame(spp,DATASRCIDX.all,cov, treatment.str, treatment.subset, task,File,minpergrp ) %>% return
}) %>%
bind_rows

}

}

# ====================
# set boolean for hyper or hypomethyaltion
# ====================
fn.boolean.meth.direction = function(data = NULL, meth.char = NULL){
'Test if hypo or hyper- meth' %>% print

stopifnot(
is.data.frame(data),
dim(data)[1]>0,
is.character(meth.char),
grepl('hyp',meth.char)
)

if(
grepl('hyper',meth.char)
){
data %<>%
filter( hyper == T )
}else{
if(
grepl('hypo',meth.char)
){
data %<>%
filter( hyper != T )
}else{
'Not specified' %>% print
}}
return(data)
}


# fn.boolean.meth.direction(data = NULL, meth.char = NULL)

#data.df = fn.boolean.meth.direction(data = sitesxfile.db, meth.char = 'hyp-all')

# ====================
# Filter data 
# ====================
fn.mkupsetplot.filter = function(
data = NULL,
cov.input = NULL,
chr.input = NULL,
cutoff.input = NULL,
res.input = NULL
){

stopifnot(
is.data.frame(data),
dim(data)[1] > 1,
is.character(cov.input),
is.character(chr.input),
is.numeric(cutoff.input),
is.character(res.input),
c('mincov','chr','resolution','task') %in% names(data) %>% all
)


# 'Filter by criteria' %>% print
data %<>% dplyr::filter(grepl(eval(cov.input),mincov)) 
#str(data)
#summary(data)
#table(data$cov)

# This was to deal with 'duplicated' tests under 0, 1 and 0.75 coverage
# it is only a problem now for the pollution non-pool and needs to be handled properly
# 'Filter by criteria' %>% print
#data %<>% filter(grepl('minovrlap1',File))  
#str(data)
#table(data$File)

# 'Filter by criteria' %>% print
data %<>% filter(grepl(eval(chr.input),chr))  
#str(data)
#table(data$chr)

# 'Filter by criteria' %>% print
data %<>% filter(abs(meth.diff) >  cutoff.input )  
#str(data)
#summary(data$meth.diff)

# 'Filter by criteria' %>% print
data %<>% filter( resolution == res.input ) 
#str(data)
#table(data$resolution)


data$task %<>% 
gsub('tile','', . ) %>%
gsub('diff','', . ) %>%
gsub(cov.input,'', . ) %>%
gsub('[_]+','_', . ) 
#gsub('[non]*pseudopool','', . ) %>% # keep this for computational pooling
#%>% factor(.)

# str(data)
# 'Confirm that the distribution of values is correct...' %>% print
# data %>% 
# group_by(task,cov,minpergrp,hyp,resolution) %>%  
# summarise(n = n())%>% 
# as.data.frame %>% print
# THE SET LABELS STILL MATCH THE NUMBER OF LOCI AT THIS STAGE

# ==========================
# 'Filter by criteria' %>% print
#data %<>% dplyr::select(site.code, task)
#table(data$task)

'str after all the filtering...' %>% print
str(data) %>% print

return(data)

}

# fn.mkupsetplot.filter(
# data = sitesxfile.db,
# cov.input = '10',
# chr.input = 'LG',
# cutoff.input = 10,
# res.input = 'DMR')

# tmp2 = fn.mkupsetplot.filter(
# data = tmp,
# cov.input = '10',
# chr.input = 'LG',
# cutoff.input = 10,
# res.input = 'DMR')

# tmp = fn.mkupsetplot.filter(
# data = data.df,
# cov.input = '10',
# chr.input = 'LG',
# cutoff.input = 10,
# res.input = 'DMR')


# ========================
# make df for ggupset
# ========================
fn.mkupsetplot.mkdf = function(data = NULL){

stopifnot(
is.data.frame(data),
c('species','site.code','hyper','mincov','chr','resolution','task') %in% names(data) %>% all
)

# MOVING TO GGUPSET AT THIS POINT
# requires a tibble, not binary matrix
# We want a tibble with a list-column of loci overlaps

# Dplyr does not support list-columns as grouping variables so summary stats will need a dummie grouping column
# manually collapse the list-column to a chr string and use that to do the grouping

'Split by task...' %>% print

sitesxfile.upsetlist = data %>% 
group_by(site.code)  %>%
 distinct(site.code, task, hyper, mincov, .keep_all=TRUE) 
#dplyr::select(site.code, task) %>% 
 
# sitesxfile.upsetlist %>% str
 
 sitesxfile.upsetlist  %<>%
  mutate(
  task_collapsed = sapply(task, function(x) paste0(sort(x), collapse="-"))
  ) 

  # =======
  # Get summary by task
  # =======
  
   sitesxfile.upsetlist.smryxtask =  sitesxfile.upsetlist %>%
  group_by(task,species,hyper) %>%
  dplyr::summarize(`DML Count` = n()) %>%
    group_by(task,species) %>%
  mutate(
  `DML %` = `DML Count`/sum(`DML Count`, na.rm = T),
`Methylation direction` =  case_when(
hyper == T ~ 'Hyper',
hyper == F ~ 'Hypo'
), 
Intersection = task
) %>%
    dplyr::select(`Methylation direction`, Intersection,`DML Count` ,`DML %`)

   
  sitesxfile.upsetlist  %<>%
  mutate(
  task_collapsed = fct_lump(fct_infreq(as.factor(task_collapsed)))
  ) %>%
dplyr::summarize(
#dplyr::reframe(
task = list(task), hyper = unique(hyper), mincov = unique(mincov), species = unique(species)
) 


#sitesxfile.upsetlist %>% str

# Summary by intersection
#  sitesxfile.upsetlist %>%
#  group_by(hyp, task_collapsed) %>% 
#   summarize(percent_dm = sum()) %>%
#   group_by(task_collapsed) %>%
#   mutate(percent_rating = percent_rating / sum(percent_rating)) %>%
#   arrange(task_collapsed)
#str(sitesxfile.upsetlist )

#'Show the names of the list...' %>% print
#names(sitesxfile.upsetlist) %>% str %>% print

#names(sitesxfile.upsetlist) = levels(dat2plot$task) # I think this assigns the wrong names to the sets
sitesxfile.upset.sets = data$task %>% unique 
# I think this assigns the wrong names to the sets

#'str of the upsetlist...' %>% print
#str(sitesxfile.upset.sets)
return(list(sitesxfile.upsetlist , sitesxfile.upset.sets,    sitesxfile.upsetlist.smryxtask))
}

# tmp = fn.mkupsetplot.mkdf(
# data = sitesxfile.db) 

# tmp %<>% fn.mkupsetplot.mkdf(data = .) 


# ========================
# make summary of df
# ========================
fn.mkupsetplot.sum = function(
input.list = NULL,
plot.title = NULL,
path = OUTPUTDIR
){
# input.df = sitesxfile.upsetlist[[1]]

stopifnot(
is.character(plot.title),
is.character(path),
is.list( input.list),
is.data.frame( input.list[[1]]),
is.data.frame( input.list[[3]]),
c('site.code','hyper','mincov','task') %in% names( input.list[[1]]) %>% all
)


input.df = input.list[[1]]

 input.df %<>%
 mutate(task_collapsed = paste(task,collapse='_'))
 
# Summary by intersections
# count and prop of loci split by intersection x direction
 
  intersecxdir.sum.df =  input.df %>%
 group_by(hyper, task, mincov,task_collapsed) %>% 
dplyr::summarize(`DML Count` = n()) %>%
 group_by(hyper, mincov) %>% 
  mutate(`DML %` = `DML Count` / sum(`DML Count`)) %>%
  arrange(task_collapsed)
 
    intersecxdir.sum.df  %<>%
    ungroup %>%
    dplyr::rename(
    Intersection = 'task_collapsed'
    ) %>%
    mutate(
`Methylation direction` =  case_when(
hyper == T ~ 'Hyper',
hyper == F ~ 'Hypo'
)) %>% 
    dplyr::select(`Methylation direction`, Intersection,`DML Count` ,`DML %`)
  
  # =====
  # Estimate the total per group
  # =====
  
input.smry =  input.list[[3]] %>%
  ungroup %>%
  dplyr::select(-task) %>%
     full_join(intersecxdir.sum.df ,.) %>%
      relocate(species, `Methylation direction`,Intersection) %>%
      arrange(species, `Methylation direction`,Intersection)
  
# =======
# Export the table
# =======

dat2export = input.smry

ltx.tit = paste0('TallyxIntersection')
outname = paste0(path,"/SupTable_Methylkit_UpsetSummary")

dat2export %>%
 write_ods(path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit,'_',plot.title), 
 x = .,
 append = F, update = F)

# ========
 # latex
# ========

outname = paste0(path,"/SupTable_Methylkit_UpsetSummary_",ltx.tit,'_',plot.title) 

ltx.lab2use = paste0('stab:Results:',paste0(ltx.tit,'_',plot.title)) 

ltx.cap2use = paste0('\\small 
{\\bf Summary of DML overlap between data sets.} Shown are the total DMLs in each possible intersection of the data sets.')

# Format the numbers
# Sprintf does not do commas ... I couldn't find any way to use it for this
# ONLY FOR COLS WITH MEAN,SD

dat2export %<>%
mutate(
across(
.cols = contains('Count'),
 ~ formatC(.x, big.mark=",", digits = 0 , format = 'f')
),
across(
.cols = contains('%'),
 ~ sprintf('%.2f', .x)
)) 


tab2export = dat2export  %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use)

tab2export %>%
print(.,
file = paste0(outname,".tex"),
hline.after=c(-1, 0),
tabular.environment = "longtable",
floating = F, caption.placement = "top",
include.rownames=F,
size = " \\small")

return(list(input.smry ))
}


#tmp2 = fn.mkupsetplot.sum( input.list = tmp, plot.title = 'hello')


# ========================
# Create ggplot
# ========================

# a = upsetlist.input
# b = upset.sets.input
# c=  cov.input
# d =  res.input
# e = chr.input
# f = hyp.input
# g = log.input
# h = cutoff.input
# i = textcol.input
# j = geom.text.size
# k = academ_theme
# l = spp.input
# m =  TODAY
# n =  intersecxdir.sum.df

 # Create summary of loci per intersection
 # Add annot for the number of loci 
 # Split annots by hyper/hypo
 # Add summary for percentage intersection on total
 # split perc by meth direction
# set the height of text labels to a common value to prevent confusion about where the labels belong

# Don't use spp ... it is redundant with  chr...

fn.mkupsetplot.ggcode = function(
upsetlist.input = NULL,
upset.sets.input = NULL,
cov.input = NULL,
res.input = NULL,
chr.input = NULL,
hyp.input = NULL,
log.input = NULL,
cutoff.input = NULL,
textcol.input = NULL,
plot.title = NULL,
#spp.input = NULL,
path = OUTPUTDIR
){

stopifnot(
is.data.frame(upsetlist.input),
is.vector(upset.sets.input),
is.character(cov.input),
is.character(res.input),
is.character(chr.input),
is.character(hyp.input),
is.character(log.input),
is.numeric(cutoff.input),
is.character(plot.title),
is.character(textcol.input),
#is.character(spp.input),
is.character(path)
)


pacman::p_load(ggupset)

'Set export names...' %>% print

outname = paste0( path,"/Plot_SitexFile_",plot.title)

print(outname  )

'create plot...' %>% print

p1  = upsetlist.input %>%
ggplot(aes(x=task))  +
    geom_bar(
    aes(group=hyper, fill=hyper)
    ) + 
    scale_x_upset(
    order_by = "degree",
     sets = upset.sets.input,
                  position="bottom",
                   name = "Intersections")  +
     geom_text(stat='count', 
     aes(label=paste0(after_stat(count),' (',sprintf("%2.2f",after_stat(count/sum(count))),')'), 
     group = hyper), 
     size = geom.text.size*1, position = position_dodge(width = .9)) +
#     geom_text(stat='count', aes(label=after_stat(count), group=hyp),  size = geom.text.size*1.2, position = position_dodge(width = .9), hjust = 0.6) +
labs(x='DMR count')  +
theme_combmatrix(
                      combmatrix.panel.point.color.fill = textcol.input,
                     combmatrix.panel.line.size = 1.2,
                     combmatrix.label.make_space = FALSE,
                     combmatrix.label.text = element_text(size=theme.size*1.5),
                     combmatrix.label.extra_spacing = 1.2
                     ) +
academ_theme +
theme(
legend.position = "left",
axis.text = element_text(size = theme.size*1),
axis.title = element_text(size = theme.size*1.1)
) 
#ggtitle( paste0(c,'_',d,'_',e,"_",f,"_",g,"_cut",h)) +

#p1 %>% print

'Export image...' %>% print

ggsave(p1,
filename = paste0(outname, '.',device2use),
height = 170,
width = 269,
units = "mm",
dpi = 300,
device = device2use)

'done exporting...' %>% print
return(p1)

}

#     geom_text(stat='count', aes(label=after_stat(count / sum(count))), vjust=-1.1, hjust=-1.1, size = geom.text.size) + # This produces a perc of the sum of the intersections, I need to have this run by groups (sets) 
#    scale_x_mergelist(sep = "-") +
#    axis_combmatrix(sep = "-") +
# Plot the graph

# allowed width for figures: 80, 112, 169mm
# pdf(
# file = paste0(OUTPUTDIR,outname),
# width = 300/2.51,
# height = 150/2.51)


# fn.mkupsetplot.ggcode(
# upsetlist.input = tmp[[1]],
# upset.sets.input = tmp[[2]],
# cov.input = '10',
# res.input = 'DMR',
# chr.input = 'LG',
# hyp.input = 'hypo',
# log.input = 'lin',
# cutoff.input = 10,
# textcol.input = 'task',
# spp.input = 'Cflu',
# path = OUTPUTDIR)


# ========================
# run entire plot pipe from start to end
# ========================

fn.mkplots.upset = function(
loop.idx = NULL,
data.df = NULL,
param.df = NULL,
plot.tit.extra = 'basic'
){

# Most of the checks are done  in the subfunctions  ...
# just do some bigger checks here

stopifnot(
is.numeric(loop.idx),
is.data.frame(data.df),
dim(data.df)[1]>1,
is.data.frame(param.df),
dim(param.df)[1]>0
)


'Set some env vars' %>% print
log2use = param.df[loop.idx,'log.vec']
chr2use = param.df[loop.idx,'chr.vec']
res2use = param.df[loop.idx,'res.vec']
cov2use = param.df[loop.idx,'cov.vec']
cutoff2use = param.df[loop.idx,'cutoff.vec']
hyp2use = param.df[loop.idx,'hyp.vec']
#stat2use = param.df[loop.idx,'stat.vec']

plot.title2use =  paste0( "overlap_", cov2use,"_res",res2use,'_',chr2use,"_",hyp2use,"_",log2use,"_cut",cutoff2use,'_',plot.tit.extra)

'decide on methyl dir...' %>% print
data.df %<>%  fn.boolean.meth.direction(., meth.char = hyp2use)

'test if df is non-empty' %>% print
if(
dim(data.df)[1] < 1
){
print('skipping this iteration, empty df...')
return(NULL)
}else{

data.df %<>%  fn.mkupsetplot.filter(
data = .,
cov.input = cov2use,
chr.input = chr2use,
cutoff.input = cutoff2use,
res.input = res2use
)
}

# test again
if(
dim(data.df)[1] < 1
){
print('skipping this iteration, empty df...')
return(NULL)
}else{

sitesxfile.upsetlist.list = fn.mkupsetplot.mkdf(data.df)

'Test for more than one task...' %>% print

if(
length(sitesxfile.upsetlist.list[[1]]) < 2
){
print('Only one task, skipping the plots...')
return(NULL)
}else{

intersecxdir.sum.df = fn.mkupsetplot.sum(sitesxfile.upsetlist.list, plot.title = plot.title2use
)

plot.out = fn.mkupsetplot.ggcode(
upsetlist.input = sitesxfile.upsetlist.list[[1]],
upset.sets.input = sitesxfile.upsetlist.list[[2]],
cov.input = cov2use,
res.input = res2use,
chr.input = chr2use,
hyp.input = hyp2use,
log.input = log2use,
cutoff.input = cutoff2use,
textcol.input = textcol2use,
plot.title = plot.title2use,
#spp.input = spp2use,
path = OUTPUTDIR
)
                  }}
                  
                  }

