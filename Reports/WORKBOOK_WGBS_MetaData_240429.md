---
Title : Misc tasks for preparing meta data
---

# Overview

This script is a revised version of the 'WORKBOOK_EMseq_RDBCreate_local_220908.md' file.


Most of this is done interactively in R.

To make these RDS objects available in R (in SLURM scripts), they should be loaded in the startup script for R.

- [] Prepare metadata 
- [] Prepare metadata RDS (for R)
- [] create plot guide RDS (for R)

# Set up local environment

## Data set

Runs are setup by Project.

```bash

export FILEROOT=MetaData

```


## Set up

```bash
# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=RDS_Prep

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi


# ===========
# Additional Paths
# ===========

export RESRCDIR=${PRJDIR}/../Resources
export RDSDIR=${RESRCDIR}/R_RelationalDB

# There is an R_RelationalDB dir with the RDS data
# There is a softlink in the analysis dir for legacy support R_RelationalDB 


```

## R setup

```bash
# ILIFU
sinteractive
module load  R/RStudio2022.12.0-353-R4.2.2
R

```

## Data Prep


Genome information on the study species.
```R
# ========
# Run Startup scripts
# ========
CMMNT = Sys.getenv(c('CMMNT'))
# Load Env variables
source(paste0(CMMNT,'/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.R'))
# Load general R stuff
# This is now in the Env vars step above
source(paste0(CODEDIR,'/StartupScript_Sources_260220.R'))
# =============


# ============
# For interactive work
# ============
INPUTFILE = Sys.getenv(c('INPUTFILE'))

PRJDIR = Sys.getenv(c('PRJDIR'))

RDSDIR = Sys.getenv(c('RDSDIR'))
rRESRCDIR = Sys.getenv(c('RESRCDIR'))
INPUTDIR = Sys.getenv(c('RDSDIR'))
OUTPUTDIR = Sys.getenv(c('OUTPUTDIR'))

FILE.PATTERN.SUFFIX = Sys.getenv(c('FILE.PATTERN.SUFFIX'))

# ==========
pacman::p_load(magrittr, tidyverse, ggplot2, cowplot, xtable, papeR)
# ============


device2use='jpeg'
    
#device2use='1'
stopifnot(
{device2use %in% c('jpeg','jpg','png','pdf','svg','eps')})

# ==========
# Load functions
# ==========
# ==========


# ==========
# safety checks
# ==========

stopifnot(
is.character(INPUTFILE),
is.character(INPUTDIR),
is.character(OUTPUTDIR)
)

print(paste('Variables in use are ...' ))
print(paste('CMMNT   is ...',	CMMNT  ))
print(paste('INPUTFILE   is ...',	INPUTFILE  ))
print(paste('INPUTDIR   is ...',	INPUTDIR  ))
print(paste('OUTPUTDIR   is ...',	OUTPUTDIR  ))
print(paste('RDSDIR   is ...',	RDSDIR  ))
print(paste('RESRCDIR   is ...',	RESRCDIR  ))

FILE.PATTERN = paste0(INPUTFILE,'.*',FILE.PATTERN.SUFFIX,'$')
print('FILE.PATTERN.SUFFIX is ...')
print(FILE.PATTERN.SUFFIX)
print('FILE.PATTERN is ...')
print(FILE.PATTERN)


#INPUTDIR = paste0(PRJDIR,"/Data/ProjectInfo/")
RDSDIR= RDSDIR

```





## Example of reading in the created RDS

```R

bismark.sum.df = bismark.multiqc.reports.list.db = readRDS(paste0(RDSDIR,'/','DB_Bismark_ConversionRates','.RDS'))
seq.pool.db = readRDS(paste0(RDSDIR,'/','DB_SequencingInfo_pool','.RDS'))
seq.nonpool.db = readRDS(paste0(RDSDIR,'/','DB_SequencingInfo_nonpool','.RDS'))
site.db = readRDS(paste0(RDSDIR,'/','DB_SiteInfo','.RDS'))
spp.genome.db = readRDS(paste0(RDSDIR,'/','DB_SpeciesGenomeSummary','.RDS'))
multiqc.sum.allstages.db = readRDS(paste0(RDSDIR,'/','DB_Bismark_MultiQC','.RDS'))
static.guide = readRDS(paste0(RDSDIR,'/','DB_PlotGuide_Static','.RDS'))


```


## Species Genome info
```R

INPUTFILE='DB_SpeciesGenomeSummary'
OUTPUT = paste0(RDSDIR,'/',INPUTFILE,'_',TODAY,'.RDS')
CLEANOUTPUT = paste0(RDSDIR,'/',INPUTFILE,'.RDS')

# The original ods is MIA at the moment
#readODS::read_ods(), ... sheet = "GenomeSummary_221015")
#spp.genome.df

#spp.genome.df %>% saveRDS()

# refresh the default file to load 

system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========
spp.genome.df = readRDS(CLEANOUTPUT)

```


### Study site info
```R
INPUTFILE='DB_SiteInfo'
OUTPUT = paste0(RDSDIR,'/',INPUTFILE,'_',TODAY,'.RDS')
CLEANOUTPUT = paste0(RDSDIR,'/',INPUTFILE,'.RDS')

# sites.df = readODS::read_ods(paste0(INPUTDIR,'/	DB_SiteInfo_220908.ods'))

sites.df %>% saveRDS(OUTPUT)

system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========

sites.df = readRDS(CLEANOUTPUT)

```

## Sample information.


```R

### Sequencing pool information
INPUTFILE='DB_SequencingInfo_pool'
OUTPUT = paste0(RDSDIR,'/',INPUTFILE,'_',TODAY,'.RDS')
CLEANOUTPUT = paste0(RDSDIR,'/',INPUTFILE,'.RDS')

seq.pool.df = readODS::read_ods(paste0(INPUTDIR,'/DB_SequencingInfo_220908.ods'), sheet='Pools')

seq.pool.df %>% saveRDS(OUTPUT)
system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========

seq.pool.df = readRDS(CLEANOUTPUT)

```


```R
### Sequencing information
INPUTFILE='DB_SequencingInfo_nonpool'
OUTPUT = paste0(RDSDIR,'/',INPUTFILE,'_',TODAY,'.RDS')
CLEANOUTPUT = paste0(RDSDIR,'/',INPUTFILE,'.RDS')

seq.nonpool.df = readODS::read_ods(paste0(INPUTDIR,'/DB_SequencingInfo_220912.ods'),sheet='Samples')

seq.nonpool.df %>% saveRDS(OUTPUT)
system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========
seq.nonpool.df = readRDS(CLEANOUTPUT)

```




## Bismark Summary information.

```R
INPUTFILE='DB_bismark_summary'
OUTPUT = paste0(RDSDIR,'/',INPUTFILE,'_',TODAY,'.RDS')
CLEANOUTPUT = paste0(RDSDIR,'/',INPUTFILE,'.RDS')

#INPUTDIR = paste0(PRJDIR,"/Analyses/Pollution_EMseq/")

### read in bismark summary data
files.vec = list.files(
path=PRJDIR,
 pattern="bismark_summary_report.txt",
full.names=T,
include.dirs=T,
recursive=T)

bismark.sum.df = lapply(
files.vec, 
function(file){
temp.bismsum.df = read.delim(file)
temp.bismsum.df %<>% data.frame(.,ReportFile=file)
}) %>% 
bind_rows

head(bismark.sum.df )

bismark.sum.df %>% saveRDS(OUTPUT)
system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========
bismark.sum.df = readRDS(CLEANOUTPUT)

```

## Plotguide 

Create plotguide to use everywhere
The most basic values should be set here so they are standard across plots
- [] spp
- [] pool
- [] poll

To do later...
- [] cov
- [] tile
- [] hyper/hypo


```R
INPUTFILE='DB_PlotGuide_Static'
OUTPUT = paste0(RDSDIR,'/',INPUTFILE,'_',TODAY,'.RDS')
CLEANOUTPUT = paste0(RDSDIR,'/',INPUTFILE,'.RDS')


data2use = multiqc.sum.allstages.db[['general_stats']] %>% 
mutate(PollutionPool = Sample.Code %>% gsub("[0-9]","",.)) 

names(data2use) %<>% 
gsub("_mqc.generalstats.","",.) %>% 
gsub("[Ff]ast[Qq][Cc]","",.) %>% 
gsub("[Bb]ismark","",.) %>%
gsub("^\\.","",.)

data2use %<>%
filter(!is.na(total_c )) %>%
mutate(Sample = gsub('split[0-9]_','',Sample)) %>%
dplyr::select(Sample.Code,Sample,PollutionPool) %>%
distinct %>%
merge(., seq.nonpool.db, by = "Sample.Code", all.x = T, all.y = F)


data2use %>% str

# Now fix the factor levels

data2use %<>% 
dplyr::select(Sample.Code,Sample,PollutionPool,Site.Code,Species) %>%
mutate(
Species = factor(Species, levels = c('Corbicula fluminea','Dreissena polymorpha')),
Spp = PollutionPool %>% gsub('pool','',.) %>% gsub('[N]*P','',.) %>% factor(., levels = c('C','D')),
PollutionPool = PollutionPool %>% gsub('D|C','',.) %>% factor(., levels = c('P','NP','poolP','poolNP')),
Pollution = PollutionPool %>% gsub('pool','',.) %>% factor(., levels = c('P','NP')),
Pool = PollutionPool %>% grepl('pool',.)
) 

data2use %>% 
str


# New function for creating stable guides

# Add an option for setting labels ... so translating the column entries into fuller labels

fn.CreateStaticGuide = function(var2use = NULL , dat2input = NULL, cols = rep(colchoice,times=7)){

# DF is usable?
stopifnot(
is.data.frame(dat2input),
dim(dat2input)[1]>0,
dim(dat2input)[2]>0
)

# Var is a single column name
stopifnot(
is.character(var2use),
length(var2use) == 1
)

# Column is a factor
stopifnot(
dat2input %>% dplyr::pull(!!sym(var2use)) %>% is.factor
)

# ========
## Colours
# ========
tmp.lvls = dat2input %>% dplyr::pull(!!sym(var2use)) %>% levels()
tmp.lvls.length = length(tmp.lvls)
tmp.guide = colchoice %>% .[1:tmp.lvls.length]
names(tmp.guide) = tmp.lvls

colguide=tmp.guide

# ========
## Rev Colours
# ========
tmp.lvls = dat2input %>% dplyr::pull(!!sym(var2use)) %>% levels()
tmp.lvls.length = length(tmp.lvls)
tmp.guide = colchoice %>% rev %>% .[1:tmp.lvls.length] 
names(tmp.guide) = tmp.lvls

revcolguide=tmp.guide

# ========
## Shapes
# ========
tmp.lvls = dat2input %>% dplyr::pull(!!sym(var2use)) %>% levels()
tmp.lvls.length = length(tmp.lvls)
tmp.guide = rep(0:18,7) %>% .[1:tmp.lvls.length]
names(tmp.guide) = tmp.lvls

shpguide=tmp.guide

list(
guide.col = colguide,
guide.revcol = revcolguide,
guide.shp = shpguide
) %>% return

}

tmp.col.vec

data2use[,tmp.col.vec[3]] %>% is.factor

tmp.col.vec = data2use %>% dplyr::select(where(is.factor)) %>% names 


static.guide = lapply(tmp.col.vec,
fn.CreateStaticGuide,
dat2input = data2use)

names(static.guide) = tmp.col.vec

static.guide %>% saveRDS(OUTPUT)
system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========
static.guide = readRDS(CLEANOUTPUT)

```

