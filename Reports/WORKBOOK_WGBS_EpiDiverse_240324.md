---
Title : Estimate SNPs from WGBS data using the Epidiverse tool
---

# Overview

## Background info

# Requires

## Installation

```bash
```

# Produces
TBC...

# Analyses
TBC...


# Set up local environment

You need to run within a sing container.

```bash

```

## Data set


### Bismark test data

```bash
export FILEROOT=test_data

## Ilifu
export REFGENOMEDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark/Bismark/test_files/ref
export REFGENOMEPRFX=NC_010473
#.fa.gz




# Shorter Ref name
export TMPREF=NC_010473

```


### Gannet Oyster

```bash
export FILEROOT=test_oyster

## Ilifu
export REFGENOMEDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark/Bismark/test_files/ref
export REFGENOMEPRFX=NC_010473
#.fa.gz


# Shorter Ref name
export TMPREF=NC_010473

```

```bash

```bash

https://gannet.fish.washington.edu/spartina/project-gigas-oa-meth/output/bismark-roslin/zr3616_1_R1_val_1_val_1_val_1_bismark_bt2_pe.deduplicated.sorted.bam

wget https://gannet.fish.washington.edu/spartina/project-oyster-oa/Haws/BS-Snper/CHH-meth-info.tab
wget https://gannet.fish.washington.edu/spartina/project-oyster-oa/Haws/BS-Snper/CHG-meth-info.tab
wget https://gannet.fish.washington.edu/spartina/project-oyster-oa/Haws/BS-Snper/CpG-meth-info.tab
wget https://gannet.fish.washington.edu/spartina/project-oyster-oa/Haws/BS-Snper/zr3616_1_R1_val_1_val_1_val_1_bismark_bt2_pe.deduplicated.sorted.bam

```

```

### Dreissena

```bash
export FILEROOT=test_data

## Euler
#export REFGENOMEDIR=/cluster/work/gdc/shared/p866/Data/Reference_Genomes/Deissena_polymorpha/ncbi-genomes-2022-07-18

## Ilifu
export REFGENOMEDIR=${CMMNT}/Projects/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCA_020536995.1
export REFGENOMEDIR=${GRPDIR}/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCA_020536995.1
export REFGENOMEPRFX=GCA_020536995.1_UMN_Dpol_1.0_genomic

# Shorter Ref name
export TMPREF=Dpol

# Annotations
export GFFDIR=${REFGENOMEDIR}
export GFFPRFX=genomic
# fna or fasta.gz

# Methylkit input
export METHKITRAWDIR=${CMMNT}/Projects/23_Epigenome/HPC/Analyses/Euler/MethylKit/Dpol_vV0y0/out_vV0y0-DIq12-gmsCV_10x_pollution_nonpool_diff_pseudopool/methylDB/
#export METHKITRAWDIR=${GRPDIR}/23_Epigenome/HPC/Analyses/Euler/MethylKit_Dpol/out_diffmeth/methylDB/
export METHKITRAWPRFX=methylDiff_united_minpergrp_0_230712_array_pseudopool_NP_PoolFALSE_n10_P_PoolFALSE_n8_230712_diffmeth_minovrlap0_230712
#.txt.bgz file

```




## Set up
```bash

# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=BSSnper

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi
# =======

# File with the names of all the files to run
export ROOTLISTFILE=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming/${FILEROOT}_FileRootList.txt
export FILEROOTLIST=${ROOTLISTFILE}

# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')
echo $ARRAYLENGTH
## ARRAYLENGTH=2

# Trimmed data
#export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming
export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark/out_Bismark/Trimming

# Aligned data
#export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Align_${TMPREF}/out_Bismark_Align_${TMPREF}
export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark/out_Bismark/alignment/

# Dedup data
#export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Dedup_${TMPREF}/out_Bismark_Dedup_${TMPREF}
export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}//Bismark/out_Bismark/deduplication

# Methyl call
export METHDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark/out_Bismark/MethylExtractor


export ALGLINKS=${TASKDIR}/align_links

```


## Data Prep

### Test

```bash

# Links to the input data
mkdir -p $ALGLINKS

find ${ALIGNDIR}  -iname "${FILEROOT}*"  -type f | xargs -I {} ln -s "{}" ${ALGLINKS}/ 
find ${METHDIR}  -iname "${FILEROOT}*"  -type f | xargs -I {} ln -s "{}" ${ALGLINKS}/ 

```

# Common Code


/home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/test_R1_bismark_bt2_pe.deduplicated.bismark.cov.gz


/home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CpG_OB_test_R1_bismark_bt2_pe.deduplicated.txt.gz

/home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CHG_OB_test_R1_bismark_bt2_pe.deduplicated.txt.gz

/home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CHH_OB_test_R1_bismark_bt2_pe.deduplicated.txt.gz







```bash

rjd-bs-snper.BS-Snper  ${DEDUPDIR}/test_R1_bismark_bt2_pe.deduplicated.bam \
 --fa ls ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa.gz  \
 --output ${OUTPUTDIR}/${FILEROOT}.out \
 --methcg /home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CpG_OT_test_R1_bismark_bt2_pe.deduplicated.txt \
 --methchg /home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CHG_OT_test_R1_bismark_bt2_pe.deduplicated.txt \
 --methchh /home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CHH_OT_test_R1_bismark_bt2_pe.deduplicated.txt \
 --minhetfreq 0.1 \
 --minhomfreq 0.85 \
 --minquali 15 \
 --mincover 10 \
 --maxcover 1000 \
 --minread2 2 \
 --errorate 0.02 \
 --mapvalue 20 >SNP.out 2>ERR.log

 
 samtools view \
${DEDUPDIR}/test_R1_bismark_bt2_pe.deduplicated.bam 
 
 
rjd-bs-snper.BS-Snper  \
 --fa ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa \
 --output ${OUTPUTDIR}/${FILEROOT}.out \
 --methcg /home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CpG_OT_test_R1_bismark_bt2_pe.deduplicated.txt /home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CpG_OB_test_R1_bismark_bt2_pe.deduplicated.txt \
 --methchg /home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CHG_OT_test_R1_bismark_bt2_pe.deduplicated.txt /home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CHG_OB_test_R1_bismark_bt2_pe.deduplicated.txt \
 --methchh /home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CHH_OT_test_R1_bismark_bt2_pe.deduplicated.txt  /home/rjd/msRNA_Mobile/Projects/23_Epigenome/HPC/Analyses/test_data-rUT/Bismark/out_Bismark/MethylExtractor/CHH_OB_test_R1_bismark_bt2_pe.deduplicated.txt  \
${DEDUPDIR}/test_R1_bismark_bt2_pe.deduplicated.bam \
 >SNP.out 2>ERR.log

     # filterCG_SNP
    filterCG_SNP.pl \
        ${NAME}.BS-SNP.out \
        ${NAME}.meth.cg \
        ${NAME}.CpG_meth_filter_file \
        ${NAME}.CpG_meth_SNP_file \
        > ${NAME}.filterCG_SNP.log 2>&1
```






