---
Title : Bismark step ; reference data prep
---

# Overview
Processing sequence data with Bismark.
This is entirely slurm submission based.

The process below will need to be run at least three times per dataset, once for each reference genome.
- [] Lambda ref
- [] phage ref
- [] species ref

Genome prep is usually not needed. Euler has a preped genome already for many species and/or I have already done it.
If the index/conversions already exist, you only need to run the methylation calls.

## Background info
TBC...

# Requires

## Installation

# Analyses
TBC...

# Set up local environment

## Data set

### Reference Folder

All genomes are put in a single Reference genome folder. So fileroot is References.
The species-specific stuff follows the initial set up.


```bash
export FILEROOT=References

```

## Set up

```bash
# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=Bismark_RefIndex

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi


```

## Data Prep


## Lambda

!! This has not been updated !!
```bash

# For lambda :Conversion rate estimates
REFGENOMEDIR=${GRPDIR}/RawData_epigenomics/Reference_Genomes/NEB_lambda
REFGENOME=NEB_lambda.fa

```

## pUC19

!! This has not been updated !!
```bash

# For pUC :Conversion rate estimates
REFGENOMEDIR=${GRPDIR}/RawData_epigenomics/Reference_Genomes/pUC19
REFGENOME=pUC19.fa
```

## Corbicula
```bash
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Corbicula_fluminea/Lachesis/methylseq

export REFGENOMEPRFX=Lachesis_assembly_changed

```

## Dreissena

```bash
## Ilifu
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1/methylseq
export REFGENOMEPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic

```

## C Gigas

```bash

## Ilifu
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/CGigas/Roslin_v1
export REFGENOMEPRFX=cgigas_uk_roslin_v1_genomic-mito

```
## Test Data
 Download testing data
For testing the software...

```bash
export REFGENOMEPRFX=NC_010473
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Bismark_test_data/methylseq

mkdir -p ${REFGENOMEDIR}


## Ilifu
## for training data
cd ${CODEDIR}
cd ../
git clone https://github.com/FelixKrueger/Bismark.git


ln -s ${CMMNT}/CodeRepos/Bismark/test_files/${REFGENOMEPRFX}.fa.gz ${REFGENOMEDIR}/
cd ${TASKDIR}

```

# Data Prep

## C Gigas

Download some of the data from https://gannet.fish.washington.edu/

```bash

mkdir -p $REFGENOMEDIR

# Reference genome
wget https://gannet.fish.washington.edu/spartina/project-oyster-oa/data/Cg-roslin/cgigas_uk_roslin_v1_genomic-mito.fa

gzip ${REFGENOMEDIR}/cgigas_uk_roslin_v1_genomic-mito.fa

wget https://gannet.fish.washington.edu/spartina/project-oyster-oa/data/Cg-roslin/cgigas_uk_roslin_v1_genomic-mito.fa.fai


```


## Dreissena

Download reference genomes and meta data

```bash

# USCS Collection
wget -l 0 -r ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/020/536/995/GCF_020536995.1_UMN_Dpol_1.0/ /users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/

```




# Fasta Indexing

The process needs to be repeated for each reference genome.

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/SAMTOOLS/Script_Samtools_RefIndex.sh

JOBNAME=${TASK}'_FastaIndex_'${REFGENOMEPRFX}
JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Devel:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:10GB:g" \
-e "s/ZZWALLTIMEZZ/02:00:00/g" \
-e "s:ZZJOBNAMEZZ:${JOBNAME}:g" \
-e "s:ZZARRAYLENGTHZZ:1:g" \
-e "s:ZZARRAYCONCURZZ:1:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_bismark.sif  bash \' >> ${JOBSCRIPT}
# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export REFGENOMEDIR=${1}
echo ${REFGENOMEDIR}' \' >> ${JOBSCRIPT}

#export REFGENOME=${2}
echo ${REFGENOMEPRFX}' \' >> ${JOBSCRIPT}

```



# Reference Indexing and Conversion

The process needs to be repeated for each reference genome.

## SLURM submission

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_RefGenomeIndex_240204.sh

JOBNAME=${TASK}'_RefGenome_'${REFGENOMEPRFX}
JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:10GB:g" \
-e "s/ZZWALLTIMEZZ/02:00:00/g" \
-e "s:ZZJOBNAMEZZ:${JOBNAME}:g" \
-e "s:ZZARRAYLENGTHZZ:1:g" \
-e "s:ZZARRAYCONCURZZ:1:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_bismark.sif  bash \' >> ${JOBSCRIPT}
# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export REFGENOMEDIR=${1}
echo ${REFGENOMEDIR}' \' >> ${JOBSCRIPT}

#export REFGENOME=${2}
echo ${REFGENOMEPRFX}' \' >> ${JOBSCRIPT}


```

