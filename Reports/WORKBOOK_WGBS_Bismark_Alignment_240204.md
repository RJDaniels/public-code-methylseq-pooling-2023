---
Title : Bismark step ; Alignment
---

# Overview
Processing sequence data with Bismark.
This is entirely slurm submission based.

The alignment should be run several times for each dataset.
- [] align against pUC19
- [] align against lambda
- [] align against species genome

## Background info

TBC...


# Set up local environment

```bash
module load anaconda3/login

echo ${PRJDIR}
export CONDA_ENVS_PATH=${PRJDIR}

conda activate python39-env

conda install -c conda-forge \
-c bioconda \
pyfasta \
fastqsplitter

conda deactivate

```


## Data set

### Bismark_Test

```bash

export FILEROOT=Bismark_Test
export REFGENOMEPRFX=NC_010473
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Bismark_test_data/methylseq
# Short name for the ref
export TMPREF=NC_010473

export RAWDATADIR=${CMMNT}/CodeRepos/Bismark/test_files
export RAWDATPRFX=test

```


### Dreissena

```bash

export FILEROOT=Dpol

## Ilifu
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1/methylseq
export REFGENOMEPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic
# Short name for the ref
export TMPREF=Dpol1.0

export RAWDATADIR=/users/rjdaniels/GRPDIR/23_Epigenome/RawData/DeleteOnSRAConfirmation/Dpol_emseq/

export RAWDATPRFX=20220720.A-o287561

```
### Corbicula

```bash
export FILEROOT=Cflu

## Ilifu
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Corbicula_fluminea/Lachesis/methylseq
export REFGENOMEPRFX=Lachesis_assembly_changed
# Short name for the ref
export TMPREF=Lachesis

export RAWDATADIR=/users/rjdaniels/GRPDIR/23_Epigenome/RawData/DeleteOnSRAConfirmation/Cflu_emseq/

export RAWDATPRFX=20220720.A-o287562

```

## Set up

```bash

# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=Bismark_Align_${TMPREF}

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi
# =======

# Raw input data if needed
export RAWINPUTDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_RefIndex/InputData

# File with the names of all the files to run
export ROOTLISTFILE=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming/${FILEROOT}_FileRootList.txt
export FILEROOTLIST=${ROOTLISTFILE}

# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')
 
echo $ARRAYLENGTH
## ARRAYLENGTH=2

# Trimmed data
export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming

# Aligned data
export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Align_${TMPREF}/out_Bismark_Align_${TMPREF}

# Dedup data
export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Dedup_${TMPREF}/out_Bismark_Dedup_${TMPREF}


```


# Requires

## Installation

# Analyses

## Data Prep

## Alignment

### Split data into chunks

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_SplitFastq_240204.sh

export JOBNAME=${TASK}'_SplitFastq'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

# uses one thread per file and two additional threads 
# So we need 4 at most per array idx
# Takes 3 minutes per file


sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:4:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:10GB:g" \
-e "s:ZZSTOREZZ:1GB:g" \
-e "s/ZZWALLTIMEZZ/00:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:10:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${TRIMDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

```


### Get alignment of raw data

For this we temporarily swap to a different  ROOTLISTFILE, one based on the split file list

```bash

# File with the names of all the files to run
export ROOTLISTFILE=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming/${FILEROOT}_FileRootList_splits.txt
export FILEROOTLIST=${ROOTLISTFILE}

rm ${ROOTLISTFILE}

find ${ALIGNDIR} -name 'split[1-9]*_R1_val*.fq.gz' \
 -type f \
  -exec bash -c ' basename -s _R1_val_1.fq.gz "{}" >> ${ROOTLISTFILE}
 ' \;


# This allows us to use job arrays, one per file pair

head ${ROOTLISTFILE} 
wc -l ${ROOTLISTFILE} 

export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')

echo $ARRAYLENGTH
## ARRAYLENGTH=2


```



```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_Alignment_240204.sh

JOBNAME=${TASK}'_Align'
JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:2:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:1GB:g" \
-e "s/ZZWALLTIMEZZ/49:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:10:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_bismark.sif \' >> ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export REFGENOMEDIR=${2}
echo ${REFGENOMEDIR}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${3}
echo ${ALIGNDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${4}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

```

```bash

export RJD_LASTJOBID=$(sbatch \
--parseable \
--dependency=aftercorr:${RJD_LASTJOBID} \
${JOBSCRIPT})

```



## Merge bam files 

Can be done by bismark ... do not do it independently

