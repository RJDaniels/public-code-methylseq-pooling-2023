---
Title : Misc summaries from the work
---

# Overview

This is a revision of the earlier script called 'README_EMseq_Bismark_Rsummary_Local_220908.md'

This work book is used for post-hoc summaries and exporting RDS and publication tables.


# Set up local environment

## Data set

Summaries are done per project, not per dataset.


### Joint Dpol and Cflu Results

```bash
export FILEROOT=Joint_Dpol_Cflu

# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=Bismark_Summaries

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi

export RESRCDIR=${PRJDIR}/../Resources
export RDSDIR=${RESRCDIR}/R_RelationalDB

# There is an R_RelationalDB dir with the RDS data
# There is a softlink in the analysis dir for legacy support R_RelationalDB 

```

## Data Prep

## R setup

```bash
# ILIFU
#sinteractive --x11
sinteractive 
module load  R/RStudio2022.12.0-353-R4.2.2
R

```

## Data Prep

Genome information on the study species.

```R
# ========
# Run Startup scripts
# ========
CMMNT = Sys.getenv(c('CMMNT'))
# Load Env variables
source(paste0(CMMNT,'/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.R'))
# Load general R stuff
# This is now in the Env vars step above
source(paste0(CODEDIR,'/StartupScript_Sources_260220.R'))
# =============


# ============
# For interactive work
# ============
INPUTFILE = Sys.getenv(c('INPUTFILE'))

PRJDIR = Sys.getenv(c('PRJDIR'))

RDSDIR = Sys.getenv(c('RDSDIR'))
RESRCDIR = Sys.getenv(c('RESRCDIR'))
INPUTDIR = Sys.getenv(c('RDSDIR'))
OUTPUTDIR = Sys.getenv(c('OUTPUTDIR'))

FILE.PATTERN.SUFFIX = Sys.getenv(c('FILE.PATTERN.SUFFIX'))

# ==========
pacman::p_load(magrittr, tidyverse, ggplot2, cowplot, xtable, papeR, methylKit, gridExtra, ComplexUpset)

# ============


device2use='svg'
    
#device2use='1'
stopifnot(
{device2use %in% c('jpeg','jpg','png','pdf','svg','eps')})

# ==========
# Load functions
# ==========
source(paste0(CODEDIR,'/Rsourcecode/Functions_EMseq_MethylKit_221124.R'))

source(paste0(CODEDIR,'/Rsourcecode/Functions_DMLUpsetPlots_231107.R'))
# ==========


# ==========
# safety checks
# ==========

stopifnot(
is.character(INPUTFILE),
is.character(INPUTDIR),
is.character(OUTPUTDIR)
)

print(paste('Variables in use are ...' ))
print(paste('CMMNT   is ...',	CMMNT  ))
print(paste('INPUTFILE   is ...',	INPUTFILE  ))
print(paste('INPUTDIR   is ...',	INPUTDIR  ))
print(paste('OUTPUTDIR   is ...',	OUTPUTDIR  ))
print(paste('RDSDIR   is ...',	RDSDIR  ))
print(paste('RESRCDIR   is ...',	RESRCDIR  ))

FILE.PATTERN = paste0(INPUTFILE,'.*',FILE.PATTERN.SUFFIX,'$')
print('FILE.PATTERN.SUFFIX is ...')
print(FILE.PATTERN.SUFFIX)
print('FILE.PATTERN is ...')
print(FILE.PATTERN)
#INPUTDIR = paste0(PRJDIR,"/Data/ProjectInfo/")
RDBASE= RDSDIR


# =========
# Read RDS
# =========
bismark.sum.df = bismark.multiqc.reports.list.db = readRDS(paste0(RDBASE,'/','DB_Bismark_ConversionRates','.RDS'))
seq.pool.db = readRDS(paste0(RDBASE,'/','DB_SequencingInfo_pool','.RDS'))
seq.nonpool.db = readRDS(paste0(RDBASE,'/','DB_SequencingInfo_nonpool','.RDS'))
site.db = readRDS(paste0(RDBASE,'/','DB_SiteInfo','.RDS'))
spp.genome.db = readRDS(paste0(RDBASE,'/','DB_SpeciesGenomeSummary','.RDS'))
multiqc.sum.allstages.db = readRDS(paste0(RDBASE,'/','DB_Bismark_MultiQC','.RDS'))


# ========
# Set some variables
# ========
stage2process=c('alignment.txt','dedup.txt','methextract.txt','general_stats.txt','fastqc.txt','cutadapt.txt','bam2nuc.txt')
# List of treatment alignments to process
align.treatment=c('pUC19','lambda','MethylExt')

```




## Bismark


### ??

```R
align.treatment=c('pUC19','Lambda')

align.treatment

files.vec = list.files(path=PRJDIR, pattern="bismark_summary_report.txt",full.names=T,include.dirs=T,recursive=T)

#files.vec

# Requires some manual subsetting

files.vec %<>% .[1:2]

bismark.sum.conv.df = lapply(align.treatment,function(x){
temp.files.vec=files.vec %>% grep(x,.,value=T)
lapply(files.vec, function(file){
temp.bismsum.df = read.delim(file)
temp.bismsum.df %<>% data.frame(.,ReportFile=file,Treatment=x)
}) %>% bind_rows
})

temp.files.vec = files.vec %>% grep('pUC19',.,value=T,invert=T)  %>% grep('Lambda',.,value=T,invert=T)
temp.files.vec

bismark.sum.dna.df = lapply(files.vec, function(file){
temp.bismsum.df = read.delim(file)
temp.bismsum.df %<>% data.frame(.,ReportFile=file,Treatment='DNA')
}) %>% bind_rows

bismark.sum.df=list(bismark.sum.dna.df,bismark.sum.conv.df) %>% bind_rows()

head(bismark.sum.df )

bismark.sum.df$Sample.Code = bismark.sum.df$File %>% str_split(.,"[_-]") %>% lapply(.,function(item){item[4]}) %>% unlist


# =============
# Save the output
# =============

INPUTFILE='DB_Bismark_ConversionRates'
OUTPUT = paste0(RDBASE,'/',INPUTFILE,'_',TODAY,'.RDS')
CLEANOUTPUT = paste0(RDBASE,'/',INPUTFILE,'.RDS')

# =============
bismark.sum.df %>% saveRDS(OUTPUT)

system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========
bismark.sum.df = readRDS(CLEANOUTPUT)

```


#### Summary tables


```R

outname = paste0(OUTPUTDIR,"/",plot.id,"_SuppTable_STR_","220110")

neat.dat2use = bismark.sum.df   %>% dplyr::select(c("Sample.Code",'Treatment', "Total.Reads", "Aligned.Reads","Unique.Reads..remaining.", "Total.Cs"),contains('ethyl'))

names(neat.dat2use) %<>% gsub("\\."," ",.) %>% gsub("\\s+"," ",.)

neat.dat2use %<>% rename("Unique reads" = "Unique Reads remaining ")
head(neat.dat2use)



%>%
 write_ods(path=paste0(outname,".ods"), sheet = paste0("UpdatedJD_",TODAY), x = .,append=T,update=T)

neat.dat2use %>%
xtable(., type = "latex",label=c("tabs:STRSpecimen"),
         caption=c("Niente"))  %>%
  print(include.rownames=F,
        file = paste0(outname,".tex"))
```


### MultiQC information read in.

```R

## list of multiqc input files saved

files.vec = list.files(
path=PRJDIR, 
pattern="multiqc_",
full.names=T,
include.dirs=T,
recursive=T)

str(files.vec )
head(files.vec )
tail(files.vec )


files.vec %<>%
grep('-rUT',.,value = T) %>%
grep('archive',.,invert = T,value = T) %>%
grep('MethylExt|conversion',.,value = T)


str(files.vec )
head(files.vec )
tail(files.vec )

## List of stages to process
## Because these are all different tables, they cannot be processed in one pipe
## Each needs a separate output

files.vec %>%
grep('general',.,value = T) 


## Read in data from all stages
multiqc.sum.df.allstages = lapply(
stage2process,
function(temp.stage){

## MultiQC for the conversion data
multiqc.sum.conv.df = lapply(
align.treatment,
function(x){

temp.files.vec = files.vec %>%
 grep(x,.,value=T)  %>% 
 grep(temp.stage,.,value=T)

tmp.out = lapply(
temp.files.vec, 
function(file){
temp.bismsum.df = read.delim(file)
temp.bismsum.df %<>% 
data.frame(
.,
ReportFile=file,
Treatment=x,
stage=temp.stage
) %>% 
as_tibble %>%
mutate(
across(.cols = contains('count_genomic'), as.numeric )
) 
})
tmp.out %>% return
}) %>% bind_rows


if(length(multiqc.sum.conv.df) > 0){
## Merge the outputs within each stage
# Remove unpaired
multiqc.sum.conv.df  %<>%
 bind_rows() %>% 
 dplyr::filter( 
 grepl('unpaired', Sample) %>% `!`
 )
 
multiqc.sum.conv.df$Sample.Code = multiqc.sum.conv.df$Sample %>%
sapply(.,
fn.StripTextOf,
c('split[0-9]_' = '')
) %>%
 str_split(.,"[_-]") %>%
  lapply(.,
  function(item){item[4]}
  ) %>% 
  unlist
}else{
multiqc.sum.conv.df = NULL
}

multiqc.sum.conv.df %>% return

})


## Check on output

string2strip = c('.txt' = '')
names(multiqc.sum.df.allstages) = stage2process %>% sapply(., fn.StripTextOf, string2strip)

multiqc.sum.df.allstages %>% str

multiqc.sum.df.allstages %>% names

multiqc.sum.df.allstages[[1]]$Treatment %>% table
multiqc.sum.df.allstages[[1]]$ReportFile

multiqc.sum.df.allstages[[1]]$Sample

multiqc.sum.df.allstages[[1]] %>% names
multiqc.sum.df.allstages[[1]]$ReportFile



# Confirm that sample.codes are correct
multiqc.sum.df.allstages[1:2] %>% 
lapply(.,
function(x){
x$Sample.Code %>% table})


```

### RDS

```R
# ===========
# Save the output
# ===========
INPUTFILE='DB_Bismark_MultiQC'
OUTPUT = paste0(RDBASE,'/',INPUTFILE,'_',TODAY,'.RDS')
CLEANOUTPUT = paste0(RDBASE,'/',INPUTFILE,'.RDS')

# ===========
multiqc.sum.df.allstages %>% saveRDS(OUTPUT)
system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========
multiqc.sum.allstages.db = readRDS(CLEANOUTPUT)
multiqc.sum.df.allstages  = multiqc.sum.allstages.db

```



###    Making the summary of the conversion rates.

```R

multiqc.sum.allstages.db %>% names

data2use = multiqc.sum.allstages.db[["general_stats"]]

string2strip = c(
"_mqc.generalstats."="",
"[Ff]ast[Qq][Cc]"="",
"[Bb]ismark"="",
"^\\."=""
)


names(data2use)  %<>% sapply(., fn.StripTextOf, string2strip)

data2use %>% names
data2use %>% head

data2use %<>% 
filter(Treatment != align.treatment[3])  %>%
dplyr::select(c("Sample.Code",'Treatment', "total_sequences","total_c","C_coverage","percent_cpg_meth","percent_chg_meth","percent_chh_meth"))  %>%
mutate(PollutionPool = Sample.Code %>% gsub("[0-9]","",.))

data2use$Treatment %>% table

data2use   %<>%
mutate(
bad_lambda_cpg = {Treatment == "lambda" & percent_cpg_meth > 1.4},
bad_lambda_cph = {Treatment == "lambda" & percent_cpg_meth > 1.4},
bad_lambda_chh = {Treatment == "lambda" & percent_cpg_meth > 1.4},
bad_puc19_cpg = {Treatment == "pUC19" & percent_cpg_meth < 96},
bad_puc19_cph = {Treatment == "pUC19" & percent_cpg_meth > 5},
bad_puc19_chh = {Treatment == "pUC19" & percent_cpg_meth > 5},
bad_cov = {C_coverage < 10},
bad_sample = {bad_lambda_cpg +bad_lambda_cph +bad_lambda_chh + bad_puc19_cpg +bad_puc19_cph +bad_puc19_chh +bad_cov }> 2
) 

data2use %>% str
data2use$Sample.Code %>% table


# ========
# Summaries with bad samples removed
# ========

neat.data2use.smry = data2use   %>%
filter(bad_sample == F)

neat.data2use.smry %>% head

names(neat.data2use.smry) %<>%
gsub("\\."," ",.) %>%
gsub("\\s+"," ",.) %>%
gsub("[_]"," ",.) %>%
str_to_sentence() %>%
gsub("cpg","CpG",.)  %>%
gsub("chg","CHG",.)  %>%
gsub("chh","CHH",.)  %>%
gsub("meth","",.)  %>%
gsub(" c"," C",.)  %>%
gsub("Percent","%",.)


neat.data2use.smry %<>%
group_by( Pollutionpool, Treatment) %>%
dplyr::summarise(
across(.cols =where(is.numeric),
.fns = list(Mean = ~mean(.x,na.rm = TRUE), SD = ~ sd(.x,na.rm = TRUE)),
.names = "{col} {fn}")) %>%
arrange(Treatment, Pollutionpool)

neat.data2use.smry %>% print(n = 10,  width = Inf)

```


#### Export Tables

```R

## Summary of Conversion rates

outname = paste0(OUTPUTDIR,"/","SupTable_ConversionRates")

# ODS
neat.data2use.smry %>%
 write_ods(
 path = paste0(outname,".ods"),
 sheet = paste0("MultiQC_Summary_postQC_",TODAY),
 x = .,
 append = F,
 update = F)

# Latex

ltx.lab2use = paste0('stab:Results:ConversionRates')

ltx.cap2use = paste0('Conversion rate estimates for both species. ')

neat.data2use.smry %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,".tex"),
include.rownames=F)
 
```
 
###    Conversion rates per sample

 ```R
 
 ## Raw MultiQC per-sample values
neat.dat2use = data2use  %>% filter(!grepl(align.treatment[3],Treatment))

names(neat.dat2use)

names(neat.dat2use) %<>%
gsub("\\."," ",.) %>%
gsub("\\s+"," ",.) %>%
gsub("[_]"," ",.) %>%
str_to_sentence() %>%
gsub("cpg","CpG",.)  %>%
gsub("chg","CHG",.)  %>%
gsub("chh","CHH",.)  %>%
gsub("meth","",.)  %>%
gsub(" c"," C",.)  %>%
gsub("Percent","%",.)

head(neat.dat2use)

outname = paste0(OUTPUTDIR,"/","SupTable_ConversionRates")

neat.dat2use  %>%
dplyr::select(-contains('bad')) %>% 
 write_ods(path = paste0(outname,".ods"),
 sheet = paste0("MultiQC_conversion_bysample_",TODAY),
 x = .,
 append = T,
 update = T)

# Latex

ltx.lab2use = paste0('stab:Results:ConversionRatesXSample')

ltx.cap2use = paste0(
'Conversion rate estimates for both species by sample. '
)

neat.dat2use  %>%
dplyr::select(-contains('bad')) %>% 
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,"_bySample.tex"),
include.rownames=F)
 
```



###    Making the summary of the Alignment.


### With for each report separately

```R
 # No need to read in the RDS files. They are loaded with the startup scripts.

 multiqc.sum.allstages.db %>% names

lapply(multiqc.sum.allstages.db,names)

lapply(multiqc.sum.allstages.db,function(x){x$Sample.Code %>% table})


data2use.list = lapply(multiqc.sum.allstages.db,
 function(x){
 x %>% dplyr::filter(Treatment == align.treatment[3])%>%
mutate(PollutionPool = Sample.Code %>% gsub("[0-9]","",.))
 })



lapply( data2use.list,names)
lapply( data2use.list,head)
lapply( data2use.list,str)
 

# The dfs can be merged as follows:
# fastqc, cutadapt (one entry per sample file name)
# Alignment ... several entries per sample name
#dedup, methextract (one entry per sample file name)
 
# ==========

data2use.list[['fastqc']]%>% .$Sample
data2use.list[['fastqc']]%>% head
data2use.list[['cutadapt']] %>% .$Sample


data2use.list[['fastqc']]%>% dim
data2use.list[['cutadapt']] %>% dim

data2use.trim.list = right_join(data2use.list[['fastqc']], data2use.list[['cutadapt']], join_by(Sample, Treatment, Sample.Code, PollutionPool)) 
 
 # ==========
data2use.list[['alignment']]%>% .$Sample
data2use.list[['bam2nuc']]%>% .$Sample

data2use.list[['bam2nuc']]%>% head
data2use.list[['alignment']]%>% head


data2use.list[['bam2nuc']]%>% names
data2use.list[['alignment']]%>% names

 
 
data2use.align.list = right_join(data2use.list[['bam2nuc']], data2use.list[['alignment']], join_by(Sample, Treatment, Sample.Code, PollutionPool)) 


# ==========
data2use.list[['dedup']]%>% .$Sample
data2use.list[['methextract']] %>% .$Sample

data2use.list[['dedup']]%>% head
data2use.list[['methextract']] %>% head


data2use.list[['dedup']]%>% dim
data2use.list[['methextract']] %>% dim

data2use.dedup.list = right_join(data2use.list[['dedup']], data2use.list[['methextract']], join_by(Sample, Treatment, Sample.Code, PollutionPool)) 

data2use.dedup.list %>% dim

# ==========

# To join the dataframes I need to :
# Summarise the alignment values (and cross check that they match the deduplication estimates)
# Summarise the fastqc to cross check

data2use.trim.list$Sample %>% head
data2use.dedup.list$Sample %>% head
data2use.align.list$Sample %>% head 

# Fastqc + trim
data2use.trim.list %>% head
data2use.trim.list %>% names

data2use.trim.list %<>% 
dplyr::select(
c("Sample",
"Sample.Code",
"PollutionPool",
"Filename",
"Total.Sequences",
"Total.Bases",
"X.GC",
"total_deduplicated_percentage",
"avg_sequence_length",
"median_sequence_length",
"r_processed",
"r_written",
"percent_trimmed")
)

data2use.trim.list %>% head


# Alignment
data2use.align.list %>% head(n=30)
data2use.align.list %>% str

# bam2nuc 
# coverage = sample base count/total of base in genome
# percent = sample base count/total of bases in sample 
# But do not do the count with bam2nuc estimates ... these are inflated due to repeated counts across split runs
# Copy the genomic counts to the summary table and then later use the dedup estimates of total_c to get the coverage

data2use.align.list %>% names

data2use.align.sum.df = data2use.align.list %>%   
mutate(
Sample.split = Sample,
Sample = gsub('split[0-9]+_','',Sample.split)
) %>% 
group_by(Sample) %>% 
dplyr::summarise(
across(.cols = contains(c('percent','count_genomic')), .fns = list(Mean = ~ mean(.x, na.rm = T), S.D. = ~ sd(.x, na.rm = T)), .names = '{.fn}_{.col}'),
across(.cols = ends_with(c('_reads','_alignments','_c')), .fns = list(Total = ~ sum(.x, na.rm = T), S.D. = ~ sd(.x, na.rm = T)), .names = '{.fn}_{.col}'),
across(.cols = starts_with(c('meth','unmeth')), .fns = list(Total = ~ sum(.x, na.rm = T), S.D. = ~ sd(.x, na.rm = T)), .names = '{.fn}_{.col}')
) 


data2use.align.sum.df %>% head
data2use.align.sum.df %>% str


data2use.trim.list %>% dplyr::select(Sample, r_written)
data2use.align.sum.df %>% dplyr::select(Sample, Total_total_reads)
# post trimming read count is approx == total reads used for alignment

# ================

data2use.align.sum.df$Sample %>% head
data2use.dedup.list$Sample %>% head

data2use.dedup.list %<>%   
mutate(
Sample.split = Sample,
Sample = gsub('split[0-9]+_','',Sample.split)
) 


data2use.postprocessing.df = right_join(
data2use.align.sum.df,
data2use.dedup.list,
join_by(Sample)
)


data2use.postprocessing.df %>% dim 
data2use.postprocessing.df %>% head
data2use.postprocessing.df %>% names
data2use.postprocessing.df %>% str

data2use.postprocessing.df %>% print(n=40, width = Inf)


data2use.postprocessing.df %>% 
dplyr::select(Sample,contains('reads')) %>%
print(n=5, width = Inf)

# Test if aligned reads calculated == aligned reads from deduplication

data2use.postprocessing.df %>% 
mutate(diff.aligned = Total_aligned_reads - aligned_reads) %>%
.$diff.aligned

# The differences == discarded reads reported...
data2use.postprocessing.df$Total_discarded_reads
# So we confirm that deduplication reports the same total input as the sum of the separate alignments

# Calculate the C_coverage

data2use.postprocessing.df %<>% 
rowwise() %>%
mutate(C_coverage = total_c/Mean_C_count_genomic)


# ===============

data2use = data2use.postprocessing.df 

seq.nonpool.db %>% str

data2use %<>%
filter(!is.na(total_c )) %>%
left_join(., seq.nonpool.db, join_by(Sample.Code))

data2use %>% names
data2use %>% str

# ============
# some checks

data2use %>%
dplyr::select(contains('meth_')) %>%
print(n=5, width = Inf)

# The alignment summary reports meth and unmeth per file ... some loci are counted multiple times because they are present in multiple files ... this gives inflated estimates for meth and unmeth count values in the alignment...
# Report the deduplication values ... these are unique Cs

data2use %>%
rowwise() %>%
mutate(
diff_meth_cpg = Total_meth_cpg - meth_cpg
) %>%
dplyr::select(contains('meth_')) %>%
print(n=5, width = Inf)


data2use %>% str



# ============
# 
data2use %>% names

# !! The SD values here are NOT what I want in the summary table !!

# "PollutionPool" 'Site.Code' are used to group/order the samples in the table



data2use %<>%
dplyr::select(
c("Species",
"PollutionPool",
'Site.Code',
"Sample.Code",
'Total_total_reads',
#'S.D._total_reads',
#'aligned_reads',
"Mean_percent_aligned",
"S.D._percent_aligned",
"dup_reads_percent",
"total_c",
'C_coverage',
"percent_cpg_meth",
"percent_chg_meth",
"percent_chh_meth")
)


# =============

data2use %>% names
data2use %>% str

names(data2use) %<>%
gsub("\\."," ",.) %>%
gsub("\\s+"," ",.) %>%
gsub("[_]"," ",.) %>%
str_to_sentence() %>%
gsub("cpg","CpG",.)  %>%
gsub("chg","CHG",.)  %>%
gsub("chh","CHH",.)  %>%
gsub("meth","",.)  %>%
gsub(" c"," C",.)  %>%
gsub("Percent","%",.)


data2use$`Sample Code`  %>% table
data2use %>% head(.,n=25)

```


### With general_stats
!!This won't work if you have the alignments split into separate files.!!

```R
 # No need to read in the RDS files. They are loaded with the startup scripts.
multiqc.sum.allstages.db %>% names

lapply(multiqc.sum.allstages.db,names)

lapply(multiqc.sum.allstages.db,function(x){x$Sample.Code %>% table})




multiqc.sum.allstages.db[["general_stats"]] %>% head

data2use = multiqc.sum.allstages.db[["general_stats"]] %>%
filter(Treatment == align.treatment[3])%>%
mutate(PollutionPool = Sample.Code %>% gsub("[0-9]","",.))

data2use %>% 
dplyr::filter(Sample.Code == 'D1P') 



names(data2use) %<>% 
gsub("_mqc.generalstats.","",.) %>% 
gsub("[Ff]ast[Qq][Cc]","",.) %>% 
gsub("[Bb]ismark","",.) %>%
gsub("^\\.","",.)

data2use %>% names
data2use %>% head
data2use %>% str


data2use %>% 
dplyr::select(-ReportFile) %>% 
dplyr::filter(Sample.Code == 'D1P') 


%>% 
tail(n=3)




seq.nonpool.db %>% str
data2use$stage %>% table

data2use %<>%
filter(!is.na(total_c )) %>%
merge(., seq.nonpool.db, by = "Sample.Code", all.x = T, all.y = F)

data2use %>% names
data2use %>% str

data2use %<>%
dplyr::select(c("Species","PollutionPool",'Site.Code',"Sample.Code", "percent_aligned","dup_reads_percent","total_c","C_coverage","percent_cpg_meth","percent_chg_meth","percent_chh_meth"))

data2use %>% names
data2use %>% str

names(data2use) %<>%
gsub("\\."," ",.) %>%
gsub("\\s+"," ",.) %>%
gsub("[_]"," ",.) %>%
str_to_sentence() %>%
gsub("cpg","CpG",.)  %>%
gsub("chg","CHG",.)  %>%
gsub("chh","CHH",.)  %>%
gsub("meth","",.)  %>%
gsub(" c"," C",.)  %>%
gsub("Percent","%",.)


data2use$`Sample Code`  %>% table
data2use %>% head(.,n=25)

```

### Export per-sample info

```R
## Raw MultiQC per-sample values
outname = paste0(OUTPUTDIR,"/","SupTable_Bismark")

neat.dat2use = data2use %>% arrange(Species,`Sample Code`) %>% dplyr::select(-c("Pollutionpool"))

head(neat.dat2use)
str(neat.dat2use)
 
# Reformat large numbers with comma for thousands position
neat.dat2use$`Total C` %<>% formatC(., format="f", big.mark=",", digits=0)
neat.dat2use$`Total total reads` %<>% formatC(., format="f", big.mark=",", digits=0)
 
# ODS
ltx.tit = 'BismarkPerSample'

neat.dat2use %>%
 write_ods(
 path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit,"_",TODAY),
 x = .,
 append = T,
 update = T)

# Latex

ltx.lab2use = paste0('stab:Results:',ltx.tit)
ltx.cap2use = paste0('Per sample data for alignment and methylation call outcomes for both species. ')

neat.dat2use %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,'_',ltx.tit,".tex"),
include.rownames=F)
 
```

### Get summary across good samples
```R
neat.data2use.smry = data2use   %>%
 filter(!`Sample Code` %in% c("D6P","D7P","C2P","C4P","C5NP","C9NP"))

neat.data2use.smry %>% names
neat.data2use.smry$`Sample Code`  %>% table


neat.data2use.smry %<>%
 group_by(Species, Pollutionpool) %>%
dplyr::summarise(
across(.cols =where(is.numeric),
.fns = list(
Mean = ~ mean(.x, na.rm = TRUE),
 SD = ~ sd(.x, na.rm = TRUE)),
.names = "{col} {fn}")
)

neat.data2use.smry %>% head
neat.data2use.smry %>% print(n=25, width = Inf)

neat.data2use.smry$`Total C Mean` %<>% formatC(., format="f", big.mark=",", digits=0)
neat.data2use.smry$`Total C SD` %<>% formatC(., format="f", big.mark=",", digits=0)


```

#### Export Tables

```R

outname = paste0(OUTPUTDIR,"/","SupTable_Bismark")

# ODS
ltx.tit = 'BismarkSummary'

neat.data2use.smry %>%
 write_ods(
 path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit,"_",TODAY),
 x = .,
 append = T,
 update = F)

# Latex

ltx.lab2use = paste0('stab:Results:',ltx.tit)
ltx.cap2use = paste0('Summary of alignment and methylation call data for both species. ')

neat.data2use.smry %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,'_',ltx.tit,".tex"),
include.rownames=F)
 
```

```R

## Testing for significant differences

 neat.data2use.smry = data2use   %>%
 filter(!`Sample Code` %in% c("D6P","D7P","C2P","C4P","C5NP","C9NP"))

 neat.data2use.smry %>% names
 
shapiro.smry.df =  neat.data2use.smry %>%
 dplyr::filter(!grepl('pool',Pollutionpool)) %>%
 group_by(Pollutionpool) %>%
dplyr::summarise(
across(.cols =where(is.numeric),
.fns = list( shapiro = ~ shapiro.test(.x )$p.value),
.names = "{col}_{fn}"))

## Summaries using formula notation
## Test for heteroscedacity

bartlett.smry.df = neat.data2use.smry %>%
dplyr::summarise(across(.cols =where(is.numeric),
.fns = list(
bartlett.p = ~ bartlett.test(formula = .x ~ `Site Code`, na.action = na.exclude)$p.value,
bartlett.stat = ~ bartlett.test(formula = .x ~ `Site Code`, na.action = na.exclude)$statistic,
bartlett.df = ~ bartlett.test(formula = .x ~ `Site Code`, na.action = na.exclude)$parameter
),
.names = "{col}_{fn}"))


```

#### Export Tables


```R
outname = paste0(OUTPUTDIR,"/","SupTable_Bismark")

# =====
# Shapiro
# =====

# ODS
shapiro.smry.df  %>%
 write_ods(
 path = paste0(outname,".ods"),
 sheet = paste0("shapiro_",TODAY),
 x = .,
 append = T,
 update = F)

# Latex

ltx.tit = 'BismarkReadsShapiro'
ltx.lab2use = paste0('stab:Results:')
ltx.cap2use = paste0('Summary of test for normality of alignment and methylation call data for both species. ')

shapiro.smry.df  %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,'_',ltx.tit ,".tex"),
include.rownames=F)

# =====
# Bartlett
# =====
 bartlett.smry.df %>%
 write_ods(
 path = paste0(outname,".ods"),
 sheet = paste0("Bartlett_",TODAY),
 x = .,
 append = T,
 update = F)

ltx.tit = 'BismarkReadsBartlett'
ltx.lab2use = paste0('stab:Results:',ltx.tit)
ltx.cap2use = paste0('Summary of test for heteroscedacity of alignment and methylation call data for both species. ')

 bartlett.smry.df %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,'_',ltx.tit ,".tex"),
include.rownames=F)
 
```

### Anova

```R

tmp.spp.vec = neat.data2use.smry$Species %>% unique

anova.sppxpollpool.list = lapply(
tmp.spp.vec,
function(tmp.spp){

temp.dat2use = neat.data2use.smry %>%
ungroup %>%
dplyr::filter(!grepl('pool',Pollutionpool)) %>%
dplyr::filter(Species == tmp.spp)

y.var = temp.dat2use  %>%
 dplyr::select( where(is.numeric)) %>%
as.matrix

#head(y.var)
#temp.dat2use $`Site Code`

fit.pval = apply(y.var,2,function(x){
mod = aov(x ~ temp.dat2use $Pollutionpool) %>% summary %>% .[[1]]
data.frame(DF = mod %>% .$Df  %>% .[1],
Fval = mod %>% .$`F value`  %>% .[1],
Pval = mod %>% .$`Pr(>F)` %>% .[1]) %>% return()
}) %>%
  do.call('rbind',.) %>%
  as.data.frame

fit.pval  %>% return
  
})

 
         

names(anova.sppxpollpool.list) = tmp.spp.vec

anova.sppxpollpool.df = anova.sppxpollpool.list %>% lapply(., rownames_to_column,'Param') %>% melt %>% 
pivot_wider(id_cols = c('L1','variable') , names_from = c('Param'), values_from = value)


```



#### Export Tables


```R
outname = paste0(OUTPUTDIR,"/","SupTable_Bismark")

# =====
# Shapiro
# =====
ltx.tit = 'BismarkANOVAReads'

# ODS
anova.sppxpollpool.df  %>%
 write_ods(
 path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit,"_",TODAY),
 x = .,
 append = T,
 update = F)

# Latex

ltx.lab2use = paste0('stab:Results:')
ltx.cap2use = paste0('Summary of test differences in alignment results and methylation call data for both species. ')

anova.sppxpollpool.df  %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,'_',ltx.tit ,".tex"),
include.rownames=F)
 
```

### HSD by trait, Remove the outlier in DNP

__!! THIS HAS NOT BEEN UPDATED YET :: 240504 !!__


### Dpol treatment  tests
```R
temp.dat2use = neat.data2use.smry %>%
ungroup %>%
filter(Species != "Corbicula fluminea") %>%
 filter(`Total sequences` < 90000000)


y.var = temp.dat2use  %>%
 dplyr::select( where(is.numeric)) %>%
as.matrix

head(y.var)
temp.dat2use $`Site Code`
temp.dat2use %>% dim

fit.pval = apply(y.var,2,function(x){
mod = aov(x ~ temp.dat2use $`Site Code`) %>% summary %>% .[[1]]
data.frame(DF = mod %>% .$Df  %>% .[1],
Fval = mod %>% .$`F value`  %>% .[1],
Pval = mod %>% .$`Pr(>F)` %>% .[1]) %>% return()
}) %>%
  do.call('rbind',.) %>%
  as.data.frame

fit.pval

fit.pval %>%
  write_ods(path = paste0(outname,".ods"),
 sheet = paste0(" Anova_Dpol_DNPoutlierRemoved_",TODAY),
 row_names = T,
 x = .,
 append = T,
 update = T)

```




## MethylKit
### Summary of the variant counts 

```R

## list of input files saved

files.vec.tmp = list.files(
path=paste0(PRJDIR),
pattern = ".*NumberVars_*", # only the unfiltered loci counts
full.names=T,
include.dirs=T,
recursive=T) %>%
grep( 'archive',.,value = T, invert= T)

files.vec.tmp

## Exclude certain files
# For this , we will run tile and non tile separately
files.vec = files.vec.tmp %>%
grep( 'Euler',.,value = T, invert= T) %>% # Old results
grep( '_diff_',.,value = T, invert= T) %>% # exclude post post filter
grep( '_all_',.,value = T, invert= T) %>%  # exclude the counts for uniting all data
grep( 'methdiff',.,value = T, invert= T) %>% # exclude diff meth 
grep( 'Density',.,value = T, invert= T) %>%  # exclude density counts
grep( 'united',.,value = T, invert= T) # exclude post united, file paths differ ...
#grep( 'tile',.,value = T, invert= T) %>%
#grep( 'clustering',.,value = T, invert= T) # exclude post filter, pre cluster (redundant I think)

files.vec %<>% sort

methylkit.numvars.df =  lapply(files.vec, function(file){
temp.df = read.delim(file, sep = "", row.names = NULL)
temp.df %<>% data.frame(.,ReportFile=file)
}) %>% bind_rows


head(methylkit.numvars.df )
tail(methylkit.numvars.df )
methylkit.numvars.df  %>% str

# Need to edit the ReportFile for Dpoly so that the chr do not get split when parsing the file  paths

methylkit.numvars.df$ReportFile %<>% gsub('out_NC_','out_NC',.)

# Define parameters for pulling path info
path.params =  list(
 '8' = c('species' = 1,
'DATASRCIDX1' = 2),
  '9' = c('mincov' = 3),
  '11' = c('task' = 1),
 '12' = c('chr' = 2)
 )


methylkit.numvars.qualifiers.df = methylkit.numvars.df$ReportFile %>%
lapply(.,
fn.InfoFromPath,
 params2use = path.params
 ) %>% bind_rows

methylkit.numvars.qualifiers.df %>% str
methylkit.numvars.df %>% str

methylkit.numvars.qualifiers.df %>% head
methylkit.numvars.df$ReportFile %>% head

methylkit.numvars.qualifiers.df %>% tail
methylkit.numvars.df$ReportFile %>% tail

 # Now change the Report file and chr names back
methylkit.numvars.df$ReportFile %<>% gsub('out_NC0','out_NC_0',.)
methylkit.numvars.qualifiers.df$chr %<>% gsub('NC0','NC_0',.)

methylkit.numvars.df %<>% cbind(methylkit.numvars.qualifiers.df,.)

methylkit.numvars.df  %>% str
methylkit.numvars.df  %>% summary

methylkit.numvars.df$stage %>% table

methylkit.numvars.df  %>% 
names

methylkit.numvars.df  %>% 
head

# ====
# some checks
# ====

methylkit.numvars.df$n.vars %>% summary

methylkit.numvars.df %>%
dplyr::filter(is.na(n.vars))

methylkit.numvars.df %>%
distinct %>%
str

methylkit.numvars.df %>%
dplyr::select(species,task,chr) %>% 
table

# ===
# fill in missing Pool and Treatment values
# ===

methylkit.numvars.df  %<>%
mutate(
Pool = case_when(
grepl('pool',Sample.Code) ~ T,
!grepl('pool',Sample.Code) ~ F
),
Treatment = gsub('[0-9-]','',Sample.Code) %>% gsub('[CD]','',.)
)

methylkit.numvars.df  %>%
filter(Sample.Code == '19-CpoolP' & chr == 'LG01') 


```

#### Export tables

```R
# =========
# RDS
# =========

INPUTFILE='DB_CompiledPostMethylkit'
OUTPUT = paste0(RDBASE,'/',INPUTFILE,'_NumberLoci','.RDS')
CLEANOUTPUT = paste0(RDBASE,'/',INPUTFILE,'.RDS')

# ===========
methylkit.numvars.df %>% saveRDS(OUTPUT)
system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========
methylkit.numvars.db = readRDS(CLEANOUTPUT)


# ========
# ods
# ========

# ===============

# Create entry for sample totals

methylkit.numvars.db %>% names
methylkit.numvars.db %>% str

methylkit.numvars.db %<>% 
ungroup %>%
mutate( grouping = 'ind') %>%
dplyr::select(-ReportFile,-Context,-mincov,-res)

methylkit.numvars.db.totals = methylkit.numvars.db %>% 
dplyr::summarise(
.by = -contains(c('chr','n.vars')),
across(
.cols = n.vars,
.fns = list(total = ~ sum( .x ,na.rm = T) ),
.names = "{.col}"
)) %>%
mutate(grouping = 'Total') 

str(methylkit.numvars.db.totals)

methylkit.numvars.db.totals %>%
dplyr::select(species,Sample.Code,task) %>%
table

methylkit.numvars.db %<>% bind_rows(methylkit.numvars.db.totals,.)

str(methylkit.numvars.db)

# Now get mean sd per chr (not sample)

rm('methylkit.numvars.db.mean')

methylkit.numvars.db.mean =   methylkit.numvars.db %>% 
ungroup %>%
dplyr::summarise(
.by = -contains(c('Sample.Code', 'n.vars')),
across(
.col = n.vars ,
.fns = list(mean = ~ mean( .x ,na.rm = T), sd = ~ sd( .x ,na.rm = T) ),
.names = '{.col}_{.fn}'
)) %>%
pivot_longer(
cols = contains('n.vars'),
values_to = 'n.vars', 
names_to = c('metric'), 
names_pattern = "n.vars_(.*)"
)

str(methylkit.numvars.db.mean)

## ACROSS USES .FN AND .COL FOR THE .NAMES OPTION
  
methylkit.numvars.db %<>% 
bind_rows(methylkit.numvars.db.mean,.) 

str(methylkit.numvars.db)

# ========
# Now add in % loss
# ========

methylkit.numvars.db %>% 
str

methylkit.numvars.db %<>% 
pivot_wider(
values_from = n.vars,
names_from = c(task,stage),
names_sep = '_',
) 

methylkit.numvars.db %<>% 
rowwise %>%
mutate(perc.site.loss = {RawSummary_prefilter -RawSummary_postfilter}/RawSummary_prefilter ) %>%
ungroup

# ========
methylkit.numvars.db %>% 
print(n=5,Inf)

# Format the table
# Remove columns with no data
dat2export = methylkit.numvars.db %>% 
dplyr::select(-c(DATASRCIDX1,RawSummary_postNorm)) %>%
rowwise %>%
mutate(
remove = case_when(
all(is.na(RawSummary_postfilter),
is.na(RawSummary_prefilter),
is.na(Tile_postfilter),
is.na(perc.site.loss)) ~ T,
.default = F
)) %>%
dplyr::filter(remove != T) %>%
arrange(species, Treatment, Pool, chr, grouping, metric) %>%
dplyr::relocate(species, Treatment, Pool, Sample.Code, chr, RawSummary_prefilter, RawSummary_postfilter) %>%
dplyr::relocate(c(grouping, metric) ,.after = last_col())  

dat2export  %>%
print(n=15, width = Inf)

names(dat2export) %<>%
gsub("perc.site.loss","% loss",.) %>%
gsub("RawSummary_postfilter","After filter",.) %>%
gsub("RawSummary_prefilter","Before filter",.) %>%
gsub("Tile_postfilter","Regions formed",.) %>%
gsub("\\."," ",.) %>%
gsub("\\s+"," ",.) %>%
gsub("[_]"," ",.) %>%
str_to_sentence() 

dat2export  %>% head
dat2export  %>% tail

# All data in big table

ltx.tit = paste0('VariantsXFilteringXChr')
plottit2use = paste0(ltx.tit)

outname = paste0(OUTPUTDIR,"/","Table_MethylKit")

dat2export  %>%
 write_ods(path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit),
 x = .,
 append = F,
 update = F)

 
 # Only by sample, no chr

ltx.tit = paste0('VariantsXFilteringXSample')
plottit2use = paste0(ltx.tit)

outname = paste0(OUTPUTDIR,"/","Table_MethylKit")

dat2export  %>%
dplyr::filter(Grouping != 'ind') %>%
dplyr::select(-Chr, -Grouping, -Metric) %>%
 write_ods(path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit),
 x = .,
 append = T,
 update = F)
 
# ========
 # latex
# ========
# Big table with everything

ltx.tit = paste0('VariantsXFilteringXChr')
plottit2use = paste0(ltx.tit)

outname = paste0(OUTPUTDIR,"/","Table_MethylKit_",plottit2use)

ltx.lab2use = paste0('stab:Results:',ltx.tit)

ltx.cap2use = paste0('\\small 
{\\bf Summary of data loss throughout QC.} Presented are the mean ($\\pm$ S.D.) number of CpG sites before and after applying a filter for 10x minimum coverage, as well as the amount of data lost as a percentage. The final column indicates the number of regions formed from the available sites.')

# Format the numbers
# Sprintf does not do commas ... I couldn't find any way to use it for this
# ONLY FOR COLS WITH MEAN,SD

dat2export %>% str

dat2export %<>%
mutate(
across(
.cols = c(`Before filter`,`After filter`,`Regions formed`),
 ~ formatC(.x, big.mark=",", digits = 0 , format = 'f')
))  %>%
mutate(
`% Loss` =  sprintf('%.2f',`% Loss`)
)  



tab2export = dat2export  %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use)

tab2export %>%
print(.,
file = paste0(outname,".tex"),
hline.after=c(-1, 0),
tabular.environment = "longtable",
floating = F, caption.placement = "top",
include.rownames=F,
size = " \\small")


# Only by sample, no chr

ltx.tit = paste0('VariantsXFilteringXSample')
plottit2use = paste0(ltx.tit)

outname = paste0(OUTPUTDIR,"/","Table_MethylKit_",plottit2use)


tab2export = dat2export %>%
dplyr::filter(Grouping != 'ind') %>%
dplyr::select(-Chr, -Grouping, -Metric) %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use)

tab2export %>%
print(.,
file = paste0(outname,".tex"),
hline.after=c(-1, 0),
tabular.environment = "longtable", floating = F, caption.placement = "top",
include.rownames=F,
size = " \\small")

# SUMMARY FOR PUBLICATION

ltx.tit = paste0('VariantsXFilteringSummary')
plottit2use = paste0(ltx.tit)
outname = paste0(OUTPUTDIR,"/","Table_MethylKit_",plottit2use)


str(dat2export)

tab2export =   dat2export %>%
dplyr::filter(Grouping != 'ind' & is.na(Chr)) %>%
dplyr::filter(is.na(`Sample code`) ) %>%
dplyr::select(-Chr, -Grouping, -`Sample code`) %>%
pivot_wider(
names_from = c(Metric),
names_sep = "_",
values_from = contains(c('filter','loss','formed'))
) %>%
mutate(
Pool = case_when(
Pool == F ~ 'N',
Pool == T ~ 'Y'
)) %>%
dplyr::select( -`Regions formed_mean`, -`Regions formed_sd`) %>%
arrange(Species,Pool,Treatment) %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use
)

tab2export %>%
print(.,
file = paste0(outname,".tex"),
include.rownames=F,
size = " \\small",
hline.after=c(-1, 0),
tabular.environment = "longtable", floating = F, caption.placement = "top",
)

```


### Summary of the Diff meth

#### Diff meth nvars
```R

## list of multiqc input files saved
files.vec = list.files(path=PRJDIR,
pattern = "NumberVars",
full.names=T,
include.dirs=T,
recursive=T) %>%
grep( 'archive',.,value = T, invert= T)

# files.vec 

## Exclude certain files
# For this , we will run tile and non tile separately
files.vec %<>%
grep( 'Euler',.,value = T, invert= T) %>% # Old results
grep( 'poolAll_',.,value = T, invert= T) %>%  # exclude the counts for uniting all data 
grep( 'Density',.,value = T, invert= T) %>% # site density output exclude
grep( 'DiffMeth',.,value = T, invert= T) %>%  # the counts pre-unite exclude
grep( 'Clustering',.,value = T, invert= F) %>% # post unite output only !!
grep( '-rUT',.,value = T, invert= F) %>% # keep a specific set of runs
grep( 'tileT',.,value = T, invert= F)  # keep only tiled ... for now


files.vec %>% sort

methylkit.diffmeth.nvars =  lapply(files.vec, function(file){
temp.df = read.delim(file, sep = "", row.names = NULL)
temp.df %<>% data.frame(.,ReportFile=file)
}) %>% bind_rows


methylkit.diffmeth.nvars$ReportFile %<>% gsub('out_NC_','out_NC',.)


head(methylkit.diffmeth.nvars )
methylkit.diffmeth.nvars  %>% str


# Define parameters for pulling path info
path.params =  list(
 '8' = c('species' = 1,
'DATASRCIDX1' = 2),
'9' = c('mincov' = 3,
'pollution' = 4,
'pool' = 5,
'tile' = 6,
'minpergrp' = 7),
  '11' = c('task' = 1),
  '12' = c('chr' = 2)
 )


methylkit.numvars.qualifiers.df = methylkit.diffmeth.nvars$ReportFile %>%
lapply(.,
fn.InfoFromPath,
 params2use = path.params
 ) %>% bind_rows

 methylkit.diffmeth.nvars$ReportFile %<>% gsub('NC0','NC_0',.)
 methylkit.numvars.qualifiers.df$chr %<>% gsub('NC0','NC_0',.)

 
 methylkit.diffmeth.nvars.df  = cbind(methylkit.numvars.qualifiers.df, methylkit.diffmeth.nvars)

 str(methylkit.diffmeth.nvars.df)
 head(methylkit.diffmeth.nvars.df)
 tail(methylkit.diffmeth.nvars.df)

# Run some checks on numbers..
  
 methylkit.diffmeth.nvars.df %>%
 dplyr::select(species,chr) %>%
 table
 
 methylkit.diffmeth.nvars.df %>%
 dplyr::select(species,tile) %>%
 table

  methylkit.diffmeth.nvars.df %>%
 dplyr::select(species,stage,res) %>%
 table


```

##### Export tables

```R

# =========
# RDS
# =========

INPUTFILE='DB_CompiledPostMethylkit_PostUnion'
OUTPUT = paste0(RDBASE,'/',INPUTFILE,'_NumberLoci','.RDS')
CLEANOUTPUT = paste0(RDBASE,'/',INPUTFILE,'.RDS')

# ===========
methylkit.diffmeth.nvars.df %>% saveRDS(OUTPUT)
system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========
methylkit.diffmeth.nvars.df = readRDS(CLEANOUTPUT)

```


#### Diff meth values

```R

## list of multiqc input files saved
files.vec = list.files(path=PRJDIR,
pattern = "NumberVars",
full.names=T,
include.dirs=T,
recursive=T) %>%
grep( 'archive',.,value = T, invert= T)

# files.vec 


## Exclude certain files
# For this , we will run tile and non tile separately
files.vec %<>%
grep( 'Euler',.,value = T, invert= T) %>% # Old results
grep( 'poolAll_',.,value = T, invert= T) %>%  # exclude the counts for uniting all data 
grep( 'Density',.,value = T, invert= T) %>% # site density output exclude
grep( 'clustering',.,value = T, invert= T) %>% # post unite output exclude
grep( 'DiffMeth',.,value = T, invert= F) %>%  # the counts pre-unite include
grep( '-rUT',.,value = T, invert= F) %>% # keep a specific set of runs
grep( 'tileT',.,value = T, invert= F)  # keep only tiled ... for now


files.vec %>% sort

methylkitdiffmeth.df =  lapply(files.vec, function(file){
temp.df = read.delim(file, sep = "", row.names = NULL)
temp.df %<>% data.frame(.,ReportFile=file)
}) %>% bind_rows


head(methylkitdiffmeth.df )
methylkitdiffmeth.df  %>% str


# Define parameters for pulling path info
path.params =  list(
 '8' = c('species' = 1,
'DATASRCIDX1' = 2),
'9' = c('mincov' = 3,
'pollution' = 4,
'pool' = 5,
'tile' = 6,
'minpergrp' = 7),
  '11' = c('task' = 1)
 )


methylkitdiffmeth.qualifiers.df = methylkitdiffmeth.df$ReportFile %>%
lapply(.,
fn.InfoFromPath,
 params2use = path.params
 ) %>% bind_rows

 methylkit.diffmeth.df  = cbind(methylkitdiffmeth.qualifiers.df, methylkitdiffmeth.df)

 str(methylkit.diffmeth.df)
 head(methylkit.diffmeth.df)
 tail(methylkit.diffmeth.df)

```



```R


## Merge with nvars df 
# exporting rds


# CHR don't match for Dpol .. need to add back the NC_

methylkit.diffmeth.df$chr %>% unique
methylkit.diffmeth.nvars.df$chr %>% unique


names(methylkit.diffmeth.df)
names(methylkit.diffmeth.nvars.df )

str(methylkit.diffmeth.df)
str(methylkit.diffmeth.nvars.df )

head(methylkit.diffmeth.df)
head(methylkit.diffmeth.nvars.df )

dim(methylkit.diffmeth.df)
dim(methylkit.diffmeth.nvars.df )

methylkit.diffmeth.nvars.join = full_join(
methylkit.diffmeth.df,
methylkit.diffmeth.nvars.df ,
by = join_by(
species, DATASRCIDX1,chr,
mincov,
pollution,
pool,
tile,
minpergrp
)) 


str(methylkit.diffmeth.nvars.join )

dim(methylkit.diffmeth.nvars.join )
dim(methylkit.diffmeth.df)
dim(methylkit.diffmeth.nvars.df )


head(methylkit.diffmeth.nvars.join )
tail(methylkit.diffmeth.nvars.join )

methylkit.diffmeth.nvars.join  %>% str

# Run some checks on numbers..
  
 methylkit.diffmeth.nvars.join %>%
 dplyr::select(species,chr, pool) %>%
 table
 
 methylkit.diffmeth.nvars.join %>%
 dplyr::select(species,tile,pool) %>%
 table

  methylkit.diffmeth.nvars.join %>%
 dplyr::select(species,stage,res,pool) %>%
 table

 ```


```R
## Add analysis info
# This does not need to be a separate function 
# use the dplyr case_when

methylkit.diffmeth.nvars.join  %<>%
mutate(
tile.verb = case_when(
grepl('tileT',tile) ~ 'DMR',
grepl('tileF',tile) ~ 'DMS',
.default = 'DMS'
),
poll.verb = case_when(
grepl('pollAll',pollution) ~ 'Across pollution condition',
grepl('pollT',pollution) ~ 'Pollution samples only',
grepl('pollF',pollution) ~ 'Non-pollution samples only',
.default = NA
),
pool.verb = case_when(
grepl('poolT',pool) ~ 'Wet-lab pooling, individually sequenced',
grepl('poolF',pool) ~ 'Individual pools, individually sequenced',
grepl('poolComp',pool) ~ 'Individually sequenced, computational pooling',
.default = NA
),
stats.verb = case_when(
grepl('pool[TC]',pool) ~ "Fisher's Exact Test",
grepl('poolF',pool) ~ 'Logistic regression',
.default = NA
)
) 


# if(FALSE){
# # Old process with the function
# tmp.SampleInfo.diffmeth.nvars = methylkit.diffmeth.nvars.df$task %>%
# lapply(., fn.Verbose.AnalysisInfo)  %>%
# bind_rows
# 
# tmp.SampleInfo.diffmeth.nvars %>% head
# 
# methylkit.diffmeth.nvars.df %<>% cbind(tmp.SampleInfo.diffmeth.nvars, .)
# }


methylkit.diffmeth.nvars.join %>% names
methylkit.diffmeth.nvars.join %>% str

```

##### Export tables
```R

# =========
# RDS
# =========
INPUTFILE='DB_CompiledPostMethylkit_PostUnion'
OUTPUT = paste0(RDBASE,'/',INPUTFILE,'_DiffMethSummary','.RDS')
CLEANOUTPUT = paste0(RDBASE,'/',INPUTFILE,'.RDS')

# ===========
methylkit.diffmeth.nvars.join %>% saveRDS(OUTPUT)
system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========
methylkit.diffmeth.nvars.join = readRDS(CLEANOUTPUT)

```


A quick tidy up after reading in RDS
```R
neat.data2use.smry.nvars =  methylkit.diffmeth.nvars.join  %>%
dplyr::select(-contains(c("ReportFile",'task','.of.')), -stage, -res, -cutoff)  %>%
distinct() %>%
dplyr::relocate(contains('verb'), species, n.vars, Context, everything()) %>%
arrange(tile.verb, poll.verb, species) 

str(neat.data2use.smry.nvars)
head(neat.data2use.smry.nvars)
names(neat.data2use.smry.nvars)



```

Add in some values from the pre-united nvars

```R


# read in from here... but  you need to run through the whole process of summarising the df 

methylkit.numvars.db = readRDS(paste0(RDBASE,'/','DB_CompiledPostMethylkit.RDS'))

# I need just the tile info, average across samples


methylkit.numvars.db %>% str
methylkit.numvars.db %>% head
methylkit.numvars.db %>% names

methylkit.numvars.db$Sample.Code %>% unique

# This starts from after the calculation of perc.site.loss

methylkit.numvars.db %<>% 
dplyr::filter(is.na(Sample.Code))  %>%
dplyr::filter(!is.na(chr)) %>%
pivot_wider(
names_from = c(metric),
names_sep = "_",
values_from = contains(c('filter','loss','norm'))
) %>%
dplyr::select( -Sample.Code) 


methylkit.numvars.db %>%
print(n=3, width = Inf)

methylkit.numvars.db %>%
str

# I need summaries to correspond to the groups in the diff meth table

# Summary for pollAll

meth.nvars.sumry = methylkit.numvars.db %>%
ungroup %>%
group_by(species,DATASRCIDX1,Pool) %>%
dplyr::summarise(across(
.cols = where(is.numeric )  & contains('mean') & contains('_p'),
.fns = list(total = ~ sum(.x, na.rm =T ))
)) %>%
mutate(
pollution = 'pollAll',
pool = case_when(
Pool == F ~ 'poolF',
Pool == T ~ 'poolT',
.default = NA
)) 

# %>%
# print(n=13, width = Inf)

# Now create summary of the union results

neat.data2use.smry.nvars %>% str
meth.nvars.sumry %>% str

neat.data2use.smry.nvars %>% names


# Duplicate the unpooled row to join with Comp Pool
tmp = meth.nvars.sumry %>% 
dplyr::filter(Pool == F) %>%
mutate(pool = 'poolComp')

meth.nvars.sumry %<>% bind_rows(tmp,.)

#group_by(species,DATASRCIDX1,pool,pollution,minpergrp,mincov) %>%


meth.union.smry = neat.data2use.smry.nvars %>% 
ungroup %>%
dplyr::summarise(
.by = c(species,DATASRCIDX1,pool,pollution,minpergrp,mincov, contains('verb')),
across(
.cols = n.vars ,
.fns = list(total = ~ sum(.x, na.rm =T ))
)) %>%
full_join(.,meth.nvars.sumry) %>%
dplyr::select(-contains('RawSummary')) %>%
mutate(
tiles.diff = Tile_postfilter_mean_total - n.vars_total,
tiles.diff.perc = tiles.diff/Tile_postfilter_mean_total
) 


```

##### Export Tables

```R
# ========

meth.union.smry %>% str

meth.union.smry %<>% 
dplyr::select(-pollution, -mincov, -DATASRCIDX1, -Pool, -pool , -poll.verb) %>%
relocate(species, contains('verb'),minpergrp,Tile_postfilter_mean_total, n.vars_total, tiles.diff, tiles.diff.perc)


# Now make a tidy version of the df
names(meth.union.smry) %<>%
gsub("pool.verb","Pooling",.) %>%
gsub("stats.verb","Statistical test",.) %>%
gsub("minpergrp","Overlap",.) %>%
gsub("_total","",.) %>%
gsub("_mean","",.) %>%
gsub("_postfilter","",.) %>%
gsub("tiles.diff","loss",.) %>%
gsub("loss.perc","perc.loss",.) %>%
gsub("Tile","Before unite",.) %>%
gsub("n.vars","After unite",.) %>%
gsub("perc","%",.) %>%
str_to_sentence() %>%
gsub("\\."," ",.) %>%
gsub("\\s+"," ",.) %>%
gsub("[_]"," ",.) 

meth.union.smry 
meth.union.smry %>% head


# All data in big table

dat2export  = meth.union.smry

ltx.tit = paste0('DiffMethUnionLoss')
plottit2use = paste0(ltx.tit)
outname = paste0(OUTPUTDIR,"/","Table_MethylKit")

dat2export  %>%
 write_ods(path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit),
 x = .,
 append = T,
 update = F)

# ========
 # latex
# ========

ltx.tit = paste0('DiffMethUnionLoss')
plottit2use = paste0(ltx.tit)
outname = paste0(OUTPUTDIR,"/","Table_MethylKit_",plottit2use)

ltx.lab2use = paste0('stab:Results:',ltx.tit)

ltx.cap2use = paste0('\\small 
{\\bf Summary of data loss through uniting samples.} Presented are the mean number of CpG regions per contrast and the final number after uniting across samples in the contrast.')

# Format the numbers
# Sprintf does not do commas ... I couldn't find any way to use it for this
# ONLY FOR COLS WITH MEAN,SD


dat2export  = meth.union.smry

dat2export %<>%
mutate(
across(
.cols = c(`Before unite`,`After unite`,`Loss`),
 ~ formatC(.x, big.mark=",", digits = 0 , format = 'f')
))  %>%
mutate(
`% Loss` =  sprintf('%.2f',`% Loss`)
)  



tab2export = dat2export  %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use)

tab2export %>%
print(.,
file = paste0(outname,".tex"),
hline.after=c(-1, 0),
tabular.environment = "longtable",
floating = F, caption.placement = "top",
include.rownames=F,
size = " \\small")

```


#### Now get the diff meth data

```R
# =========
# Read RDS
# =========
methylkit.diffmeth.db = readRDS(paste0(RDBASE,'/','DB_CompiledPostMethylkit_PostUnion','.RDS'))

```


The code above already prepares the diff meth data.
```INACTIVE

## list of multiqc input files saved
files.vec = list.files(path=INPUTDIR,
pattern = "methdiffperchr",
full.names=T,
include.dirs=T,
recursive=T) %>%
grep( 'archive',.,value = T, invert= T)

## Exclude certain files
files.vec %<>%
#grep( 'Cflu',.,value = T, invert= T) %>%
grep( '_diff_',.,value = T, invert= F) %>%
grep( '_all_',.,value = T, invert= T) %>%  # exclude the counts for uniting all data
grep( 'vV0y0-DIq12',.,value = T, invert= F) %>%
#grep( 'vV0y0-dEE2h-gdo0v',.,value = T, invert= F) %>%
grep( 'MethylKit_[CD]+',.,value = T, invert= T) %>%
grep( 'Density',.,value = T, invert= T) %>%
grep( 'clustering',.,value = T, invert= T)
#grep( 'diff',.,value = T, invert= T)

files.vec %>% sort
files.vec[[1]]

lapply(files.vec, function(file){
temp.df = read.delim(file, sep = "", row.names = NULL)
temp.df %>% dim
temp.df %>% names
})



## Merge dfs
methylkit.diffmeth.df =  lapply(files.vec, 
function(file){
temp.df = read.delim(file, sep = "", row.names = NULL)
if(dim(temp.df)[1]>0){
temp.df %<>% data.frame(. , ReportFile=file)
}else{

temp.df[nrow(temp.df)+1,] = NA
temp.df %<>% data.frame(. , ReportFile=file)
}
}) %>% bind_rows


head(methylkit.diffmeth.df )

## Pull info
methylkit.diffmeth.qualifiers.df = methylkit.diffmeth.df$ReportFile %>%
fn.Info4rmCol(col2use = ., spp.no=9, task.no=10)
#fn.Info4rmCol(col2use = ., spp.no=8, task.no=9)

methylkit.diffmeth.qualifiers.df %>% str
methylkit.diffmeth.df  %>% str

methylkit.diffmeth.df %<>% cbind(methylkit.diffmeth.qualifiers.df,.)
```


```R

## Quick tidy up

methylkit.diffmeth.db %<>% 
as_tibble %>%
dplyr::select(-contains(c("ReportFile",'task')),  -res, -n.vars, -stage, -DATASRCIDX1, -Context)  %>%
distinct() %>%
dplyr::relocate(species, everything()) 



%>%
print(n=13, width = Inf)



str(methylkit.diffmeth.db)
head(methylkit.diffmeth.db)
names(methylkit.diffmeth.db)


## Have a look at the output

methylkit.diffmeth.db  %>% summary

methylkit.diffmeth.db %>%
dplyr::select(species,chr,pool) %>%
table

methylkit.diffmeth.db %>%
dplyr::select(species,chr,cutoff) %>%
table


# ## Add analysis info
# 
# tmp.SampleInfo.diffmeth = methylkit.diffmeth.db$task %>%
# lapply(., fn.Verbose.AnalysisInfo)  %>%
# bind_rows
# 
# # tmp.SampleInfo.diffmeth %>% head
# 
# str(methylkit.diffmeth.db )
# str(tmp.SampleInfo.diffmeth )
# 
# methylkit.diffmeth.db %<>% cbind(tmp.SampleInfo.diffmeth , .)
# 
# 
# str(methylkit.diffmeth.db )
# distinct(methylkit.diffmeth.db ) %>% str

```

##### Export 

```R
methylkit.diffmeth.db  %>% str


neat.data2use.smry.diffmeth = methylkit.diffmeth.db  %>%
pivot_wider(
names_from = c('cutoff'),
values_from = contains("methyl")) %>%
dplyr::select(-pollution, -mincov , -poll.verb, -tile, -contains('verb') ) %>%
relocate(species, pool, chr) %>%
arrange(species, pool, chr) %>%
dplyr::select(-contains('_NA'))
 

neat.data2use.smry.diffmeth %>%
print(n=3, width = Inf)


names(neat.data2use.smry.diffmeth ) %<>%
gsub("\\."," ",.) %>%
gsub("\\s+"," ",.) %>%
gsub("[_]"," ",.) %>%
gsub(" of "," ",.) %>%
gsub("n.vars","No. sites",.) %>%
gsub("number","No.",.) %>%
gsub("cutoff","% diff. cutoff ",.) %>%
gsub("methylated","meth.",.) %>%
str_to_sentence() %>%
gsub("cpg","CpG",.)  %>%
gsub("chg","CHG",.)  %>%
gsub("chh","CHH",.)  %>%
gsub("Percentage","%",.)


# All data in big table

dat2export  = neat.data2use.smry.diffmeth

ltx.tit = paste0('DiffMethLongTab')
plottit2use = paste0(ltx.tit)
outname = paste0(OUTPUTDIR,"/","Table_MethylKit")

dat2export  %>%
 write_ods(path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit),
 x = .,
 append = T,
 update = F)

# ========
 # latex
# ========

ltx.tit = paste0('DiffMethLongTab')
plottit2use = paste0(ltx.tit)
outname = paste0(OUTPUTDIR,"/","Table_MethylKit_",plottit2use)

ltx.lab2use = paste0('stab:Results:',ltx.tit)

ltx.cap2use = paste0('\\small 
{\\bf Details of detected differential methylation.} Presented are the number of regions per contrast and -percentage of total regions detected for differential methylation. Minimum level of difference in methylation was varied from 10 to 25 %.')

# Format the numbers
# Sprintf does not do commas ... I couldn't find any way to use it for this
# ONLY FOR COLS WITH MEAN,SD

dat2export %<>%
mutate(
across(
.cols = contains('No.'),
 ~ formatC(.x, big.mark=",", digits = 0 , format = 'f')
),
across(
.cols = contains('%'),
 ~ sprintf('%.2f', .x)
)) 


tab2export = dat2export  %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use)

tab2export %>%
print(.,
file = paste0(outname,".tex"),
hline.after=c(-1, 0),
tabular.environment = "longtable",
floating = F, caption.placement = "top",
include.rownames=F,
size = " \\small")


```

```INACTIVE
# Find duplicated entries if needed 

if(1==2){
head(neat.data2use.smry.diffmeth)
distinct(neat.data2use.smry.diffmeth) %>% str
neat.data2use.smry.diffmeth %>% dplyr::select(ReportFile,chr,cutoff) %>% table
}


## Reduce the nvars df to unique entries

neat.data2use.smry.nvars %>% str

# Manually remove treatment groups
#neat.data2use.smry.nvars %<>% .[,-c(3,17) ]

neat.data2use.smry.nvars %<>% dplyr::select(-c(Pool, Treatment, Sample.Code))

neat.data2use.smry.nvars  %<>% distinct


head(neat.data2use.smry.diffmeth)
head(neat.data2use.smry.nvars)

names(neat.data2use.smry.nvars)
names(neat.data2use.smry.diffmeth)

dim(neat.data2use.smry.diffmeth)
dim(neat.data2use.smry.nvars)

data2use.merge = merge(
neat.data2use.smry.nvars,
neat.data2use.smry.diffmeth,
all.y = T)

#, by=c( "Datasrcidx all", "Resolution","Spp","Treatment info","Sample handling","Statistical test","Task","File")

data2use.merge %>% str
data2use.merge %>% dim
data2use.merge %>% head
data2use.merge %>% names

neat.data2use.smry = data2use.merge




## Now add in the number of vars
# names(neat.data2use.smry)
# names(neat.data2use.smry.nvars)
# head(neat.data2use.smry)
# head(neat.data2use.smry.nvars)
# merge(neat.data2use.smry, neat.data2use.smry.nvars,
# by=c('spp'), all.x = T) %>% head
##

neat.data2use.smry %>% head


OUTPUTDIR = paste0(INPUTDIR,"/out_",task.id)
system(paste0("mkdir -p ", OUTPUTDIR))

outname = paste0(OUTPUTDIR,"/","SupTab_MethylKit")

# outname

neat.data2use.smry %>%
 write_ods(path = paste0(outname,".ods"),
 sheet = paste0("DiffMeth_",TODAY),
 x = .,
 append = T,
 update = F)


```


### Table of Loci overlap between tests

```R

## INPUT/OUTPUT
pacman::p_load(methylKit)

## list of multiqc input files saved
files.vec = list.files(
paste0(PRJDIR),
pattern = "methylDiff_.*txt.bgz$",
full.names=T,
include.dirs=T,
recursive=T) %>%
grep( 'archive',.,value = T, invert= T)

files.vec 


## Exclude certain files
# For this , we will run tile and non tile separately
files.vec %<>%
grep( 'Euler',.,value = T, invert= T) %>%
grep( 'SubsetDiff10',.,value = T, invert= T) %>%
grep( 'MethylLandscape',.,value = T, invert= T) %>%
grep( '-rUT',.,value = T, invert= F) %>%
grep( '.tbi',.,value = T, invert= T) 

myobjDB=lapply(files.vec, 
readMethylDB)

str(myobjDB)


sitesxfile.list = lapply(
myobjDB, 
function(tmp.db){
getData(tmp.db) %>%
mutate(hyper = {meth.diff > 0}, site.code = paste(chr,start,end,sep='_'))
})


str(sitesxfile.list)

names(sitesxfile.list) = files.vec

sitesxfile.df  = sitesxfile.list %>%
bind_rows(.id = 'file')

# Define parameters for pulling path info
path.params =  list(
 '8' = c('species' = 1,
'DATASRCIDX1' = 2),
'9' = c('mincov' = 3,
'pollution' = 4,
'pool' = 5,
'tile' = 6,
'minpergrp' = 7)
 )



sitesxfile.qualifiers.df = files.vec %>%
lapply(.,
fn.InfoFromPath,
 params2use = path.params
 ) %>% bind_rows


files.vec  %<>% cbind(., sitesxfile.qualifiers.df)

tmp.idx = match(sitesxfile.df$file, files.vec[,1])

sitesxfile.df  %<>% cbind(files.vec[tmp.idx,-1], .)

str(sitesxfile.df)

```

```R
# =========
# RDS
# =========
INPUTFILE='DB_DiffMethUpSetOverlap'
OUTPUT = paste0(RDBASE,'/',INPUTFILE,'_Fulllist','.RDS')
CLEANOUTPUT = paste0(RDBASE,'/',INPUTFILE,'.RDS')

# ===========
sitesxfile.df %>% saveRDS(OUTPUT)
system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========
sitesxfile.db = readRDS(CLEANOUTPUT)

```



### Intersections

#### Load data
```R
# =========
# RDS
# =========
sitesxfile.db = readRDS(paste0(RDBASE,'/','DB_DiffMethUpSetOverlap','.RDS'))

```


#### Data Prep

##### All Loci
```R
plottitextra2use = 'AllRegions'


# filter for p and q values

sitesxfile.db %>% names
sitesxfile.db %>% str

max(sitesxfile.db$qvalue)

sitesxfile.db %<>% 
as_tibble %>%
dplyr::select(-file, -pvalue, -DATASRCIDX1, -strand) %>%
# filter( qvalue < 0.01 ) %>%
# filter( abs(meth.diff) > 9.99 ) %>%
mutate(
resolution = case_when(
tile == 'tileT' ~ 'DMR',
tile == 'tileF' ~ 'DMS'
))

sitesxfile.db$qvalue %>% summary
sitesxfile.db$meth.diff %>% summary

sitesxfile.db %>% str



```
##### All DMRs 

```R

plottitextra2use = 'Qvalue1e-3'

# filter for p and q values

sitesxfile.db %>% names
sitesxfile.db %>% str

max(sitesxfile.db$qvalue)

sitesxfile.db %<>% 
as_tibble %>%
dplyr::select(-file, -pvalue, -DATASRCIDX1, -strand) %>%
filter( qvalue < 0.01 ) %>%
filter( abs(meth.diff) > 9.99 ) %>%
mutate(
resolution = case_when(
tile == 'tileT' ~ 'DMR',
tile == 'tileF' ~ 'DMS'
))

sitesxfile.db$qvalue %>% summary
sitesxfile.db$meth.diff %>% summary

sitesxfile.db %>% str



```

#### Make Complex Upset
SKIP... 
use ggupset instead

```R
## Loop over cutoff, hypo/hyper, log, contig/LG 

cutoff.vec = c(10)
#cutoff.vec = c(20,25)
log.vec = c('linr')
hyp.vec = c('hypermethylated','hypomethylated') 
#chr.vec = c('LG','Contig')  # For Cflu
chr.vec = c('LG')  # For Cflu
#chr.vec = c('CM','JAI')  # For Dpol
#res.vec = c('DMS','DMR') 
res.vec = c('DMR') 
#cov.vec = c('10x','5x')
cov.vec = c('10')


loop.df = merge(data.frame(cutoff.vec),data.frame(log.vec )) %>%
merge(.,data.frame(hyp.vec)) %>% 
merge(.,data.frame(chr.vec)) %>% 
merge(.,data.frame(res.vec)) %>% 
merge(.,data.frame(cov.vec)) %>% 
#merge(.,data.frame(stat.vec)) %>% 
arrange(cutoff.vec)

#loop.idx = 1

lapply(
1:dim(loop.df)[1], 
function(loop.idx){

cutoff2use = loop.df[loop.idx,'cutoff.vec']
log2use = loop.df[loop.idx,'log.vec']
hyp2use = loop.df[loop.idx,'hyp.vec']
chr2use = loop.df[loop.idx,'chr.vec']
res2use = loop.df[loop.idx,'res.vec']
cov2use = loop.df[loop.idx,'cov.vec']
#stat2use = loop.df[loop.idx,'stat.vec']

if(
hyp2use == 'hypermethylated'
){
dat2plot = sitesxfile.db %>%
filter( hyp == T )
}else{
dat2plot = sitesxfile.db %>%
filter( hyp != T )
}

dat2plot %<>% 
filter(grepl(eval(cov2use),mincov)) %>% 
#filter(grepl('minovrlap1',File)) %>% 
filter(grepl(eval(chr2use),chr)) %>% 
filter(abs(meth.diff) > cutoff2use ) %>% 
filter( resolution == res2use ) 

if(
dim(dat2plot)[1] < 1
){
print('skipping this iteration, empty df...')
return(NULL)
}else{


sitesxfile.upsetlist = dat2plot %>% 
group_split(pool, .keep = F)

names(sitesxfile.upsetlist) = dat2plot$pool %>% unique 

if(FALSE){

# using the original upsetR
names(sitesxfile.upsetlist)

system(paste0("mkdir -p ", OUTPUTDIR))
outname = paste0(OUTPUTDIR,"/Plot_SitexFile_overlap_res",res2use,'_',chr2use,"_",hyp2use,"_",log2use,"_cut",cutoff2use)


pdf(paste0(outname,".pdf"))
UpSetR::upset(
UpSetR::fromList(
lapply(
sitesxfile.upsetlist, function(x)x$site.code)
),
order.by = "freq",
sets = c(
"poolT",
"poolF",
"poolComp"),
nsets = 3,
number.angles = 90
)

dev.off()

p1 = UpSetR::upset(UpSetR::fromList(lapply(sitesxfile.upsetlist, function(x)x$site.code)),
order.by = "freq",
sets = c("10x_pollution_nonpool_diff_nonpseudopool",
"10x_pollution_nonpool_diff_pseudopool",   
"10x_pollution_pool_diff_nonpseudopool",   
"10x_pool_nonpol_diff_pseudopool",         
"10x_pool_pol_diff_pseudopool"),
nsets = 5,
number.angles = 90,
#group.by = 'sets',
point.size = 3.5,
line.size = 2, 
mainbar.y.label = "Sites in common", sets.x.label = "Sites per test", 
empty.intersections = "on",
  keep.order = TRUE)

  
  }

  
  
if(FALSE){

movies = as.data.frame(ggplot2movies::movies)
genres = colnames(movies)[18:24]
genres
movies[genres] = movies[genres] == 1
movies[movies$mpaa == '', 'mpaa'] = NA
movies = na.omit(movies)

upset(movies, genres, name='genre', width_ratio=0.1)

names(movies)
class(movies)
str(movies)
attributes(movies)

tmp = movies
tmp = movies[,1:24]
str(tmp)
str(movies)


ComplexUpset::upset(movies, genres)
ComplexUpset::upset(tmp, genres)


ComplexUpset::upset(tmp, genres, name='genre', width_ratio=0.1)

}


sitesxfile.upset.df = UpSetR::fromList(
lapply(
sitesxfile.upsetlist, function(x)x$site.code)
)

# ComplexUpset 
# requires a matrix of intersection
# create using upsetR

sitesxfile.upset.df %>% str
sitesxfile.upset.df %>% dim
task = colnames(sitesxfile.upset.df)

# sitesxfile.upset.df %>% apply(
# .,
# c(1,2),
# as.logical) %>% str

p1 =  ComplexUpset::upset(
sitesxfile.upset.df,
task,
name='Intersections',
min_size=0, 
keep_empty_groups=TRUE,
width_ratio=0.1,
sort_sets=FALSE)  + ggtitle(paste0(res2use,'_',chr2use,"_",hyp2use,"_",log2use,"_cut",cutoff2use))


# Plot the graph

system(paste0("mkdir -p ", OUTPUTDIR))
outname = paste0(OUTPUTDIR,"/Plot_SitexFile_overlap_res",res2use,'_',chr2use,"_",hyp2use,"_",log2use,"_cut",cutoff2use)

# allowed width for figures: 80,	112,	169mm

ggsave(p1,
filename = paste0(outname,".",device2use),
height = 120,
width = 169,
units = "mm",
dpi = 300,
device = device2use)

}
})



```



This was done above already...
```INACTIVE

## list of multiqc input files saved
files.vec = list.files(path=INPUTDIR,
pattern = "txt.bgz",
full.names=T,
include.dirs=T,
recursive=T) %>%
grep( 'archive',.,value = T, invert= T)

# files.vec 


## Exclude certain files
# For this , we will run tile and non tile separately
files.vec %<>%
grep( spp2use,.,value = T, invert= F) %>%
grep( 'vV0y0-DIq12',.,value = T, invert= F) %>%
#grep( 'vV0y0-dEE2h-gdo0v',.,value = T, invert= F) %>%
grep( '.tbi',.,value = T, invert= T) %>%
grep( 'MethylKit_[CD]+',.,value = T, invert= T) %>%
grep( 'methylDiff_',.,value = T, invert= F)


files.vec  %>% str
#files.vec %<>% .[[1]]

myobjDB=lapply(files.vec, methylKit::readMethylDB)

myobjDB %>% length
myobjDB %>% str

myobjDB_bk = myobjDB 

# This crashes
# Probably due to data size


sitesxfile.list = lapply(
myobjDB, 
function(tmp.db){

tmp.diff10.db =  tryCatch(
        
          suppressWarnings({
          getMethylDiff(tmp.db ,
difference =10,
qvalue = 0.5,
type = "all",
chunk.size = 1e+06,
save.db = T,
dbdir = paste0('.'),
suffix = paste0('SubsetDiff10_',TODAY)
)
# UNFORTUNATELY ANYTHING CLOSE TO Q OF 0.01 GIVES ERRORS FOR EMPTY METHDIFF OBJECTS
}),
        error=function(cond) {
        message('error occurred')
            return(NULL)
        },
        warning=function(cond) {
        message('warning occurred')
        return(NA)
        }
    )    

str(tmp.diff10.db )
    
if(
!is.null(tmp.diff10.db )
){
tmp.diff10.db  %<>% getData() %>%
mutate(hyp = {meth.diff > 0}, site.code = paste(chr,start,end,sep='_'))
}

tmp.diff10.db %>% return 
})


length(sitesxfile.list)
str(sitesxfile.list)

names(sitesxfile.list) = files.vec

sitesxfile.list %>% saveRDS(paste0(RDBASE,'/DB_',task.id,'_spp_',spp2use,'_diffmeth_sitesxfile_',TODAY,'.RDS'))

# ===============================


```


#### Create ggplots

```r
# ========
'Confirm that the distribution of values is correct...' %>% print

sitesxfile.db %>% str 

sitesxfile.db %>% 
group_by(hyper,tile,species,pool) %>%  
dplyr::summarise(n = n()) %>% 
arrange(hyper) %>%
as.data.frame %>% print

'Confirm that the distribution of values is correct...' %>% print
sitesxfile.db %>% 
group_by(pool,hyper,resolution) %>%  
dplyr::summarise(n = n())%>% 
arrange(hyper, resolution) %>%
as.data.frame %>% print


sitesxfile.db  %>% str
sitesxfile.db  %>% names

sitesxfile.db$pool %>% table %>% print
sitesxfile.db$resolution %>% table %>% print
sitesxfile.db$species %>% table %>% print
sitesxfile.db$chr %>% table %>% print
sitesxfile.db$hyper %>% table %>% print
sitesxfile.db$mincov %>% table %>% print

# get summary of the number of sites per res-species-pool-minpergrp

'Confirm that the distribution of values is correct...' %>% print
sitesxfile.db %>% 
group_by(pool , resolution, hyper, mincov, minpergrp) %>%  
dplyr::summarise(count = tally())%>% 
print

'Confirm that the distribution of values is correct...' %>% print

sitesxfile.db %>% 
group_by(pool,mincov,minpergrp,hyper,resolution) %>%  
dplyr::summarise(n = n())%>% 
as.data.frame

```


#### Create parameter df
 Loop over cutoff, hypo/hyper, log, contig/LG 

```R

# ========
# Define the levels here
# ========
# Task is used for some of the functions below ...
# recreate it and then set the factor levels for it
# task needs to include the mincov value to avoid double counting sites for different mincov runs


sitesxfile.db %<>% 
rowwise %>%
mutate(
mincov = gsub('mincov','',mincov),
minpergrp = gsub('minpergrp','',minpergrp),
task = paste0(pool,'_',minpergrp)
) %>%
ungroup 

sitesxfile.db$task %>% factor


sitesxfile.db$task %<>% factor(., levels = c(
'poolF_0.75',
'poolT_0.75',
'poolComp_0'
))

sitesxfile.db %>% str


#spp2use = c('Cflu','Dpol') # use a wild card to match anything
# Don't use spp ... it is redundant with chr

chr.vec = c('LG','NC') 

#cutoff.vec = c(10,15,20,25)
cutoff.vec = c(10,15,20,25)
#hyper.vec = c('hypermethylated','hypomethylated') 
hyp.vec = c('hyp-all')  # Must have hyp in the name
log.vec = c('linr')
#res.vec = c('DMS','DMR') 
res.vec = c('DMR') 
#cov.vec = c('10x','5x')
cov.vec = c('10')


# hyp vec  and the cutoff not needed with the ggupset package
loop.df = merge(data.frame(chr.vec),data.frame(log.vec )) %>%
merge(.,data.frame(hyp.vec)) %>% 
merge(.,data.frame(cutoff.vec)) %>% 
merge(.,data.frame(res.vec)) %>% 
merge(.,data.frame(cov.vec)) 
#merge(.,data.frame(stat.vec)) %>% 
#arrange(cutoff.vec)

```


#### Create Upset Plots

- count of loci from each set in each intersection (so each set totals 100%)
- count of loci form each intersection that each set represents (so each intersection totals 100%)
- prop of loci from each set in each intersection (so each set totals 100%)
- prop of loci form each intersection that each set represents (so each intersection totals 100%)
- as above, split by genomic context
- split by hypo/hyper ( separte plots)


```r

# ==============
# Remove the pool tests
# Not needed in latest run
if(FALSE){
dat2plot = sitesxfile.db %>%
filter( treatment.str != 'pool' ) %>%
filter({ treatment.subset == "nonpool" & minpergrp == "1" } %>% `!` )
}

# ==============
# seems I need to run this one manually, running each step in the function one-by-one

plot.list = lapply(
1:dim(loop.df)[1],
fn.mkplots.upset,
data.df = sitesxfile.db,
param.df = loop.df,
plot.tit.extra = plottitextra2use
)


names(plot.list)
length(plot.list)
str(plot.list)
loop.df

p1 = plot.list[[1]]
p2 = plot.list[[2]]
p_leg1 = fn.g.legend(p1)
p_leg2 = fn.g.legend(p2)

p.final = ggdraw() + 
draw_plot(p1 + theme(legend.position = 'none'), x = 0, y = 0, width = 0.5, height = 1 ) +
draw_plot(p2 + theme(legend.position = 'none'), x = 0.5 , y = 0, width = 0.5, height = 1 ) +
draw_grob(p_leg1  + theme(legend.position = 'bottom', legend.direction = 'horizontal'), x = 0.35 , y = 0.75, width = 0.25, height = 0.25 )
draw_grob(p_leg2  + theme(legend.position = 'bottom', legend.direction = 'horizontal'), x = 0.85 , y = 0.75, width = 0.25, height = 0.25 )

'Export image...' %>% print

outname = paste0(OUTPUTDIR, "/Plot_SitexFile_overlap_jntspp_Composite",'_',plottitextra2use)
print(outname  )


ggsave(p.final ,
filename = paste0(outname, '.',device2use),
height = 90,
width = 170,
units = "mm",
dpi = 300,
device = device2use)
  
'done exporting...' %>% print


```

