---
Title : README File for  Initiation of the project
---

# Overview

This is a  guide to setting up a new project using my scripts.

~~The first half of this file has code that you should run each time you start working on a task on the  HPC.~~
The first half is an example of code that will be included at the beginning of the WORKBOOKS. These lines of code are used to load a bunch of variables when you start a new task. 

The second section of the file explains how to set up your working directory the first time you start the project.

# Set up environment

The code here should be common to all tasks in the project. This is an example template to copy to the beginning of the workbook for each task..
Works that look like this ```<WORD>``` are placeholders. You should replace these with what you need for your project.

Set project vars.

```bash

# Set the project name
export PRJNAME=<PROJECTNAME>
#export PRJNAME=23_Epigenome

# Set the input file.
# This is usually a common character string for the input data
export FILEROOT=<FILEPREFIX>
#export FILEROOT=TEST

# The main folder/activity to use in 
export TASK=<SOFTWARE/TASK/STEP/STAGE>
#export TASK=SETUP

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[  -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo '	ERR: project.init not found!'
fi

```

## Running an interactive HPC session
This was written for the Rackham HPC uppsala. 

```bash
# Start on interactive node

interactive -A ${PRJACC}

source ${CMMNT}/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.sh

# Start singularity instance if needed
singularity shell  ${CMMNT}/containers/PopGenomics_1.1_220522.sif
#  source /home/rdaniels/CodeRepos/xxxx
```



# Initial Prep

This is the stuff you should do at the beginning of the project to do the initial set up. 
There are two parts:
- Setting up on the platform you are using (the HPC, laptop or computer you will use)
- Setting up for the project you are working on

## Platform set up

For each platform you use, you need to add some commands to the ~/.bashrc file so that some variables are loaded each time you login.

You need to run these commands from the computer/platform you will use.
First set the common mount point. This is the base directory for all folders in your project.

```bash

# This is the base directory for all your projects
# It will change for each platform you use (your computer, Euler, Rackham). You need to decide what to use.
# Your home directory is usually a good idea.

#export CMMNT=/cluster/home/${USER} # euler
export CMMNT=${HOME} # ilifu
#export CMMNT=/home/${USER}/msRNA_Mobile

```


Download the script for the setup.
```bash

if ! [[  -d  ${CMMNT}/Platform_Specifics ]] ; then
cd ${CMMNT}
git clone git@gitlab.com:RJDaniels/hpc_platform_specifics.git
mv ${CMMNT}/hpc_platform_specifics ${CMMNT}/Platform_Specifics
# Create symbolic link to the previous name for this dir ... some older code may still look for this dir
ln -s ${CMMNT}/Platform_Specifics ${CMMNT}/231117_Platform_Specifics
else
cd ${CMMNT}
echo 'Platform_Specifics dir already exists...'
fi

# Create symbolic link to the Euler file
# If you are using another platform, you should not use Euler... use the one for that platform
ln -s ${CMMNT}/Platform_Specifics/Script_Platform_VariableMap_Euler.sh ${CMMNT}/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.sh

# ln -s ${CMMNT}/Platform_Specifics/Script_Platform_VariableMap_ILIFU.sh ${CMMNT}/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.sh

#ln -s ${CMMNT}/Platform_Specifics/Script_Platform_VariableMap_Laptop.sh ${CMMNT}/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.sh

```

Once you have downloaded the ${CMMNT}/Platform_Specifics/Script_Platform_VariableMap_*.sh  file, you need to edit it. Inside the file are comments on how to change things.



## CodeRepo

Create a copy of the CodeRepo. This is where most of the code is stored and edited.

```bash

if ! [[  -d  ${CMMNT}/CodeRepos/hpc-workflows ]] ; then
OLDDIR=$PWD
mkdir -p ${CMMNT}/CodeRepos
cd ${CMMNT}/CodeRepos
git clone git@gitlab.com:RJDaniels/hpc-workflows.git
cd $OLDDIR
else
echo 'CodeRepo dir already exists...'
fi
 
```


## Project initiation


Once the platform has been prepped, you can prep the project folder.

This needs to be done for each new project.

Create project folder.
Set project vars.

```bash

# Set the project name
export PRJNAME=<PROJECTNAME>
#export PRJNAME=23_Epigenome

```

When using zfs file system.
If you don't know what zfs is, you can just go to ext4 below.

```bash
sudo zfs create  ExtHome/msRNA/Projects/${PRJNAME}

sudo zfs create  ExtHome/msRNA/Projects/Platform_Scripts

cd ${CMMNT}/Projects/${PRJNAME}

mkdir -p HPC/Reports HPC/Analyses Admin Resources Drafts
```


When on ext4 system.

```bash

# Project directory
mkdir -p ${CMMNT}/Projects/${PRJNAME}

cd ${CMMNT}/Projects/${PRJNAME}

# Create folders inside the project
mkdir -p HPC/Reports HPC/Analyses Admin Resources Drafts

```

Now we set up a project-specific script with some commonly used variables.



```bash

# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.

if [[  -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'Cannot find the project.init script! Copying from the CODEDIR...But you need to check this file and set appropriate values'
cp ${CMMNT}/CodeRepos/hpc-workflows/ProjectInitDir/project.init ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
fi

```

It is a good idea to have a look at this file to make sure you agree with all the values that have been set.

```bash

cat ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
nano ${CMMNT}/Projects/${PRJNAME}/HPC/project.init

# Test if everything seems ok...
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init

```


