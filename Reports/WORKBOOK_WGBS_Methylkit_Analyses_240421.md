---
Title : Methylkit analyses of united data
---

# Overview

This workbook is a launch point for a series of scripts.

# Bookkeeping

# Requires
## Installation

# Set up local environment

You need to run within a sing container.

# Data set
## Dreissena

```bash
export FILEROOT=Dpol

export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1/methylseq
export REFGENOMEPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic

export REFFASTA=${REFGENOMEDIR}/${REFGENOMEPRFX}.fa

# Shorter Ref name
export TMPREF=Dpol1.0

```

### Dreissena 10x pollAll poolAll TileF 0.75

-[] Coverage profile
-[] Methylation profile
-[x] Clustering
-[x] Correlations
-[x] Merge all chr
-[x] PCA
-[SKIP] Diff meth
-[SKIP] Power analysis


```bash
# =======
# Methylkit variables
# =======
export POLL=All
export POOL=All
export TILE=F
export EXCLUDEREGEX='unpaired'
export MINCOVFILTER='10'
export MINOVRLAP='0.75'
#export STEPSIZE=''
#export WINDOWSIZE=''
export SAMPLE_EXCLUDE_VEC='"NONE"'

```

### Dreissena 10x pollAll poolAll Tile 0.75

-[] Coverage profile
-[] Methylation profile
-[x] Clustering
-[x] Correlations
-[x] Merge all chr
-[x] PCA
-[SKIP] Diff meth
-[SKIP] Power analysis


```bash
# =======
# Methylkit variables
# =======
export POLL=All
export POOL=All
export TILE=T
export EXCLUDEREGEX='unpaired'
export MINOVRLAP='0.75'
export SAMPLE_EXCLUDE_VEC='"NONE"'
export MINCOVFILTER='10'
export STEPSIZE='1000'
export WINDOWSIZE='1000'

```

### Dreissena 10x pollAll poolT Tile 0.75

-[] Coverage profile
-[] Methylation profile
-[x] Clustering
-[SKIP] Correlations
-[x] Merge all chr
-[SKIP] PCA
-[x] Diff meth
-[x] Power analysis


```bash
# =======
# Methylkit variables
# =======
export POLL=All
export POOL=T
export TILE=T
export EXCLUDEREGEX='unpaired'
export MINOVRLAP='0.75'
export SAMPLE_EXCLUDE_VEC='"[0-9][N]*P"'
export MINCOVFILTER='10'
export STEPSIZE='1000'
export WINDOWSIZE='1000'

```
### Dreissena 10x pollAll poolF Tile 0.75

-[] Coverage profile
-[] Methylation profile
-[x] Clustering
-[SKIP] Correlations
-[x] Merge all chr
-[SKIP] PCA
-[x] Diff meth
-[x] Power analysis
-[] Context


```bash
# =======
# Methylkit variables
# =======
export POLL=All
export POOL=F
export TILE=T
export EXCLUDEREGEX='unpaired'
export MINOVRLAP='0.75'
export SAMPLE_EXCLUDE_VEC='"pool"'
export MINCOVFILTER='10'
export STEPSIZE='1000'
export WINDOWSIZE='1000'

```
### Dreissena 10x pollAll poolComp Tile 0.00

-[] Coverage profile
-[] Methylation profile
-[x] Clustering
-[SKIP] Correlations
-[x] Comp Pooling
-[x] Merge all chr
-[SKIP] PCA
-[x] Diff meth
-[x] Power analysis


```bash
# =======
# Methylkit variables
# =======
export POLL=All
export POOL=Comp
export TILE=T
export EXCLUDEREGEX='unpaired'
export MINOVRLAP='0'
export SAMPLE_EXCLUDE_VEC='"pool"'
export MINCOVFILTER='10'
export STEPSIZE='1000'
export WINDOWSIZE='1000'

```


## Corbicula

```bash
export FILEROOT=Cflu

## Ilifu
#export REFGENOMEDIR=${CMMNT}/Projects/23_Epigenome/Resources/reference_genomes/
export REFGENOMEDIR=${GRPDIR}/23_Epigenome/Resources/reference_genomes/Corbicula_fluminea/Lachesis/methylseq
export REFGENOMEPRFX=Lachesis_assembly_changed
# Shorter Ref name
export TMPREF=Lachesis

```


### Corbicula 10x pollAll poolAll TileF 0.75

-[] Coverage profile
-[] Methylation profile
-[x] Clustering
-[x] Correlations
-[x] Merge all chr
-[x] PCA
-[SKIP] Diff meth
-[SKIP] Power analysis


```bash
# =======
# Methylkit variables
# =======

export POLL=All
export POOL=All
export TILE=F
export EXCLUDEREGEX='unpaired'
export MINCOVFILTER='10'
export MINOVRLAP='0.75'
#STEPSIZE=''
#WINDOWSIZE=''
export SAMPLE_EXCLUDE_VEC='"NONE"'

```
### Corbicula 10x pollAll poolAll Tile 0.75

-[] Coverage profile
-[] Methylation profile
-[x] Clustering
-[x] Correlations
-[x] Merge all chr
-[x] PCA
-[SKIP] Diff meth
-[SKIP] Power analysis

```bash
# =======
# Methylkit variables
# =======

export POLL=All
export POOL=All
export TILE=T
export EXCLUDEREGEX='unpaired'
export MINOVRLAP='0.75'
export MINCOVFILTER='10'
export SAMPLE_EXCLUDE_VEC='"NONE"'
export STEPSIZE='1000'
export WINDOWSIZE='1000'

```

### Corbicula 10x pollAll poolT Tile 0.75

-[] Coverage profile
-[] Methylation profile
-[x] Clustering
-[SKIP] Correlations
-[x] Merge all chr
-[SKIP] PCA
-[x] Diff meth
-[x] Power analysis

```bash
# =======
# Methylkit variables
# =======

export POLL=All
export POOL=T
export TILE=T
export EXCLUDEREGEX='unpaired'
export MINOVRLAP='0.75'
export MINCOVFILTER='10'
export SAMPLE_EXCLUDE_VEC='"[0-9][N]*P"'
export STEPSIZE='1000'
export WINDOWSIZE='1000'

```

### Corbicula 10x pollAll poolF Tile 0.75

-[] Coverage profile
-[] Methylation profile
-[x] Clustering
-[SKIP] Correlations
-[x] Merge all chr
-[SKIP] PCA
-[x] Diff meth
-[x] Power analysis


```bash
# =======
# Methylkit variables
# =======

export POLL=All
export POOL=F
export TILE=T
export EXCLUDEREGEX='unpaired'
export MINOVRLAP='0.75'
export MINCOVFILTER='10'
export SAMPLE_EXCLUDE_VEC='"pool"'
export STEPSIZE='1000'
export WINDOWSIZE='1000'

```

### Corbicula 10x pollAll poolComp Tile 0.00

-[] Coverage profile
-[] Methylation profile
-[x] Clustering
-[x] Comp Pooling
-[SKIP] Correlations
-[x] Merge all chr
-[SKIP] PCA
-[x] Diff meth
-[x] Power analysis


```bash
# =======
# Methylkit variables
# =======

export POLL=All
export POOL=Comp
export TILE=T
export EXCLUDEREGEX='unpaired'
export MINOVRLAP='0'
export MINCOVFILTER='10'
export SAMPLE_EXCLUDE_VEC='"pool"'
export STEPSIZE='1000'
export WINDOWSIZE='1000'

```

# Set up
```bash

# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=MethylKit_${TMPREF}_mincov${MINCOVFILTER}_poll${POLL}_pool${POOL}_tile${TILE}_minpergrp${MINOVRLAP}

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi
# =======

# =======
# File with the names of all the files to run
# =======
export ROOTLISTFILEPRFX=${FILEROOT}_ContigsSize1e07
export ROOTLISTFILE=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/MethylKit_${TMPREF}_mincov${MINCOVFILTER}/${ROOTLISTFILEPRFX}.tsv
export FILEROOTLIST=${ROOTLISTFILE}

cat ${FILEROOTLIST}

# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')
echo $ARRAYLENGTH
## ARRAYLENGTH=2
# =======

# =======
# DIRECTORIES
# =======

export RESRCDIR=${PRJDIR}/../Resources
export RDSDIR=${RESRCDIR}/R_RelationalDB
RDBASE=${RDSDIR}

# Trimmed data
export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming
#export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming

# Aligned data
export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Align_${TMPREF}/out_Bismark_Align_${TMPREF}
#export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_alignment/out_Bismark_alignment

# Dedup data
export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Dedup_${TMPREF}/out_Bismark_Dedup_${TMPREF}
#export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_deduplication/out_Bismark_deduplication

# Methyl call
export METHDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_MethylExt_${TMPREF}/out_Bismark_MethylExt_${TMPREF}

# BSSnper dir
export BSSNPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/BSSnper_${TMPREF}/out_BSSnper_${TMPREF}

# Variant Calling dir
export SNPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/VariantCalling_${TMPREF}/out_VariantCalling_${TMPREF}

# Methylkit preprocessing dir
export METHKITDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/MethylKit_${TMPREF}_mincov${MINCOVFILTER}/out_MethylKit_${TMPREF}_mincov${MINCOVFILTER}


```

# Data Prep

# Common Code

## Setup R
For general (on the fly)  use  further down.

```bash
#srun -x11 --pty --time=05:00:00 --mem=16GB bash
sinteractive 
module load  R/RStudio2022.12.0-353-R4.2.2
R

```


```R
#! /bin/R

# ========
# Run Startup scripts
# ========
CMMNT = Sys.getenv(c('CMMNT'))

# Load Env variables
source(paste0(CMMNT,'/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.R'))

# Load general R stuff
# This is now in the Env vars step above
source(paste0(CODEDIR,'/StartupScript_Sources_260220.R'))

# =============


# ============

if(FALSE){
args = commandArgs(TRUE)
INPUTDIR = args[1]
OUTPUTDIR = args[2]
EIGENPRFX = args[3]
GENOMAP = args[4]
PHENOMAP = args[5]
}



if(TRUE){
OUTPUTDIR = Sys.getenv(c('OUTPUTDIR'))

INPUTDIR = Sys.getenv(c('INPUTDIR'))
EIGENPRFX = Sys.getenv(c('EIGENPRFX'))
GENOMAP = Sys.getenv(c('GENOMAP'))
PHENOMAP = Sys.getenv(c('PHENOMAP'))

INPUTDIR = Sys.getenv(c('INPUTDIR'))
EIGENPRFX = Sys.getenv(c('EIGENPRFX'))
GENOMAP = Sys.getenv(c('GENOMAP'))
PHENOMAP = Sys.getenv(c('PHENOMAP'))

}

# ==========

#BiocManager::install(c('genomation', 'AnnotationHub', 'GenomicFeatures', 'methylKit', 'VariantAnnotation'), force=F)
#BiocManager::install(c('methylKit', 'VariantAnnotation'), force=F)

pacman::p_load(papeR, tidyverse, magrittr, ggthemes, e1071, genomation, AnnotationHub, GenomicFeatures, methylKit)

# ============

device2use='svg'
stopifnot(
{device2use %in% c('jpeg','jpg','png','pdf','svg','eps')})


# ==========
# Load functions
# ==========
source(paste0(CODEDIR,'/Rsourcecode/Functions_EMseq_MethylKit_221124.R'))
source(paste0(CODEDIR,"/SRC/Rcode/Functions_DMLUpsetPlots_231107.R"))

# ==========

stopifnot(
is.character(INPUTDIR),
is.character(OUTPUTDIR),
is.character(EIGENPRFX),
is.character(PHENOMAP))

print(paste('Variables in use are ...' ))
print(paste('OUTPUTDIR   is ...',	OUTPUTDIR  ))
print(paste('INPUTDIR   is ...',	INPUTDIR  ))
print(paste('EIGENPRFX   is ...',	EIGENPRFX  ))
print(paste('GENOMAP   is ...',	GENOMAP  ))
print(paste('PHENOMAP   is ...',	PHENOMAP  ))

# ============

seq.nonpool.db = readRDS(paste0(RDSDIR,'/','DB_SequencingInfo_nonpool','.RDS'))
multiqc.sum.allstages.db = readRDS(paste0(RDSDIR,'/','DB_Bismark_MultiQC','.RDS'))
static.guide = readRDS(paste0(RDSDIR,'/','DB_PlotGuide_Static','.RDS'))

```

## Coverage profiles

NOT READY.

## Methylation profiles

NOT READY.

## Clustering

### SLURM 

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/MethylKit/Script_MethylKit_SLURMArray_Rsubmit_RawPreprocessing.sh

RSCRIPT=${CODEDIR}/SRC/Rcode/Script_EMseq_MethylKit_clustering_220922.R

export JOBNAME=${TASK}'_Clustering'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

# !! Bigger chr need lots of memory !!
# 36GB is not enough for some chr!!

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:2:g" \
-e "s:ZZMEMZZ:42GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/25:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

echo ${RSCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

# # !! For non-tile data !!
# #export INPUTDIR=${2}
# echo ${METHKITDIR}/RawSummary/' \' >> ${JOBSCRIPT} 
# #export OUTPUTDIR=${3}
# echo ${OUTPUTDIR}/Clustering/' \' >> ${JOBSCRIPT}
# #export FILE.PATTERN=${4}
# echo '"normed.txt.bgz" \' >> ${JOBSCRIPT} ## For non-tile data

## !! For tile data !!
#export INPUTDIR=${2}
echo ${METHKITDIR}/Tile/' \' >> ${JOBSCRIPT} # For tile data
#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}/Clustering/' \' >> ${JOBSCRIPT}
#export FILE.PATTERN=${4}
echo '"cv[0-9]*.txt.bgz" \' >> ${JOBSCRIPT} ## For tile data

#export EXCLUDE.REGEX=${5}
echo ${EXCLUDEREGEX}' \' >> ${JOBSCRIPT}

#export MINOVRLAP=${6}
echo ${MINOVRLAP}' \' >> ${JOBSCRIPT}

#export SAMPLE_EXCLUDE_VEC=${7}
# This needs to be provided, even if it is just set to 'NOTHING'
echo ${SAMPLE_EXCLUDE_VEC}' \' >> ${JOBSCRIPT}

```

### Submit
```bash
#export RJD_LASTJOBID=9681560
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})

```

## Correlations

### SLURM 

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/MethylKit/Script_MethylKit_SLURMArray_Rsubmit_Correlation.sh

RSCRIPT=${CODEDIR}/SRC/Rcode/Script_EMseq_MethylKit_PostProcess_Correlations_231119.R

export JOBNAME=${TASK}'_Correlations'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

# !! Bigger chr need lots of memory !!
# 36GB is not enough for some chr!!

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:8GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/02:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:1:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

echo ${RSCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo '"MethylKit_Correlations_processed" \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${OUTPUTDIR}/Clustering/' \' >> ${JOBSCRIPT} 

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}/Clustering/' \' >> ${JOBSCRIPT}

#export FILEPATTERNSUFFIX=${4}
echo '".rds" \' >> ${JOBSCRIPT}


```

### Submit
```bash
#export RJD_LASTJOBID=9448808
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=afterok:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})

```


## Computational Pooling

### SLURM 
```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/MethylKit/Script_MethylKit_SLURMArray_Rsubmit_RawPreprocessing.sh

RSCRIPT=${CODEDIR}/SRC/Rcode/Script_EMseq_MethylKit_pool_221121.R

export JOBNAME=${TASK}'_Pooling'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

# !! Bigger chr need lots of memory !!
# 36GB is not enough for some chr!!

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:2:g" \
-e "s:ZZMEMZZ:42GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/25:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

echo ${RSCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${OUTPUTDIR}/Clustering/' \' >> ${JOBSCRIPT} 

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}/Pooling/' \' >> ${JOBSCRIPT}

#export FILE.PATTERN=${4}
echo '".txt.bgz" \' >> ${JOBSCRIPT} ## Names will not have similar suffixes beyond the txt.bgz .. so keep this

#export EXCLUDE.REGEX=${5}
echo ${EXCLUDEREGEX}' \' >> ${JOBSCRIPT}

#export MINOVRLAP=${6}
echo ${MINOVRLAP}' \' >> ${JOBSCRIPT}

#export SAMPLE_EXCLUDE_VEC=${7}
# This needs to be provided, even if it is just set to 'NOTHING'
echo ${SAMPLE_EXCLUDE_VEC}' \' >> ${JOBSCRIPT}

```

### Submit
```bash
#export RJD_LASTJOBID=9448808
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})

```

## Merge all chr

!! NO JOB ARRAYS HERE!!
Depending on the dataset, the file names and inputdir need to change!


```bash

TMPINPUTFILE='methylBase_united_minpergrp_'${MINOVRLAP}

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/MethylKit/Script_MethylKit_SLURMArray_Rsubmit_ConcatChr.sh

RSCRIPT=${CODEDIR}/SRC/Rcode/Script_EMseq_MethylKit_ContigConcat.R

export JOBNAME=${TASK}'_ConcatChr'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

# !!This is not an array job!!

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/02:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:1:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

echo ${RSCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${TMPINPUTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${OUTPUTDIR}/Clustering/' \' >> ${JOBSCRIPT} # Not poolComp
#echo ${OUTPUTDIR}/Pooling/' \' >> ${JOBSCRIPT} # poolComp Only

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}/ConcatChr/' \' >> ${JOBSCRIPT}

```

### Submit
```bash
echo ${JOBSCRIPT}

#export RJD_LASTJOBID=9564533
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=afterok:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})

```

## Remove all SNP loci

!! NOT IN USE !!

I had some issues subsetting methylkit objects and re-exporting them to DB objects.
List of loci should be prepped on the BSSNPer work flow.

### Remove loci 

```bash

ln -s ${BSSNPDIR}/BSSnper_SNPList_Joint.bed ${TASKDIR}

```

!! NO JOB ARRAYS HERE!!

```bash
TMPINPUTFILE='methylBase_united_minpergrp_'${MINOVRLAP}

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/MethylKit/Script_MethylKit_SLURMArray_Rsubmit_ConcatChr.sh

RSCRIPT=${CODEDIR}/SRC/Rcode/Script_EMseq_MethylKit_ExcludeLoci.R

export JOBNAME=${TASK}'_BSSNPer_Subset'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

# !!This is not an array job!!

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/10:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:1:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

echo ${RSCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${TMPINPUTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${OUTPUTDIR}/ConcatChr/' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}/SNPRemoval/' \' >> ${JOBSCRIPT}

```

```bash
```


## PCA with Jackknife 

This PCA needs to be all samples together, 100% overlap (or zero since non-ovelapping sites are removed)
This requires huge amount of memory but still hits seg fault after 50GB ... 
The process needs to be reworked. getData causes the issue.

```bash

TMPINPUTFILE='methylBase_united_minpergrp_'${MINOVRLAP}

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/MethylKit/Script_MethylKit_SLURMArray_Rsubmit_PCAjackknife.sh

RSCRIPT=${CODEDIR}/SRC/Rcode/Script_EMseq_MethylKit_PCAjacknife_221022.R

export JOBNAME=${TASK}'_PCAjackknife'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

# With the full genome, this will require lots of RAM

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:2:g" \
-e "s:ZZMEMZZ:24GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/10:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:1:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

echo ${RSCRIPT}'  \' >> ${JOBSCRIPT}

# The name of the file, used to find the file to load and as the prefix of the output files
#export INPUTFILE=${1}
echo ${TMPINPUTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${OUTPUTDIR}/ConcatChr/' \' >> ${JOBSCRIPT}
# Until I get the snp removal step correct, I need to use the raw concat data

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}/PCA/' \' >> ${JOBSCRIPT}

#export FILE.PATTERN.SUFFIX=${4}
echo ' ".txt.bgz" \' >> ${JOBSCRIPT}

```


### Submit

```bash
#export RJD_LASTJOBID=9448808
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=afterok:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})

```


### Estimate group average PCA coords

!! NOT IN USE !!

```R
# Prep R with the beginning bits of the Jackknife script

ltx.tit = 'PCAJackknife_methylBase_united_minpergrp_0.75'
OUTPUT = paste0(OUTPUTDIR,'/PCA/',ltx.tit,'.RDS')
pcajack.list = readRDS(OUTPUT)

seq.pool.db = readRDS(paste0(RDBASE,'/','DB_SequencingInfo_pool','.RDS'))
seq.nonpool.db = readRDS(paste0(RDBASE,'/','DB_SequencingInfo_nonpool','.RDS'))

```

#### Estimation

```R

str(pcajack.list)

tmp.samples = row.names(pcajack.list$main.pca)

tmp.main.pca  = pcajack.list$main.pca %>%
as_tibble %>% 
mutate(
sample = tmp.samples,
Sample.Code = str_split_i(sample,'[-]',2)) %>%
mutate(
pool = case_when(
grepl('pool',Sample.Code) ~ T,
!grepl('pool',Sample.Code) ~ F),
pollution = case_when(
grepl('NP',Sample.Code) ~ 'NP',
!grepl('NP',Sample.Code) ~ 'P'
))




tmp.main.pca  %>%
dplyr::filter(pool == F) %>%
group_by(pollution) %>%
dplyr::summarise(
across(
.cols = contains('PC'),
.fns = list(mean = ~ mean(.x, na.rm =T), sd = ~ sd(.x, na.rm =T)),
.names = "{.fn}_{.col}"
))


poolonly.main.pca.df = 
tmp.main.pca   %>%
dplyr::filter(pool == T) 





head(tmp.main.pca)
head(seq.nonpool.db)

dat.mainpca.df = left_join(tmp.main.pca, seq.nonpool.db, by = 'Sample.Code' ) %>%
 distinct()

```



#### Export Tables


```R

## Summary of Conversion rates

outname = paste0(OUTPUTDIR,"/","SupTable_PCAJack")

ltx.tit = 'PCAMeanPosition'

# ODS
grp.mean.main.pca.df %>%
 write_ods(
 path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit,"_",TODAY),
 x = .,
 append = T,
 update = F)

# Latex

ltx.lab2use = paste0('stab:Results:',ltx.tit)
ltx.cap2use = paste0('Mean position of each treatment group in PC space for \\textit{Species Name Here}. ')

grp.mean.main.pca.df %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,'_',ltx.tit,".tex"),
include.rownames=F)

# pool positions

ltx.tit = 'PCAPoolPosition'

ltx.lab2use = paste0('stab:Results:',ltx.tit)
ltx.cap2use = paste0('Mean position of each treatment group in PC space for \\textit{Species Name Here}. ')

poolonly.main.pca.df %>%
 write_ods(
 path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit,'_',TODAY),
 x = .,
 append = T,
 update = F)

poolonly.main.pca.df %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,'_',ltx.tit,".tex"),
include.rownames=F)

 
```


## Diff meth

```bash

TMPINPUTFILE='methylBase_united_minpergrp_'${MINOVRLAP}

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/MethylKit/Script_MethylKit_SLURMArray_Rsubmit_PCAjackknife.sh

RSCRIPT=${CODEDIR}/SRC/Rcode/Script_EMseq_MethylKit_differentialMeth_220922.R

export JOBNAME=${TASK}'_DiffMeth'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/01:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:1:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

echo ${RSCRIPT}'  \' >> ${JOBSCRIPT}

# The name of the file, used to find the file to load and as the prefix of the output files
#export INPUTFILE=${1}
echo ${TMPINPUTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${OUTPUTDIR}/ConcatChr/' \' >> ${JOBSCRIPT}
# Until I get the snp removal step correct, I need to use the raw concat data

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}/DiffMeth/' \' >> ${JOBSCRIPT}

#export FILE.PATTERN.SUFFIX=${4}
echo ' ".txt.bgz" \' >> ${JOBSCRIPT}

#export MINOVRLAP=${5}
echo ${MINOVRLAP}' \' >> ${JOBSCRIPT}

```


### Submit

```bash
#export RJD_LASTJOBID=9448808
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=afterok:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})

```

## Power Analysis

```bash

TMPINPUTFILE='methylBase_united_minpergrp_'${MINOVRLAP}

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/MethylKit/Script_MethylKit_SLURMArray_Rsubmit_Power.sh

RSCRIPT=${CODEDIR}/SRC/Rcode/Script_EMseq_MethylKit_Power_230826.R

export JOBNAME=${TASK}'_Power'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/01:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:1:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

echo ${RSCRIPT}'  \' >> ${JOBSCRIPT}

# The name of the file, used to find the file to load and as the prefix of the output files
#export INPUTFILE=${1}
echo ${TMPINPUTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${OUTPUTDIR}/ConcatChr/' \' >> ${JOBSCRIPT}
# Until I get the snp removal step correct, I need to use the raw concat data

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}/Power/' \' >> ${JOBSCRIPT}

#export FILE.PATTERN.SUFFIX=${4}
echo ' ".txt.bgz" \' >> ${JOBSCRIPT}

#export MINOVRLAP=${5}
echo ${MINOVRLAP}' \' >> ${JOBSCRIPT}

#export DIFFMETHDIR=${6}
echo ${OUTPUTDIR}/DiffMeth/' \' >> ${JOBSCRIPT}
# Until I get the snp removal step correct, I need to use the raw concat data

```


### Submit

```bash
#export RJD_LASTJOBID=9448808
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=afterok:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})

```

## Genic Context

Go to the methLandscape workbook to work interactively...

# Next?

After all versions of the diff meth etc have been run, you can go to the GeneralSummary workbook to create the summaries across runs.

