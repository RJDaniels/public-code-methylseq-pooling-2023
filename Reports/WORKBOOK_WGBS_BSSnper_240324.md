---
Title : Estimate SNPs from WGBS data
---

# Overview

## Background info

Examples of input files:
- [] https://gannet.fish.washington.edu/spartina/project-oyster-oa/Haws/BS-Snper/CHH-meth-info.tab
- [] Discussion on using BS Snper : https://github.com/RobertsLab/resources/discussions/1151


# Requires


# Set up local environment

You need to run within a sing container.

## Data set

### Bismark_Test

```bash

export FILEROOT=Bismark_Test

export REFGENOMEPRFX=NC_010473
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Bismark_test_data/methylseq
export TMPREF=NC_010473

export RAWDATADIR=${CMMNT}/CodeRepos/Bismark/test_files
export RAWDATPRFX=test

```


### Oyster

```bash
export FILEROOT=test_oyster

## Ilifu
#export REFGENOMEDIR=${CMMNT}/Projects/23_Epigenome/Resources/reference_genomes/CGigas/Roslin_v1
export REFGENOMEDIR=${GRPDIR}/23_Epigenome/Resources/reference_genomes/CGigas/Roslin_v1
export REFGENOMEPRFX=cgigas_uk_roslin_v1_genomic-mito

# Shorter Ref name
export TMPREF=CGig

# Annotations
export GFFDIR=${REFGENOMEDIR}
export GFFPRFX=genomic
# fna or fasta.gz


```


### Dreissena

```bash
export FILEROOT=Dpol

export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1/methylseq
export REFGENOMEPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic

# Shorter Ref name
export TMPREF=Dpol1.0


```


### Corbicula

```bash
export FILEROOT=Cflu

## Ilifu
#export REFGENOMEDIR=${CMMNT}/Projects/23_Epigenome/Resources/reference_genomes/
export REFGENOMEDIR=${GRPDIR}/23_Epigenome/Resources/reference_genomes/Corbicula_fluminea/Lachesis/methylseq
export REFGENOMEPRFX=Lachesis_assembly_changed
# Shorter Ref name
export TMPREF=Lachesis

```



## Set up
```bash

# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=BSSnper_${TMPREF}

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi
# =======

# File with the names of all the files to run
export ROOTLISTFILE=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming/${FILEROOT}_FileRootList.txt
export FILEROOTLIST=${ROOTLISTFILE}

export REFFASTA=${REFGENOMEDIR}/${REFGENOMEPRFX}.fa


# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')
echo $ARRAYLENGTH
## ARRAYLENGTH=2

# Trimmed data
export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming
#export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming

# Aligned data
export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Align_${TMPREF}/out_Bismark_Align_${TMPREF}
#export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_alignment/out_Bismark_alignment

# Dedup data
export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Dedup_${TMPREF}/out_Bismark_Dedup_${TMPREF}
#export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_deduplication/out_Bismark_deduplication

# Methyl call
export METHDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_MethylExt_${TMPREF}/out_Bismark_MethylExt_${TMPREF}

```


## Data Prep

### Corbicula Interleaved fasta
BSSnper requirers interleaved/multiline fasta file, unzipped

Convert fasta to interleaved

```bash

ls ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa

# Get chromosome names and sizes
samtools faidx ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa

cut -f1,2 ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa.fai > ${REFGENOMEDIR}/${REFGENOMEPRFX}_genome_summary.txt

# Produce list of regions to use for subset
cut -f1,1 ${REFGENOMEDIR}/${REFGENOMEPRFX}_genome_summary.txt > ${REFGENOMEDIR}/${REFGENOMEPRFX}_contiglist.txt

# Create new fasta file
samtools faidx \
 -r ${REFGENOMEDIR}/${REFGENOMEPRFX}_contiglist.txt \
 -n 80 \
  -o ${REFGENOMEDIR}/${REFGENOMEPRFX}_interleaved.fa \
  ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa


```



### Oyster

Download some of the data from https://gannet.fish.washington.edu/



```bash

# The log file 

wget https://gannet.fish.washington.edu/spartina/project-oyster-oa/Haws/BS-Snper/zr3616_8_R1_val_1_val_1_val_1_bismark_bt2_pe.ERR.log

# Bam input

wget https://gannet.fish.washington.edu/spartina/project-gigas-oa-meth/output/bismark/zr3616_8_R1_val_1_val_1_val_1_bismark_bt2_pe.deduplicated.sorted.bam

wget https://gannet.fish.washington.edu/spartina/project-gigas-oa-meth/output/bismark/zr3616_8_R1_val_1_val_1_val_1_bismark_bt2_pe.deduplicated.sorted.bam.bai

# Reference genome
wget https://gannet.fish.washington.edu/spartina/project-oyster-oa/data/Cg-roslin/cgigas_uk_roslin_v1_genomic-mito.fa

```



# Common Code

## Sort BAM files by chr

Change to new BAM concat script... NOT NECESSARY...
The old process already had the files merged ahead of variant calling ... the file name was just kept as 'split1_' even though it had all the data.
.... No reason to redo the analyses...

```bash

#export JOBNAME=${TASK}'_BSSnper_bamsort'
export JOBNAME=${TASK}'_BSSnper_BAMConcat'

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

#SCRIPT=${CODEDIR}/Bismark/Script_Bismark_samtool_sortbychr_240204.sh
SCRIPT=${CODEDIR}/SAMTOOLS/Script_Samtools_BAMConcat.sh
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/05:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_methylseqtools_v0.3.sif \' >> ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${DEDUPDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

#export REFFASTA=${4} # The Samtools BAMConcat requires a fasta
echo ${REFFASTA}' \' >> ${JOBSCRIPT}

```



```bash
#export RJD_LASTJOBID=9448808
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})


```



## BSSnper calling

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/BSSnper/Script_BSSnper_Calling_240412.sh

export JOBNAME=${TASK}'_BSSnperCall'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/49:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:10:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_methylseqtools_v0.3.sif \' >> ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export REFGENOMEDIR=${2}
echo ${REFGENOMEDIR}' \' >> ${JOBSCRIPT}

#export REFGENOMEPRFX=${3}
#echo ${REFGENOMEPRFX}_interleaved' \' >> ${JOBSCRIPT}
echo ${REFGENOMEPRFX}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${4}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${5}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

```



```bash
#export RJD_LASTJOBID=9508265
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

```


## Histogram of loci detected

Done in R
Interactive for now

```bash
sinteractive --x11
module load  R/RStudio2022.12.0-353-R4.2.2
R

```


```R
#! /bin/R

# BSSnper workflow
# Script to tally SNPs by individuals 

# ============

CMMNT =Sys.getenv(c('CMMNT'))
#CMMNT = args[1]

# Load Env variables
source(paste0(CMMNT,'/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.R'))

# Load general R stuff
# This is now in the Env vars step above
source(paste0(CODEDIR,'/StartupScript_Sources_260220.R'))

# ==========
args = commandArgs(TRUE)

INPUTDIR = args[1]
OUTPUTDIR = args[2]
VCFFILE = args[3]

# ==========
# Interactive
# ==========

if(FALSE){

INPUTDIR = Sys.getenv(c('OUTPUTDIR'))
OUTPUTDIR = Sys.getenv(c('OUTPUTDIR'))
#VCFFILE = Sys.getenv(c('Dp'))
VCFFILE = '.vcf'

}

assert(
1==5,
msg='ERR:Code is not ready to use on the CLI.',
stop=T)

# ========
# Run Startup scripts
# ========

# ==========
pacman::p_load(assert, bedr, papeR)
# ============

device2use='jpeg'

assert(
{device2use %in% c('jpeg','jpg','png','pdf','svg','eps')},
msg='ERR:device2use not one of the likely stings.',
stop=T)

stopifnot(
is.character(VCFFILE),
is.character(INPUTDIR),
is.character(OUTPUTDIR)
)

# ============
print(paste('OUTPUTDIR   is ...',	OUTPUTDIR  ))

# ============
# Read in data

file.input.vec = list.files(path = INPUTDIR,
pattern = VCFFILE,
all.files = FALSE,
full.names = F,
recursive = T,
ignore.case = FALSE,
include.dirs = F)

assert(
length(file.input.vec)>0,
msg='ERR:file.input.vec is empty.',
stop=T)

#str(file.input.vec)

# ============
# Read in data
# ============

text2strip = c(
'20220720.A-o287562_' = '',
'20220720.A-o287561_' = '',
'_BSSnper.vcf' = '')

datasets.list = lapply(
file.input.vec,
function(tmp.file){

# get data

tmp.dat =  paste0(INPUTDIR,'/',tmp.file ) %>% 
read.vcf(. ,
 split.info = FALSE, 
 split.samples = FALSE, 
 nrows = -1, 
 verbose = TRUE) %>% 
 .$vcf %>% 
.[,1:5]
  
#str(tmp.dat)

 
assert(
length(tmp.dat )>0,
is.list(tmp.dat ) == T,
msg='ERR:file.input.list is empty/not df.',
stop=T)

names(tmp.dat)=gsub("X.","",names(tmp.dat))
#str(tmp.dat)

assert(
length(tmp.dat )>0,
is.data.frame(tmp.dat ) == T,
msg='ERR:file.input.list is empty/not df.',
stop=T)


# Add col for file name
tmp.dat %<>%
rowwise %>%
mutate(
dataset = fn.StripTextOf(tmp.file, text2strip)) %>%
as_tibble

assert(
length(tmp.dat )>0,
is.data.frame(tmp.dat ) == T,
msg='ERR:file.input.list is empty/not df.',
stop=T)

# return df
tmp.dat %>% return
}) %>%
bind_rows 

str(datasets.list)

# ==============

datasets.list %<>%
mutate(
Pool = grepl('[Pp]ool',dataset),
Polluted = grepl('[nN]P',dataset) %>% `!`,
ID = paste0(CHROM,'_',POS)
) 

snp.summary.pool.pol.df =  datasets.list %>%
  group_by(ID,Pool,Polluted) %>%
 dplyr::summarise(Seq = n()) %>%
  group_by(Pool,Polluted,Seq) %>%
 dplyr::summarise(tally = n()) 

 snp.summary.df =  datasets.list %>%
  group_by(ID) %>%
 dplyr::summarise(Seq = n()) %>%
  group_by(Seq) %>%
 dplyr::summarise(tally = n()) 


p1 =  ggplot( 
snp.summary.df,
 aes(
x = Seq,
y = tally
)) +
geom_histogram(stat='identity')

OUTPUT = paste0(OUTPUTDIR, "/Plot_BSSnper_SNPSharedxLibraries_Histogram")
print(paste0('Output saved to ... ',OUTPUT ))

p1 %>% ggsave(.,
filename=paste0(OUTPUT,".",device2use),
width=80,
height=50,
device=device2use,
units = 'mm')

# ===========
# Tables exported
# ===========


ltx.lab2use = paste0(
'stab:Results:BSSNPer:Summary:'
)

ltx.cap2use = paste0(
'Frequency distribution of SNPs among sequenced libraries for \\textit{SPECIES NAME}.'
)

OUTPUT = paste0(OUTPUTDIR, "/Table_BSSnper_SummaryxPoolPollution_")
print(paste0('Output saved to ... ',OUTPUT ))

snp.summary.pool.pol.df %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(OUTPUT,".tex"),
include.rownames=F)


OUTPUT = paste0(OUTPUTDIR, "/Table_BSSnper_Summary_")
print(paste0('Output saved to ... ',OUTPUT ))

snp.summary.df %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(OUTPUT,".tex"),
include.rownames=F)

# ========
# Final  List of SNPs 
# ========
# Export as a zero-based bed file

OUTPUT = paste0(OUTPUTDIR, "/BSSnper_SNPList_Joint")
print(paste0('Output saved to ... ',OUTPUT ))

datasets.list %>% 
mutate(
START = {POS - 1},
END = {POS}
) %>%
dplyr::select(CHROM,START,END,ID,REF,ALT) %>%
unique %>%
write.table(., 
file = paste0(OUTPUT,".bed"),
quote = F,
row.names=F,
col.names = F,
sep = '\t')


datasets.list %>% 
mutate(
START = {POS - 1},
END = {POS}
) %>%
dplyr::select(CHROM,START,END,ID,REF,ALT) %>%
unique %>%
rtracklayer::export(., con = paste0(OUTPUT,".bed"), format = 'bed')
 
#genomation::readBed(paste0(OUTPUT,".bed"), zero.based=T)

# ========
# Rough work
# ========

datasets.list %>%
  group_by(ID,Pool) %>%
 dplyr::summarise(Seq = n()) %>%
  group_by(Pool) %>%
 dplyr::summarise(tally = n()) 

 datasets.list %>%
  group_by(ID,Pool) %>%
 dplyr::summarise(Seq = n()) %>%
  ungroup() %>%
 dplyr::summarise(tally = n()) 

```

### Compile Loci List






