---
Title : Methylkit analyses preprocessing individual runs
---

# Overview

This workbook is a launch point for a series of scripts.

## Bookkeeping

# Requires
## Installation

# Set up local environment

You need to run within a sing container.

## Data set
### Bismark_Test

```bash

export FILEROOT=Bismark_Test

export REFGENOMEPRFX=NC_010473
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Bismark_test_data/methylseq
export TMPREF=NC_010473

export RAWDATADIR=${CMMNT}/CodeRepos/Bismark/test_files
export RAWDATPRFX=test

```

### Dreissena 10x

```bash
export FILEROOT=Dpol

export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1/methylseq
export REFGENOMEPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic

# Shorter Ref name
export TMPREF=Dpol1.0

# =======
# Methylkit variables
# =======
export EXCLUDEREGEX='unpaired'
export MINCOVFILTER='10' # For subsequent filtering
export SAMPLE_EXCLUDE_VEC='"06-D6P|07-D7P"'
export STEPSIZE='1000'
export WINDOWSIZE='1000'

```


### Corbicula 10x

```bash
export FILEROOT=Cflu

## Ilifu
#export REFGENOMEDIR=${CMMNT}/Projects/23_Epigenome/Resources/reference_genomes/
export REFGENOMEDIR=${GRPDIR}/23_Epigenome/Resources/reference_genomes/Corbicula_fluminea/Lachesis/methylseq
export REFGENOMEPRFX=Lachesis_assembly_changed
# Shorter Ref name
export TMPREF=Lachesis


# =======
# Methylkit variables
# =======

export EXCLUDEREGEX='unpaired'
export MINCOVFILTER='10' # For subsequent filtering
export SAMPLE_EXCLUDE_VEC='"02-C2P|04-C4P|14-C5NP|17-C9NP"'
export STEPSIZE='1000'
export WINDOWSIZE='1000'

```


## Set up

```bash
# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=MethylKit_${TMPREF}_mincov${MINCOVFILTER}

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi
# =======

# =======
# File with the names of all the files to run
# =======
export ROOTLISTFILE=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming/${FILEROOT}_FileRootList.txt
export FILEROOTLIST=${ROOTLISTFILE}

# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')
echo $ARRAYLENGTH
## ARRAYLENGTH=2
# =======

# =======
# DIRECTORIES
# =======

# Trimmed data
export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming
#export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming

# Aligned data
export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Align_${TMPREF}/out_Bismark_Align_${TMPREF}
#export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_alignment/out_Bismark_alignment

# Dedup data
export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Dedup_${TMPREF}/out_Bismark_Dedup_${TMPREF}
#export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_deduplication/out_Bismark_deduplication

# Methyl call
export METHDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_MethylExt_${TMPREF}/out_Bismark_MethylExt_${TMPREF}

# BSSnper dir
export BSSNPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/BSSnper_${TMPREF}/out_BSSnper_${TMPREF}

# Variant Calling dir
export SNPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/VariantCalling_${TMPREF}/out_VariantCalling_${TMPREF}

```

## Data Prep

# Common Code

## Split data by chr

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_SplitxChr.sh

export JOBNAME=${TASK}'_SplitxChr'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/10:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${METHDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

#export REFGENOMEDIR=${4}
echo ${REFGENOMEDIR}' \' >> ${JOBSCRIPT}

#export REFGENOMEPRFX=${5}
echo ${REFGENOMEPRFX}' \' >> ${JOBSCRIPT}

```

### Submit

```bash
#export RJD_LASTJOBID=9448808
echo $RJD_LASTJOBID
RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

```


## Remove empty files

```bash

# List loose contigs to drop and move them to archive
awk '$2>1e7{print $1}' ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa.fai 

awk '$2<1e7{print $1}' ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa.fai > ${OUTPUTDIR}/LooseContigList.tsv 

wc -l ${OUTPUTDIR}/LooseContigList.tsv
cat ${OUTPUTDIR}/LooseContigList.tsv

mkdir -p ${OUTPUTDIR}/archive ${OUTPUTDIR}/CovGz

cat ${OUTPUTDIR}/LooseContigList.tsv |  \
xargs -I % bash -c "find ${OUTPUTDIR}/ -iname '*%*' -maxdepth 1 -type f -exec mv {} ${OUTPUTDIR}/archive/ \; " ;

# Of the remaining files, test for any with low content

find ${OUTPUTDIR} -maxdepth 1  -iname '*cov.gz' -type f -exec bash -c ' LINECNT=$(zcat {} | wc -l) ; echo ${LINECNT} {} >> ${OUTPUTDIR}/LineCountPerCovGz.tsv  ' \;

cat ${OUTPUTDIR}/LineCountPerCovGz.tsv 

# Drop any file with less than 100 lines
awk '$1<100{print $2}' ${OUTPUTDIR}/LineCountPerCovGz.tsv 
# None

mv ${OUTPUTDIR}/*cov.gz ${OUTPUTDIR}/CovGz/

```


## Prep individual cov.gz files

Change FILEROOTLIST to loop over chr

```bash
export ROOTLISTFILEPRFX=${FILEROOT}_ContigsSize1e07
export ROOTLISTFILE=${TASKDIR}/${ROOTLISTFILEPRFX}.tsv
export FILEROOTLIST=${ROOTLISTFILE}

awk '$2>1e7{print $1}' ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa.fai > ${ROOTLISTFILE}


# cat ${ROOTLISTFILE}

# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')
echo $ARRAYLENGTH
## ARRAYLENGTH=2

```

Looping over chromosomes will happen within the Rscript with the lapply. No special treatment needed.
This needs a SLURM script to interface to the R script.


```bash

# Unpaired MUST be removed here, it WILL overwrite the paired reads

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/MethylKit/Script_MethylKit_SLURMArray_Rsubmit.sh

RSCRIPT=${CODEDIR}/SRC/Rcode/Script_EMseq_MethylKit_PreprocessingxSample_221116.R

export JOBNAME=${TASK}'_DataPrep'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/05:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

# The R script to run
echo ${RSCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
# OUTPUTDIR for split by chr
# METHDIR for entire data
echo ${OUTPUTDIR}/CovGz' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}/RawDB/' \' >> ${JOBSCRIPT}

#export FILE.PATTERN=${4}
echo ' ".cov.gz" \' >> ${JOBSCRIPT}

#export EXCLUDE.REGEX=${5}
echo ${EXCLUDEREGEX}' \' >> ${JOBSCRIPT}

#export MINCOVVAR=${6} ## Default to zero so I can use the same files for everything else
echo '"0" \' >> ${JOBSCRIPT}

#export SAMPLE_EXCLUDE_VEC=${7}
echo ' "NONE" \' >> ${JOBSCRIPT}

```

### Submit

```bash
#export RJD_LASTJOBID=9448808
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})

```

## Summary of raw data

!! YOU MUST EXCLUDE THE BAD SAMPLES AT THIS POINT !!
!! YOU NEED TO SET MINCOV HERE. THE PREVIOUS STEP JUST USED 0 AS A DEFAULT!!

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/MethylKit/Script_MethylKit_SLURMArray_Rsubmit_RawPreprocessing.sh

RSCRIPT=${CODEDIR}/SRC/Rcode/Script_EMseq_MethylKit_RawSummaryxSample_240421.R

export JOBNAME=${TASK}'_RawSummary'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:16:g" \
-e "s:ZZCORESZZ:2:g" \
-e "s:ZZMEMZZ:24GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/02:40:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

echo ${RSCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${OUTPUTDIR}/RawDB/' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}/RawSummary/' \' >> ${JOBSCRIPT}

#export FILE.PATTERN=${4}
echo '".txt.bgz" \' >> ${JOBSCRIPT}

#export EXCLUDE.REGEX=${5}
echo ${EXCLUDEREGEX}' \' >> ${JOBSCRIPT}

#export MINCOVFILTER=${6}
echo ${MINCOVFILTER}' \' >> ${JOBSCRIPT}

#export SAMPLE_EXCLUDE_VEC=${7}
echo ${SAMPLE_EXCLUDE_VEC}' \' >> ${JOBSCRIPT}

```

### Submit
```bash
#export RJD_LASTJOBID=9564592
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})

```

## Tiling


```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/MethylKit/Script_MethylKit_SLURMArray_Rsubmit_RawPreprocessing.sh

RSCRIPT=${CODEDIR}/SRC/Rcode/Script_EMseq_MethylKit_tile_220922.R

export JOBNAME=${TASK}'_Tile'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

# !! Bigger chr need lots of memory !!
# 36GB is not enough for some chr!!

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:12GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/03:10:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

echo ${RSCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1} # Numbering for within Rscript
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${OUTPUTDIR}/RawSummary/' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}/Tile/' \' >> ${JOBSCRIPT}

#export FILE.PATTERN.SUFFIX=${4}
echo '"normed.txt.bgz" \' >> ${JOBSCRIPT}

#export EXCLUDE.REGEX=${5}
echo ${EXCLUDEREGEX}' \' >> ${JOBSCRIPT}

#export SAMPLE_EXCLUDE_VEC=${6}
# This needs to be provided, even if it is just set to 'NOTHING'
echo ${SAMPLE_EXCLUDE_VEC}' \' >> ${JOBSCRIPT}

#export MINCOVFILTER =${7}
# This needs to be provided, even if it is just set to 'NOTHING'
echo ${MINCOVFILTER}' \' >> ${JOBSCRIPT}

#export WINDOWSIZE =${8}
# This needs to be provided, even if it is just set to 'NOTHING'
echo ${WINDOWSIZE}' \' >> ${JOBSCRIPT}

#export STEPSIZE=${9}
# This needs to be provided, even if it is just set to 'NOTHING'
echo ${STEPSIZE}' \' >> ${JOBSCRIPT}

cat ${JOBSCRIPT}

```

### Submit
```bash
#export RJD_LASTJOBID=9448808
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})

```
