---
Title : Bismark processing
---


!! NO LONGER IN USE !!


# Overview
Processing sequence data with Bismark.
This is entirely slurm submission based.

The process below will need to be run at least three times per dataset.
Data sets are likely to be:
- [] Conversion rate estimates with Lambda
- [] Conversion rate estimates with phage
- [] Actual methylation calls estimates with all data

## Background info

Used the : enzymatic methyl seq kit, NEB
https://github.com/FelixKrueger/Bismark/blob/dev/Docs/README.md#em-seq-neb

Requires conservative trimming parameters
```--clip_R1 10 --clip_R2 10 --three_prime_clip_R1 10 --three_prime_clip_R2 10```

More info:
- https://www.neb.com/protocols/2019/03/28/protocol-for-use-with-large-insert-libraries-470-520-bp-e7120
- https://github.com/FelixKrueger/Bismark/issues/509
- https://github.com/nebiolabs/EM-seq/blob/master/em-seq.nf


# Set up local environment

Set project vars.

## Data of interest

### Test Subset

Run the code on the bismark provided data to test if everything works as expected.

```bash

export FILEROOT=test_data

# Decide on a ref genome dir
export REFGENOMEDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark/Bismark/test_files/ref
# Set the ref genome file name ... will depend on the data
export REFGENOMEPRFX=NC_010473
# Shorter Ref name
export TMPREF=NC_010473

# If the raw data are in another directory, give that dir path
#RAWINPUTDIR=${TASKDIR}/InputData

```


### Test Subset

Run the code on the bismark provided data to test if everything works as expected.

```bash

export FILEROOT=test

# Decide on a ref genome dir
REFGENOMEDIR=${GRPDIR}/RawData_epigenomics
# Set the ref genome file name ... will depend on the data
REFGENOME=NC_010473

# If the raw data are in another directory, give that dir path
RAWINPUTDIR=${TASKDIR}/InputData



```


# Start up

Code to load everything into the shell.

```bash

# Set the project
export PRJNAME=23_Epigenome
# The main folder/activity
export TASK=Bismark

# ======
echo ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
# ======

# =====
# Load extra modules if needed
module load bowtie2 ## or HISAT2 ... decide which one to use. default to bowtie2
module load hisat2 ## on ILIFU
module load samtools

module load bismark

```


# Requires

## Installation

### Bismark
If bismark is not installed you can copy the executables to your local bin for running.

```bash
cd ${CMMNT}/CodeRepos
git clone https://github.com/FelixKrueger/Bismark.git

find ${CMMNT}/CodeRepos/Bismark/ -maxdepth 1 -type f -executable  -exec ln -s {} ${HOME}/.local/bin/ \;


```


# Produces

# Analyses

## Data Prep
This section is for any 'rough' work that  needs to be done before we can start. e.g. dowloading data.

### test data

Download testing data

```bash

## for training data
cd ${CMMNT}/CodeRepos
git clone https://github.com/FelixKrueger/Bismark.git

# Make ref genome dir
mkdir -p ${REFGENOMEDIR}
ln -s ${CMMNT}/CodeRepos/Bismark/test_files/${REFGENOME}* ${REFGENOMEDIR}

# Sometimes links cause issues, so you can copy the data instead
#cp ${CMMNT}/CodeRepos/Bismark/test_files/${REFGENOME}  ${REFGENOMEDIR}

# Link to the data
mkdir ${TASKDIR}/InputData
ln -s ${TASKDIR}/Bismark/test_files/${FILEROOT}* ${TASKDIR}/InputData/


# ========

#REFGENOMEDIR=/cluster/work/gdc/shared/p866/Data/Reference_Genomes/Deissena_polymorpha/ncbi-genomes-2022-07-18
REFGENOMEDIR=${TASKDIR}/RefGenome

#REFGENOME=GCA_020536995.1_UMN_Dpol_1.0_genomic.fa
REFGENOME=NC_010473.fa.gz

mkdir ${TASKDIR}/RefGenome
#ln -s ${TASKDIR}/Bismark/test_files/${REFGENOME}  ${REFGENOMEDIR}
# Sometimes links cause issues, so you can copy the data instead
 cp ${TASKDIR}/Bismark/test_files/${REFGENOME}  ${REFGENOMEDIR}


#REFGENOMEDIR=/cluster/work/gdc/shared/p866/Data/Reference_Genomes/Deissena_polymorpha/ncbi-genomes-2022-07-18
RAWINPUTDIR=${TASKDIR}/InputData

mkdir ${TASKDIR}/InputData
ln -s ${TASKDIR}/Bismark/test_files/${FILEROOT}* ${TASKDIR}/InputData/

```

Setting some more variables.
```bash

# File with the names of all the files to run
export ROOTLISTFILE=${OUTPUTDIR}/${FILEROOT}_FileRootList.txt

# Put the names of the fasta files into the file above
basename -s R1.fastq.gz ${RAWINPUTDIR}/${FILEROOT}*R1*.gz > ${ROOTLISTFILE}

cat ${ROOTLISTFILE}

# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')
echo $ARRAYLENGTH
## ARRAYLENGTH=2


```



#### Reference genome

##### Indexing reference genomes

I have already performed the indexing and transformation of the ref genomes. You only need to run the methylation calls.


```bash

# The genomes need to be unzipped to gain access
tar -xvf ${GRPDIR}/RawData_epigenomics/Reference_Genomes.tar.gz

# For lambda :Conversion rate estimates
REFGENOMEDIR=${GRPDIR}/RawData_epigenomics/Reference_Genomes/NEB_lambda
REFGENOME=NEB_lambda.fa

# For pUC :Conversion rate estimates
REFGENOMEDIR=${GRPDIR}/RawData_epigenomics/Reference_Genomes/pUC19
REFGENOME=pUC19.fa

# You need to make sure the ref genomes are in these folders
#mkdir -p ${REFGENOMEDIR}
#cd ${REFGENOMEDIR}
ls ${REFGENOMEDIR}

```


Genome prep is not needed. Euler has a preped genome already for many species.
If not on Euler, you can run the code below.


```bash
#gunzip ${REFGENOMEDIR}/${REFGENOME}.fa.gz
#bismark_genome_preparation -hisat2 --verbose ${REFGENOMEDIR}
bismark_genome_preparation --verbose ${REFGENOMEDIR}

```


#### Data Prep

The scratch space on Euler is really big and lasts two weeks ... which is unusual for scratch space on an HPC.
I have decided to use it for temporary storage but you don't need to do the same.

```bash

# ## !!THE WORK BELOW IS STILL IN THE OLD FORMAT FROM PRE-2024!!
#
# ls $SCRATCH/Data/RawData_epigenomics
#
# cd $SCRATCH/Data/RawData
#
#
# mkdir $SCRATCH/Data/RawData/DpolGrei_EMseq \
# $SCRATCH/Data/RawData/DpolGrei_pool_EMseq
#
# ls $SCRATCH/Data/RawData/DpolGrei_EMseq/ \
# $SCRATCH/Data/RawData/DpolGrei_pool_EMseq/
#
#
# du -hs $SCRATCH/Data/RawData/DpolGrei_EMseq/ \
# $SCRATCH/Data/RawData/DpolGrei_pool_EMseq/
# ## 150 GB of raw zipped data
# ## 5GB each

```


## SLURM submission

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_RefGenomeIndex_240204.sh

JOBNAME=${TASK}'_RefGenome'

sed \
-e "s:ZZTHREADSZZ:1:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:1:g" \
-e "s:ZZSTOREZZ:1:g" \
-e "s/ZZWALLTIMEZZ/02:00:00/g" \
-e "s:ZZJOBNAMEZZ:${JOBNAME}:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

echo 'bash \' >> ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

# The script to run
echo ${SCRIPT}'  \' >> ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

#export REFGENOMEDIR=${1}
echo ${REFGENOMEDIR}' \' >> ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

#export REFGENOME=${2}
echo ${REFGENOME}' \' >> ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}' \' >> ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

ls ${OUTPUTDIR}/sh_dir/SLURM_*.sh

sbatch SLURM_${JOBNAME}.sh

```


### FastQC and Trimming

Need to run separately for the pooled and unpooled data. Pooled will have drastically different memory requirements.


```bash

ls ${PRJDIR}/fastqc/
rm -rf ${PRJDIR}/fastqc
mkdir ${PRJDIR}/fastqc
cd ${PRJDIR}/fastqc

pull_git_repo.sh # I have a script that updates the code repos automatically .... you will need to do this manually
# The command would be ``` git pull ``` which you run from inside the $CODEDIR


mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_FastQC_240215.sh

JOBNAME=${TASK}'_FastQC'

sed \
-e "s:ZZTHREADSZZ:1:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:1:g" \
-e "s:ZZSTOREZZ:1:g" \
-e "s/ZZWALLTIMEZZ/02:00:00/g" \
-e "s:ZZJOBNAMEZZ:${JOBNAME}:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

echo 'bash \' >> ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

# The script to run
echo ${SCRIPT}'  \' >> ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

#export REFGENOMEDIR=${1}
echo ${REFGENOMEDIR}' \' >> ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

#export REFGENOME=${2}
echo ${REFGENOME}' \' >> ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}' \' >> ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

ls ${OUTPUTDIR}/sh_dir/SLURM_*.sh

sbatch sh_dir/SLURM_${JOBNAME}.sh

```


```bash
sed -e "s:ZZINPUTDIRZZ:${RAWINPUTDIR}/:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/fastqc:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/fastqc/Fastqc.lsf.sh

cat ${PRJDIR}/fastqc/Fastqc.lsf.sh

#bsub < ${PRJDIR}/fastqc/Fastqc.lsf.sh
#bsub < ${PRJDIR}/fastqc/Fastqc.lsf.sh

```

```bash
ls ${PRJDIR}/trimmomatic/*
rm -rf ${PRJDIR}/trimmomatic
mkdir -p ${PRJDIR}/trimmomatic
cd ${PRJDIR}/trimmomatic

## ARRAYLENGTHBK=${ARRAYLENGTH}
ARRAYLENGTH=1
# ARRAYLENGTH=${ARRAYLENGTHBK}

TEMPLATE=TEMPLATE_Script_Trimmomatic_LSF_Euler_220815

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${RAWINPUTDIR}:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/trimmomatic:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/trimmomatic/trimmomatic.lsf.sh

cat ${PRJDIR}/trimmomatic/trimmomatic.lsf.sh

bsub < ${PRJDIR}/trimmomatic/trimmomatic.lsf.sh

bjobs

bbjobs

```
```bash
ls ${PRJDIR}/trim_galore/*
rm -rf ${PRJDIR}/trim_galore
mkdir ${PRJDIR}/trim_galore
cd ${PRJDIR}/trim_galore

ARRAYLENGTHBK=${ARRAYLENGTH}
# ARRAYLENGTH=1
# ARRAYLENGTH=${ARRAYLENGTHBK}

TEMPLATE=TEMPLATE_Script_Trim_galore_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${RAWINPUTDIR}:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/trim_galore:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/trim_galore/trim_galore.lsf.sh

cat ${PRJDIR}/trim_galore/trim_galore.lsf.sh

bsub < ${PRJDIR}/trim_galore/trim_galore.lsf.sh

bjobs

bbjobs 228683933

```

### MultiQC Report

```bash
ls ${PRJDIR}/reports/MultiQC
rm -rf ${PRJDIR}/reports/MultiQC
mkdir -p ${PRJDIR}/reports/MultiQC
cd ${PRJDIR}/reports/MultiQC

TEMPLATE=TEMPLATE_Script_MultiQC_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/reports/MultiQC:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/reports/MultiQC.lsf.sh

cat ${PRJDIR}/reports/MultiQC.lsf.sh

bsub < ${PRJDIR}/reports/MultiQC.lsf.sh

bjobs
bbjobs

less

```


Run on local computer to view html summaries.
```bash

rsync --relative -auvz rdaniels@euler.ethz.ch:/cluster/scratch/rdaniels/rdaniels/./Pollution_EMseq/DpolGrei_All/reports /home/rjd/msRNA_local/Projects/eawag_pollution_220801/Analyses/

```

### Conversion rate estimates
#### Lambda
```bash
REFGENOMEDIR=/cluster/work/gdc/shared/p866/Data/Reference_Genomes/NEB_lambda
REFGENOME=NEB_lambda.fa

ls ${PRJDIR}/conversion_rates/NEB_lambda/*
rm -rf ${PRJDIR}/conversion_rates/NEB_lambda

mkdir -p ${PRJDIR}/conversion_rates/NEB_lambda
cd ${PRJDIR}/conversion_rates/NEB_lambda

#  cp ${ROOTLISTFILE} ${ROOTLISTFILE}_alignment_rerun
#  mv ${ROOTLISTFILE}_bkup ${ROOTLISTFILE}

 cat ${ROOTLISTFILE}

echo $ARRAYLENGTH
# ARRAYLENGTHBK=${ARRAYLENGTH}
# ARRAYLENGTH=2
# ARRAYLENGTH=${ARRAYLENGTHBK}

pull_git_repo.sh

TEMPLATE=TEMPLATE_Script_Bismark_Alignment_LSF_Euler_220807

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/trim_galore:" \
-e "s:ZZREFGENOMEDIRZZ:${REFGENOMEDIR}:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_lambda:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/conversion_rates/NEB_lambda/Bismark_alignment.lsf.sh

cat ${PRJDIR}/conversion_rates/NEB_lambda/Bismark_alignment.lsf.sh

cat ${ROOTLISTFILE}


##nano ${PRJDIR}/conversion_rates/NEB_lambda/Bismark_alignment.lsf.sh

bsub < ${PRJDIR}/conversion_rates/NEB_lambda/Bismark_alignment.lsf.sh

# This is the second data set which needs to be watched on the run because it is performing badliy with CPU resources
231864232
#This is the first data set which was performing well, taking up to 90per CPU
231853095

```
### Deduplication


```bash
ls ${PRJDIR}/conversion_rates/NEB_lambda/deduplication/*
rm -rf ${PRJDIR}/conversion_rates/NEB_lambda/deduplication

mkdir ${PRJDIR}/conversion_rates/NEB_lambda/deduplication
cd ${PRJDIR}/conversion_rates/NEB_lambda/deduplication


TEMPLATE=TEMPLATE_Script_Bismark_Deduplication_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_lambda:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_lambda/deduplication:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/conversion_rates/NEB_lambda/deduplication/Bismark_deduplication.lsf.sh

cat ${PRJDIR}/conversion_rates/NEB_lambda/deduplication/Bismark_deduplication.lsf.sh
nano ${PRJDIR}/conversion_rates/NEB_lambda/deduplication/Bismark_deduplication.lsf.sh

bsub < ${PRJDIR}/conversion_rates/NEB_lambda/deduplication/Bismark_deduplication.lsf.sh

bjobs
bbjobs 228550639

```


### Bismark methylation extractor

```bash
ls ${PRJDIR}/conversion_rates/NEB_lambda/methylextraction/*
rm -rf ${PRJDIR}/conversion_rates/NEB_lambda/methylextraction

mkdir ${PRJDIR}/conversion_rates/NEB_lambda/methylextraction
cd ${PRJDIR}/conversion_rates/NEB_lambda/methylextraction

TEMPLATE=TEMPLATE_Script_Bismark_MethylationExt_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_lambda/deduplication:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_lambda/methylextraction:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/conversion_rates/NEB_lambda/methylextraction/Bismark_methylextraction.lsf.sh

cat ${PRJDIR}/conversion_rates/NEB_lambda/methylextraction/Bismark_methylextraction.lsf.sh

bsub < ${PRJDIR}/conversion_rates/NEB_lambda/methylextraction/Bismark_methylextraction.lsf.sh

bjobs

```

### MultiQC Report

```bash

rm -rf ${PRJDIR}/reports/conversion_rates/NEB_lambda/MultiQC
mkdir -p ${PRJDIR}/reports/conversion_rates/NEB_lambda/MultiQC

ls ${PRJDIR}/reports/conversion_rates/NEB_lambda/MultiQC
cd ${PRJDIR}/reports/conversion_rates/NEB_lambda/MultiQC

TEMPLATE=TEMPLATE_Script_MultiQC_ConversionRates_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_lambda:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/reports/conversion_rates/NEB_lambda/MultiQC:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/reports/conversion_rates/NEB_lambda/MultiQC.lsf.sh

cat ${PRJDIR}/reports/conversion_rates/NEB_lambda/MultiQC.lsf.sh

bsub < ${PRJDIR}/reports/conversion_rates/NEB_lambda/MultiQC.lsf.sh

bjobs

```


#### NEB_pUC19

```bash
REFGENOMEDIR=/cluster/work/gdc/shared/p866/Data/Reference_Genomes/pUC19
REFGENOME=pUC19.fa

ls ${PRJDIR}/conversion_rates/NEB_pUC19/*
rm -rf ${PRJDIR}/conversion_rates/NEB_pUC19

mkdir -p ${PRJDIR}/conversion_rates/NEB_pUC19
cd ${PRJDIR}/conversion_rates/NEB_pUC19

echo $ARRAYLENGTH
# ARRAYLENGTHBK=${ARRAYLENGTH}
# ARRAYLENGTH=2
# ARRAYLENGTH=${ARRAYLENGTHBK}

pull_git_repo.sh

TEMPLATE=TEMPLATE_Script_Bismark_Alignment_LSF_Euler_220807

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/trim_galore:" \
-e "s:ZZREFGENOMEDIRZZ:${REFGENOMEDIR}:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_pUC19:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/conversion_rates/NEB_pUC19/Bismark_alignment.lsf.sh

cat ${PRJDIR}/conversion_rates/NEB_pUC19/Bismark_alignment.lsf.sh

cat ${ROOTLISTFILE}

## cp ${ROOTLISTFILE} ${ROOTLISTFILE}_alignment_rerun
## mv ${ROOTLISTFILE}_bk ${ROOTLISTFILE}

cat ${ROOTLISTFILE}
wc ${ROOTLISTFILE}

nano ${PRJDIR}/conversion_rates/NEB_pUC19/Bismark_alignment.lsf.sh

bsub < ${PRJDIR}/conversion_rates/NEB_pUC19/Bismark_alignment.lsf.sh

bjobs

bkill

# this dataset is stuck at 10per CPU usage
bkill 231853107[1]
# resubmitted as below
bbjobs 231864333

```
### Deduplication


```bash
ls ${PRJDIR}/conversion_rates/NEB_pUC19/deduplication/*
rm -rf ${PRJDIR}/conversion_rates/NEB_pUC19/deduplication

mkdir ${PRJDIR}/conversion_rates/NEB_pUC19/deduplication
cd ${PRJDIR}/conversion_rates/NEB_pUC19/deduplication


TEMPLATE=TEMPLATE_Script_Bismark_Deduplication_LSF_Euler_220807

pull_git_repo.sh


sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_pUC19:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_pUC19/deduplication:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/conversion_rates/NEB_pUC19/deduplication/Bismark_deduplication.lsf.sh

cat ${PRJDIR}/conversion_rates/NEB_pUC19/deduplication/Bismark_deduplication.lsf.sh
nano ${PRJDIR}/conversion_rates/NEB_pUC19/deduplication/Bismark_deduplication.lsf.sh

bsub < ${PRJDIR}/conversion_rates/NEB_pUC19/deduplication/Bismark_deduplication.lsf.sh

bjobs
bbjobs 228550639

```


### Bismark methylation extractor

```bash
ls ${PRJDIR}/conversion_rates/NEB_pUC19/methylextraction/*
rm -rf ${PRJDIR}/conversion_rates/NEB_pUC19/methylextraction

mkdir ${PRJDIR}/conversion_rates/NEB_pUC19/methylextraction
cd ${PRJDIR}/conversion_rates/NEB_pUC19/methylextraction

TEMPLATE=TEMPLATE_Script_Bismark_MethylationExt_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_pUC19/deduplication:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_pUC19/methylextraction:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/conversion_rates/NEB_pUC19/methylextraction/Bismark_methylextraction.lsf.sh

cat ${PRJDIR}/conversion_rates/NEB_pUC19/methylextraction/Bismark_methylextraction.lsf.sh

bsub < ${PRJDIR}/conversion_rates/NEB_pUC19/methylextraction/Bismark_methylextraction.lsf.sh

bjobs

```

### MultiQC Report

```bash

rm -rf ${PRJDIR}/reports/conversion_rates/NEB_pUC19/MultiQC
mkdir -p ${PRJDIR}/reports/conversion_rates/NEB_pUC19/MultiQC

ls ${PRJDIR}/reports/conversion_rates/NEB_pUC19/MultiQC
cd ${PRJDIR}/reports/conversion_rates/NEB_pUC19/MultiQC

TEMPLATE=TEMPLATE_Script_MultiQC_ConversionRates_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_pUC19:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/reports/conversion_rates/NEB_pUC19/MultiQC:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/reports/conversion_rates/NEB_pUC19/MultiQC.lsf.sh

cat ${PRJDIR}/reports/conversion_rates/NEB_pUC19/MultiQC.lsf.sh

bsub < ${PRJDIR}/reports/conversion_rates/NEB_pUC19/MultiQC.lsf.sh

bjobs

```
### Bismark reports

```bash
rm -rf ${PRJDIR}/reports/conversion_rates/NEB_pUC19/Bismark_reports
mkdir -p ${PRJDIR}/reports/conversion_rates/NEB_pUC19/Bismark_reports

ls ${PRJDIR}/reports/conversion_rates/NEB_pUC19/Bismark_reports
cd ${PRJDIR}/reports/conversion_rates/NEB_pUC19/Bismark_reports


TEMPLATE=TEMPLATE_Script_Bismark_Reports_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/conversion_rates/NEB_pUC19:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/reports/conversion_rates/NEB_pUC19/Bismark_reports:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/reports/Bismark_reports.lsf.sh

cat ${PRJDIR}/reports/Bismark_reports.lsf.sh

bsub < ${PRJDIR}/reports/Bismark_reports.lsf.sh

bjobs
bbjobs

```

### Alignment of methylation reads

Bismark needs ~ 5 cores with > 16 GB RAM for the alignment.
Do not sort by position! Bismark needs the BAM/SAM files to be sorted by contig name.
One sample at a time.

Trim_galore

```bash
ls ${PRJDIR}/alignment/*


rm -rf ${PRJDIR}/alignment

mkdir ${PRJDIR}/alignment
cd ${PRJDIR}/alignment

echo $ARRAYLENGTH
# ARRAYLENGTHBK=${ARRAYLENGTH}
# ARRAYLENGTH=2
# ARRAYLENGTH=${ARRAYLENGTHBK}

pull_git_repo.sh

TEMPLATE=TEMPLATE_Script_Bismark_Alignment_40GBFiles_LSF_Euler_220807

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/trim_galore:" \
-e "s:ZZREFGENOMEDIRZZ:${REFGENOMEDIR}:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/alignment:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/alignment/Bismark_alignment.lsf.sh

cat ${PRJDIR}/alignment/Bismark_alignment.lsf.sh

cat ${ROOTLISTFILE}

##mv ${ROOTLISTFILE} ${ROOTLISTFILE}_bkup
##mv ${ROOTLISTFILE}_alignment_rerun ${ROOTLISTFILE}

nano ${PRJDIR}/alignment/Bismark_alignment.lsf.sh

bsub < ${PRJDIR}/alignment/Bismark_alignment.lsf.sh
bash ${SCRIPTDIR}/Script_ResourceTracking_220807.lsf.sh 232576912 &

bjobs


```
### Deduplication


```bash
ls ${PRJDIR}/deduplication/*

rm -rf ${PRJDIR}/deduplication

mkdir ${PRJDIR}/deduplication
cd ${PRJDIR}/deduplication

TEMPLATE=TEMPLATE_Script_Bismark_Deduplication_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/alignment:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/deduplication:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/deduplication/Bismark_deduplication.lsf.sh

cat ${PRJDIR}/deduplication/Bismark_deduplication.lsf.sh


nano ${PRJDIR}/deduplication/Bismark_deduplication.lsf.sh

bsub < ${PRJDIR}/deduplication/Bismark_deduplication.lsf.sh

bjobs

```


### Bismark methylation extractor

```bash
ls ${PRJDIR}/methylextraction/*

rm -rf ${PRJDIR}/methylextraction

mkdir ${PRJDIR}/methylextraction
cd ${PRJDIR}/methylextraction

echo $ARRAYLENGTH

TEMPLATE=TEMPLATE_Script_Bismark_MethylationExt_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}/deduplication:" \
-e "s:ZZFILEROOTLISTZZ:${ROOTLISTFILE}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/methylextraction:" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/methylextraction/Bismark_methylextraction.lsf.sh

cat ${PRJDIR}/methylextraction/Bismark_methylextraction.lsf.sh
nano ${PRJDIR}/methylextraction/Bismark_methylextraction.lsf.sh

bsub < ${PRJDIR}/methylextraction/Bismark_methylextraction.lsf.sh

bash ${SCRIPTDIR}/Script_ResourceTracking_220807.lsf.sh  232574828 &


bjobs
```


### Bismark reports

```bash
ls ${PRJDIR}/reports/*

rm -rf ${PRJDIR}/reports

mkdir ${PRJDIR}/reports
cd ${PRJDIR}/reports


# ## Use the trim_galore rootlist
# cat ${PRJDIR}/trim_galore/FileRootList.txt
# ARRAYLENGTH=$(wc -l ${PRJDIR}/trim_galore/FileRootList.txt | awk '{print $1}')
# echo $ARRAYLENGTH

TEMPLATE=TEMPLATE_Script_Bismark_Reports_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/reports:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/reports/Bismark_reports.lsf.sh

cat ${PRJDIR}/reports/Bismark_reports.lsf.sh

bsub < ${PRJDIR}/reports/Bismark_reports.lsf.sh

bjobs

```



### MultiQC Report

```bash
rm -rf ${PRJDIR}/reports/MultiQC

ls ${PRJDIR}/reports/MultiQC

mkdir ${PRJDIR}/reports/MultiQC
cd ${PRJDIR}/reports/MultiQC

TEMPLATE=TEMPLATE_Script_MultiQC_LSF_Euler_220807

pull_git_repo.sh

sed -e "s:ZZINPUTDIRZZ:${PRJDIR}:" \
-e "s:ZZOUTPUTDIRZZ:${PRJDIR}/reports/MultiQC:" \
${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/reports/MultiQC.lsf.sh

cat ${PRJDIR}/reports/MultiQC.lsf.sh
nano ${PRJDIR}/reports/MultiQC.lsf.sh

bsub < ${PRJDIR}/reports/MultiQC.lsf.sh

bjobs
bbjobs

```


Run on local computer to view html summaries.
```bash

rsync --relative -avz rdaniels@euler.ethz.ch://cluster/scratch/rdaniels/rdaniels/Pollution_EMseq/./DpolGrei_All/reports/* /home/rjd/msRNA_local/Projects/Pollution_EMseq/Analyses/

rsync --relative -avz rdaniels@euler.ethz.ch://cluster/scratch/rdaniels/rdaniels/Pollution_EMseq/CorFlum_All/reports/* /home/rjd/msRNA_local/Projects/eawag_pollution_220801/Analyses/


```

##### Compression of intermediate files
Firstly, decide which of the files are worth keeping.
Delete those that are not and then compress the stages that have already been completed.

```bash


mkdir -p ${PRJDIR}/DataCompression
ls ${PRJDIR}/

mkdir -p ${GRPHOME}/Pollution_EMseq/Analyses/DpolGrei_All/DataCompression
ls ${GRPHOME}/Pollution_EMseq/Analyses/DpolGrei_All/DataCompression

du -hs ${GRPHOME}/Pollution_EMseq/Analyses/DpolGrei_All/DataCompression/*
du -sh ${PRJDIR}/*
#ls ${GRPDIR}/*  #This is the shared folder. the one above is the private space on the shared folder


cd ${PRJDIR}/DataCompression

  TEMPLATE=TEMPLATE_Script_zstd_compression_LSF_Euler_220807

 pull_git_repo.sh

sed -e "s:ZZWORKDIRZZ:${PRJDIR}:" \
-e "s:ZZINPUTFILEZZ:fastqc:" \
-e "s:ZZOUTPUTDIRZZ:${GRPHOME}/Pollution_EMseq/Analyses/DpolGrei_All/DataCompression:" \
  ${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/DataCompression/DataCompression.lsf.sh

 cat ${PRJDIR}/DataCompression/DataCompression.lsf.sh
bsub < ${PRJDIR}/DataCompression/DataCompression.lsf.sh

bash ${SCRIPTDIR}/Script_ResourceTracking_220807.lsf.sh  231712313

bbjobs
bjobs

sed -e "s:ZZWORKDIRZZ:${PRJDIR}:" \
-e "s:ZZINPUTFILEZZ:alignment:" \
-e "s:ZZOUTPUTDIRZZ:${GRPHOME}/Pollution_EMseq/Analyses/DpolGrei_All/DataCompression:" \
  ${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/DataCompression/DataCompression.lsf.sh

 cat ${PRJDIR}/DataCompression/DataCompression.lsf.sh
bsub < ${PRJDIR}/DataCompression/DataCompression.lsf.sh

bash ${SCRIPTDIR}/Script_ResourceTracking_220807.lsf.sh  231723062 & bg

bbjobs
bjobs


sed -e "s:ZZWORKDIRZZ:${PRJDIR}:" \
-e "s:ZZINPUTFILEZZ:deduplication:" \
-e "s:ZZOUTPUTDIRZZ:${GRPHOME}/Pollution_EMseq/Analyses/DpolGrei_All/DataCompression:" \
  ${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/DataCompression/DataCompression.lsf.sh

cat ${PRJDIR}/DataCompression/DataCompression.lsf.sh

bsub < ${PRJDIR}/DataCompression/DataCompression.lsf.sh

bash ${SCRIPTDIR}/Script_ResourceTracking_220807.lsf.sh  231712313  &

bbjobs
bjobs


sed -e "s:ZZWORKDIRZZ:${PRJDIR}:" \
-e "s:ZZINPUTFILEZZ:reports:" \
-e "s:ZZOUTPUTDIRZZ:${GRPHOME}/Pollution_EMseq/Analyses/DpolGrei_All/DataCompression:" \
  ${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/DataCompression/DataCompression.lsf.sh

 cat ${PRJDIR}/DataCompression/DataCompression.lsf.sh
bsub < ${PRJDIR}/DataCompression/DataCompression.lsf.sh

bash ${SCRIPTDIR}/Script_ResourceTracking_220807.lsf.sh  231715129  &


bbjobs
bjobs

sed -e "s:ZZWORKDIRZZ:${PRJDIR}:" \
-e "s:ZZINPUTFILEZZ:methylextraction:" \
-e "s:ZZOUTPUTDIRZZ:${GRPHOME}/Pollution_EMseq/Analyses/DpolGrei_All/DataCompression:" \
  ${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/DataCompression/DataCompression.lsf.sh

nano ${PRJDIR}/DataCompression/DataCompression.lsf.sh

cat ${PRJDIR}/DataCompression/DataCompression.lsf.sh

bsub < ${PRJDIR}/DataCompression/DataCompression.lsf.sh

bash ${SCRIPTDIR}/Script_ResourceTracking_220807.lsf.sh  231715241 &


bbjobs
bjobs




sed -e "s:ZZWORKDIRZZ:${PRJDIR}:" \
-e "s:ZZINPUTFILEZZ:trim_galore:" \
-e "s:ZZOUTPUTDIRZZ:${GRPHOME}/Pollution_EMseq/Analyses/DpolGrei_All/DataCompression:" \
  ${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/DataCompression/DataCompression.lsf.sh

 cat ${PRJDIR}/DataCompression/DataCompression.lsf.sh
bsub < ${PRJDIR}/DataCompression/DataCompression.lsf.sh

bash ${SCRIPTDIR}/Script_ResourceTracking_220807.lsf.sh  231715129  & bg


bbjobs
bjobs

sed -e "s:ZZWORKDIRZZ:${PRJDIR}:" \
-e "s:ZZINPUTFILEZZ:conversion_rates:" \
-e "s:ZZOUTPUTDIRZZ:${GRPHOME}/Pollution_EMseq/Analyses/DpolGrei_All/DataCompression:" \
  ${SCRIPTDIR}/${TEMPLATE}.lsf.sh > ${PRJDIR}/DataCompression/DataCompression.lsf.sh

nano ${PRJDIR}/DataCompression/DataCompression.lsf.sh
 cat ${PRJDIR}/DataCompression/DataCompression.lsf.sh
bsub < ${PRJDIR}/DataCompression/DataCompression.lsf.sh

bash ${SCRIPTDIR}/Script_ResourceTracking_220807.lsf.sh  231715241 & bg


bbjobs
bjobs

 ```
