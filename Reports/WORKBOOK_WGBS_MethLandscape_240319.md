---
Title : Methylation landscape
---

# Overview

Using methylkit/processed methylation information for profiling different regions.
This is done entirely in R and interactively.
For the time-being, all code is here, interactive and there are no automatic scripts.

## Background info

#### GFF/GVF
More recent that GTF. More flexible. TSS, UTRs are not explicitly reported but can be deduced.
https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md

#### GTF
Used narrowly for gene info. 
TSS, UTR can be explicitly reported.
http://mblab.wustl.edu/GTF22.html

#### GCF/GCA
GCF = NCBI maintained, aka RefSeq
GCA = aka, GenBank, Submitter owned, no additional annotations


## Installation

```bash
```

# Set up local environment

You need to run within a sing container.

```bash
# Ilifu
srun -x11 --pty --time=05:00:00 --mem=16GB bash
srun --pty --time=05:00:00 --mem=16GB bash


# Euler
srun --x11 --mem-per-cpu=5GB --pty --time=05:00:00 bash

# X FORWARDING DOES NOT WORK FOR ME
# login with -Y to get x forwarding

# With x support
sinteractive --x11 # ilifu only
srun --x11 --pty bash

srun --X11 --pty --time=05:00:00 --mem=8GB --qos qos-interactive bash
srun --X11 --pty bash 

```

## Data set

DONT USE THIS SETUP CODE. RATHER HOP OVER FROM THE METHYLKIT_ANALYSIS WORKBOOK.
ONLY ADD THE ANNOTATION FILES.

### Test

```bash
export FILEROOT=Test

## Euler
#export REFGENOMEDIR=/cluster/work/gdc/shared/p866/Data/Reference_Genomes/Deissena_polymorpha/ncbi-genomes-2022-07-18

## Ilifu
#export REFGENOMEDIR=${CMMNT}/Projects/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCA_020536995.1
#export REFGENOMEPRFX=GCA_020536995.1_UMN_Dpol_1.0_genomic

#export REFGENOMEDIR=${GRPDIR}/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCA_020536995.1
export REFGENOMEDIR=${GRPDIR}/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1
export REFGENOMEPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic

# Shorter Ref name
export TMPREF=Dpol

# Annotations
export GFFDIR=${REFGENOMEDIR}
export GFFPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic
#export GFFPRFX=genomic
# fna or fasta.gz

# Methylkit input
export METHKITRAWDIR=${CMMNT}/Projects/23_Epigenome/HPC/Analyses/Euler/MethylKit/Dpol_vV0y0
#out_vV0y0-DIq12-gmsCV_10x_pollution_nonpool_diff_pseudopool/methylDB/
#export METHKITRAWDIR=${GRPDIR}/23_Epigenome/HPC/Analyses/Euler/MethylKit_Dpol/out_diffmeth/methylDB/
export METHKITRAWPRFX=methylDiff_united_minpergrp
#0_230712_array_pseudopool_NP_PoolFALSE_n10_P_PoolFALSE_n8_230712_diffmeth_minovrlap0_230712
#.txt.bgz file

```

### Dreissena

```bash
export FILEROOT=Dpol

## Euler
#export REFGENOMEDIR=/cluster/work/gdc/shared/p866/Data/Reference_Genomes/Deissena_polymorpha/ncbi-genomes-2022-07-18

## Ilifu
#export REFGENOMEDIR=${CMMNT}/Projects/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCA_020536995.1
#export REFGENOMEPRFX=GCA_020536995.1_UMN_Dpol_1.0_genomic

#export REFGENOMEDIR=${GRPDIR}/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCA_020536995.1
export REFGENOMEDIR=${GRPDIR}/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1/methylseq
export REFGENOMEPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic

# Shorter Ref name
export TMPREF=Dpol

# Annotations
export GFFDIR=${REFGENOMEDIR}/../
export GFFPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic
#export GFFPRFX=genomic
# fna or fasta.gz

# Methylkit input
#export METHKITRAWDIR=${CMMNT}/Projects/23_Epigenome/HPC/Analyses/Euler/MethylKit/Dpol_vV0y0
#out_vV0y0-DIq12-gmsCV_10x_pollution_nonpool_diff_pseudopool/methylDB/
#export METHKITRAWDIR=${GRPDIR}/23_Epigenome/HPC/Analyses/Euler/MethylKit_Dpol/out_diffmeth/methylDB/
#export METHKITRAWPRFX=methylDiff_united_minpergrp


```


### Corbicula

```bash

export FILEROOT=Test

## Euler
#export REFGENOMEDIR=/cluster/work/gdc/shared/p866/Data/Reference_Genomes???

## Ilifu
export REFGENOMEDIR=${CMMNT}/Projects/23_Epigenome/Resources/reference_genomes/Corbicula_fluminea/Lachesis/
export REFGENOMEPRFX=Lachesis_assembly_changed.fa
# fna or fasta.gz


# Shorter Ref name
export TMPREF=Cflu

# Annotations
export GFFDIR=${REFGENOMEDIR}
export GFFPRFX=Final_corrected_assemly_genome_gene
# gff gff3

# Methylkit input
export METHKITRAWDIR=${CMMNT}/Projects/23_Epigenome/HPC/Analyses/Euler/MethylKit/Cflu_vV0y0/out_vV0y0-DIq12-gmsCV_10x_pool_pol_diff_pseudopool/methylDB/

export METHKITRAWPRFX=methylDiff_united_minpergrp_0_230711_array_pseudopool_P_PoolFALSE_n7_poolP_PoolTRUE_n1_230712_diffmeth_minovrlap0_230712
#.txt.bgz file



```



## Set up
```bash
# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=MethylLandscape

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi
# =======

# File with the names of all the files to run
export ROOTLISTFILE=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming/${FILEROOT}_FileRootList.txt
export FILEROOTLIST=${ROOTLISTFILE}

# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')
echo $ARRAYLENGTH
## ARRAYLENGTH=2

# =======
# DIRECTORIES
# =======

export RESRCDIR=${PRJDIR}/../Resources
export RDSDIR=${RESRCDIR}/R_RelationalDB
RDBASE=${RDSDIR}

# Trimmed data
export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming
#export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming

# Aligned data
export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Align_${TMPREF}/out_Bismark_Align_${TMPREF}
#export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_alignment/out_Bismark_alignment

# Dedup data
export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Dedup_${TMPREF}/out_Bismark_Dedup_${TMPREF}
#export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_deduplication/out_Bismark_deduplication

# Methyl call
export METHDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_MethylExt_${TMPREF}/out_Bismark_MethylExt_${TMPREF}

# BSSnper dir
export BSSNPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/BSSnper_${TMPREF}/out_BSSnper_${TMPREF}

# Variant Calling dir
export SNPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/VariantCalling_${TMPREF}/out_VariantCalling_${TMPREF}

# Methylkit preprocessing dir
export METHKITDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/MethylKit_${TMPREF}_mincov${MINCOVFILTER}/out_MethylKit_${TMPREF}_mincov${MINCOVFILTER}

```


## Data Prep

### Test

```bash

# Links to the input data
# Conflicted about whether this is necessary or not
mkdir -p $METHKITDIR

find ${METHKITRAWDIR}  -iname "${METHKITRAWPRFX}*txt.bgz"  -type f | xargs -I {} ln -s "{}" ${METHKITDIR}/ 

```


# Common Code

## Startup in R

```bash
# This is very old
#singularity exec ${CONTAINERDIR}/PopGenomics_1.1_220522.sif R

# or 
# Try to use the same as the sing container

# On Euler
module load  gcc/8.2.0 curl/7.73.0 r/4.2.2

# On Ilifu
#!! Only R/RStudio2022.12.0-353-R4.2.2 seems to be working
module load  R/RStudio2022.12.0-353-R4.2.2
R

```



```R
#! /bin/R

# ========
# Run Startup scripts
# ========
CMMNT = Sys.getenv(c('CMMNT'))

# Load Env variables
source(paste0(CMMNT,'/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.R'))

# Load general R stuff
# This is now in the Env vars step above
source(paste0(CODEDIR,'/StartupScript_Sources_260220.R'))

# =============


# ============
if(FALSE){
args = commandArgs(TRUE)

#CMMNT = args[1]
INPUTDIR = args[2]
OUTPUTDIR = args[3]
EIGENPRFX = args[4]
GENOMAP = args[5]
PHENOMAP = args[6]
}

if(TRUE){

OUTPUTDIR = Sys.getenv(c('OUTPUTDIR'))

INPUTDIR = Sys.getenv(c('INPUTDIR'))
EIGENPRFX = Sys.getenv(c('EIGENPRFX'))
GENOMAP = Sys.getenv(c('GENOMAP'))
PHENOMAP = Sys.getenv(c('PHENOMAP'))

INPUTDIR = Sys.getenv(c('INPUTDIR'))
EIGENPRFX = Sys.getenv(c('EIGENPRFX'))
GENOMAP = Sys.getenv(c('GENOMAP'))
PHENOMAP = Sys.getenv(c('PHENOMAP'))

METHKITRAWDIR=Sys.getenv(c('METHKITRAWDIR'))
METHKITRAWPRFX=Sys.getenv(c('METHKITRAWPRFX'))


GFFDIR=Sys.getenv(c('GFFDIR'))
GFFPRFX=Sys.getenv(c('GFFPRFX'))

METHKITRAWDIR=PRJDIR
METHKITDIR= PRJDIR
METHKITRAWPRFX= 'methylDiff_'

}


# ==========

#BiocManager::install(c('genomation', 'AnnotationHub', 'GenomicFeatures', 'methylKit', 'VariantAnnotation'), force=F)
#BiocManager::install(c('methylKit', 'VariantAnnotation'), force=F)

pacman::p_load(papeR, tidyverse, magrittr, ggthemes, e1071, genomation, AnnotationHub, GenomicFeatures, methylKit)

# ============

device2use='svg'
stopifnot(
{device2use %in% c('jpeg','jpg','png','pdf','svg','eps')})

# ==========
# Load functions
# ==========
source(paste0(CODEDIR,'/Rsourcecode/Functions_EMseq_MethylKit_221124.R'))

# ==========

#INPUTSTRING = NULL

stopifnot(
is.character(INPUTDIR),
is.character(OUTPUTDIR),
is.character(EIGENPRFX),
is.character(PHENOMAP))

# assert(
# is.character(INPUTDIR),
# is.character(OUTPUTDIR),
# is.character(EIGENPRFX),
# is.character(PHENOMAP),
# #is.character(GENOMAP),
# msg='ERR:INPUTSTRING/EIGENPRFX not a char string.',
# stop=T)

print(paste('Variables in use are ...' ))
print(CMMNT)
print(INPUTDIR)
print(EIGENPRFX) 
print(GENOMAP) 
print(PHENOMAP) 

# ============
print(paste('OUTPUTDIR   is ...',	OUTPUTDIR  ))

```

## Load variables
```R
# =========
# RDS
# =========
sitesxfile.db = readRDS(paste0(RDSDIR,'/','DB_DiffMethUpSetOverlap','.RDS'))

sitesxfile.db %>% str

# ===========
# Create granges lists
# ===========
# ONE SPP AT A TIME 
# Split by hyper/hypo and by library treatment
# Create a dummie column with the accumulated grouping variables


sitesxfile.db %>% names

sitesxfile.db %<>%
dplyr::filter(species == 'Dpol') %>%
dplyr::filter( qvalue < 0.01 ) %>%
dplyr::filter( {abs(meth.diff) > 9.99} ) %>%
mutate(group.key = paste0(pool, tile, pollution, minpergrp, hyper)) %>%
group_by(group.key) %>%
group_split(.keep = TRUE) 

str(sitesxfile.db)

 group.keys.vec = sapply(
sitesxfile.db,
function(x){
x$group.key %>% unique %>% return
})


sitesxfile.db %<>%  lapply(
.,
as.data.frame
)


dat2process.trim.gr.list = lapply(
sitesxfile.db,
as,
"GRanges"
)


names(dat2process.trim.gr.list) =  group.keys.vec 

```


```INACTIVE

filepaths.vec = list.files(
pattern = paste0(METHKITRAWPRFX,'.*.txt.bgz$'),
path = METHKITRAWDIR,
recursive = T,
include.dirs = F,
full.names = T
) %>%
.[grepl('archive',.) %>%`!`] 


filepaths.vec %<>%
grep( 'Euler',.,value = T, invert= T) %>%
grep( 'SubsetDiff10',.,value = T, invert= F) %>%
grep( '-rUT',.,value = T, invert= F) %>%
grep( '.tbi',.,value = T, invert= T) 

diffmeth.list =  lapply(
filepaths.vec,
readMethylDB
)

head(diffmeth.list)
str(diffmeth.list)


# ===============
# Get file names/lables
# ===============

string2strip = c(
'out_vV0y0-DIq12-gmsCV' = '',
'[Mm]ethyl[dD]iff_united' = '',
'diffmeth' = '',
'diff' = '',
'230712' = '',
'array' = '',
'txt.bgz' = '',
'[Pp]ollution' = 'P',
'[Pp]ool' = 'Pl',
'[Pp]seudo' = 'Ps',
'[Nn]on' = 'Nn',
'minpergrp' = 'mpg',
'minovrlap' = 'mol'
)



filename.split.df = filepaths.vec %>%
lapply(.,
  fn.ExtractFromPath,
  part2pull = c(11,13)
  )  %>%
  lapply(
  .,
  function(x){
   x %>% 
  sapply(
  .,
  fn.MakeDfFromString,
  string2strip =  string2strip,
  part2pull = c(1:14)
  )   %>%
  unlist %>%
  as.matrix %>% 
  t %>% 
   as.data.frame %>% 
   return  
  }) %>%
  bind_rows

  
  
filename.split.vec = filename.split.df %>% 
apply(.,1, paste, collapse = '') %>%
sapply(
.,
fn.StripTextOf,
c('[NA]*' = '')
)
  
# Get hypo and hyper lists

myDiff25p.hyper.list = lapply(
diffmeth.list,
getMethylDiff,
difference=25,
qvalue=0.01,
type="hyper",
save.db = F
)


myDiff25p.hypo.list = lapply(
diffmeth.list,
getMethylDiff,
difference=25,
qvalue=0.01,
type="hypo",
save.db = F
)



# faux filtering just to get the same object type for all lists
myDiffAll.list = lapply(
diffmeth.list,
getMethylDiff,
difference=0,
qvalue=1,
type="all",
save.db = F
)


names(myDiffAll.list) = filename.split.vec
names(myDiff25p.hyper.list) = filename.split.vec
names(myDiff25p.hypo.list) = filename.split.vec

dat2process.list = list(
myDiffAll.list,
myDiff25p.hyper.list,
myDiff25p.hypo.list
)


names(dat2process.list) = c('All',
'25p.hyper',
'25p.hypo'
)


str(dat2process.list)

# ==============
# Clear empty data sets
# ==============

dat2process.trim.list = lapply(
dat2process.list,
function(x){

sapply(
x,
function(y){
dim(y)[1] > 0
}) %>%
unlist %>%
x[.] %>% 
return

})

# ===========
# Create granges lists
# ===========

dat2process.trim.gr.list = lapply(
dat2process.trim.list,
function(x){
lapply(
x,
as,
"GRanges"
)
})


```


## Read in features from annotation hub
Don't go down this route ... it ends with a sqlite object that needs to be reformated. ...
Just down load the data files from USCS and read them in here.

```R
# get annotation snapshot

annhub.snapshot = AnnotationHub()
annhub.snapshot

# Search for target species
unique(annhub.snapshot$species) %>%
grep("Dreissena poly",., value=T)

# exists!!

# search for data classes
unique(annhub.snapshot$rdataclass)

spp.annhub  = query(annhub.snapshot, c("Dreissena polymorpha")) # does not have our spp

# Only one record


spp.annhub %<>% .[[names(spp.annhub)]]

mcols(spp.annhub)


# If needed, filter for species
# ================
pacman::p_load(GenomicFeatures)
# make TxDb from GFF files

dpol.tx.id.tmp1 = loadTaxonomyDb()$genus %>% grep("^Dreissena",.,value=F)
dpol.tx.id.tmp2  = loadTaxonomyDb()$species %>% grep("^polymorpha",.,value=F)
dpol.tx.id = dpol.tx.id.tmp1 %in% dpol.tx.id.tmp2 %>% dpol.tx.id.tmp1[.] %>% loadTaxonomyDb()[.,]
dpol.tx.id
# ================
# 

keytypes(spp.annhub)
columns(spp.annhub)

spp.annhub %>% str

table(spp.annhub$sourcetype)

```


## Read in GFF 

Rather use the GTF. It is more explicit for gene features.

```R

GFFDIR=Sys.getenv(c('GFFDIR'))
#GFFDIR='/users/rjdaniels/Projects/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1'

GFFPRFX=Sys.getenv(c('GFFPRFX'))
#GFFPRFX='GCF_020536995.1_UMN_Dpol_1.0_genomic'


file2read = paste0(GFFDIR,'/',GFFPRFX,'.gff.gz')

annot.gff.gr =  genomation::gffToGRanges(
file2read,
zero.based = FALSE,
ensembl = FALSE)

annot2use.gr  = annot.gff.gr

# ==============
# Summary
# ==============

summary(annot2use.gr )

names(mcols(annot2use.gr ))

head(mcols(annot2use.gr ))
tail(mcols(annot2use.gr ))

mcols(annot2use.gr )$type %>% table

apply(
mcols(annot2use.gr )[,c('source','type','gbkey','gene_biotype')],
2,
table
)

# ==============

```


## Read in GTF 

```R

file2read = paste0(GFFDIR,'/',GFFPRFX,'.gtf.gz')

# GTF and GFF are NOT 0-based
annot.gtf.gr =  genomation::gffToGRanges(
file2read,
zero.based = FALSE,
ensembl = FALSE)

# Alternatively, use rtracklayer
annot.gtf.gr  = rtracklayer::import(file2read)

annot2use.gr  = annot.gtf.gr

# ==============
# Summary
# ==============

summary(annot2use.gr )

names(mcols(annot2use.gr ))
head(mcols(annot2use.gr ))
tail(mcols(annot2use.gr ))

mcols(annot2use.gr)$type %>% table

apply(
mcols(annot2use.gr )[,c('source','type','gbkey','gene_biotype','transcript_biotype')],
2,
table
)

# ============== 

pacman::p_load(GenomicDistributions)

tss.gr = GenomicDistributions::getTssFromGTF(
  file2read,
  convertEnsemblUCSC = FALSE,
  destDir = NULL,
  filterProteinCoding = TRUE)

```

## Read in Promoters

Use the GTF data.


```R

# Get genes
# Get promoters
# Follow Britta's lead and use  upstream = 1500, downstream = 500

promoters.genes.gtf.gr = GenomicFeatures::makeTxDbFromGFF(file2read) %>%
genes %>% 
promoters(. , upstream = 1500, downstream = 500) 

```


## Read in CpG

bb format is bigbed.

```R

#GFFDIR='/users/rjdaniels/Projects/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1'

CPGPRFX='GCF_020536995.1_UMN_Dpol_1.0.cpgIslandExt.bb'

cpg.bb.gr =  paste0(GFFDIR,'/bbi/',CPGPRFX) %>%
genomation::gffToGRanges(
.,
zero.based = FALSE,
ensembl = FALSE)

# Alternatively, use rtracklayers ... it should work well
cpg.bb.gr = paste0(GFFDIR,'/bbi/',CPGPRFX) %>% 
rtracklayer::import(.,format='bb')

cpg.bb.gr

# ==============
# Summary
# ==============

summary(cpg.bb.gr)

names(mcols(cpg.bb.gr))

head(mcols(cpg.bb.gr))
tail(mcols(cpg.bb.gr))

mcols(cpg.bb.gr)$perCpg %>% summary

# ==============

# There is a methylkit function that covers the genomation one
cpg.flanks.bb.gr = genomation::getFlanks( grange = cpg.bb.gr, clean = T)

# annotateWithFeatureFlank(xx, cpg.bb.gr , cpg.flanks.bb.gr)

```

### Annotation list

```R
annot.list = list(
annot.gtf.gr,
tss.gr,
promoters.genes.gtf.gr,
cpg.bb.gr,
cpg.flanks.bb.gr
)

# Start off simple, just using gtf file
annot2use.gr = annot.list[[1]]


```



### Read in by loop 

To split by feature...

```R

feature.col2use = 'type'

feature.vec =  mcols(annot2use.gr)[,feature.col2use] %>% unique

## Region = Entire chr ?
# tmp.idx = mcols(annot2use.gr) %>% {.$type == 'region' } %>% which
# seq(annot2use.gr[tmp.idx])

annot2use.gr.list = lapply(
feature.vec,
function(x){
genomation::gffToGRanges(
file2read ,
filter = x,
zero.based = FALSE,
ensembl = FALSE)
}) %>%
unlist(., recursive = F) %>%
as(.,'GRangesList') %>%
 GRangesList()

 
names(annot2use.gr.list) = feature.vec
annot2use.gr.list %>% names

annot2use.gr.list %<>% GRangesList() 
annot2use.gr.list %>% class

```

### Compare Seq Names


The USCS files use different chr names 
These need to be changed

NOT NEEDED FOR THE NEW RUNS.
```R
#seqlevels(dat2process.trim.gr.list[[1]] %>% .[[1]] ) %in% seqlevels(annot2use.gr.list )
seqlevels(dat2process.trim.gr.list[[1]]) %in% seqlevels(annot2use.gr.list )

# Get the map for chr aliases
CHRALIAS='GCF_020536995.1.chromAlias.txt'

chr.map = paste0(GFFDIR,'/',CHRALIAS) %>%
read.delim()

# Find the appropriate col to use for the gff
tmp.col.idx = sapply(
1:dim(chr.map)[2],
function(x){
seqlevels(annot2use.gr.list ) %>%
head %>%
{.} %in% chr.map[,x] %>%
all
}) %>% which


# double check that it is correct
seqlevels(annot2use.gr.list ) %in% chr.map[,tmp.col.idx]


# Find the appropriate col to use for the methyl data
tmp.col.meth.idx = sapply(
1:dim(chr.map)[2],
function(x){
seqlevels(dat2process.trim.gr.list[[1]] %>% .[[1]] ) %>%
head %>%
{.} %in% chr.map[,x] %>%
all
}) %>% which


# double check that it is correct
seqlevels(dat2process.trim.gr.list[[1]] %>% .[[1]]  ) %in% chr.map[,tmp.col.meth.idx]

# Rename one of the seqnames, preferably the gff
seqlevels(annot2use.gr.list ) %<>%
match(., chr.map[,tmp.col.idx]) %>%
chr.map[.,tmp.col.meth.idx]

```


## Annotate
Methylkits tools are defunct in favour of genomation. Use that instead.


```R 
dat2process.trim.gr.feature.list =  lapply(
dat2process.trim.gr.list,
annotateWithFeatures,
features = annot2use.gr.list ,
intersect.chr = TRUE
)


# annotateWithGeneParts has trouble  ... to be resolved

# Diff25p.hypo.annot.trim.gr.genepart.list =  lapply(
# Diff25p.hypo.trim.gr.list,
# annotateWithGeneParts,
# feature = annot2use.gr.list,
# intersect.chr = TRUE
# )

# Diff25p.hyper.annot.trim.gr.genepart.list =  lapply(
# Diff25p.hyper.trim.gr.list,
# annotateWithGeneParts,
# feature = annot2use.gr.list,
# intersect.chr = TRUE
# )

# annotateWithFeatures(Diff25p.hyper.trim.gr.list[[3]],
# features = annot2use.gr.list,
# intersect.chr = TRUE)


# This needs annotate by gene part...
#Diff25p.hyper.annot.trim.gr.TSS.list = getAssociationWithTSS(Diff25p.hyper.annot.trim.gr.features.list[[1]])

# ==============
# Clear anything with less than 3 entries
# ==============


#NOT WORKING FOR RDS READ IN
dat2process.trim2.gr.feature.list =  dat2process.trim.gr.feature.list


dat2process.trim2.gr.feature.list =  lapply(
dat2process.trim.gr.feature.list,
function(x){
tmp.idx = sapply(
x,
function(y){
genomation::getMembers(y) %>% 
{dim(.)[1] > 3}
})
x[tmp.idx ] %>% return
})


```

### Create annotated GR

This process can take a very long time. 
Save the output.

```R

dat2process.trim2.gr.feature.sumtab.pretidy.list =  lapply(
dat2process.trim2.gr.feature.list,
fn.MakeAnnotSummary
)

dat2process.trim2.gr.feature.sumtab.pretidy.list %>% names
dat2process.trim2.gr.feature.sumtab.pretidy.list %>% str

```

```R
# =========
# RDS
# =========
INPUTFILE= 'DB_GenomicRanges_Methyl_Annotated'
OUTPUT = paste0(RDSDIR,'/',INPUTFILE,'_',GFFPRFX,'.RDS')
CLEANOUTPUT = paste0(RDSDIR,'/',INPUTFILE,'.RDS')

# ===========
dat2process.trim2.gr.feature.sumtab.pretidy.list %>% saveRDS(OUTPUT)
system(paste('ln -vfns',OUTPUT,CLEANOUTPUT))

# =========
# Read RDS
# =========
dat2process.trim2.gr.feature.sumtab.pretidy.list = readRDS(CLEANOUTPUT)

```

```INACTIVE
# Save RDS

plottit2use = paste0(GFFPRFX)

OUTPUT = paste0(OUTPUTDIR, "/GenomicRanges_Methyl_Annotated_",plottit2use )
print(paste0('Output saved to ... ',OUTPUT ))

dat2process.trim2.gr.feature.sumtab.pretidy.list %>%
saveRDS(
.,
paste0(OUTPUT,'.RDS')
)

```


#### Reduce the summary

Read in the RDS.
```R
# =========
# RDS
# =========
dat2process.trim2.gr.feature.sumtab.pretidy.list = readRDS(paste0(RDSDIR,'/','DB_GenomicRanges_Methyl_Annotated','_',GFFPRFX,'.RDS'))

```

##### For GFF

_!!DO FOR GFF!!_
_!DO NOT DO FOR GTF!_


```R
# To get labels for broader categories

dat2process.trim2.gr.feature.sumtab.list = dat2process.trim2.gr.feature.sumtab.pretidy.list %>%
lapply(
.,
function(x){
lapply(
x,
function(y){
y %>%
group_by(category.parent) %>%
dplyr::summarise(
tally2use = sum(tally),
prop2use = sum(prop),
perc2use = as.numeric(perc) %>% sum()
 ) %>%
 mutate(category2use = category.parent) %>%
 return
})

})


```


##### For GFT etc
If we did not use the summary function above...
We use this one...


At this point it is better to dissolve the entire list into one df to keep the figure legends standardised.
```R
# Won't work for RDS readin
#names(dat2process.trim2.gr.feature.sumtab.pretidy.list ) = names(dat2process.list)


dat2process.trim2.gr.feature.sumtab.df = dat2process.trim2.gr.feature.sumtab.pretidy.list  %>% 
bind_rows(,.id='data.type')


str(dat2process.trim2.gr.feature.sumtab.df)

dat2process.trim2.gr.feature.sumtab.df  %<>%
mutate(
hyper = case_when(
grepl('TRUE',data.type) ~ T,
grepl('FALSE',data.type) ~ F
) ,
scheme = case_when(
grepl('poolComp',data.type) ~ 'poolComp',
grepl('poolT',data.type) ~ 'poolT',
grepl('poolF',data.type) ~ 'poolF'
) %>% factor(., levels = c('poolF','poolT','poolComp'))
) 


```




```R

str(dat2process.trim2.gr.feature.sumtab.df)
dat2use = dat2process.trim2.gr.feature.sumtab.df

# To force a particular position for the bar stack and text on the plot
# I need to set a or

# !! not done !!

category.parent.lvls = dat2use$category.parent %>% unique %>% .[c(2, 1,3)]


# code.name.reduced.lvls = 
# dat2use$code.name.reduced %>% unique %>% .[c(1,2,3)]


dat2use %<>%
mutate(
y2use = as.numeric(tally),
group2orderby1 = category.parent %>% factor(., category.parent.lvls),
group2orderby2 = code.name.reduced %>% factor,
group2shpby = code.name.reduced %>% factor,
group2fillby = code.name.reduced %>% factor,
group2colby = category.parent %>% factor(., category.parent.lvls),
group2labby2 = as.numeric(tally),
group2labby = as.numeric(perc)
 ) 
  
# dat2plot.plots.df.list =  lapply(
# names(dat2process.trim2.gr.feature.sumtab.list),
# function(dat2plot.char){
# 
# tmp.df.list  = dat2process.trim2.gr.feature.sumtab.list[[dat2plot.char]]
# 
# tmp.df.list %<>%
# lapply(
# .,
# function(x){
# x %>%
# mutate(
# y2use = tally,
# group2orderby1 = category.parent,
# group2orderby2 = code.name.reduced,
# group2shpby = category.parent,
# group2fillby = code.name.reduced,
# group2colby = category.parent,
# group2labby2 = as.numeric(tally),
# group2labby = as.numeric(perc)
#  ) %>%
#  return 
# })
# 
# return(tmp.df.list)
# 
# })
# names(dat2plot.plots.df.list) = names(dat2process.trim2.gr.feature.sumtab.list)

```





### Plot pie charts


Produce a list of dfs for plotting

```INACTIVE

# 
# dat2plot.plots.list = lapply(
# names(dat2plot.plots.df.list),
# function(dat2plot){
# 
# #names(get(dat2plot))
# #length(plots.list )
# 
# # lapply(
# # 1:length(get(dat2plot)),
# # function(x){
# # fnPlotPieChart(dat2input = get(dat2plot)[[x]], plottit2use = x) %>% return
# # }) %>% return
# # })
# 
# tmp.dat = dat2plot.plots.df.list[[dat2plot]]
# 
# lapply(
# 1:length(tmp.dat),
# function(x){
# fnPlotPieChart(dat2input = tmp.dat[[x]], plottit2use = x) %>%
#  return
# }) %>% 
# return
# })
# 
# names(dat2plot.plots.list) =  names(dat2plot.plots.df.list)


# names(dat2use)
# 
# tmp.dat.df = dat2use %>%
# dplyr::select(
# data.type,
# scheme
# ) %>%
# unique 


## Should be done when reading in the data
# # subset for the tile data 
# tmp.dat.df %<>%
# filter(grepl('tile', scheme)) %>%
# filter(!grepl('mpg1mol1.10xPnPltilenPsPl', scheme))

 
# lapply(
# 1:length(tmp.dat),
# function(x){
# fnPlotPieChart(dat2input = tmp.dat[[x]], plottit2use = x) %>%
#  return
# }) %>% 
# return


#names(dat2plot.plots.list) =  names(dat2plot.plots.df.list)

```


### Create guide for plots

For a legend that is consistent across plots, the guide needs to be produced outside of the individual plot calls

```R

guide2use = dat2use %>%
dplyr::select(contains('group2')) %>%
unique %>%
group_by(
group2fillby,
group2colby
) %>%
arrange(
group2orderby1,
group2orderby2
) %>% 
tibble::rowid_to_column("guide.idx") %>%
fn.MakePlotGuides(dat2input = .)


```


#### Bar charts 

```R


dat2plot.plot = fnPlotGTFBar(dat2input = dat2use, plottit2use = 'all', guide2input = guide2use ) 


tmp.plottit  = paste0(GFFPRFX,'_AllData')

# Final plots ...
OUTPUT = paste0(OUTPUTDIR, "/Plot_PieChart_",tmp.plottit )
print(paste0('Output saved to ... ',OUTPUT ))


# #   Create legend
plot.leg = fn.g.legend(
dat2plot.plot +  theme(
          legend.direction="vertical",
          legend.position = "top",
          legend.justification=c(0,1)
          )
          ) 


plot2plot =  ggdraw() +
draw_plot(
dat2plot.plot + theme(legend.position = "none"),
 x = 0, y = 0, width = 1, height = 0.8) +
draw_plot(
plot.leg ,
 x = 0, y = 0.8, width = 1, height = 0.2)

 
plot2plot %>% ggsave(.,
  filename = paste0(OUTPUT,".",device2use),
  width=210,
  height=200,
  device=device2use,
  units = 'mm')



# lapply(
# names(dat2plot.plots.list),
# function(dat2plot){
# 
# # Final plots ...
# OUTPUT = paste0(OUTPUTDIR, "/Plot_PieChart_",dat2plot,'_gtf')
# print(paste0('Output saved to ... ',OUTPUT ))
# 
# #tmp.idx = names(get(dat2plot)) %>% grepl(.,'tile')
# # all tiled!
# # To get the plots to match the main figures, use :
# # - (A) Individual sequences  3
# # - (B) Pooled 6
# # - (C) Computationally pooled 4
# 
# plots.list = dat2plot.plots.list[[dat2plot]]
# 
# # #   Create legend
# plot.leg = fn.g.legend(
# plots.list[[4]] +  theme(
#           legend.direction="vertical",
#           legend.position = "bottom",
#           legend.justification=c(0,1)
#           )
#           ) 
# 
# 
# plot2plot =  ggdraw() +
# draw_plot(
# plots.list[[3]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(A)', x = 1.65, y = 0.0),
#  x = 0, y = 0, width = 1, height = 0.80) +
# draw_plot(
# plots.list[[6]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(B)', x = 1.65, y = 0.0),
#  x = 0.33, y = 0, width = 1, height = 0.80) +
# draw_plot(
# plots.list[[4]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(C)', x = 1.65, y = 0.0),
#  x = 0.66, y = 0, width = 1, height = 0.80) +
# draw_plot(
# plot.leg ,
#  x = 0, y = 0.80, width = 1, height = 0.2)
# 
# plot2plot %>% ggsave(.,
#   filename = paste0(OUTPUT,".",device2use),
#   width=170,
#   height=100,
#   device=device2use,
#   units = 'mm')
# 
# })


```


#### PIE CHARTS

Defunct code .... use the bar plot code
```R

# dat2plot.plots.list %>% str
# dat2plot.plots.list %>% names


dat2plot.plots.list = lapply(
1:dim(tmp.dat.df)[1],
function(tmp.idx){

tmp.dat = dat2use %>%
filter(scheme == tmp.dat.df$scheme[tmp.idx]) %>%
filter(data.type == tmp.dat.df$data.type[tmp.idx])

tmp.plottit = tmp.dat.df[tmp.idx,] %>%
apply(., 1, paste, collapse = '_')


# This is not piechart code any longer
fnPlotPieChart(dat2input = tmp.dat, plottit2use = tmp.plottit, guide2input = guide2use )  %>% return

 })

dat2plot.plots.list
str(dat2plot.plots.list)
length(dat2plot.plots.list)

tmp.plottit  = paste0(GFFPRFX,'_AllData')

# Final plots ...
OUTPUT = paste0(OUTPUTDIR, "/Plot_PieChart_",tmp.plottit )
print(paste0('Output saved to ... ',OUTPUT ))


# #   Create legend
plot.leg = fn.g.legend(
dat2plot.plots.list[[1]] +  theme(
          legend.direction="vertical",
          legend.position = "top",
          legend.justification=c(0,1)
          )
          ) 


plot2plot =  ggdraw() +
# draw_plot(
# dat2plot.plots.list[[7]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(G)', x = 4.0, y = 0.0),
#  x = 0, y = 0, width = 0.33, height = 0.30) +
# draw_plot(
# dat2plot.plots.list[[8]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(H)', x = 4.0, y = 0.0),
#  x = 0.33, y = 0, width = 0.33, height = 0.30) +
# draw_plot(
# dat2plot.plots.list[[9]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(I)', x = 4.0, y = 0.0),
#  x = 0.66, y = 0, width = 0.33, height = 0.30) +
draw_plot(
dat2plot.plots.list[[4]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(D)', x = 4.0, y = 0.0),
 x = 0, y = 0.30, width = 0.33, height = 0.30) +
draw_plot(
dat2plot.plots.list[[5]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(E)', x = 4.0, y = 0.0),
 x = 0.33, y = 0.30, width = 0.33, height = 0.30) +
draw_plot(
dat2plot.plots.list[[6]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(F)', x = 4.0, y = 0.0),
 x = 0.66, y = 0.30, width = 0.33, height = 0.30) +
draw_plot(
dat2plot.plots.list[[1]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(A)', x = 4.0, y = 0.0),
 x = 0, y = 0.60, width = 0.33, height = 0.30) +
draw_plot(
dat2plot.plots.list[[2]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(B)', x = 4.0, y = 0.0),
 x = 0.33, y = 0.60, width = 0.33, height = 0.30) +
draw_plot(
dat2plot.plots.list[[3]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(C)', x = 4.0, y = 0.0),
 x = 0.66, y = 0.60, width = 0.33, height = 0.30) +
draw_plot(
plot.leg ,
 x = 0, y = 0.90, width = 1, height = 0.1)

 
plot2plot %>% ggsave(.,
  filename = paste0(OUTPUT,".",device2use),
  width=210,
  height=200,
  device=device2use,
  units = 'mm')



# lapply(
# names(dat2plot.plots.list),
# function(dat2plot){
# 
# # Final plots ...
# OUTPUT = paste0(OUTPUTDIR, "/Plot_PieChart_",dat2plot,'_gtf')
# print(paste0('Output saved to ... ',OUTPUT ))
# 
# #tmp.idx = names(get(dat2plot)) %>% grepl(.,'tile')
# # all tiled!
# # To get the plots to match the main figures, use :
# # - (A) Individual sequences  3
# # - (B) Pooled 6
# # - (C) Computationally pooled 4
# 
# plots.list = dat2plot.plots.list[[dat2plot]]
# 
# # #   Create legend
# plot.leg = fn.g.legend(
# plots.list[[4]] +  theme(
#           legend.direction="vertical",
#           legend.position = "bottom",
#           legend.justification=c(0,1)
#           )
#           ) 
# 
# 
# plot2plot =  ggdraw() +
# draw_plot(
# plots.list[[3]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(A)', x = 1.65, y = 0.0),
#  x = 0, y = 0, width = 1, height = 0.80) +
# draw_plot(
# plots.list[[6]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(B)', x = 1.65, y = 0.0),
#  x = 0.33, y = 0, width = 1, height = 0.80) +
# draw_plot(
# plots.list[[4]] + theme(legend.position = "none") + annotate(geom = 'text', label = '(C)', x = 1.65, y = 0.0),
#  x = 0.66, y = 0, width = 1, height = 0.80) +
# draw_plot(
# plot.leg ,
#  x = 0, y = 0.80, width = 1, height = 0.2)
# 
# plot2plot %>% ggsave(.,
#   filename = paste0(OUTPUT,".",device2use),
#   width=170,
#   height=100,
#   device=device2use,
#   units = 'mm')
# 
# })


```



### Methylation around features 

```R
# Once we have read the files
# now we can build base-pair resolution matrices of scores for each experiment.
# The returned list of matrices can be used to draw meta profiles or heatmaps of read coverage around promoters.


targets <- list(WGBS.H1=wgbs.H1, WGBS.IMR90=wgbs.IMR90, RRBS.H1=rrbs.H1, RRBS.IMR90=rrbs.IMR90) 
sml <- ScoreMatrixList(targets=targets, windows=transcriptFeat$promoters, bin.num=20, strand.aware=TRUE, weight.col="score", is.noCovNA = TRUE)
 
sml.sub =intersectScoreMatrixList(sml, reorder = FALSE) # because scoreMatrices have different dimensions, we need them to cover the same promoters for the heatmap
multiHeatMatrix(sml.sub, xcoords=c(-2000, 2000), matrix.main=names(targets), xlab="region around TSS")

```

### Methylation Landscape

Plots of percent methylation across chromosomes

```R

y2use = 'qvalue'
x2use = 'ranges'
col2use = 'meth.diff'
facet2use = 'seqnames'


dat2use %>%
ggplot() +
geom_point(
aes(
x = !!sym(x2use),
y = !!sym(y2use),
col = !!sym(col2use)
)
) +
facet_grid( 
. ~ get(facet2use)
)


```







### Annotate regions 

```R

diffmeth.exon.location.gr = VariantAnnotation::locateVariants(diffmeth.gr, annot.exon.gff.gr , AllVariants())

table(diffmeth.exon.location.gr$LOCATION)

```

genomation::readTranscriptFeatures the default option is to take -1000,+1000bp around the TSS and you can change that. 
Same goes for CpG islands when reading them in via genomation::readFeatureFlank function. Default is to take 2000bp flanking regions on each side of the CpG island as shores. 


## Get overlap between data

```R
# filter for width greater than 1
tmp.2drop =  {width(ranges(annot.exon.gff.gr)) < 3} %>% which(.)

annot.exon.gff.gr %<>% .[-tmp.2drop]

# for variable window sizes
smb = ScoreMatrixBin(
target =  meth2use.gr,
 windows = annot.exon.gff.gr
 ) 

sm = ScoreMatrix(
target =  meth2use.gr, 
windows = annot.exon.gff.gr
) 
# for fixed window sizes

heatMatrix(sm)
plotMeta(sm)


```


# 6 Annotation of the CpG positions.
The R-code below focuses on converting genomic data into a format suitable for analysis (GRanges) and parsing genomic features from a BED file. Here we use the function bed_to_granges to a file derived and reformatted from the UCSC server and encompassing the ensemble gene predictions "ensGenegenepred.bed".


## 6.2 Create a gene.parts object.

```NOTINUSE
# Read transcript features from the specified BED file. This function is designed to parse genomic features
# such as exons, introns, promoters, and transcription start sites (TSS) from the file, returning a GRangesList.
# The GRangesList object contains the locations of these features. Parameters are set to include a 1500 bp
# upstream flank (up.flank) and a 500 bp downstream flank (down.flank) for promoters, and to ensure that
# unique promoters (unique.prom) are considered.

gene.parts = readTranscriptFeatures(bed.file, remove.unusual = FALSE,
                                    up.flank = 1500, down.flank = 500, unique.prom = TRUE) 

# Print the names of the elements in the GRangesList and show the class of the 'gene.parts' object. This confirms that the output is a GRangesList,
# a list-like container where each element represents a different type of genomic feature (e.g., exon, intron).
names(gene.parts)
class(gene.parts)
```

## 6.3 Combine GRanges with gene.parts and find the overlap 

### 6.3.1 Creation of an overview table with absolute numbers/percentage
```R
# Annotate a GRanges object (genomic regions of interest) with gene parts (exon, intron, promoter, etc.).
  newStickle_annot=annotateWithGeneParts(unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl, gene.parts, intersect.chr=TRUE)
# Print the annotated GRanges object to review the annotations.
  newStickle_annot

# Below I calculate the annotation statistics in differnt ways with absolute numbers vs percentatges and with and without precedence.

    # Obtain and print annotation statistics for the annotated genomic regions, considering the precedence of annotations.
    # Precedence here means that if a region can be annotated as more than one type (e.g., both exon and intron)
      genomation::getTargetAnnotationStats(newStickle_annot,precedence = TRUE)
    # Obtain annotation statistics without considering precedence and without converting counts to percentages.
      genomation::getTargetAnnotationStats(newStickle_annot, percentage =FALSE,precedence = TRUE)
    # Obtain annotation statistics considering all possible annotations for each region (no precedence).
      genomation::getTargetAnnotationStats(newStickle_annot, percentage =FALSE,precedence = FALSE)
```
### 6.3.2 a) Add genomic feature (exon, intron, promoter, etc.) to each CpG

```R
# Annotate a GRanges object (genomic regions of interest) with gene parts (exon, intron, promoter, etc.).
newStickle_annot_2=annotateWithGeneParts(unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl, gene.parts, intersect.chr=TRUE)

## Combine genomic region annotations related to transcription start sites (TSS) with membership information 
# regarding whether these regions overlap with promoters, exons, and introns. The getAssociationWithTSS function 
# retrieves distance to the nearest TSS for each region, while getMembers indicates the type of genomic feature each 
# region overlaps with.
test = cbind(getAssociationWithTSS(newStickle_annot_2), as.data.frame(genomation::getMembers(newStickle_annot_2)))

# Further extend the combined dataset by including genomic sequence names (chromosomes), ranges, and additional metadata 
# for each region. The `@seqnames` property contains chromosome names for each region, `@ranges` provides genomic coordinates, 
# and `@elementMetadata@listData` contains associated metadata.This step enriches the dataset with detailed location and feature
# information.
test = cbind(test,   unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl@seqnames,unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl@ranges,unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl@elementMetadata@listData)
  
dim(test)#32925   233
```
### 6.3.2 b) Annotate the promoter-overlapping genomic regions 

```  R
# Annotate the promoter-overlapping genomic regions with gene parts to identify their relation to gene structures.
# This uses the 'annotateWithGeneParts' function which annotates regions based on their overlap with gene parts like exons, introns, etc.
# only prom - check if cbind works -- here with another granges --->  unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl_promoters

unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl_promoters=subsetByOverlaps(unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl,gene.parts$promoters)
newStickle_annot_prom=annotateWithGeneParts(unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl_promoters, gene.parts, intersect.chr=TRUE)

# Print the newly annotated GRanges object to inspect the annotations.
newStickle_annot_prom

# Combine the annotation data with additional metadata for a comprehensive dataset.
# This includes distance to the nearest transcription start site (TSS), membership information (overlap with gene parts),
# genomic sequence names (chromosomes), ranges, and other associated metadata. The resulting dataset provides a detailed view of each promoter-overlapping
# region including its genomic context and annotations.

test_prom = cbind(getAssociationWithTSS(newStickle_annot_prom), as.data.frame(genomation::getMembers(newStickle_annot_prom)))
test_prom = cbind(test_prom,   unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl_promoters@seqnames,unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl_promoters@ranges,unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl_promoters@elementMetadata@listData)
test_prom

# This file 'newStickle_annot_prom.txt' will contain the detailed annotations and metadata of promoter-overlapping regions.
write.csv(test_prom,file="2023/Annonation_2023/newStickle_annot_prom.txt")
``` 
### 6.3.2 c) Annotate the overlapping genomic to all genomic features and add a priority/precendene handling to it. 

This section of the code applies a comprehensive annotation process to genomic regions, assigning them to specific gene parts such as promoters, exons, introns, and intragenic areas, while handling precedence among these categories to ensure accurate categorization. The process involves validation checks, creation of binary flags to mark the presence of these features, and the final assignment of each region to a CpG based on a set hierarchy of gene part importance. 

```R
# Initial combination of annotation data with genomic region information
test = cbind(getAssociationWithTSS(newStickle_annot_2), as.data.frame(genomation::getMembers(newStickle_annot_2)))
test = cbind(test,   unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl@seqnames,unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl@ranges,unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl@elementMetadata@listData)

# 1. Promoter Annotation Check
# A new column 'prom_br' is created to flag regions as promoter based on existing annotations.
# This is primarily a validation step to ensure that promoter annotations align with expectations.

  test<- test%>% 
    #  dplyr::select(prom,exon, intron, prom_br) %>% 
    mutate(prom_br = case_when((prom == 1) ~ 1,
                               (prom == 0 ) ~ 0,
                               TRUE ~ 0))
# Verifies if the newly created 'prom_br' column matches the original 'prom' annotations exactly.
  identical(test[['prom_br']],test[['prom']]) #TRUE

# Analyzes dimensions of the dataset under various conditions to understand the overlap and exclusivity of annotations.
# These checks are crucial for confirming the integrity and expected behavior of the genomic annotations.
  dim(test[test$prom==1 &test$exon==1 &test$intron==1,]) # promotor, exon and intron 2203  
  dim(test[test$prom==1 &test$exon==0 &test$intron==0,]) # pure promotor, no exon, introm 41797 
  dim(test[test$prom==1 &test$exon==1 &test$intron==0,]) # pure promotor and exon 40025
  dim(test[test$prom==1 &test$exon==0 &test$intron==1,]) # pure promotor and intron 19516
  2203+ 41797 + 40025 + 19516 #103541

  dim(test[test$prom_br==1,])#pure promotor 103541

# Sum of all promoter-related annotations matches expected total for 'prom_br'.


# 2. Exon Annotation Check
# A similar process is applied to exon annotations, creating an 'exon_br' flag to indicate pure exon regions.
# The logic includes excluding regions already flagged as promoters to refine exon annotations.

  test<- test%>% 
    #  dplyr::select(prom,prom_br,exon, intron) %>%
    mutate(exon_br = case_when((exon == 1) & (prom == 0 ) ~ 1,
                               (prom == 1) & (exon == 1 )~ 0,
                               (prom == 0 ) ~ 0,
                               (intron == 0 ) ~ 0,
                               TRUE ~ 0)) 
  identical(test[['exon_br']],test[['exon']]) #FALSE

# Unlike 'prom_br', 'exon_br' may not identically match 'exon' due to the exclusion of promoter-flagged regions.
# Further validation checks are performed for exon annotations, ensuring accuracy.
    # Regions overlapping with both exons and promoters are verified for correct assignment.
    # The dimensions of pure exon and exon-intron regions are analyzed to confirm the accuracy of the 'exon_br' flag.
    
      test[test$exon==1 &test$prom==1,] 
      # target.row dist.to.feature         feature.name feature.strand prom exon intron
      # 8.29        149             296 ENSGACT00000005883.1              -    1    1      0
      # 8.30        150             290 ENSGACT00000005883.1              -    1    1      0
      # 8.32        152             134 ENSGACT00000005883.1              -    1    1      0

      # exon_br
      # 8.29       0
      # 8.30       0
      # 8.32       0
    
      #they are correctly assigned to promotor
    
      test[test$exon==1 &test$intron==1 &test$prom==0,] 
      #target.row dist.to.feature         feature.name feature.strand prom exon intron
      #6.5         97            1409 ENSGACT00000005856.1              -    0    1      1
      #6.6         98            1378 ENSGACT00000005856.1              -    0    1      1
      #6.7         99            1369 ENSGACT00000005856.1              -    0    1      1
      #numCs87 numTs87 prom_br exon_br
      #6.5      11       3       0       1
      #6.6      13       1       0       1
      #6.7      13       1       0       1
      #they are correctly assigned to exon
    
      
      dim(test[test$prom==0 &test$exon==1 &test$intron==0,]) #pure exon 88572
      dim(test[test$prom==0 &test$exon==1 &test$intron==1,]) #pure exon and intron 2640
      
      88572+2640 #91212
      dim(test[test$exon_br==1,])#pure exon 91212
  
# 3. Intron Annotation Check
# Intron-specific annotations are validated by creating an 'intron_br' flag for pure intron regions,
# excluding those already categorized as promoter or exon regions.

    test<- test%>% 
    dplyr::select(prom,prom_br,exon,exon_br, intron) %>%
          mutate(intron_br = case_when(intron == 1 & (exon == 0) & (prom == 0 ) ~ 1,
                                       (prom == 1) & (exon == 1 )~ 0,
                                       (intron == 0 ) ~ 0,
                                       TRUE ~ 0)) 
    identical(test[['intron_br']],test[['intron']]) #FALSE
# Analyzes the dimension for pure intron regions to ensure the 'intron_br' flag is accurately assigned. 
    dim(test[test$prom==0 &test$exon==0 &test$intron==1,]) #pure intron 113842
    dim(test[test$intron_br==1,])#pure intron 113842
  
  
# 4. Intragenic Annotation
# Identifies regions that do not overlap with any known gene parts (exons, introns, promoters) as 'intragenic'.
# This step is important for understanding the genomic landscape outside of gene-encoding regions.
 
    test<- test%>% 
    dplyr::select(prom,prom_br,exon,exon_br, intron,intron_br) %>%
    mutate(intragenic_br = case_when(intron == 0 & (exon == 0) & (prom == 0 ) ~ 1,
                                     TRUE ~ 0)) 
    dim(test[test$intragenic_br==1,])# pure intragenic 187040


# Final Feature Categorization
# Based on the prior annotations, a comprehensive 'feature' column is created to summarize the genomic region type.
# This is crucial for easy visualization and analysis of the dataset's annotation composition.

# Adding column based on other column:
  test= test %>%
    mutate(feature = case_when(
      (prom_br == "1") ~ "promoter",
      (exon_br == "1") ~ "exon",
      (intron_br == "1")~ "intron",
      (intragenic_br == "1")~ "intragenic",
    ))
  
  test
# The unique features and their summaries provide insights into the distribution of genomic regions across categories.
  unique(factor(test$feature)) #exon intragenic intron promoter
  summary(factor(test$feature))

# The final annotated dataset is written to a CSV file, providing a valuable resource for further genomic analysis.
  write.csv(test,file="2023/Annonation_2023/newStickle_annot_all_withpriority.txt")
```
This is the resulting table (first 5 lines).

```R
"","target.row","dist.to.feature","feature.name","feature.strand","prom","exon","intron","unite_norm_87_10x_70_CT_GA_corr_without_Sex_grl@seqnames","start","end","width","coverage1","numCs1","numTs1","coverage2","numCs2","numTs2","coverage3","numCs3","numTs3","coverage4","numCs4","numTs4","coverage5","numCs5","numTs5","coverage6","numCs6","numTs6","coverage7","numCs7","numTs7","coverage8","numCs8","numTs8","coverage9","numCs9","numTs9","coverage10","numCs10","numTs10","coverage11","numCs11","numTs11","coverage12","numCs12","numTs12","coverage13","numCs13","numTs13","coverage14","numCs14","numTs14","coverage15","numCs15","numTs15","coverage16","numCs16","numTs16","coverage17","numCs17","numTs17","coverage18","numCs18","numTs18","coverage19","numCs19","numTs19","coverage20","numCs20","numTs20","coverage21","numCs21","numTs21","coverage22","numCs22","numTs22","coverage23","numCs23","numTs23","coverage24","numCs24","numTs24","coverage25","numCs25","numTs25","coverage26","numCs26","numTs26","coverage27","numCs27","numTs27","coverage28","numCs28","numTs28","coverage29","numCs29","numTs29","coverage30","numCs30","numTs30","coverage31","numCs31","numTs31","coverage32","numCs32","numTs32","coverage33","numCs33","numTs33","coverage34","numCs34","numTs34","coverage35","numCs35","numTs35","coverage36","numCs36","numTs36","coverage37","numCs37","numTs37","coverage38","numCs38","numTs38","coverage39","numCs39","numTs39","coverage40","numCs40","numTs40","coverage41","numCs41","numTs41","coverage42","numCs42","numTs42","coverage43","numCs43","numTs43","coverage44","numCs44","numTs44","coverage45","numCs45","numTs45","coverage46","numCs46","numTs46","coverage47","numCs47","numTs47","coverage48","numCs48","numTs48","coverage49","numCs49","numTs49","coverage50","numCs50","numTs50","coverage51","numCs51","numTs51","coverage52","numCs52","numTs52","coverage53","numCs53","numTs53","coverage54","numCs54","numTs54","coverage55","numCs55","numTs55","coverage56","numCs56","numTs56","coverage57","numCs57","numTs57","coverage58","numCs58","numTs58","coverage59","numCs59","numTs59","coverage60","numCs60","numTs60","coverage61","numCs61","numTs61","coverage62","numCs62","numTs62","coverage63","numCs63","numTs63","coverage64","numCs64","numTs64","coverage65","numCs65","numTs65","coverage66","numCs66","numTs66","coverage67","numCs67","numTs67","coverage68","numCs68","numTs68","coverage69","numCs69","numTs69","coverage70","numCs70","numTs70","coverage71","numCs71","numTs71","coverage72","numCs72","numTs72","coverage73","numCs73","numTs73","coverage74","numCs74","numTs74","coverage75","numCs75","numTs75","coverage76","numCs76","numTs76","coverage77","numCs77","numTs77","coverage78","numCs78","numTs78","coverage79","numCs79","numTs79","coverage80","numCs80","numTs80","coverage81","numCs81","numTs81","coverage82","numCs82","numTs82","coverage83","numCs83","numTs83","coverage84","numCs84","numTs84","coverage85","numCs85","numTs85","coverage86","numCs86","numTs86","coverage87","numCs87","numTs87","prom_br","exon_br","intron_br","intragenic_br","feature"
"1",1,3987,"ENSGACT00000005823.1","-",0,1,0,"chrI",5924,5924,1,13,10,3,15,15,0,21,21,0,13,10,3,15,15,0,11,10,1,NA,NA,NA,NA,NA,NA,12,9,3,14,13,1,14,13,1,23,22,1,25,25,0,27,26,1,19,18,1,24,24,0,12,12,0,23,22,1,21,21,0,27,24,3,28,27,1,25,22,3,NA,NA,NA,15,14,1,18,16,2,24,24,0,20,19,1,22,21,1,24,23,1,21,21,0,10,9,1,24,24,0,17,14,3,16,16,0,37,34,3,46,42,4,26,26,0,13,13,0,16,16,0,17,13,4,28,28,0,19,19,0,16,16,0,23,20,3,15,15,0,11,11,0,16,15,1,NA,NA,NA,14,14,0,29,24,5,27,25,2,11,10,1,16,14,2,11,10,1,12,11,1,14,13,1,15,14,1,24,23,1,20,17,3,18,16,2,15,14,1,25,23,2,21,21,0,15,15,0,24,20,4,41,38,3,19,16,3,10,9,1,16,16,0,18,17,1,23,21,2,21,19,2,24,24,0,15,14,1,13,12,1,21,20,1,19,19,0,19,18,1,22,21,1,18,17,1,16,16,0,22,21,1,16,16,0,23,21,2,11,9,2,17,17,0,25,24,1,0,1,0,0,"exon"
"1.1",2,3983,"ENSGACT00000005823.1","-",0,1,0,"chrI",5928,5928,1,13,11,2,16,15,1,22,19,3,12,9,3,15,13,2,12,12,0,NA,NA,NA,10,10,0,12,11,1,14,12,2,13,11,2,23,21,2,24,22,2,29,26,3,17,17,0,24,22,2,13,13,0,23,19,4,21,19,2,26,18,8,27,24,3,25,21,4,NA,NA,NA,15,14,1,18,18,0,24,24,0,20,16,4,22,19,3,25,21,4,21,19,2,10,9,1,24,22,2,17,15,2,16,16,0,35,33,2,45,40,5,24,20,4,13,12,1,16,15,1,17,11,6,28,26,2,17,16,1,15,15,0,23,22,1,16,16,0,12,12,0,16,15,1,NA,NA,NA,14,13,1,28,24,4,27,22,5,11,10,1,15,11,4,12,8,4,12,12,0,14,13,1,15,14,1,23,22,1,20,18,2,18,17,1,15,14,1,25,23,2,19,19,0,15,12,3,24,23,1,40,37,3,19,17,2,10,8,2,15,14,1,18,17,1,23,21,2,21,18,3,24,23,1,14,13,1,12,12,0,21,20,1,18,18,0,18,16,2,22,18,4,16,14,2,16,15,1,22,22,0,16,16,0,24,21,3,11,9,2,17,16,1,25,24,1,0,1,0,0,"exon"
"1.2",3,3968,"ENSGACT00000005823.1","-",0,1,0,"chrI",5943,5943,1,13,9,4,16,13,3,22,17,5,13,12,1,15,13,2,12,12,0,NA,NA,NA,10,9,1,12,11,1,15,14,1,14,11,3,24,18,6,25,20,5,29,26,3,19,17,2,24,19,5,13,12,1,23,20,3,21,16,5,27,22,5,28,22,6,25,24,1,NA,NA,NA,15,14,1,18,18,0,24,22,2,20,17,3,22,16,6,25,20,5,21,15,6,10,9,1,24,24,0,17,15,2,16,13,3,36,32,4,46,39,7,26,21,5,13,10,3,16,14,2,17,12,5,28,25,3,19,18,1,16,13,3,23,19,4,16,15,1,12,11,1,16,13,3,NA,NA,NA,14,14,0,29,23,6,27,22,5,11,9,2,16,13,3,12,7,5,12,11,1,14,12,2,15,11,4,24,20,4,20,18,2,18,15,3,15,13,2,25,22,3,21,18,3,15,13,2,24,19,5,41,33,8,20,15,5,10,7,3,16,14,2,18,16,2,23,19,4,21,18,3,24,24,0,14,13,1,13,12,1,21,19,2,19,18,1,19,17,2,22,21,1,18,15,3,16,16,0,22,18,4,16,15,1,24,19,5,11,8,3,17,13,4,25,22,3,0,1,0,0,"exon"
"1.3",4,3961,"ENSGACT00000005823.1","-",0,1,0,"chrI",5950,5950,1,13,10,3,16,14,2,22,17,5,13,11,2,15,11,4,12,11,1,NA,NA,NA,10,10,0,12,10,2,15,15,0,14,9,5,23,17,6,25,23,2,29,22,7,19,17,2,24,21,3,12,10,2,23,20,3,21,19,2,27,20,7,27,24,3,25,23,2,NA,NA,NA,15,15,0,18,15,3,24,14,10,20,16,4,20,12,8,25,21,4,20,17,3,10,7,3,24,20,4,17,14,3,16,15,1,36,29,7,46,34,12,26,20,6,13,12,1,16,14,2,16,12,4,28,23,5,18,16,2,15,13,2,23,18,5,16,14,2,11,9,2,16,12,4,NA,NA,NA,14,13,1,29,22,7,27,21,6,11,9,2,16,9,7,12,8,4,12,10,2,14,10,4,15,12,3,24,18,6,20,18,2,18,13,5,15,13,2,25,22,3,21,17,4,15,12,3,24,16,8,41,36,5,20,15,5,10,9,1,16,12,4,17,15,2,23,17,6,21,16,5,24,20,4,15,13,2,13,12,1,21,18,3,19,19,0,19,17,2,22,18,4,18,11,7,16,14,2,22,18,4,16,14,2,24,18,6,11,11,0,17,11,6,25,22,3,0,1,0,0,"exon"


```



## TXDB Workflow 

```R
txdb = makeTxDbFromGFF(paste0(MOUNT,"/Data/reference_genomes/Corbicula_fluminea_genome/Final_corrected_assemly_genome_gene.gff3"),
dataSource="Zhang2021",
organism="Corbicula fluminea",
circ_seqs=NULL,
chrominfo=NULL,
metadata=NULL)

txdb %>% str
txdb

```

