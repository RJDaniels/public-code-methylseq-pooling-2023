---
Title : Bismark step ; Deduplication
---


# Overview
Processing sequence data with Bismark.
This is entirely slurm submission based.

The alignment should be run several times for each dataset.
- [] Deduplicate the alignment against pUC19
- [] Deduplicate the alignment against lambda
- [] Deduplicate the alignment against species genome

## Background info

TBC...


# Set up local environment

## Data set

### Bismark_Test

```bash

export FILEROOT=Bismark_Test
export REFGENOMEPRFX=NC_010473
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Bismark_test_data/methylseq
# Short name for the ref
export TMPREF=NC_010473

export RAWDATADIR=${CMMNT}/CodeRepos/Bismark/test_files
export RAWDATPRFX=test

```

### Corbicula

```bash
export FILEROOT=Cflu

## Ilifu
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Corbicula_fluminea/Lachesis/methylseq
export REFGENOMEPRFX=Lachesis_assembly_changed
# Short name for the ref
export TMPREF=Lachesis

export RAWDATADIR=/users/rjdaniels/GRPDIR/23_Epigenome/RawData/DeleteOnSRAConfirmation/Cflu_emseq/

export RAWDATPRFX=20220720.A-o287562

```

### Dreissena

```bash

export FILEROOT=Dpol

## Ilifu
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1/methylseq
export REFGENOMEPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic
# Short name for the ref
export TMPREF=Dpol1.0

export RAWDATADIR=/users/rjdaniels/GRPDIR/23_Epigenome/RawData/DeleteOnSRAConfirmation/Dpol_emseq/

export RAWDATPRFX=20220720.A-o287561

```


## Set up

```bash

# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=Bismark_Dedup_${TMPREF}

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi
# =======

# File with the names of all the files to run
export ROOTLISTFILE=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming/${FILEROOT}_FileRootList.txt
export FILEROOTLIST=${ROOTLISTFILE}

# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}') 

$ARRAYLENGTH
## ARRAYLENGTH=2

# Trimmed data
export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming

# Aligned data
export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Align_${TMPREF}/out_Bismark_Align_${TMPREF}

# Dedup data
export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Dedup_${TMPREF}/out_Bismark_Dedup_${TMPREF}

```


# Requires

# Deduplication

## SLURM submission

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_Deduplication_240204.sh

export JOBNAME=${TASK}'_Dedup'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:64GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/25:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:10:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_bismark.sif \' >> ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}
# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${ALIGNDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

```

```bash
#export RJD_LASTJOBID=9459411
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT} )


```



## Resort bams by read name

NOT NEEDED. CODE INCLUDED IN DEDUPLICATION CODE.

```INACTIVE

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_samtool_sortbyname_240204.sh

export JOBNAME=${TASK}'_SamtoolSort'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:20GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/10:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:10:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_bismark.sif \' >> ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}
# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${DEDUPDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

```

```INACTIVE
#export RJD_LASTJOBID=9495837
echo ${RJD_LASTJOBID}

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  --dependency=afterok:${RJD_LASTJOBID} ${JOBSCRIPT} )


```


