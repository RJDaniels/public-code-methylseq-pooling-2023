---
Title : Bismark step ; Trimming flanks
---

# Overview
Processing sequence data with Bismark.
This is entirely slurm submission based.


## Background info

Used the : enzymatic methyl seq kit, NEB
https://github.com/FelixKrueger/Bismark/blob/dev/Docs/README.md#em-seq-neb

Requires conservative trimming parameters
```--clip_R1 10 --clip_R2 10 --three_prime_clip_R1 10 --three_prime_clip_R2 10```

More info:
- https://www.neb.com/protocols/2019/03/28/protocol-for-use-with-large-insert-libraries-470-520-bp-e7120
- https://github.com/FelixKrueger/Bismark/issues/509
- https://github.com/nebiolabs/EM-seq/blob/master/em-seq.nf


# Requires
# Produces
TBC...

# Analyses
TBC...

# Set up local environment

## Installation


## Set up conda env

```bash

# clean up small files ones in a while
if [[ "$($RANDOM % 2)" -eq 0 ]]; then
conda clean -a
fi



module load anaconda3/login

echo ${PRJDIR}
export CONDA_ENVS_PATH=${PRJDIR}

conda info --env

conda create --name python39-env python=3.9

conda activate python39-env

conda install -c conda-forge \
-c bioconda \
pysam \
pandas \
numpy \
trim-galore \
multiqc

conda deactivate

```


## Data set

### Bismark_Test

```bash

export FILEROOT=Bismark_Test

export REFGENOMEPRFX=NC_010473
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Bismark_test_data/methylseq

export RAWDATADIR=${CMMNT}/CodeRepos/Bismark/test_files
export RAWDATPRFX=test

```


### Dreissena

```bash

export FILEROOT=Dpol

## Ilifu
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1/methylseq
export REFGENOMEPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic
export TMPREF=Dpol1.0

export RAWDATADIR=/users/rjdaniels/GRPDIR/23_Epigenome/RawData/DeleteOnSRAConfirmation/Dpol_emseq/

export RAWDATPRFX=20220720.A-o287561

```
### Corbicula

```bash

export FILEROOT=Cflu

## Ilifu
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Corbicula_fluminea/Lachesis/methylseq
export REFGENOMEPRFX=Lachesis_assembly_changed

export RAWDATADIR=/users/rjdaniels/GRPDIR/23_Epigenome/RawData/DeleteOnSRAConfirmation/Cflu_emseq/

export RAWDATPRFX=20220720.A-o287562

```


## Set up

```bash
# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=Bismark_Trimming

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi

# =======
# File with the names of all the files to run
export ROOTLISTFILE=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming/${FILEROOT}_FileRootList.txt
export FILEROOTLIST=${ROOTLISTFILE}

# Put the names of the fasta files into the file above
basename -s _R1.fastq.gz ${RAWDATADIR}/${RAWDATPRFX}*R1*.gz > ${ROOTLISTFILE}

# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')

echo $ARRAYLENGTH
## ARRAYLENGTH=2

# Trimmed data
export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming

# Aligned data
export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Align_${TMPREF}/out_Bismark_Align_${TMPREF}

# Dedup data
export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Dedup_${TMPREF}/out_Bismark_Dedup_${TMPREF}

```


## Data Prep

### All data
 
```bash

find ${RAWDATADIR} -name "${RAWDATPRFX}*gz" -type f -exec ln -s "{}" ${TASKDIR} \;

# export FASTADIR=${TASKDIR}/fasta_links
# 
# if [[ ! -d ${FASTADIR} ]]
# then
# 
# mkdir -p ${FASTADIR}
# 
# find ${RAWDATADIR} -iname "${RAWDATPRFX}*" -type f -print |  xargs -I {} ln -s "{}" ${FASTADIR} 
# 
# fi

```



## Get fastqc of raw data


```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_FastQC_240215.sh

export JOBNAME=${TASK}'_FastQC'
export JOBSCRIPT= ${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:6GB:g" \
-e "s:ZZSTOREZZ:1GB:g" \
-e "s/ZZWALLTIMEZZ/01:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:10:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${TASKDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}


```

# Do trimming

If we split the fasta files into chunks, all data can be run together.

Need to run separately for the pooled and unpooled data. Pooled will have drastically different memory requirements.

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_TrimGalore_240204.sh

JOBNAME=${TASK}'_TrimGalore'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

# Partitions : Devel or Main

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:2:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:1GB:g" \
-e "s/ZZWALLTIMEZZ/05:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:10:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${TASKDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}


```

# Get MultiQC of trimmed data

## SLURM submission

NO JOB ARRAY HERE.

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_MultiQC_240215.sh

export JOBNAME=${TASK}'_MultiQC'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh


sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:6GB:g" \
-e "s:ZZSTOREZZ:1GB:g" \
-e "s/ZZWALLTIMEZZ/00:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:1:g" \
-e "s:ZZARRAYCONCURZZ:10:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT} 

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FASTQCDIR=${1}
echo ${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/' \' >> ${JOBSCRIPT}

#export TRIMDIR=${2}
echo ${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/' \' >> ${JOBSCRIPT}

## When these directories do not exist, point the software to an empty dir, in this case the OUTPUTDIR
#export ALIGNDIR=${3}
#echo ${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Align/' \' >> ${JOBSCRIPT}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

#export DEUPDIR=${4}
#echo ${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Dedup/' \' >> ${JOBSCRIPT}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

#export METHDIR=${5}
#echo ${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_MethExt/' \' >> ${JOBSCRIPT}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${6}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}


```

To view the MultiQC and FastQC reports, you need to download them to your computer with Rsync or scp.
Run on local computer to view html summaries.
The command needs to be run on your local computer, NOT the HPC.
```bash

# This command need to change to your user name etc
rsync --relative -auvz rdaniels@euler.ethz.ch:/cluster/scratch/rdaniels/rdaniels/./Pollution_EMseq/DpolGrei_All/reports /home/rjd/msRNA_local/Projects/eawag_pollution_220801/Analyses/

```
