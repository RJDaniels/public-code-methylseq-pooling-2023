---
Title : Bismark step ; Methylation Extraction
---

# Overview
Processing sequence data with Bismark.
This is entirely slurm submission based.

The alignment should be run several times for each dataset.
- [] Extraction of the pUC19 bams
- [] Extraction of the lambda bams
- [] Extraction of the species genome bams

## Background info

TBC...

# Data set

## Bismark_Test

```bash

export FILEROOT=Bismark_Test
export REFGENOMEPRFX=NC_010473
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Bismark_test_data/methylseq
# Short name for the ref
export TMPREF=NC_010473

export RAWDATADIR=${CMMNT}/CodeRepos/Bismark/test_files
export RAWDATPRFX=test

```

# Set up local environment

```bash

# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=Bismark_MethylExt_${TMPREF}

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi
# =======

# File with the names of all the files to run
ROOTLISTFILE=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming/${FILEROOT}_FileRootList.txt

# This allows us to use job arrays, one per file pair
ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')
echo $ARRAYLENGTH
## ARRAYLENGTH=2

# Trimmed data
export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming

# Aligned data
export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Align_${TMPREF}/out_Bismark_Align_${TMPREF}

# Dedup data
export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Dedup_${TMPREF}/out_Bismark_Dedup_${TMPREF}

```


# Requires

## Installation

# Extract methylation data

## SLURM submission

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_MethylExtraction_240204.sh

export JOBNAME=${TASK}'_MethylExt'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:3:g" \
-e "s:ZZMEMZZ:32GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/45:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:10:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_bismark.sif \' >> ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${DEDUPDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

```

```bash


#export RJD_LASTJOBID=9502791
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

```



# Get MultiQC of entire process

No array job here.

## SLURM submission

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/Bismark/Script_Bismark_MultiQC_240215.sh

JOBNAME=${TASK}'_MultiQC'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:8GB:g" \
-e "s:ZZSTOREZZ:5GB:g" \
-e "s/ZZWALLTIMEZZ/00:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:1:g" \
-e "s:ZZARRAYCONCURZZ:1:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FASTQCDIR=${1}
echo ${TRIMDIR}' \' >> ${JOBSCRIPT}

#export TRIMDIR=${2}
echo ${TRIMDIR}' \' >> ${JOBSCRIPT}

## When these directories do not exist, point the software to an empty dir, in this case the OUTPUTDIR
#export ALIGNDIR=${3}
echo ${ALIGNDIR}' \' >> ${JOBSCRIPT}

#export DEDUPDIR=${4}
echo ${DEDUPDIR}' \' >> ${JOBSCRIPT}

#export METHDIR=${5}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${6}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

```


```bash
#export RJD_LASTJOBID=9461353
echo $RJD_LASTJOBID

sbatch --parsable  --dependency=afterok:${RJD_LASTJOBID} ${JOBSCRIPT}

#RJD_LASTJOBID=$(sbatch --parsable  --dependency=afterok:${RJD_LASTJOBID} ${JOBSCRIPT} )

```



To view the MultiQC and FastQC reports, you need to download them to your computer with Rsync or scp.
Run on local computer to view html summaries.
The command needs to be run on your local computer, NOT the HPC.
```bash

# This command need to change to your user name etc
rsync --relative -auvz rdaniels@euler.ethz.ch:/cluster/scratch/rdaniels/rdaniels/./Pollution_EMseq/DpolGrei_All/reports /home/rjd/msRNA_local/Projects/eawag_pollution_220801/Analyses/

```

