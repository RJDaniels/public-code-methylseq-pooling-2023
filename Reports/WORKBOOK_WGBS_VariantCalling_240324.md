---
Title : Estimate SNPs from WGBS data
---

# Overview

## Background info

# Requires

## Installation

# Set up local environment

You need to run within a sing container.

## Data set

### Bismark_Test

```bash

export FILEROOT=Bismark_Test

export REFGENOMEPRFX=NC_010473
export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Bismark_test_data/methylseq
export TMPREF=NC_010473

export RAWDATADIR=${CMMNT}/CodeRepos/Bismark/test_files
export RAWDATPRFX=test

```


### Dreissena

```bash
export FILEROOT=Dpol

export REFGENOMEDIR=/users/rjdaniels/GRPDIR/23_Epigenome/Resources/reference_genomes/Dreissena_polymorpha/GCF_020536995.1/methylseq
export REFGENOMEPRFX=GCF_020536995.1_UMN_Dpol_1.0_genomic


# Shorter Ref name
export TMPREF=Dpol1.0


```


### Corbicula

```bash
export FILEROOT=Cflu

## Ilifu
#export REFGENOMEDIR=${CMMNT}/Projects/23_Epigenome/Resources/reference_genomes/
export REFGENOMEDIR=${GRPDIR}/23_Epigenome/Resources/reference_genomes/Corbicula_fluminea/Lachesis/methylseq
export REFGENOMEPRFX=Lachesis_assembly_changed
# Shorter Ref name
export TMPREF=Lachesis

```



## Set up
```bash

# Set the project
export PRJNAME=23_Epigenome

# The main folder/activity
export TASK=VariantCalling_${TMPREF}

# ======
# This is a script that loads some environmental variables specific to this project ... rather than needing to copy-paste it to the terminal each time.
if [[ -f "${CMMNT}/Projects/${PRJNAME}/HPC/project.init" ]]; then
source ${CMMNT}/Projects/${PRJNAME}/HPC/project.init
else
echo 'ERR: project.init not found!'
fi
# =======

# File with the names of all the files to run
export ROOTLISTFILE=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming/${FILEROOT}_FileRootList.txt
export FILEROOTLIST=${ROOTLISTFILE}

# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')
echo $ARRAYLENGTH
## ARRAYLENGTH=2

# Trimmed data
export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming
#export TRIMDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Trimming/out_Bismark_Trimming

# Aligned data
export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Align_${TMPREF}/out_Bismark_Align_${TMPREF}
#export ALIGNDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_alignment/out_Bismark_alignment

# Dedup data
export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_Dedup_${TMPREF}/out_Bismark_Dedup_${TMPREF}
#export DEDUPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_deduplication/out_Bismark_deduplication

# Methyl call
export METHDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/Bismark_MethylExt_${TMPREF}/out_Bismark_MethylExt_${TMPREF}

# BSSnper dir
export BSSNPDIR=${PRJDIR}/${FILEROOT}-${DATSRCIDX1}/BSSnper_${TMPREF}/out_BSSnper_${TMPREF}

```

# Common Code

## Index Bam files

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/SAMTOOLS/Script_Samtools_BAMIndex.sh

export JOBNAME=${TASK}'_samtools_bamindex'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Devel:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/00:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_methylseqtools_v0.3.sif \' >> ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${2}
echo ${BSSNPDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${3} # put the index back in the BSSNPer dir
echo ${BSSNPDIR}' \' >> ${JOBSCRIPT}

```



```bash
#export RJD_LASTJOBID=9448808
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})


```


## Variant calling

Variant calling is done using all available bam files. So we redefine the FILEROOTLIST.
We also split the data by chr, so the ROOTFILELIST should have a line per chr.
for the following steps.

```bash

# File with the names of all the files to run
# this will be edited to be simpler so don't stress if there are unwanted versions of the files in the list
export ROOTLISTFILEPRFX=${FILEROOT}_FileRootList_VariantCalling

# list bam files to use 

rm ${TASKDIR}/${ROOTLISTFILEPRFX}_tmp.txt
find ${BSSNPDIR} -iname '*_val_*sorted.bam' -type f -exec bash -c "basename -s .bam '{}' >> ${TASKDIR}/${ROOTLISTFILEPRFX}_tmp.txt"  \;

# !! ADJUST THE NAMES TO BE MORE GENERAL!!
## One name per set of files 

head ${TASKDIR}/${ROOTLISTFILEPRFX}_tmp.txt
wc -l ${TASKDIR}/${ROOTLISTFILEPRFX}_tmp.txt
sort ${TASKDIR}/${ROOTLISTFILEPRFX}_tmp.txt | less

# Remove unpaired files
# keep the 'val' qualifier for avoiding the unpaired files
sed  \
 -e 's/split1_//g' \
 -e 's/.multiple.deduplicated.namesorted//g' \
 -e 's/_R1_val_1_bismark_bt2_pe//g' \
 -e '/_R1_unpaired_1_bismark_bt2/d' \
${TASKDIR}/${ROOTLISTFILEPRFX}_tmp.txt | \
head -n 1 > ${TASKDIR}/${ROOTLISTFILEPRFX}_tmp2.txt

# Edit the tmp file to only have the general name
cat ${TASKDIR}/${ROOTLISTFILEPRFX}_tmp2.txt
#nano ${TASKDIR}/${ROOTLISTFILEPRFX}_tmp2.txt

# Create list of files x chr

# Get list of contigs
awk '{ print $1}'  ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa.fai | \
while read CONTIG
do
awk -v CONTIG=${CONTIG} '{print $1,CONTIG}' ${TASKDIR}/${ROOTLISTFILEPRFX}_tmp2.txt 
done > ${TASKDIR}/${ROOTLISTFILEPRFX}.txt


#less ${TASKDIR}/${ROOTLISTFILEPRFX}.txt
#cat ${TASKDIR}/${ROOTLISTFILEPRFX}.txt

export FILEROOTLIST=${TASKDIR}/${ROOTLISTFILEPRFX}.txt
export ROOTLISTFILE=${FILEROOTLIST}
#cat ${FILEROOTLIST}

# This allows us to use job arrays, one per file pair
export ARRAYLENGTH=$(wc -l ${ROOTLISTFILE} | awk '{print $1}')
echo $ARRAYLENGTH
## ARRAYLENGTH=2

```



```bash
mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/SAMTOOLS/Script_Samtools_mpileup.sh

export JOBNAME=${TASK}'_samtools_mpileup'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/72:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_methylseqtools_v0.3.sif \' >> ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export REFGENOMEDIR=${2}
echo ${REFGENOMEDIR}' \' >> ${JOBSCRIPT}

#export REFGENOMEPRFX=${3}
#echo ${REFGENOMEPRFX}_interleaved' \' >> ${JOBSCRIPT}
echo ${REFGENOMEPRFX}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${4}
echo ${BSSNPDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${5} 
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

```

### Submission


```bash
#export RJD_LASTJOBID=9480571
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=afterok:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT} )

```

## Add filters

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/SAMTOOLS/Script_Samtools_addfilters.sh

export JOBNAME=${TASK}'_samtools_addfilters'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/72:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_methylseqtools_v0.3.sif \' >> ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export REFGENOMEDIR=${2}
echo ${REFGENOMEDIR}' \' >> ${JOBSCRIPT}

#export REFGENOMEPRFX=${3}
#echo ${REFGENOMEPRFX}_interleaved' \' >> ${JOBSCRIPT}
echo ${REFGENOMEPRFX}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${4}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${5} 
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

```



```bash
#export RJD_LASTJOBID=9481668
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

RJD_LASTJOBID=$(sbatch --parsable  ${JOBSCRIPT})


```


## Create filtered files

```bash

mkdir -p ${OUTPUTDIR}/sh_dir
cd ${OUTPUTDIR}/sh_dir

SCRIPT=${CODEDIR}/SAMTOOLS/Script_Samtools_bcf_filtering.sh

export JOBNAME=${TASK}'_samtools_filtering'
export JOBSCRIPT=${OUTPUTDIR}/sh_dir/SLURM_${JOBNAME}.sh

sed \
-e "s:ZZPARTZZ:Main:g" \
-e "s:ZZTHREADSZZ:8:g" \
-e "s:ZZCORESZZ:1:g" \
-e "s:ZZMEMZZ:16GB:g" \
-e "s:ZZSTOREZZ:20GB:g" \
-e "s/ZZWALLTIMEZZ/24:30:00/g" \
-e "s/ZZJOBNAMEZZ/${JOBNAME}/g" \
-e "s:ZZARRAYLENGTHZZ:${ARRAYLENGTH}:g" \
-e "s:ZZARRAYCONCURZZ:20:g" \
${CMMNT}/Platform_Specifics/TEMPLATE_SLURM_Header_LocalOnly.sh > ${JOBSCRIPT}

echo 'singularity exec ${CONTAINERDIR}/ubuntu_focal_ilifu_methylseqtools_v0.3.sif \' >> ${JOBSCRIPT}

echo 'bash \' >> ${JOBSCRIPT}

# The script to run
echo ${SCRIPT}'  \' >> ${JOBSCRIPT}

#export FILEROOTLIST=${1}
echo ${ROOTLISTFILE}' \' >> ${JOBSCRIPT}

#export REFGENOMEDIR=${2}
echo ${REFGENOMEDIR}' \' >> ${JOBSCRIPT}

#export REFGENOMEPRFX=${3}
#echo ${REFGENOMEPRFX}_interleaved' \' >> ${JOBSCRIPT}
echo ${REFGENOMEPRFX}' \' >> ${JOBSCRIPT}

#export INPUTDIR=${4}
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

#export OUTPUTDIR=${5} 
echo ${OUTPUTDIR}' \' >> ${JOBSCRIPT}

```


```bash
#export RJD_LASTJOBID=9448808
echo $RJD_LASTJOBID

RJD_LASTJOBID=$(sbatch --parsable  --dependency=aftercorr:${RJD_LASTJOBID} ${JOBSCRIPT} )

#sbatch --parsable  ${JOBSCRIPT}

```
