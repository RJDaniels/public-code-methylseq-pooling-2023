#!/bin/R

# Provide two objects, a dataframe of methylation percentages with rownames as the sample IDs and a GenomeRanges object with the chromosome information

# ============
# ROAD MAP
# ============
# ============

# ========
# Run Startup scripts
# ========
CMMNT = Sys.getenv(c('CMMNT'))

# Load Env variables
source(paste0(CMMNT,'/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.R'))

# Load general R stuff
# This is now in the Env vars step above
source(paste0(CODEDIR,'/StartupScript_Sources_260220.R'))

# =============


# ============
# Load specific vars .... must happen after the startup scripts to prevent being overwritten
# ============
args = commandArgs(TRUE)

#FILEROOTLIST = args[2] # this was for arrays within R
INPUTFILE = args[1] # The file pattern can now be a single file or something that matches a number of files

INPUTDIR = args[2]

OUTPUTDIR = args[3]

FILE.PATTERN.SUFFIX = args[4] # The file pattern can now be a single file or something

# Explicitly set for now ...  pointing to symlink
RDSDIR = paste0(PRJDIR,'/R_RelationalDB')

# ============
# For interactive work
# ============

if(FALSE){

INPUTFILE = Sys.getenv(c('INPUTFILE'))
METHKITDIR = Sys.getenv(c('METHKITDIR'))
OUTPUTDIR = Sys.getenv(c('OUTPUTDIR'))
INPUTDIR = paste0(OUTPUTDIR,'/ConcatChr')

RDSDIR = Sys.getenv(c('RDSDIR'))
RESRCDIR = Sys.getenv(c('RESRCDIR'))


OUTPUTDIR = '/scratch3/users/rjdaniels/TMP_rjdaniels_535533f3-fe65-4f65-a0e0-1e07340d9f4a'
INPUTDIR = '/scratch3/users/rjdaniels/TMP_rjdaniels_535533f3-fe65-4f65-a0e0-1e07340d9f4a'


FILE.PATTERN.SUFFIX = Sys.getenv(c('FILE.PATTERN.SUFFIX'))

INPUTFILE = 'methylBase_united_minpergrp_0.75'
FILE.PATTERN.SUFFIX = 'txt.bgz'
#INPUTFILE = 'TMP'
 }

# ==========

# change of plan, only source scripts with functions,
# dont nest calls to source
# source causes issues with finding objects

# ============

## RegularizedSCA is defunct, so it needs to be installed via devtools
#require(devtools)
#install_version("RegularizedSCA", version = "0.5.4", repos = "http://cran.us.r-project.org")

pacman::p_load(magrittr, tidyverse, ggplot2, GenomicRanges, cowplot,methylKit, pracma, RegularizedSCA)


# ============
device2use='svg'

#device2use='1'
stopifnot(
{device2use %in% c('jpeg','jpg','png','pdf','svg','eps')})

# ==========
# Load functions
# ==========

source(paste0(CODEDIR,'/SRC/Rcode/Functions_EMseq_MethylKit_221124.R'))

source(paste0(CODEDIR,'/SRC/Rcode/Functions_EMseq_MethylKit_PCAjacknife_221105.R'))

source(paste0(CODEDIR,'/SRC/Rcode/Functions_DMLUpsetPlots_231107.R'))

# NOT NEEDED NOW
#fn.DetectIfJobArray()

# ==========


# ==========
# safety checks
# ==========

stopifnot(
is.character(INPUTFILE),
is.character(INPUTDIR),
is.character(OUTPUTDIR),
is.character(RDSDIR)
)


paste('Variables in use are ...' ) %>% print
paste('CMMNT   is ...',	CMMNT  ) %>% print
paste('INPUTFILE   is ...',	INPUTFILE  )  %>% print
paste('INPUTDIR   is ...',	INPUTDIR  )  %>% print
paste('OUTPUTDIR   is ...',	OUTPUTDIR  )  %>% print

FILE.PATTERN = paste0(INPUTFILE,'.*',FILE.PATTERN.SUFFIX,'$')
'FILE.PATTERN.SUFFIX is ...'  %>% print
FILE.PATTERN.SUFFIX  %>% print
'FILE.PATTERN is ...'  %>% print
FILE.PATTERN  %>% print

RDBASE = RDSDIR

# ==============
# Read in the list of files to read, the function should pull all variables from global enviroment
# This also confirms that the variables are accessible from the global env
# ===============

stopifnot(
grepl("txt.bgz",FILE.PATTERN)
)


files.vec2split = fn.readincheck()

'File list generated ....' %>% print
print(files.vec2split)

# files.vec2split %<>% .[c(1,5)]

'Read in files ....' %>% print
meth.list = lapply(
files.vec2split,
readMethylDB
)


stopifnot(
class(meth.list[[1]]) == 'methylBaseDB'
)

sample.include.vec = sapply(meth.list, getSampleID)
treatment.char.vec = sapply(sample.include.vec ,
fn.mk.trtmt.vec)

# ========
# I have done away with the reorganise step entirely!!
# ========

#str(meth.list.db)

meth.united.db  = meth.list[[1]]

#meth.united.db %>% head
print('str of the final DB....')
meth.united.db %>% str
#meth.united.db %>% dim
#meth.united.db %>% getSampleID


'Starting the PCA jackknife...' %>% print
source(
paste0(CODEDIR,"/SRC/Rcode/Script_EMseq_MethylKit_perchrjacknife_221022.R"),
local = T)

#meth.united.db	%>% str

#full.pca %>% str

#jack.scores.procrustes.list %>% str

# ==========
# Procrustes df
# ==========

jack.distort.df = jack.scores.procrustes.list %>%
sapply(.,
function(x) c(procrustes.d =x$procrustes.d, tucker.value  = x$TuckCoef$tucker_value))   %>% t

# ==========
# Save as RDS
# ==========
OUTPUT = paste0(OUTPUTDIR,"/PCAJackknife_",INPUTFILE)
print(paste0('Output saved to ... ',OUTPUT ))

list(
main.pca = main.pca,
full.pca = full.pca,
pca.jack_final = pca.jack_final,
 jack.scores.procrustes.list = jack.scores.procrustes.list,
  jack.distort.df = jack.distort.df
  ) %>% saveRDS(paste0(OUTPUT,'.RDS'))

if(FALSE){

OUTPUT = paste0(OUTPUTDIR,"/PCA/PCAJackknife_",INPUTFILE)
print(paste0('Output saved to ... ',OUTPUT ))

pcajack.list = readRDS(paste0(OUTPUT,'.RDS'))
str(pcajack.list)

full.pca = pcajack.list$full.pca
main.pca = pcajack.list$main.pca
pca.jack_final = pcajack.list$pca.jack_final
jack.scores.procrustes.list = pcajack.list$jack.scores.procrustes.list
jack.distort.df = pcajack.list$jack.distort.df
}

static.guide = readRDS(paste0(RDBASE,'/','DB_PlotGuide_Static','.RDS'))

# =========
# Variance explained plot
# =========

cum.var.df = full.pca %>%
summary(.) %>%
.$importance %>%
t %>%
as.data.frame %>%
rownames_to_column('PC')	%>%
mutate(Top = {`Cumulative Proportion` < 0.90})

cum.var.df $PC %<>% factor(., levels = .)

p.percvar = cum.var.df  %>%
ggplot(data =	., aes(x = PC, y = `Proportion of Variance`, fill = Top))	+
geom_col()	+
scale_fill_manual(values=greychoice[1:2],
guide=guide_legend(title.position = "top"),
na.value=NULL,
name="First 90% of variance")	+
labs(
x="Component",y = "Proportion variance explained", tag = paste0("(","A",")"), title = 'Species Name Here')   +
academ_theme +
theme(axis.text.x = element_text(angle = 90),
plot.tag.position = "topleft")
#p.percvar



# Plot the PCA
axes2use = list(
c(x = "PC1", y = "PC2"),
c(x = "PC3", y = "PC4"),
c(x = "PC5", y = "PC6")
)


p.PC = lapply(
1:length(axes2use),
function(axes.subset.idx){

axes.subset = axes2use[[axes.subset.idx]]

data2use =  cbind(
main.pca[,axes.subset] %>%
as.data.frame %>%
rename_with(., ~ str_c(names(axes.subset)), everything()),

pca.jack_final[,axes.subset] %>%
as.data.frame  %>% rename_with(., ~ str_c(names(axes.subset),"_se"), everything())
)

labels = c("NP" = "Not polluted", "P" = "Polluted")

data2use  %<>%
rownames_to_column("samples") %>%
mutate(Treatment = samples %>%
gsub("pool","",.) %>%
gsub("[0-9\\-]","",.)	%>%
gsub("C|D","",.) %>% factor(., levels = names(static.guide$Pollution$guide.col))
)


x.axis.lab = cum.var.df %>%
dplyr::filter(PC == axes.subset["x"]) %>%
.$`Proportion of Variance` %>%
{. * 100} %>%
sprintf('%.1f',.) %>%
paste0(axes.subset["x"],' (',.,' %)')

y.axis.lab = cum.var.df %>%
dplyr::filter(PC == axes.subset["y"]) %>%
.$`Proportion of Variance` %>%
{. * 100} %>%
sprintf('%.1f',.) %>%
paste0(axes.subset["y"],' (',.,' %)')


p.temp =  ggplot(
data = data2use ,
aes(x = x,
y = y,
label = samples,
group = Treatment,
colour = Treatment)) +
geom_point( size = 1) +
    geom_errorbar( aes(ymin = y - y_se, ymax  = y + y_se,colour = Treatment)) +
    geom_errorbarh( aes(xmin = x - x_se, xmax  = x + x_se,colour = Treatment)) +
# the previous value of 0.5 for size adjustment was slightly too large on crowded plots
geom_text( vjust = 1.5, hjust = 0.5, size = geom.text.size*0.45) +
annotate( "text", x = , y = , size = geom.text.size*0.45) +
scale_colour_manual(
values = static.guide$Pollution$guide.revcol,
labels = c("Not polluted", "Polluted")) +
labs(x = x.axis.lab,
y = y.axis.lab,
tag = paste0("(",LETTERS[axes.subset.idx + 1],")"))   +
academ_theme +
guides(colour = guide_legend(),
plot.tag.position = "topleft")

p.temp %>% return
})


#compiled.plot
# =========

# If the panel is split into distinct sections (e.g. tow different types of plots)
# these values are for the height/width of the upper right section

compiled.plot = ggdraw() +
# barplot
draw_plot(p.percvar+ theme(legend.position='none'),
x = 0, y =  0.5, width = 0.5, height = 0.5) +
draw_plot(p.PC[[1]]+ theme(legend.position='none'),
x = 0, y =  0, width = 0.5, height = 0.5) +
draw_plot( p.PC[[2]]+ theme(legend.position='none'),
x = 0.5, y =  0.5, width = 0.5, height = 0.5) +
draw_plot(p.PC[[3]]+ theme(legend.position='none'),
x = 0.5, y =  0, width = 0.5, height = 0.5)

OUTPUT = paste0(OUTPUTDIR,"/Plot_PCAMat_wjackbars_",INPUTFILE,".",device2use)
print(paste0('Output saved to ... ',OUTPUT ))

ggsave(compiled.plot,
filename = paste0(OUTPUTDIR,"/Plot_PCAMat_wjackbars","_",TODAY,".svg"),
width = 112,
height = 112,
units = "mm",
dpi = 300,
device = "svg")


# ===========
# Publication plot
# ===========

publish.plot = ggdraw() +
# barplot
draw_plot(p.percvar+ theme(legend.position='none'),
x = 0, y =  0.6, width = 1, height = 0.4) +
draw_plot(p.PC[[1]]+ theme(legend.position='none'),
width = 1, height = 0.6, x = 0, y =0)

# =========
#compiled.plot
# =========

OUTPUT = paste0(OUTPUTDIR,"/Plot_PC1_PCAMat_wjackbars_",INPUTFILE,".",device2use)
print(paste0('Output saved to ... ',OUTPUT ))

ggsave(publish.plot,
filename = OUTPUT,
width = 110,
height = 180,
units = "mm",
dpi = 300,
device=device2use
)

# ========
# legends
# ========

leg.grob1 = fn.g.legend(p.PC[[1]])
leg.grob2 = fn.g.legend(p.percvar)


compiled.legend.plot = ggdraw() +
draw_plot(leg.grob1, x = 0, y =  0, width = 0.4, height = 1)  +
draw_plot(leg.grob2, x = 0.4, y =  0, width = 0.6, height = 1)

OUTPUT = paste0(OUTPUTDIR,"/Plot_Legend_PCAMat_wjackbars_",INPUTFILE,".",device2use)
print(paste0('Output saved to ... ',OUTPUT ))

ggsave(compiled.legend.plot,
filename = OUTPUT,
width = 80,
height = 60,
units = "mm",
dpi = 300,
device=device2use
)



# =====
# Procrustes
# =====

OUTPUT = paste0(OUTPUTDIR,"/Plot_TuckVsProcrustes_Correlation_",INPUTFILE,".",device2use)
print(paste0('Output saved to ... ',OUTPUT ))

p.Procrustes = jack.distort.df %>%
as.data.frame %>%
rownames_to_column("chr") %>%
ggplot(aes(x = procrustes.d, y = tucker.value, label = chr)) +
geom_point() +
geom_text( vjust = 1.5, hjust = 0.5, size = geom.text.size*0.5) +
labs(x="Procrustes D",y = "Tucker Coefficient") +
#lims(y = c(0.99, 1)) +
geom_smooth(method = "lm", show.legend = T, colour = "black") +
academ_theme

ggsave(p.Procrustes,
filename = paste0(OUTPUTDIR,"/Plot_TuckVsProcrustes_Correlation","_",TODAY,".svg"),
width = 96,
height = 96,
units = "mm",
dpi = 300,
device = "svg")

#jack.distort.df %>% as.data.frame %>%
#lm(data = .,  tucker.value ~ procrustes.d) %>% summary


