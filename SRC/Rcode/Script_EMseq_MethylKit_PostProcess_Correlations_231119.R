#! /bin/R

#print('Not ready for automation!')
#stopifnot(FALSE)

# ========
# Run Startup scripts
# ========
CMMNT = Sys.getenv(c('CMMNT'))

# Load Env variables
source(paste0(CMMNT,'/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.R'))

# Load general R stuff
# This is now in the Env vars step above
source(paste0(CODEDIR,'/StartupScript_Sources_260220.R'))
# =============


# ============

if(TRUE){
args = commandArgs(TRUE)
INPUTFILE = args[1]
INPUTDIR = args[2]
OUTPUTDIR = args[3]
FILEPATTERNSUFFIX = args[4]
}


if(FALSE){

OUTPUTDIR = Sys.getenv(c('OUTPUTDIR'))
INPUTDIR = paste0(OUTPUTDIR)

OUTPUTDIR = '/scratch3/users/rjdaniels/TMP_rjdaniels_fe62b3d7-54bc-4b4a-8aab-9615d905e98a'
INPUTDIR = '/scratch3/users/rjdaniels/TMP_rjdaniels_fe62b3d7-54bc-4b4a-8aab-9615d905e98a'

INPUTFILE = 'MethylKit_Correlations_processed'
FILEPATTERNSUFFIX = ".rds"

}

# ==========

#BiocManager::install(c('genomation', 'AnnotationHub', 'GenomicFeatures', 'methylKit', 'VariantAnnotation'), force=F)
#BiocManager::install(c('methylKit', 'VariantAnnotation'), force=F)

pacman::p_load(papeR, tidyverse, magrittr, ggthemes, e1071, genomation, AnnotationHub, GenomicFeatures, methylKit)

# ============

device2use='svg'
stopifnot(
{device2use %in% c('jpeg','jpg','png','pdf','svg','eps')})


# ==========
# Load functions
# ==========
source(paste0(CODEDIR,'/Rsourcecode/Functions_EMseq_MethylKit_221124.R'))
source(paste0(CODEDIR,"/SRC/Rcode/Functions_DMLUpsetPlots_231107.R"))

# ==========

stopifnot(
is.character(INPUTDIR),
is.character(OUTPUTDIR),
is.character(INPUTFILE),
is.character(FILEPATTERNSUFFIX))

print(paste('Variables in use are ...' ))
print(paste('OUTPUTDIR   is ...',	OUTPUTDIR  ))
print(paste('INPUTDIR   is ...',	INPUTDIR  ))
print(paste('INPUTFILE  is ...',	INPUTFILE  ))
print(paste('FILEPATTERNSUFFIX  is ...',	FILEPATTERNSUFFIX  ))

# ============

seq.nonpool.db = readRDS(paste0(RDSDIR,'/','DB_SequencingInfo_nonpool','.RDS'))
multiqc.sum.allstages.db = readRDS(paste0(RDSDIR,'/','DB_Bismark_MultiQC','.RDS'))
static.guide = readRDS(paste0(RDSDIR,'/','DB_PlotGuide_Static','.RDS'))

# =================
'List tbl rds to read in...' %>% print
# =================


####  Start from already-processed rds
if(FALSE){
#tmp.outfile.descriptor = 'centred'
 tmp.outfile.descriptor = 'uncentred'

 # RDS
ltx.tit = paste0('Correlations_',tmp.outfile.descriptor)
outname = paste0(OUTPUTDIR,"/DB",'_ProcessedData_',ltx.tit,'.rds' )

tmp = readRDS(outname)

# cor.cent.tbl.df = tmp[['data']]
# cor.cent.tbl.sumry.df = tmp[['summary']]

 cor.uncent.tbl.df = tmp[['data']]
 cor.uncent.tbl.sumry.df = tmp[['summary']]
}


####  Start from new read-in of raw percentage values

filename2use = paste0(INPUTFILE,".*",FILEPATTERNSUFFIX)

files.list = list.files(
pattern = filename2use ,
path = paste0(INPUTDIR),
full.names = T,
recursive = T)  %>%
grep('archive', ., value = TRUE,  invert = TRUE) %>%
    as.list %>% rev



# files.list %>% str
#tmp.dat = readRDS(files.vec[1]) 
#tmp.dat  %>% str


# =================
'read in...' %>% print
# =================

if(FALSE){
files.list  %<>% .[1:3]
}

rds.list = lapply(files.list , readRDS)
str(rds.list )

# A named list ... first layer is the part of the path to pull infor form ... below that is the parts to use and the name to designate to that part

# This won't work if I run as slurm... all inputs are copied to the scratch dir and the path changes ...

path.params =  list(
 '7' = c('chr' = 3))


rds.list.qualifiers.df =  files.list   %>%  
lapply(.,
fn.InfoFromPath,
 params2use = path.params
 ) %>% bind_rows

#rds.list.qualifiers.df %>% str


# corbicula input requires some extra clean up to remove duplicated entries
#str(rds.list)

rds.list %<>% lapply(
.,
function(x){
x %>%
dplyr::select( -starts_with('V')) %>%
dplyr::select( -ends_with('.1')) %>%
return
}) 



#### Summary of percentage values
if(FALSE){
## !! NOT IN USE !!

# correlation with uncentred data

#rds.list[[1]]
#rds.list[[1]] %>% names


summary.uncent.tbl.list = lapply(1:length(rds.list), function(idx){

perc.df  = rds.list[[idx]]

perc.df %<>% 
dplyr::select(starts_with('perc')) %>%
apply(.,2,mean,na.rm = T)

perc.df %>%
cbind(.,rds.list.qualifiers.df[idx,]) %>%
return
})

#summary.uncent.tbl.list %>% lapply(.,head)


# ==============
# Create single df for downstream
# ==============


summary.uncent.tbl.df = summary.uncent.tbl.list  %>%
bind_rows()

str(summary.uncent.tbl.df )

names(summary.uncent.tbl.df )[1] = 'perc'

summary.uncent.tbl.sumry.df = summary.uncent.tbl.df %>%
group_by(x,y) %>%
dplyr::summarise(across(
.cols = r,
.fns = list(mean = ~mean(.x, na.rm = T),sd = ~sd(.x, na.rm = T)),
.names = "{fn}_{col}"
),
species = unique(species),
DATASRCIDX1 = unique(DATASRCIDX1),
mincov = unique(mincov),
pollution = unique(pollution),
pool = unique(pool),
tile = unique(tile),
minpergrp = unique(minpergrp)
) 

}



#### Correlations

# correlation with uncentred data
cor.uncent.tbl.list = lapply(1:length(rds.list), function(idx){

perc.df  = rds.list[[idx]]

perc.df %<>% 
dplyr::select(starts_with('perc')) %>%
corrr::correlate() %>%
corrr::shave() %>%
corrr::stretch() 

perc.df %>%
cbind(.,rds.list.qualifiers.df[idx,]) %>%
return
})

#cor.uncent.tbl.list %>% lapply(.,head)

# correlation with data centred

cor.cent.tbl.list = lapply(1:length(rds.list) , function(idx){
perc.df  = rds.list[[idx]]

perc.df %<>% 
dplyr::select(starts_with('centred')) %>%
corrr::correlate() %>%
corrr::shave() %>%
corrr::stretch() 

perc.df %>%
cbind(.,rds.list.qualifiers.df[idx,]) %>%
return

})

#str(cor.cent.tbl.list)
#cor.cent.tbl.list %>% lapply(.,head)

# colnames = x, y, r
# need to add in some qualifier for the type of data/data subset

# ==============
# Create single df for downstream
# ==============

cor.uncent.tbl.df = cor.uncent.tbl.list  %>%
bind_rows()

cor.uncent.tbl.sumry.df = cor.uncent.tbl.df %>%
group_by(x,y) %>%
dplyr::summarise(across(
.cols = r,
.fns = list(mean = ~mean(.x, na.rm = T),sd = ~sd(.x, na.rm = T)),
.names = "{fn}_{col}"
)) 


cor.cent.tbl.df = cor.cent.tbl.list  %>%
bind_rows()

cor.cent.tbl.sumry.df = cor.cent.tbl.df %>%
group_by(x,y) %>%
dplyr::summarise(across(
.cols = r,
.fns = list(mean = ~mean(.x, na.rm = T),sd = ~sd(.x, na.rm = T)),
.names = "{fn}_{col}"
)) 


#### Export RDS
 # Processed data

 # ==========
# centred
# ==========

dat2use = cor.cent.tbl.sumry.df
dat2use.nonsum = cor.cent.tbl.df
tmp.outfile.descriptor = 'centred'

 # RDS
ltx.tit = paste0('Correlations_',tmp.outfile.descriptor)
 outname = paste0(OUTPUTDIR,"/DB",'_ProcessedData_',ltx.tit,'.rds' )

list(data =  dat2use.nonsum, summary = dat2use) %>%
saveRDS(outname)

# ==========
# Uncentred
# ==========

dat2use = cor.uncent.tbl.sumry.df
dat2use.nonsum = cor.uncent.tbl.df
tmp.outfile.descriptor = 'uncentred'

 # RDS
ltx.tit = paste0('Correlations_',tmp.outfile.descriptor)
 outname = paste0(OUTPUTDIR,"/DB",'_ProcessedData_',ltx.tit,'.rds' )

list(data =  dat2use.nonsum, summary = dat2use) %>%
saveRDS(outname)

#### plots

fn.corr.cats = function(tmp.df){
if(
# if both samples are from the same treatment
grepl('NP',tmp.df['x']) == grepl('NP',tmp.df['y'])
){
# mark as within-treatment
tmp.df$comparison = 'within'
}else{
tmp.df$comparison = 'between'
}


if(
# if both samples are pooled
grepl('[Pp]ool',tmp.df$x) && grepl('[Pp]ool',tmp.df$y)
){
# mark both pooled
tmp.df$pool.stat = '2'
}else if(
# if any sample is pooled
grepl('[Pp]ool',tmp.df$x) || grepl('[Pp]ool',tmp.df$y)
){
tmp.df$pool.stat = '1'
}else{
# mark as not pooled
tmp.df$pool.stat = '0'
}

return(tmp.df)
}


dat2plot = apply(as.matrix(dat2use), 1,fn.corr.cats ) %>%
bind_rows %>%
mutate(r = as.numeric(mean_r))

str(dat2plot)

names(dat2use)

filename2use = paste0(OUTPUTDIR,"/Plot_Correlations",'_',tmp.outfile.descriptor)

'Producing the figure...' %>% print

p1 =  dat2plot  %>%
ggplot(aes(x = comparison, y = r, colour = pool.stat)) +
geom_violin(
position = position_dodge(),
alpha = 0.5, linewidth = 0.7, fill = NA,
draw_quantiles = c(0.25, 0.5, 0.75), drop = F) +
geom_point(
position = position_jitterdodge(),
alpha = 0.5, size = 1.5, drop = F)  +
scale_colour_manual(  
labels = c('Ind','Pool-Ind','Pool'),
values = c('black','grey80','grey30')
) +
#scale_shape_discrete(  labels = c('Ind','Pool-Ind','Pool')) +
guides(
colour = guide_legend(title = 'Library preparation')
#shape = guide_legend(title = 'Library preparation'),
) +
#facet_grid(rows = vars(species)) + # One species at a time
academ_theme +
labs(y ="Pearson correlation coefficient", x = "Grouping")  +
theme(legend.position="bottom")

 'Printing the figure...' %>% print
 # allowed width for figures: 80,	112,	169mm
ggsave(p1 ,
filename = paste0(filename2use,'.',device2use),
width = 112,
height = 90,
units = "mm",
dpi = 300,
device = device2use)


#### Export Tables

# =====
# By group and pollution summary
# =====

outname = paste0(OUTPUTDIR,"/SupTable_Correlations",'_',tmp.outfile.descriptor)

#dat2plot %>% str


# split by groups
table.nonpoll.category.summary =  dat2plot %>%
dplyr::filter(comparison == 'within' & grepl('NP',x)) %>%
group_by(pool.stat,comparison) %>%
mutate(mean_r = as.numeric(mean_r)) %>%
dplyr::summarise(
min = min(mean_r,na.rm = T),
max = max(mean_r,na.rm = T),
mean = mean(mean_r,na.rm = T)
) %>%
mutate(poll = 'NP')

table.poll.category.summary =  dat2plot %>%
dplyr::filter(comparison == 'within' & !grepl('NP',x)) %>%
group_by(pool.stat,comparison) %>%
mutate(mean_r = as.numeric(mean_r)) %>%
dplyr::summarise(
min = min(mean_r,na.rm = T),
max = max(mean_r,na.rm = T),
mean = mean(mean_r,na.rm = T)
) %>%
mutate(poll = 'P')
 
table.all.category.summary = bind_rows(table.poll.category.summary,table.nonpoll.category.summary)

ltx.tit = paste0('CorrelationByGroupxPoll',tmp.outfile.descriptor)


# ODS
table.all.category.summary  %>%
 write_ods(
 path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit,"_",TODAY),
 x = .,
 append = F,
 update = F)

# Latex

ltx.lab2use = paste0('stab:Results:')
ltx.cap2use = paste0('Summary of the Pearson correlation coefficients in pairwise tests. ')

table.all.category.summary  %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,'_',ltx.tit ,".tex"),
include.rownames=F)

# RDS

table.all.category.summary  %>%
saveRDS(paste0(OUTPUTDIR,"/DB",'_Summary_',ltx.tit,'.rds' ))


# =====
# By group summary
# =====

outname = paste0(OUTPUTDIR,"/SupTable_Correlations",'_',tmp.outfile.descriptor)

#dat2plot %>% str


# split by groups
table.all.category.summary = dat2plot %>%
group_by(pool.stat,comparison) %>%
mutate(mean_r = as.numeric(mean_r)) %>%
dplyr::summarise(
min = min(mean_r,na.rm = T),
max = max(mean_r,na.rm = T),
mean = mean(mean_r,na.rm = T)
)


ltx.tit = paste0('CorrelationByGroup',tmp.outfile.descriptor)


# ODS
table.all.category.summary  %>%
 write_ods(
 path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit,"_",TODAY),
 x = .,
 append = T,
 update = F)

# Latex

ltx.lab2use = paste0('stab:Results:')
ltx.cap2use = paste0('Summary of the Pearson correlation coefficients in pairwise tests. ')

table.all.category.summary  %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,'_',ltx.tit ,".tex"),
include.rownames=F)

# RDS

table.all.category.summary  %>%
saveRDS(paste0(OUTPUTDIR,"/DB",'_Summary_',ltx.tit,'.rds' ))

# =====
# In total
# =====

outname = paste0(OUTPUTDIR,"/DB",'_Summary_',ltx.tit)

# split by groups
table.all.category.summary = dat2plot %>%
mutate(mean_r = as.numeric(mean_r)) %>%
dplyr::summarise(
min = min(mean_r,na.rm = T),
max = max(mean_r,na.rm = T),
mean = mean(mean_r,na.rm = T)
)

ltx.tit = paste0('CorrelationAll',tmp.outfile.descriptor)

# ODS
table.all.category.summary  %>%
 write_ods(
 path = paste0(outname,".ods"),
 sheet = paste0(ltx.tit,"_",TODAY),
 x = .,
 append = F,
 update = F)

# Latex

ltx.lab2use = paste0('stab:Results:')
ltx.cap2use = paste0('Summary of the Pearson correlation coefficients in pairwise tests. ')

table.all.category.summary  %>%
xtable(.,
label=ltx.lab2use,
caption = ltx.cap2use) %>%
print(.,
file = paste0(outname,'_',ltx.tit ,".tex"),
include.rownames=F)

 # RDS
table.all.category.summary  %>%
saveRDS(paste0(OUTPUTDIR,"/DB",'_Summary_',ltx.tit,'.rds' ))

