#! /bin/R

# Summary of raw data, can be looped internally or via SLURM arrays

# ============
# ROAD MAP
# ============
# ============

# ========
# Run Startup scripts
# ========
CMMNT = Sys.getenv(c('CMMNT'))

# Load Env variables
source(paste0(CMMNT,'/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.R'))

# Load general R stuff
# This is now in the Env vars step above
source(paste0(CODEDIR,'/StartupScript_Sources_260220.R'))

# =============

# ============
args = commandArgs(TRUE)

#FILEROOTLIST = args[2] # this was for arrays within R
INPUTFILE = args[1] # The file pattern can now be a single file or something that matches a number of files

INPUTDIR = args[2]

OUTPUTDIR = args[3]

FILE.PATTERN.SUFFIX = args[4] # The file pattern can now be a single file or something 

EXCLUDE.REGEX = args[5]

MINOVRLAP  = args[6]

SAMPLE_EXCLUDE_VEC = args[7]

# ============
# For interactive work
# ============

if(FALSE){

INPUTFILE = Sys.getenv(c('INPUTFILE'))
METHKITDIR = Sys.getenv(c('METHKITDIR'))
INPUTDIR = paste0(METHKITDIR,'/RawSummary')

OUTPUTDIR = paste0(OUTPUTDIR)

EXCLUDE.REGEX = Sys.getenv(c('EXCLUDE.REGEX'))
FILE.PATTERN.SUFFIX = Sys.getenv(c('FILE.PATTERN.SUFFIX'))
MINOVRLAP = Sys.getenv(c('MINOVRLAP'))
SAMPLE_EXCLUDE_VEC = Sys.getenv(c('SAMPLE_EXCLUDE_VEC'))


INPUTFILE = ''
FILE.PATTERN.SUFFIX = 'normed.txt.bgz'
EXCLUDE.REGEX = 'NONE'
}

# ==========

# change of plan, only source scripts with functions,
# dont nest calls to source
# source causes issues with finding objects

# ============

pacman::p_load(methylKit)

# ============
device2use='jpeg'

#device2use='1'
stopifnot(
{device2use %in% c('jpeg','jpg','png','pdf','svg','eps')})

# ==========
# Load functions
# ==========

source(paste0(CODEDIR,'/SRC/Rcode/Functions_EMseq_MethylKit_221124.R'))

source(paste0(CODEDIR,'/SRC/Rcode/Functions_EMseq_MethylKit_PCAjacknife_221105.R'))

source(paste0(CODEDIR,'/SRC/Rcode/Functions_DMLUpsetPlots_231107.R'))

# NOT NEEDED NOW
#fn.DetectIfJobArray() 

# ==========


# ==========
# safety checks
# ==========

stopifnot(
is.character(INPUTFILE),
is.character(INPUTDIR),
is.character(OUTPUTDIR),
is.character(EXCLUDE.REGEX),
is.character(SAMPLE_EXCLUDE_VEC)
)

print(paste('Variables in use are ...' ))
print(paste('CMMNT   is ...',	CMMNT  ))
print(paste('INPUTFILE   is ...',	INPUTFILE  ))
print(paste('INPUTDIR   is ...',	INPUTDIR  ))
print(paste('OUTPUTDIR   is ...',	OUTPUTDIR  ))
print(paste('SAMPLE_EXCLUDE_VEC   is ...',	SAMPLE_EXCLUDE_VEC  ))
print(paste('EXCLUDE.REGEX   is ...',	EXCLUDE.REGEX  ))

FILE.PATTERN = paste0(INPUTFILE,'.*',FILE.PATTERN.SUFFIX,'$')
print('FILE.PATTERN.SUFFIX is ...')
print(FILE.PATTERN.SUFFIX)
print('FILE.PATTERN is ...')
print(FILE.PATTERN)

sample.exclude.vec = SAMPLE_EXCLUDE_VEC

# ==============
# Read in the list of files to read, the function should pull all variables from global enviroment
# This also confirms that the variables are accessible from the global env
# ===============

stopifnot(
grepl("txt.bgz",FILE.PATTERN)
)

files.vec2split = fn.readincheck()

'File list generated ....' %>% print
print(files.vec2split)

# files.vec2split %<>% .[c(1,5)]

'Read in files ....' %>% print
meth.list = lapply(
files.vec2split,
readMethylDB
)


stopifnot(
class(meth.list[[1]]) == 'methylRawDB'
)

sample.include.vec = sapply(meth.list, getSampleID)
treatment.char.vec = sapply(sample.include.vec ,
fn.mk.trtmt.vec)

#--------------  Conversion from a list of methylRaw to methylRawListDB

print(paste0("formalising list to methylRawList..."))

# meth.list is a list of RawDB (at least some of the time)
# The function below does not take a list, so unlist and pass in
# !! This may mess up later jobs as it might create methylRawListDB inside methylRawListDB

meth.list.db = methylRawListDB(
unlist(meth.list, recursive=F),
treatment = treatment.char.vec )


stopifnot(
class(meth.list.db) == 'methylRawListDB'
)
rm('meth.list')
#--------------  Check the current format/class

print(paste0("str after the conversion..."))
str(meth.list.db)

print(paste0("Print treatment info.."))
getTreatment(meth.list.db) %>% print

#--------------  Corrections to names and treatments

'corrected sample names... ' %>% print
meth.list.db %>% lapply(.,getSampleID) %>% unlist %>% print
print("Check if the obj is DB or not...")
class(meth.list.db) %>% print

# ========
# I have done away with the reorganise step entirely!!
# ========

#str(meth.list.db)

'Setting the min overlap per treatment group... ' %>% print

overlap.prp = MINOVRLAP	%>% as.numeric

'obj str before uniting...' %>% print
meth.list.db %>% str

# This manual setting treatments is not needed, the treatments are correct... but they disappear after uniting
#'manually set treatments...' %>% print
#getTreatment(meth.list.db ) = getSampleID(meth.list.db) %>% fn.mk.trtmt.vec

# get the number of reps per treatment, then get the min of the two treatments
treatment.size = meth.list.db@treatment	%>% table %>% min

# Get the number of inds that make up the overlap.prp for a treatment group
temp.min.per.grp = round({treatment.size*overlap.prp}) %>% as.integer

if(temp.min.per.grp < 1){
temp.min.per.grp = 1 %>% as.integer
}


'The treatment vec informing the uniting is ... ' %>% paste0(., meth.list.db@treatment )  %>% print
'The minpergrp value will be ... ' %>% paste0(., temp.min.per.grp )  %>% print
'based on overlap prop of... ' %>% paste0(., overlap.prp )  %>% print

#treatment.size
#temp.min.per.grp

meth.united.db = unite(meth.list.db ,
min.per.group = temp.min.per.grp ,
mc.cores = nThreads,
chunk.size = 1e+06,
save.db = T,
dbdir = paste0(OUTPUTDIR,"/methylDB"),
suffix = paste0("united_minpergrp_",overlap.prp))


print('Done uniting data.')
# ============
# Unite samples here!!
# ============

#meth.united.db %>% head
print('str of the final DB....')
meth.united.db %>% str
#meth.united.db %>% dim
#meth.united.db %>% getSampleID


n.vars.df = data.frame(
Context = meth.united.db@context,
n.vars = meth.united.db@num.records,
res = meth.united.db@resolution,
stage = "unitedpostfilter")
print('Done creating	table....')


# 'remove low variance sites...' %>% print
#
# # identify vars to keep
#
# meth.united.db
# # select vars to keep
#
# # save as DB
#
# tmp = as(meth.united.db, 'methylRawListDB')
#
# meth.united.db = unite(tmp ,
# min.per.group = temp.min.per.grp ,
# mc.cores = nThreads,
# chunk.size = 1e+04,
# save.db = T,
# dbdir = paste0(OUTPUTDIR,"/methylDB"),
# suffix = paste0("united_minpergrp_",overlap.prp,"_postVar_" ,TODAY))
#
# print('Done uniting data.')

# 'Create table of post Var filtered values'
# if(class(meth.united.db) == "methylBaseDB"){
# n.vars.df = data.frame(
# Context = meth.united.db@context,
# n.vars = meth.united.db@num.records,
# res = meth.united.db@resolution,
# stage = "united_postCov_postOvl_preVar")
# }else{
# n.vars.df = data.frame(
# Context = meth.united.db@context,
# n.vars = dim(meth.united.db) %>% .[1],
# res = meth.united.db@resolution,
# stage = "united_postCov_postOvl_preVar")
# }
# print('Done creating	table.')



# n.vars.df
# str(meth.list.db)
# str(meth.united.db)


# It looks like spearman only works for non-DB objects
# Can't easily save the output from the getCorrelation function


# adding in a way to export the correlation matrices

perc.mat = percMethylation(meth.united.db) 
colnames(perc.mat) %<>%
paste0('perc_',.)

centred.perc.df  = perc.mat %>%
apply(.,1,
function(x){
mn.tmp = mean(x, na.rm = T)
var.tmp = var(x, na.rm = T)
std.tmp = sqrt(var.tmp)

centred.row = {mn.tmp - x } 

names(centred.row) %<>%
paste0('centred_',.)

centred.df = data.frame(mean = mn.tmp,
var = var.tmp,
std = std.tmp,
t(centred.row))
return(centred.df)
}
) %>%
bind_rows
str(centred.perc.df)
  
'export tbl as rds...'
filename2use = paste0(OUTPUTDIR,"/MethylKit_Correlations_processed","_minovrlap",MINOVRLAP,".rds")

# tbl.list =  list(perc.tbl = as_tibble(perc.mat), var.tbl = as_tibble(var.mat))  

perc.df =	bind_cols(as_tibble(perc.mat),centred.perc.df)

perc.df  %>% saveRDS(filename2use)

## this would be the place to estimate variance and remove non-variable loci

rbind(n.vars.df) %>%
write.table(.,
file = paste0(OUTPUTDIR,"/MethylKit_NumberVars_united_minpergrp",overlap.prp,".tsv"),
quote = F,
row.names = F)

print('Done	writing	table....')

# =================
#  Export plots last 
#  Incase they cause R to crash
# =================

pdf(paste0(OUTPUTDIR,"/Clustering_minpergrp_",overlap.prp,".pdf"))
getCorrelation(meth.united.db,
plot=TRUE,
method = "pearson",
nrow = 1e+05)

clusterSamples(meth.united.db,
dist="correlation", method="ward", plot=TRUE)

PCASamples(meth.united.db, screeplot=TRUE)

PCASamples(meth.united.db,
comp = c(1:2))

PCASamples(meth.united.db,
comp = c(3:4))

PCASamples(meth.united.db,
comp = c(5:6))

dev.off()

print('Done	writing	PCA.')

# ============
# hc = clusterSamples(meth, dist="correlation", method="ward", plot=FALSE)

print('End of script....')

