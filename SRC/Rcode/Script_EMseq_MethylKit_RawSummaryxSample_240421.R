#! /bin/R

# Summary of raw data, can be looped internally or via SLURM arrays

# ============
# ROAD MAP
# ============
# ============

# ========
# Run Startup scripts
# ========
CMMNT = Sys.getenv(c('CMMNT'))

# Load Env variables
source(paste0(CMMNT,'/Platform_Specifics/Script_Platform_VariableMap_LocalOnly.R'))

# Load general R stuff
# This is now in the Env vars step above
source(paste0(CODEDIR,'/StartupScript_Sources_260220.R'))

# =============

# ============

if(TRUE){

args = commandArgs(TRUE)

#FILEROOTLIST = args[2] # this was for arrays within R
INPUTFILE = args[1] # The file pattern can now be a single file or something that matches a number of files

INPUTDIR = args[2]

OUTPUTDIR = args[3]

FILE.PATTERN.SUFFIX = args[4] # The file pattern can now be a single file or something 

EXCLUDE.REGEX = args[5]

MINCOVFILTER = args[6]

SAMPLE_EXCLUDE_VEC = args[7]

}


if(FALSE){

INPUTFILE = Sys.getenv(c('INPUTFILE'))
INPUTDIR = paste0(Sys.getenv(c('OUTPUTDIR')),'/RawDB')

EXCLUDE.REGEX = Sys.getenv(c('EXCLUDE.REGEX'))
FILE.PATTERN.SUFFIX = Sys.getenv(c('FILE.PATTERN.SUFFIX'))
MINCOVFILTER = Sys.getenv(c('MINCOVFILTER'))
SAMPLE_EXCLUDE_VEC = Sys.getenv(c('SAMPLE_EXCLUDE_VEC'))

INPUTFILE = ''
EXCLUDE.REGEX = 'NONE'
FILE.PATTERN.SUFFIX='.txt.bgz'

}



# ==========

# change of plan, only source scripts with functions,
# dont nest calls to source
# source causes issues with finding objects

# ============

pacman::p_load(methylKit)

# ============
device2use='jpeg'

#device2use='1'
stopifnot(
{device2use %in% c('jpeg','jpg','png','pdf','svg','eps')})

# ==========
# Load functions
# ==========

source(paste0(CODEDIR,'/SRC/Rcode/Functions_EMseq_MethylKit_221124.R'))

source(paste0(CODEDIR,'/SRC/Rcode/Functions_EMseq_MethylKit_PCAjacknife_221105.R'))

source(paste0(CODEDIR,'/SRC/Rcode/Functions_DMLUpsetPlots_231107.R'))

# NOT NEEDED NOW
#fn.DetectIfJobArray() 

# ==========


# ==========
# safety checks
# ==========

stopifnot(
is.character(INPUTFILE),
is.character(INPUTDIR),
is.character(OUTPUTDIR),
is.character(EXCLUDE.REGEX),
is.character(SAMPLE_EXCLUDE_VEC),
is.character(MINCOVFILTER) | is.numeric(MINCOVFILTER)
)


print(paste('Variables in use are ...' ))
print(paste('CMMNT   is ...',	CMMNT  ))
print(paste('INPUTFILE   is ...',	INPUTFILE  ))
print(paste('INPUTDIR   is ...',	INPUTDIR  ))
print(paste('OUTPUTDIR   is ...',	OUTPUTDIR  ))
print(paste('SAMPLE_EXCLUDE_VEC   is ...',	SAMPLE_EXCLUDE_VEC  ))
print(paste('EXCLUDE.REGEX   is ...',	EXCLUDE.REGEX  ))

print('FILE.PATTERN.SUFFIX is ...')
print(FILE.PATTERN.SUFFIX)
FILE.PATTERN = paste0(INPUTFILE,'.*',FILE.PATTERN.SUFFIX,'$')
print('FILE.PATTERN is ...')
print(FILE.PATTERN)
print('MINCOVFILTER is ...')
print(MINCOVFILTER)

sample.exclude.vec = SAMPLE_EXCLUDE_VEC
MINCOVFILTER %<>% as.numeric()

# ==============
# Read in the list of files to read, the function should pull all variables from global enviroment
# This also confirms that the variables are accessible from the global env
# ===============


# ===============
# Read in data
# ===============

stopifnot(
grepl("txt.bgz",FILE.PATTERN)
)

files.vec2split = fn.readincheck()

if(FALSE){
files.vec2split %<>% .[1:5]
}

meth.list  =  lapply(
files.vec2split,
readMethylDB)

stopifnot(
class(meth.list [[1]]) == 'methylRawDB'

)

sample.include.vec = sapply(meth.list, getSampleID)
treatment.char.vec = sapply(sample.include.vec ,
fn.mk.trtmt.vec)

#--------------  Conversion from a list of methylRaw to methylRawListDB

print(paste0("formalising list to methylRawList..."))

# meth.list is a list of RawDB (at least some of the time)
# The function below does not take a list, so unlist and pass in
# !! This may mess up later jobs as it might create methylRawListDB inside methylRawListDB

meth.list.db = methylRawListDB(
unlist(meth.list, recursive=F),
treatment = treatment.char.vec )
stopifnot(
class(meth.list.db) == 'methylRawListDB'
)
rm('meth.list')
#--------------  Check the current format/class

print(paste0("str after the conversion..."))
str(meth.list.db)
print(paste0("Print treatment info.."))
getTreatment(meth.list.db) %>% print

#--------------  Corrections to names and treatments

'corrected sample names... ' %>% print
meth.list.db %>% lapply(.,getSampleID) %>% unlist %>% print
print("Check if the obj is DB or not...")
class(meth.list.db) %>% print

# ========
# I have done away with the reorganise step entirely!!
# ========

# ===============
# Default plots
# ===============
#str(meth.list.db)

pdf(paste0(OUTPUTDIR,"/Stats_Histograms_PreFilter_",INPUTFILE,".pdf"))

lapply(meth.list.db, function(item){

print(getSampleID(item))

getMethylationStats(item, plot = TRUE, both.strands = FALSE)

getCoverageStats(item, plot = TRUE, both.strands = FALSE )
})

dev.off()


# ===============
# Custom Summaries
# ===============

n.vars.prefilt.df = meth.list.db %>%
lapply(., fn.loc.count) %>%
bind_rows() %>%
mutate(stage = "prefilter")

# n.vars.prefilt.df = meth.list.db %>%
# lapply(., function(item){if(class(item)=="methylRaw"){
# data.frame(
# Sample.Code = item@sample.id,
# Context = item@context,
# n.vars = getData(item)	%>% dim(.) %>% .[1],
# res = item@resolution,
# stage = "prefilter"
# ) %>% return
# }else{
# data.frame(
# Sample.Code = item@sample.id,
# Context = item@context,
# n.vars =item@num.records,
# res = item@resolution,
# stage = "prefilter"
# )} %>% return}
# ) %>% bind_rows()


# ===============
#n.vars.prefilt.df
# ===============
print(paste("Filtering..."))
print(paste("using mincov = ",MINCOVFILTER))

meth.list.db %<>% filterByCoverage(.,
lo.count = as.numeric(MINCOVFILTER),
lo.perc = NULL,
hi.count = NULL,
hi.perc = 99.9,
dbdir = paste0(OUTPUTDIR,"/methylDB"),
save.db = TRUE,
dbtype = "tabix",
suffix = paste0("filtered")
)

print("Check if the obj is DB or not...")
class(meth.list.db)

n.vars.postfilt.df = meth.list.db %>%
lapply(., fn.loc.count ) %>%
bind_rows() %>%
mutate(stage = "postfilter") %>%
arrange(Sample.Code)

# ===============
# Export df or vars
# ===============
# Writing out the summary tables if needed
# These tables do not get written out directly to a spreadsheet or rds
	# write out a plain text file, this can be read in with all the others to produce a singlet table
# ===============

pdf(paste0(OUTPUTDIR,"/Stats_Histograms_PostMinCov",INPUTFILE,".pdf"))
lapply(meth.list.db, function(item){	
print(getSampleID(item))
getMethylationStats(item, plot = TRUE, both.strands = FALSE)
getCoverageStats(item, plot = TRUE, both.strands = FALSE )
})
dev.off()


# ================
# Normalise
# ================

# normalising works between samples .... also I dont really understand why this is necessary
meth.list.db %<>% normalizeCoverage(. ,
save.db = TRUE,
dbtype = "tabix",
suffix = paste0("normed"),
dbdir = paste0(OUTPUTDIR,"/methylDB"))


#This should always be as DB object as it comes from the normalisation command
n.vars.postnorm.df = meth.list.db %>%
 lapply(., fn.loc.count ) %>%
bind_rows() %>%
mutate(stage = "postNorm")
 
#  lapply(., function(item)
# {data.frame(
# Sample.Code = item@sample.id,
# Context = item@context,
# n.vars = item@num.records,
# res = item@resolution,
# stage = "postNorm"
# ) %>% return}
# ) %>% bind_rows()


# ===============
# Export df or vars
# ===============
# Writing out the summary tables if needed
# These tables do not get written out directly to a spreadsheet or rds
	# write out a plain text file, this can be read in with all the others to produce a singlet table
# ===============

rbind(n.vars.prefilt.df, n.vars.postfilt.df,n.vars.postnorm.df) %>%
write.table(.,
file = paste0(OUTPUTDIR,"/MethylKit_NumberVars",INPUTFILE,".tsv"),
quote = F,
row.names = F)

# clear some RAM
list(n.vars.prefilt.df, n.vars.postfilt.df,n.vars.postnorm.df) %>% rm()

pdf(paste0(OUTPUTDIR,"/Stats_Histograms_PostNormCov",INPUTFILE,".pdf"))
lapply(meth.list.db, function(item){	
print(getSampleID(item))
getMethylationStats(item, plot = TRUE, both.strands = FALSE)
getCoverageStats(item, plot = TRUE, both.strands = FALSE )
})
dev.off()

# ===============
print("Reached the end of the script.")
# ===============

