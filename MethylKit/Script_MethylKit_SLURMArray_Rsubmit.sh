# This script should be appended to a slurm header for submission

#=============
#Job preprep
#=============

source ~/CodeRepos/hpc-workflows/HPC/Script_jobsubmission_preamble_240130.sh && sleep 1s

#Create directory for analysis
if [[ ! -d ${RJDSCRATCH} ]]
then
echo 'RJDSCRATCH not found!'
exit 1
else
echo 'RJDSCRATCH found! at ' ${RJDSCRATCH}
cd ${RJDSCRATCH}
fi

#=============
#Load required modules
#=============

if [[  ${RJDPLATFRM} == 'RACKHAM'  ]];
then
# Rackham
module load bioinfo-tools
module load R/4.3.1
module load R_packages/4.3.1
module load bcftools
fi

if [[  ${RJDPLATFRM} == 'EULER'  ]];
then
# Euler
module load samtools
module load bcftools
module load R/4.2.2
fi

if [[  ${RJDPLATFRM} == 'ILIFU'  ]];
then
module load  R/RStudio2022.12.0-353-R4.2.2
fi

###Let's hyperthread
## Bismark recommends only 4 "cores per alignment thread"
#export OMP_NUM_THREADS=4

#=============
# Read in vars
#=============

## set variables
export RSCRIPT=${1}

export FILEROOTLIST=${2}
# backup incase I forgot the variable name
export ROOTLISTFILE=${2}

export INPUTDIR=${3}
export OUTPUTDIR=${4}

export FILEPATTERNSUFFIX=${5}
#=============

#Create directory for analysis
if [[ ! -d ${OUTPUTDIR} ]]
then
    mkdir -p ${OUTPUTDIR}
fi

if [[ ! -d ${INPUTDIR} ]]
then
echo 'ERR: INPUTDIR does not exist!'
exit
fi

if [[ ! -f ${FILEROOTLIST} ]]
then
echo 'ERR: FILEROOTLIST does not exist!'
exit
fi


# ===================
source comstp 'Internal job index variable ...'
# ===================

#Internal job index variable
echo 'Job IDX running ...' ${RJD_SLURM_ARRAY_TASK_ID}

#sed -n "${RJD_SLURM_ARRAY_TASK_ID}p" ${FILEROOTLIST}

# The saved methylkit rawdb objects do not have the number prefix, so it needs to be removed from the INPUTFILE
#export INPUTFILE=$(awk -v IDX=${RJD_SLURM_ARRAY_TASK_ID} 'NR==IDX{print $1}' ${FILEROOTLIST} | awk -F _ '{print $2}' )

# export CHR=$(awk -v IDX=${RJD_SLURM_ARRAY_TASK_ID} 'NR==IDX{print $2}' ${FILEROOTLIST})

export INPUTFILE="$(awk -v IDX=${RJD_SLURM_ARRAY_TASK_ID} 'NR==IDX{print $1}' ${FILEROOTLIST} )"

source comstp 'This is the file name to run ...'
# this needs to be keep out of comstp, for some reason it doesn't get echo'd otherwise
echo ${INPUTFILE}
#echo ${CHR}

# ===================
source comstp 'Rsync data to scratch ...'
# ===================

echo 'Create input file dir...'
mkdir -p ${RJDSCRATCH}/Input_Files/

echo 'Files to upload...'

# rsync -rLptgoD -q ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa.fai ${RJDSCRATCH}/Input_Files/

rsync -rLptgoD \
 -vR \
 --prune-empty-dirs \
 --include="*/" \
 --include="*${INPUTFILE}**${FILEPATTERNSUFFIX}*" \
 --exclude="*" \
 ${INPUTDIR}/./ \
 ${RJDSCRATCH}/Input_Files/

#===============================
# Run commands
#==============================

# To prevent chromosomes from writing over each other when rsyncing

# We change the inputdir and outputdir to be the scratch space

echo 'Run Rscript...'
Rscript \
${RSCRIPT} \
${INPUTFILE} \
${RJDSCRATCH}/Input_Files/ \
${RJDSCRATCH}/ \
${FILEPATTERNSUFFIX} \
${6} \
${7} \
${8}

# =============
source comstp 'Close off & rsync back to outputdir ...'
# =============
## DON'T RENAME THE FILES .. IT CAUSES TROUBLE WITH REPORTS DOWNSTREAM
#===============================
# Remove the input data from the scratch space
rm *temp*
rm -r ${RJDSCRATCH}/Input_Files
mkdir -p ${OUTPUTDIR}/out_${INPUTFILE} && rsync -aqu ${RJDSCRATCH}/ ${OUTPUTDIR}/out_${INPUTFILE}/ &&  rm -r ${RJDSCRATCH}/
#===============================
