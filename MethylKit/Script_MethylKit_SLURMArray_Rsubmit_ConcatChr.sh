# This script should be appended to a slurm header for submission

#=============
#Job preprep
#=============

source ~/CodeRepos/hpc-workflows/HPC/Script_jobsubmission_preamble_240130.sh && sleep 1s

#Create directory for analysis
if [[ ! -d ${RJDSCRATCH} ]]
then
echo 'RJDSCRATCH not found!'
exit 1
else
echo 'RJDSCRATCH found! at ' ${RJDSCRATCH}
cd ${RJDSCRATCH}
fi

#=============
#Load required modules
#=============

if [[  ${RJDPLATFRM} == 'RACKHAM'  ]];
then
# Rackham
module load bioinfo-tools
module load R/4.2.2
module load R_packages/4.2.2
module load bcftools
fi

if [[  ${RJDPLATFRM} == 'EULER'  ]];
then
# Euler
module load samtools
module load bcftools
module load R/4.2.2
fi

if [[  ${RJDPLATFRM} == 'ILIFU'  ]];
then
module load  R/RStudio2022.12.0-353-R4.2.2
module load  htslib/1.19.1
fi

###Let's hyperthread
## Bismark recommends only 4 "cores per alignment thread"
#export OMP_NUM_THREADS=4

#=============
# Read in vars
#=============

## set variables
export RSCRIPT=${1}

export INPUTFILE=${2}

export INPUTDIR=${3}

export OUTPUTDIR=${4}


#=============

#Create directory for analysis
if [[ ! -d ${OUTPUTDIR} ]]
then
    mkdir -p ${OUTPUTDIR}
fi

if [[ ! -d ${INPUTDIR} ]]
then
echo 'ERR: INPUTDIR does not exist!'
exit
fi


# ===================
source comstp 'Rsync data to scratch ...'
# ===================

echo 'Create input file dir...'
mkdir -p ${RJDSCRATCH}/Input_Files/ ${RJDSCRATCH}/methylDB/

echo 'Files to upload...'
#echo '!! Using only the 2 smallest chr for testing !! ...'

# Try to keep some naming to allow for chr order
find ${INPUTDIR}/ -iname "*${INPUTFILE}*txt.bgz" -type f -print | sort >  ${RJDSCRATCH}/Input_Files/TMP_file.list
cat ${RJDSCRATCH}/Input_Files/TMP_file.list

# Get chr number from the folder name

awk 'BEGIN{FS="/"}; { outfn = NF - 2; gsub("out_","", $outfn); print $outfn}'  ${RJDSCRATCH}/Input_Files/TMP_file.list > ${RJDSCRATCH}/Input_Files/TMP_file_chrs.list

echo 'Files uploaded...'

cat ${RJDSCRATCH}/Input_Files/TMP_file.list | xargs -I {} uuidgen {}  >>  ${RJDSCRATCH}/Input_Files/TMP_file_uuid.list

paste ${RJDSCRATCH}/Input_Files/TMP_file.list ${RJDSCRATCH}/Input_Files/TMP_file_chrs.list ${RJDSCRATCH}/Input_Files/TMP_file_uuid.list > ${RJDSCRATCH}/Input_Files/TMP_file_joint.list

cat ${RJDSCRATCH}/Input_Files/TMP_file_joint.list | \
while read TMPFILE CHR UUID
do
ln -s ${TMPFILE} ${RJDSCRATCH}/Input_Files/TMP_${INPUTFILE}_${CHR}_${UUID}.txt.bgz
ln -s ${TMPFILE}.tbi ${RJDSCRATCH}/Input_Files/TMP_${INPUTFILE}_${CHR}_${UUID}.txt.bgz.tbi
done

# xargs -I % bash -c " \
# ln -s ${INPUTDIR}/out_'%'/methylDB/${FILENAME}.txt.bgz ${RJDSCRATCH}/Input_Files/${FILENAME}_'%'.txt.bgz
# ln -s ${INPUTDIR}/out_'%'/methylDB/${FILENAME}.txt.bgz.tbi ${RJDSCRATCH}/Input_Files/${FILENAME}_'%'.txt.bgz.tbi
# " \;

tree  ${RJDSCRATCH}/Input_Files/

# ==============
source comstp  'Necessary pre-processing'
# ==============

# Get the Tabixheadre as a separate file

# You  cannot specify -type f here ... it will ignore links
find ${RJDSCRATCH}/Input_Files/ -iname '*.txt.bgz'   | head -n 1  > ${RJDSCRATCH}/Input_Files/tmp_onefile2ues.txt

cat ${RJDSCRATCH}/Input_Files/tmp_onefile2ues.txt | \
while read line
do 
zcat ${line}  | head -n 20  | grep '#' | head -c -1 > ${RJDSCRATCH}/Input_Files/TabixHeader2use.txt

# head -c -1  : Remove the last bite (new line character)

# The output file may have empty lines, remove them
sed -i -e '/^[[:space:]]*$/d'  ${RJDSCRATCH}/Input_Files/TabixHeader2use.txt
# Remove the trailing new line 

done


if [[  ! -f "${RJDSCRATCH}/Input_Files/TabixHeader2use.txt" ]]
then
echo "file DNE : ${RJDSCRATCH}/Input_Files/TabixHeader2use.txt"
exit
fi

# Remove the header from all the input
# !!Input for R must be decompressed!!

find ${RJDSCRATCH}/Input_Files/ -iname '*.txt.bgz' -exec bash -c " bgzip -d '{}' " \; && find ${RJDSCRATCH}/Input_Files/ -iname 'TMP_methyl*.txt' -exec bash -c " sed -i -e '/^#/d' '{}' " \;


#===============================
# Run commands
#==============================

# To prevent chromosomes from writing over each other when rsyncing

# We change the inputdir and outputdir to be the scratch space

echo 'Run Rscript...'
Rscript \
${RSCRIPT} \
"${INPUTFILE}" \
${RJDSCRATCH}/Input_Files/ \
${RJDSCRATCH}/methylDB/ \
"TMP_${INPUTFILE}" \


# Move to the tmp output folder
mv ${RJDSCRATCH}/Input_Files/${INPUTFILE}_ChrConcat* ${RJDSCRATCH}/methylDB/


# =============
source comstp 'Close off & rsync back to outputdir ...'
# =============
## DON'T RENAME THE FILES .. IT CAUSES TROUBLE WITH REPORTS DOWNSTREAM
#===============================
# Remove the input data from the scratch space
rm *temp* *tmp* *TMP*
rm -r ${RJDSCRATCH}/Input_Files
rsync -aqu ${RJDSCRATCH}/ ${OUTPUTDIR}/ &&  rm -r ${RJDSCRATCH}/
#===============================
