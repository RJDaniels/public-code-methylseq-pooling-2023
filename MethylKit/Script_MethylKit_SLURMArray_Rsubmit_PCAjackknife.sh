# This script should be appended to a slurm header for submission

#=============
#Job preprep
#=============

source ~/CodeRepos/hpc-workflows/HPC/Script_jobsubmission_preamble_240130.sh && sleep 1s

#Create directory for analysis
if [[ ! -d ${RJDSCRATCH} ]]
then
echo 'RJDSCRATCH not found!'
exit 1
else
echo 'RJDSCRATCH found! at ' ${RJDSCRATCH}
cd ${RJDSCRATCH}
fi

#=============
#Load required modules
#=============

if [[  ${RJDPLATFRM} == 'RACKHAM'  ]];
then
# Rackham
module load bioinfo-tools
module load R/4.2.2
module load R_packages/4.2.2
module load bcftools
fi

if [[  ${RJDPLATFRM} == 'EULER'  ]];
then
# Euler
module load samtools
module load bcftools
module load R/4.2.2
fi

if [[  ${RJDPLATFRM} == 'ILIFU'  ]];
then
module load  R/RStudio2022.12.0-353-R4.2.2
fi

###Let's hyperthread
## Bismark recommends only 4 "cores per alignment thread"
#export OMP_NUM_THREADS=4

#=============
# Read in vars
#=============

## set variables
export RSCRIPT=${1}

export INPUTFILE=${2}

export INPUTDIR=${3}

export OUTPUTDIR=${4}

export FILEPATTERNSUFFIX=${5}

#export MINOVRLAP=${6} # for the diff meth code


#=============

#Create directory for analysis
if [[ ! -d ${OUTPUTDIR} ]]
then
    mkdir -p ${OUTPUTDIR}
fi

if [[ ! -d ${INPUTDIR} ]]
then
echo 'ERR: INPUTDIR does not exist!'
exit
fi


# ===================
source comstp 'Rsync data to scratch ...'
# ===================

echo 'Create input file dir...'
mkdir -p ${RJDSCRATCH}/Input_Files/ ${OUTPUTDIR}/

echo 'Files to upload...'

# rsync -rLptgoD -q ${REFGENOMEDIR}/${REFGENOMEPRFX}.fa.fai ${RJDSCRATCH}/Input_Files/

rsync -rLptgoD \
 -vR \
 --prune-empty-dirs \
 --include="*/" \
 --include="*${INPUTFILE}**${FILEPATTERNSUFFIX}*" \
 --exclude="*" \
 ${INPUTDIR}/./ \
 ${RJDSCRATCH}/Input_Files/

 echo 'Files uploaded...'
tree  ${RJDSCRATCH}/Input_Files/

#===============================
# Run commands
#==============================

# Get the start and end position of each chr ...
# Much simplier than running in R ... I should do this instead

echo 'Get chr start and end pos...'


find ${RJDSCRATCH}/Input_Files/ -name 'methylBase_united_minpergrp_0.75_ChrConcat.txt.bgz' -type f -exec bash -c "zcat '{}'  | awk '!/#/{print \$1,\$2,\$3}' > ${RJDSCRATCH}/Input_Files/tmp" \;


awk '{print $1}' ${RJDSCRATCH}/Input_Files/tmp | sort | uniq > ${RJDSCRATCH}/Input_Files/uniq.chr.tmp

cat ${RJDSCRATCH}/Input_Files/uniq.chr.tmp | xargs -I % bash -c "grep % ${RJDSCRATCH}/Input_Files/tmp |  sed -n '1p;\$p'"  >> ${RJDSCRATCH}/Input_Files/Chr_start_stop.bed0

if [[ ! -f ${RJDSCRATCH}/Input_Files/Chr_start_stop.bed0 ]]
then
echo 'Chr_start_stop.bed0 DNE! ...'
exit
fi

echo 'view content of bed file...'
cat ${RJDSCRATCH}/Input_Files/Chr_start_stop.bed0

# To prevent chromosomes from writing over each other when rsyncing

# We change the inputdir and outputdir to be the scratch space

echo 'Run Rscript...'
Rscript \
${RSCRIPT} \
"${INPUTFILE}" \
${RJDSCRATCH}/Input_Files/ \
${RJDSCRATCH}/ \
${FILEPATTERNSUFFIX} \
${6} \
${7} \

# =============
source comstp 'Close off & rsync back to outputdir ...'
# =============


#===============================
# Remove the input data from the scratch space
rm *temp* *tmp* *TMP*
rm -r ${RJDSCRATCH}/Input_Files
rsync -aqu ${RJDSCRATCH}/ ${OUTPUTDIR}/ &&  rm -r ${RJDSCRATCH}/

#===============================
